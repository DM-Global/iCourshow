package com.ics.cmsadmin.modules.wechat.model.auth.resp;

import com.ics.cmsadmin.modules.wechat.result.AbstractResult;
import lombok.Data;

/**
 * JS-SDK的页面配置信息
 *
 * @author phil
 * @date 2017年8月22日
 */
@Data
public class JsSDKConfig extends AbstractResult {

    private String appId;

    private long timestamp;

    private String nonceStr;

    private String signature;

    private String[] jsApiList = {"onMenuShareAppMessage", "onMenuShareTimeline", "onMenuShareQQ", "onMenuShareWeibo", "onMenuShareQZone"};
}
