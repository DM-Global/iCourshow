package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.core.enums.OrderStatusEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.utils.KeyUtil;
import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.auth.service.AccessService;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;
import com.ics.cmsadmin.modules.basic.service.SharePosterService;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.basic.dao.OrderDao;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.basic.service.PackageService;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.student.service.StudentAccountService;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.jni.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * t_order
 * Created bylvsw on 2018-16-23 21:09:34.
 */
@Service("newOrderService")
@Log4j2
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao orderDao;
    @Resource
    private AccessService accessService;
    @Autowired
    private StudentService studentService;
    @Resource
    private PackageService packageService;
    @Resource
    private RegisterService registerService;
    @Resource
    private StudentAccountService studentAccountService;
    @Resource
    private ShareDetailService shareDetailService;

    @Override
    public PageResult list(OrderBean bean, PageBean page) {
        long count = orderDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, orderDao.list(bean, page));
    }

    @Override
    public PageResult listByLoginUserId(OrderBean bean, String loginUserId, PageBean page) {
        return listByLoginUserId(bean, loginUserId, page, orderDao);
    }

    @Override
    public boolean insert(OrderBean bean) {
        if (bean == null) {
            return false;
        }
        bean.setId(KeyUtil.genUniqueKey());
        return orderDao.insert(bean) == 1;
    }

    @Override
    public boolean update(OrderBean bean) {
        return orderDao.update(bean) == 1;
    }

    @Override
    public OrderBean queryUserOrder(String userId, String courseId) {
        if (StringUtils.isBlank(userId)) {
            return null;
        }
        List<OrderBean> orderList = orderDao.list(OrderBean.builder().userId(userId).courseId(courseId).build(),
            new PageBean(1, Integer.MAX_VALUE));
        return !CollectionUtils.isEmpty(orderList) ? orderList.get(0) : null;
    }

    @Override
    public boolean getPurchaseStatus(String userId, String courseId) {
        OrderBean orderBean = orderDao.queryEffectivePackageOrder(userId);
        if (orderBean != null && orderBean.getStatus().equals(OrderStatusEnum.SUCCESS.getCode())) {
            return true;
        }
        orderBean = queryUserOrder(userId, courseId);
        return orderBean != null && orderBean.getStatus().equals(OrderStatusEnum.SUCCESS.getCode());
    }

    @Override
    public long count(OrderBean t) {
        return orderDao.count(t);
    }

    @Override
    public void batchDelete(List<String> ids) {
        orderDao.batchDelete(ids);
    }

    @Override
    public void batchSoftRemove(List<String> ids) {
        orderDao.batchSoftRemove(ids);
    }


    @Override
    public BigDecimal totalMoney(OrderBean bean, String loginUserId) {
        LoginSearchBean loginSearchBean = accessService.queryLoginInfoByLoginUserId(loginUserId);
        if (loginSearchBean == null) {
            return new BigDecimal(0);
        }
        bean.setLoginAgentNo(loginSearchBean.getLoginAgentNo());
        bean.setLoginOrgId(loginSearchBean.getLoginOrgId());
        return orderDao.totalMoney(bean);
    }

    @Override
    public OrderBean queryById(String id) {
        return orderDao.queryById(id);
    }

    @Override
    public OrderBean createOrderByAccount(OrderBean orderBean, String loginUserId) {
        StudentBean student = studentService.queryById(loginUserId);
        if (student == null) {
            throw new CmsException(ApiResultEnum.USER_NOT_EXIST);
        }
        if (!student.getIsActive()) {
            throw new CmsException(ApiResultEnum.USER_NOT_ALLOW);
        }
        PackageBean packageBean = packageService.queryById(orderBean.getPackageId());
        if (packageBean == null) {
            throw new CmsException(ApiResultEnum.DATA_NOT_EXIST);
        }
        if (null == orderBean.getOrderType() || !CommonEnums.OrderType.contains(orderBean.getOrderType())) {
            throw new CmsException(ApiResultEnum.PARAM_ERROR);
        }
        if (StringUtils.isBlank(orderBean.getPackageId())) {
            throw new CmsException(ApiResultEnum.PARAM_ERROR);
        }
        BigDecimal vipOrderPrice = registerService.queryRegisterValue(RegisterKeyValueEnum.GLOBAL_VIP_PRICE, BigDecimal::new);
        BigDecimal orderMoney = student.getSpecialVip() ? vipOrderPrice : packageBean.getPrice();
        StudentAccountBean studentAccountBean = studentAccountService.queryByStudentId(loginUserId);
        if (studentAccountBean == null || studentAccountBean.getAvailableBalance().compareTo(orderMoney) < 0) {
            throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "账户余额不足");
        }
        OrderBean order = OrderBean.builder().id(KeyUtil.genUniqueKey())
            .userId(loginUserId)
            .orderType(orderBean.getOrderType())
            .orderAmount(orderMoney)
            .status(CommonEnums.PayStatus.PAID.getCode())
            .accStatus("0")
            .isDeleted(false)
            .packageId(packageBean.getId())
            .agentNo(student.getAgentNo())
            .paymentMethod(CommonEnums.PaymentMethod.ACCOUNT_PAY.getCode())
            .payTime(new Date())
            .studyStatus(CommonEnums.StudyStatus.NOT_STARTED.getCode()).build();
        calcOrderStartAndEndDate(loginUserId, order, packageBean.getMonthCount() * 31);
        insert(order);
        // 计算分润
        shareDetailService.generateShareByOrderId(order.getId());
        // 修改账户余额
        studentAccountService.accountChanges(loginUserId, orderMoney, StudentAccountChangesEnum.BUY_ORDER,
            StudentAccountDetailBean.builder()
                .orderNo(order.getId())
                .orderStudentId(loginUserId)
                .build());
        return queryById(order.getId());
    }

    @Override
    public OrderBean create(OrderBean orderBean, String loginUserId) {
        StudentBean student = studentService.queryById(loginUserId);
        if (student == null) {
            throw new CmsException(ApiResultEnum.USER_NOT_EXIST);
        }
        if (!student.getIsActive()) {
            throw new CmsException(ApiResultEnum.USER_NOT_ALLOW);
        }

        if (null == orderBean.getOrderType() || !CommonEnums.OrderType.contains(orderBean.getOrderType())) {
            throw new CmsException(ApiResultEnum.PARAM_ERROR);
        }
        if (StringUtils.isBlank(orderBean.getPackageId())) {
            throw new CmsException(ApiResultEnum.PARAM_ERROR);
        }
        PackageBean packageBean = packageService.queryById(orderBean.getPackageId());
        if (null == packageBean) {
            throw new CmsException(ApiResultEnum.DATA_NOT_EXIST);
        }
        BigDecimal vipOrderPrice = registerService.queryRegisterValue(RegisterKeyValueEnum.GLOBAL_VIP_PRICE, BigDecimal::new);
        OrderBean order = OrderBean.builder().id(KeyUtil.genUniqueKey())
            .userId(loginUserId)
            .orderType(orderBean.getOrderType())
            .orderAmount(student.getSpecialVip() ? vipOrderPrice : packageBean.getPrice())
            .status(CommonEnums.PayStatus.UNPAID.getCode())
            .accStatus("0").isDeleted(false).packageId(packageBean.getId()).agentNo(student.getAgentNo())
            .studyStatus(CommonEnums.StudyStatus.NOT_STARTED.getCode()).build();
        calcOrderStartAndEndDate(loginUserId, order, packageBean.getMonthCount() * 31);
        insert(order);
        order = queryById(order.getId());
        return order;
    }

    /**
     * 计算订单的开始和结束时间
     * 1. 删除所有未支付完成的订单
     * 2. 根据订单的endDate倒序排序,获取最后一条以包年包月交易的订单(lastEndDate)
     * 3. 如果获取不到,或者lastEndDate已经到期,则startDate = now, endDate = now + days
     * 4. 否则startDate = lastEndDate, endDate = lastEndDate + days
     */
    private void calcOrderStartAndEndDate(String studentId, OrderBean order, int days) {
        // 删除所有未支付,或已经取消的订单
        orderDao.deleteAllUnpaid(studentId, CommonEnums.OrderType.PACKAGE.getCode());
        // 获取最后一笔支付订单
        OrderBean theLastPaidOrder = orderDao.findLastByEndDate(OrderBean.builder().userId(studentId)
            .orderType(CommonEnums.OrderType.PACKAGE.getCode())
            .status(CommonEnums.PayStatus.PAID.getCode())
            .isDeleted(false).build());
        Date today = new Date();
        // 如果查不到订单或最后一笔订单的时间已经过期,则生成一个新订单时间,否则续签
        if (theLastPaidOrder == null || theLastPaidOrder.getEndDate() == null || today.after(theLastPaidOrder.getEndDate())) {
            order.setStartDate(today);
            order.setEndDate(DateUtils.addDays(today, days));
        } else {
            order.setStartDate(theLastPaidOrder.getEndDate());
            order.setEndDate(DateUtils.addDays(theLastPaidOrder.getEndDate(), days));
        }
    }

    @Override
    public OrderBean createTrialOrderForNewUser(String userId) {
        StudentBean student = studentService.queryById(userId);
        if (student == null) {
            throw new CmsException(ApiResultEnum.USER_NOT_EXIST);
        }
        if (!student.getIsActive()) {
            throw new CmsException(ApiResultEnum.USER_NOT_ALLOW);
        }
        PageResult pageResult = list(OrderBean.builder().userId(userId).orderType(CommonEnums.OrderType.PACKAGE.getCode()).isDeleted(false).build(), new PageBean(1, Integer.MAX_VALUE));
        List<OrderBean> orderList = pageResult.getDataList();
        if (null != orderList && orderList.size() > 0) {
            throw new CmsException(ApiResultEnum.ALREADY_EXISTED_PACKAGE_ORDER);
        }
        OrderBean order = OrderBean.builder().id(KeyUtil.genUniqueKey()).userId(userId).paymentMethod(CommonEnums.PaymentMethod.FREE_TRIAL.getCode())
            .orderType(CommonEnums.OrderType.PACKAGE.getCode()).orderAmount(BigDecimal.ZERO).status(CommonEnums.PayStatus.PAID.getCode())
            .accStatus("0").isDeleted(false).studyStatus(CommonEnums.StudyStatus.STARTED.getCode()).status(CommonEnums.PayStatus.PAID.getCode()).build();
        Date startDate = new Date();
        order.setStartDate(startDate);
        Integer trialOrderDateCount = registerService.queryRegisterValue(RegisterKeyValueEnum.TRIAL_ORDER_DATE_COUNT, Integer::new);
        order.setEndDate(DateUtils.addDays(startDate, trialOrderDateCount));
        insert(order);
        return queryById(order.getId());
    }

    @Override
    public Boolean hasTrialOrder(String userId) {
        PageResult pageResult = list(OrderBean.builder().userId(userId)
            .orderType(CommonEnums.OrderType.PACKAGE.getCode())
            .paymentMethod(CommonEnums.PaymentMethod.FREE_TRIAL.getCode())
            .isDeleted(false).build(), new PageBean(1, Integer.MAX_VALUE));
        List<OrderBean> orderList = pageResult.getDataList();
        if (null != orderList && orderList.size() > 0) return Boolean.TRUE;
        return Boolean.FALSE;
    }

    @Override
    public OrderBean createFreeOrder(String studentId, int days) {
        if (days <= 0) {
            throw new CmsException(ApiResultEnum.CREATE_ORDER_ERROR, "订单时间必须为大于0");
        }
        StudentBean student = studentService.queryById(studentId);
        if (student == null) {
            throw new CmsException(ApiResultEnum.USER_NOT_EXIST);
        }
        if (!student.getIsActive()) {
            throw new CmsException(ApiResultEnum.USER_NOT_ALLOW);
        }
        OrderBean order = OrderBean.builder().id(KeyUtil.genUniqueKey())
            .userId(studentId)
            .orderAmount(BigDecimal.ZERO)
            .orderType(CommonEnums.OrderType.PACKAGE.getCode())
            .status(CommonEnums.PayStatus.PAID.getCode())
            .paymentMethod(CommonEnums.PaymentMethod.FREE_TRIAL.getCode())
            .accStatus("2")
            .isDeleted(false)
            .agentNo(student.getAgentNo())
            .studyStatus(CommonEnums.StudyStatus.NOT_STARTED.getCode())
            .build();
        calcOrderStartAndEndDate(studentId, order, days);
        insert(order);
        return queryById(order.getId());
    }

    @Override
    public OrderBean createTrialOrderForOldUser(String userId) {
        // todo4lvsw 活动已经过期,暂时注释掉以下代码
        return null;
//        StudentBean student = studentService.queryById(userId);
//        if (student == null) throw new CmsException(ApiResultEnum.USER_NOT_EXIST);
//        if (!student.getIsActive()) throw new CmsException(ApiResultEnum.USER_NOT_ALLOW);
//        Boolean existedTrialOrder = hasTrialOrder(userId);
//        if (existedTrialOrder) throw new CmsException(ApiResultEnum.ALREADY_EXISTED_TRIAL_ORDER);
//        OrderBean order = OrderBean.builder().id(KeyUtil.genUniqueKey())
//            .userId(userId)
//            .orderType(CommonEnums.OrderType.PACKAGE.getCode())
//            .orderAmount(BigDecimal.ZERO)
//            .status(CommonEnums.PayStatus.PAID.getCode())
//            .paymentMethod(CommonEnums.PaymentMethod.FREE_TRIAL.getCode())
//            .accStatus("0").isDeleted(false).agentNo(student.getAgentNo())
//            .studyStatus(CommonEnums.StudyStatus.NOT_STARTED.getCode()).build();
//        Integer trialOrderDateCount = registerService.queryRegisterValue(RegisterKeyValueEnum.TRIAL_ORDER_DATE_COUNT, Integer::new);
//        calcOrderStartAndEndDate(userId, order, trialOrderDateCount);
//        insert(order);
//        return queryById(order.getId());
    }
}
