package com.ics.cmsadmin.frame.init;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * 在spring启动后,判断初始化任务是否结束的任务
 * Created by pc on 2017/10/7.
 */
public class InitTaskOverCallback implements Callable<String> {

    private Map<String, Future<String>> mapFuture;
    private ExecutorService pool;
    protected static final Logger LOGGER = LoggerFactory.getLogger(InitTaskOverCallback.class);

    public InitTaskOverCallback(Map<String, Future<String>> mapFuture, ExecutorService pool) {
        this.mapFuture = mapFuture;
        this.pool = pool;
    }

    @Override
    public String call() throws Exception {
        for (Map.Entry<String, Future<String>> entry : mapFuture.entrySet()) {
            try {
                String taskFlag = entry.getValue().get();
                if (StringUtils.equals(taskFlag, AfterSpringStarted.TASK_DONE)) {
                    LOGGER.info("任务 " + entry.getKey() + " 执行完成! ");
                } else {
                    LOGGER.info("任务 " + entry.getKey() + " 执行后标志为: " + taskFlag);
                }
            } catch (Exception e) {
                LOGGER.error("任务 " + entry.getKey() + " 执行异常", e);
            }
        }
        this.pool.shutdown();
        return AfterSpringStarted.TASK_DONE;
    }
}
