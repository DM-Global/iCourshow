package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.ProblemSetBean;
import com.ics.cmsadmin.modules.basic.bean.PromotionBean;
import com.ics.cmsadmin.modules.basic.dao.PromotionDao;
import com.ics.cmsadmin.modules.basic.service.PromotionService;
import com.ics.cmsadmin.modules.basic.service.SubjectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * t_promotion
 * Created by sandwich on 2018-12-06 21:12:37.
 */
@Service
public class PromotionServiceImpl implements PromotionService {
    @Resource
    private PromotionDao promotionDao;
    @Resource
    private SubjectService subjectService;

    @CacheDbMember(returnClass = PromotionBean.class, group = CacheGroupEnum.CACHE_PROMOTION)
    @Override
    public PromotionBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return promotionDao.queryById(id);
    }

    @CacheDbMember(returnClass = PromotionBean.class, group = CacheGroupEnum.CACHE_PROMOTION)
    @Override
    public PageResult list(PromotionBean bean, PageBean page) {
        long count = promotionDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        List<PromotionBean> list = promotionDao.list(bean, page);
        Optional.ofNullable(list)
            .ifPresent(items -> {
                items.stream().forEach(item -> item.setLevelNodeName(subjectService.findNameByLevelNode(item.getLevelNode())));
            });
        return PageResult.getPage(count, list);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_PROMOTION)
    @Override
    public boolean insert(PromotionBean bean) {
        if (bean == null) {
            return false;
        }
        return promotionDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_PROMOTION)
    @Override
    public boolean update(String id, PromotionBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return promotionDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_PROMOTION)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return promotionDao.delete(id) == 1;
    }

}
