package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.Collect;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface CollectionDao {

    int insert(Collect collect);

    int insert(@Param("userId") String userId,
               @Param("levelName") LevelEnum levelName,
               @Param("levelId") String levelId);

    List<Collect> findByUserId(@Param("userId") String userId);

    Boolean select(Collect collect);

    Boolean select(@Param("userId") String userId,
                   @Param("levelName") LevelEnum levelName,
                   @Param("levelId") String levelId);

    int delete(Collect collect);

    int delete(@Param("userId") String userId,
               @Param("levelName") LevelEnum levelName,
               @Param("levelId") Integer levelId);

    Integer queryTotal(@Param("levelName") LevelEnum levelName,
                       @Param("levelId") String levelId);

    /**
     * 根据用户查询所给的科目/课程等id是否被收藏
     *
     * @param userId    用户id
     * @param levelEnum 类型
     * @param ids       需要查询的id
     * @return
     */
    List<String> list(@Param("userId") String userId,
                      @Param("levelEnum") LevelEnum levelEnum,
                      @Param("ids") List<String> ids);

    List<Collect> findByLevelNameAndUserId(@Param("levelName") String levelName,
                                           @Param("userId") String userId);
}
