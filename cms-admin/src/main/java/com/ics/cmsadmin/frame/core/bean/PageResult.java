package com.ics.cmsadmin.frame.core.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用于包装list数据的类
 *
 * @author wanghuan
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> {
    private List<T> dataList;
    private long totalCount;

    public static <T> PageResult getPage(long totalCount, List<T> data) {
        return new PageResult<>(data, totalCount);
    }

}
