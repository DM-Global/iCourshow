package com.kco.generate;

import com.kco.generate.config.SqlUtils;
import com.kco.generate.config.TableBean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class VmCodeGenerateUtils {
    public static void main(String[] args) throws Exception {
        String[] tables = {
            "t_student_share_record"
        };

        Properties p = new Properties();
        p.put("file.resource.loader.class",
            "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(p);

        Map<String, String> map = new HashMap<>();
        map.put("temp/bean.svm", "gemVm/bean/%sBean.java");
        map.put("temp/controller.svm", "gemVm/controller/%sController.java");
        map.put("temp/dao.svm", "gemVm/dao/%sDao.java");
        map.put("temp/daoMapper.svm", "gemVm/mapper/%sDao.xml");
        map.put("temp/service.svm", "gemVm/service/%sService.java");
        map.put("temp/serviceImpl.svm", "gemVm/service/impl/%sServiceImpl.java");
        map.put("temp/vue.svm", "gemVm/vue/%sManage.vue");

        String now = new SimpleDateFormat("YYYY-mm-dd HH:MM:ss").format(new Date());
        for (String table : tables) {
            TableBean tableBean = SqlUtils.queryByDBAndTableName("icourshow", table);
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("packagePrefix", "com.ics.cmsadmin");
            velocityContext.put("basePackage", "student");
            velocityContext.put("author", "lvsw");
            velocityContext.put("now", now);
            velocityContext.put("StringUtils", new StringUtils());

            velocityContext.put("tableName", table);
            velocityContext.put("shortTable", toShortTable(table));
            velocityContext.put("noPrefixTableName", tableBean.getNoPrefixTableName());
            velocityContext.put("className", tableBean.getClassName());
            velocityContext.put("tableComment", tableBean.getTableComment());
            velocityContext.put("classNameVariable", tableBean.getClassNameVariable());
            velocityContext.put("columns", tableBean.getColumnsBeans());
            velocityContext.put("primaryColumnKey", tableBean.getPrimaryColumnKey());
            velocityContext.put("primaryPropertyKey", tableBean.getPrimaryPropertyKey());
            velocityContext.put("primaryPropertyKeyColumnComment", tableBean.getPrimaryPropertyKeyColumnComment());

            for (Map.Entry<String, String> entry : map.entrySet()) {
                StringWriter sw = new StringWriter();
                Template template = Velocity.getTemplate(entry.getKey(), "utf-8");
                template.merge(velocityContext, sw);
                FileUtils.writeStringToFile(new File(String.format(entry.getValue(), tableBean.getClassName())),
                    sw.toString(), "utf-8");
            }
        }

    }

    private static String toShortTable(String table) {
        String[] splits = table.split("_");
        StringBuilder sb = new StringBuilder();
        for (String s : splits) {
            sb.append(s, 0, 1);
        }
        return sb.toString();
    }
}
