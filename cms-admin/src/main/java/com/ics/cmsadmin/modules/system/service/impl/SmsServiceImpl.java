package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.constant.RedisConstants;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import com.ics.cmsadmin.modules.alibaba.utils.SmsUtils;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.SmsService;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.function.Function;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-01-22 09:20
 */
@Service
public class SmsServiceImpl implements SmsService {

    @Override
    public ApiResponse sendVerifyCodeSms(String smsType, String phoneCode,
                                     String phone, String imageCodeId, String imageCode) {
        // 验证短信类型是否正确
        if (StringUtils.isBlank(smsType) || !SmsTypeEnum.isSmsTypeEnum(smsType)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_TYPE_ERROR);
        }
        // 验证码图形验证码是否正确
        String redisCode = RedisConstants.getImageCode(imageCodeId);
        if (StringUtils.isBlank(redisCode) || !StringUtils.equalsIgnoreCase(redisCode, imageCode)) {
            return new ApiResponse(ApiResultEnum.IMAGE_VERIFY_CODE_ERROR);
        }
        RedisConstants.deleteImageCode(imageCodeId);

        // 验证短信验证码是否已经发送,防止重复发送
        if (StringUtils.isNotBlank(RedisConstants.getSmsCode(SmsTypeEnum.valueOf(smsType), phone))) {
            RedisConstants.expireSmsCode(SmsTypeEnum.valueOf(smsType), phone);
            ApiResponse apiResponse = new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ALREAD_SEND);
            apiResponse.setSuccess(true);
            return apiResponse;
        }
        // 如果配置为debugger模式,则不会真实的发短信,默认验证码为1234,否则会真实的发送短信
        String debuggerMode = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.SYSTEM_IS_DEBUGGER_MODE, Function.identity());
        if (StringUtils.equalsIgnoreCase("true", debuggerMode)) {
            RedisConstants.saveSmsCode(SmsTypeEnum.valueOf(smsType), phone, "1234");
            return new ApiResponse(true);
        } else {
            // 正式发送短信验证码
            String smsCode = RandomStringUtils.random(4, Constants.NUMBER);
            boolean sendSmsFlag = SmsUtils.sendCodeSms(phoneCode, phone, smsCode, SmsTypeEnum.valueOf(smsType));
            if (sendSmsFlag) {
                RedisConstants.saveSmsCode(SmsTypeEnum.valueOf(smsType), phone, smsCode);
                return new ApiResponse(true, "发送成功");
            } else {
                return new ApiResponse(ApiResultEnum.SEND_SMS_ERROR, "短信发送失败");
            }
        }
    }

}
