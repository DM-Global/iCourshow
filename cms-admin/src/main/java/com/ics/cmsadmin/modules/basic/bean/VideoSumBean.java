package com.ics.cmsadmin.modules.basic.bean;

import lombok.Data;

@Data
public class VideoSumBean {
    private int countOfVideo;
    private long sumOfVideoTime;
}
