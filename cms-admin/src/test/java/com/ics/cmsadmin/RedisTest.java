package com.ics.cmsadmin;

import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Sandwich on 2019/1/31
 */
public class RedisTest {

    public Jedis getJedis(){

        // 操作单独的文本串
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxIdle(1000);//最大空闲时间
        config.setMaxWaitMillis(1000); //最大等待时间
        config.setMaxTotal(500); //redis池中最大对象个数
        JedisShardInfo sharInfo = new JedisShardInfo("39.106.192.186", 8085);
//        sharInfo.setPassword("123456");
        sharInfo.setConnectionTimeout(5000);//链接超时时间
        Jedis jedis = new Jedis(sharInfo);
        jedis.auth("www.icourshow.coma@#@$%@$#*%^>*<>%*^da123@#@%$&$&^*dvz@!#");
        return jedis;
    }

    @Test
    public void redisTest(){

        Jedis jedis = getJedis();
        Set<String> keys = jedis.keys("WECHAT_CODE_LOGIN*");
        List<String> myKeys = keys.stream().filter(key -> jedis.get(key).contains("凶猛的熊蜢")).collect(Collectors.toList());
        for (String key:
            myKeys) {
            System.out.println(jedis.get(key));
        }
//        jedis.del("b4f57cfdf21e4fce92bf6ca69e60de88");
//        if (jedis.del("WECHAT_CODE_LOGIN:709c8950-8abe-4fdc-90d8-eb7368def0e9") == 1){
//            System.out.print("delete successfully");
//        }
    }
}
