package com.ics.cmsadmin.modules.auth.dao;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by 666666 on 2018/4/4.
 */
@Repository
@Mapper
public interface UserRoleDao {

    /**
     * 通过角色id,删除"用户-角色"
     */
    void deleteUserRoleByRoleId(@Param("roleId") String roleId);

    /**
     * 统计拥有指定角色id的用户数量
     *
     * @param roleId 角色id
     */
    int countUserByRoleId(@Param("bean") SysUser userBean, @Param("roleId") String roleId);

    /**
     * 分页查询拥有指定角色id的用户
     *
     * @param roleId 角色id
     */
    List<SysUser> listUserByRoleId(@Param("bean") SysUser userBean,
                                   @Param("roleId") String roleId,
                                   @Param("page") PageBean page);

    /**
     * 对用户新增角色信息
     */
    void addRoles2User(@Param("roleIds") Set<String> roleIds,
                       @Param("userId") String userId);

    /**
     * 对用户移除角色信息
     */
    void removeRoles2User(@Param("roleIds") List<String> roleIds,
                          @Param("userId") String userId);

    /**
     * 分页查询不含该角色的用户信息数量
     */
    long countUserWithoutRoleId(@Param("bean") SysUser userBean,
                                @Param("roleId") String roleId);

    /**
     * 查询不含该角色的用户信息
     */
    List<SysUser> listUserWithoutRoleId(@Param("bean") SysUser userBean,
                                        @Param("roleId") String roleId,
                                        @Param("page") PageBean page);
}
