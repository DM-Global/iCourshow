package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;

import java.math.BigDecimal;

/**
 * totalIncome;         // 总收益 A = B + C
 * withdrawCash;        // 提现金额 B
 * balance;              // 余额 C = D + E
 * availableBalance;    // 可用余额 D
 * frozenBalance;       // 冻结余额 E
 * t_student_account 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface StudentAccountService {

    /**
     * 查询学生账户信息
     */
    StudentAccountBean queryByStudentId(String studentId);

    /**
     * 账户变动
     *
     * @param studentId            变动代理商
     * @param money              变动金额
     * @param studentAccountChangesEnum 变动类型
     * @param additional 额外参数
     */
    boolean accountChanges(String studentId, BigDecimal money,
                           StudentAccountChangesEnum studentAccountChangesEnum,
                           StudentAccountDetailBean additional);

    /**
     * @param bean
     * @return
     */
    boolean insert(StudentAccountBean bean);


}
