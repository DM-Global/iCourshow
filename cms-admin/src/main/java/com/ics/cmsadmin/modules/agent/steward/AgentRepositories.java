package com.ics.cmsadmin.modules.agent.steward;

import com.ics.cmsadmin.modules.agent.dao.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AgentRepositories {
    public static AccountDetailDao accountDetailDao;
    public static AgentAccountDao agentAccountDao;
    public static AgentInfoDao agentInfoDao;
    public static AgentShareRuleDao agentShareRuleDao;
    public static ApplyWithdrawCashDao applyWithdrawCashDao;
    public static ShareDetailDao shareDetailDao;
    public static AgentDaySummaryDao agentDaySummaryDao;

    @Resource
    public void setAgentDaySummaryDao(AgentDaySummaryDao agentDaySummaryDao) {
        AgentRepositories.agentDaySummaryDao = agentDaySummaryDao;
    }

    @Resource
    public void setAccountDetailDao(AccountDetailDao accountDetailDao) {
        AgentRepositories.accountDetailDao = accountDetailDao;
    }

    @Resource
    public void setAgentAccountDao(AgentAccountDao agentAccountDao) {
        AgentRepositories.agentAccountDao = agentAccountDao;
    }

    @Resource
    public void setAgentInfoDao(AgentInfoDao agentInfoDao) {
        AgentRepositories.agentInfoDao = agentInfoDao;
    }

    @Resource
    public void setAgentShareRuleDao(AgentShareRuleDao agentShareRuleDao) {
        AgentRepositories.agentShareRuleDao = agentShareRuleDao;
    }

    @Resource
    public void setApplyWithdrawCashDao(ApplyWithdrawCashDao applyWithdrawCashDao) {
        AgentRepositories.applyWithdrawCashDao = applyWithdrawCashDao;
    }

    @Resource
    public void setShareDetailDao(ShareDetailDao shareDetailDao) {
        AgentRepositories.shareDetailDao = shareDetailDao;
    }
}
