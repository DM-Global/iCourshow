package com.ics.cmsadmin.modules.student.bean;

                                        
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
* t_student_share_record
* 学生分享记录
*
* Created by lvsw on 2019-03-03 00:06:16.
*/
@Data
@Builder
public class StudentShareRecordBean {

    private String id;              // id
    private String studentId;              // 学生编号
    private String shareType;              // 分享类型,2满一小时分享 3完成课程分享
    private String scoreId;              // 课程id
    private String shareStatus;              // 完成课程分享是否分享标记, 0 未分享, 1 已经分享
    private String studyTime;              // 学习时间,单位h
        @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
        @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间

    @Tolerate
    public StudentShareRecordBean () {}
}
