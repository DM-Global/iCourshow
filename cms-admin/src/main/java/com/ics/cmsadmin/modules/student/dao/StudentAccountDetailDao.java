package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
@Mapper
public interface StudentAccountDetailDao extends BaseDataDao<StudentAccountDetailBean> {
    /**
     * 插入对账信息
     */
    void insert(@Param("afterAccount") StudentAccountBean afterAccount,
                    @Param("money") BigDecimal money,
                    @Param("studentAccountChangesEnum") StudentAccountChangesEnum studentAccountChangesEnum,
                    @Param("additional") StudentAccountDetailBean additional);
}
