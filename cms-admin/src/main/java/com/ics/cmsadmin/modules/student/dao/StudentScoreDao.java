package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentScoreBean;
import com.ics.cmsadmin.modules.student.enums.StudentScoreChangesEnum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface StudentScoreDao extends BaseDao<StudentScoreBean>, BaseDataDao<StudentScoreBean> {
    /**
     * 插入积分流水
     * @param afterScore
     * @param score
     * @param studentScoreChangesEnum
     * @param remark
     */
    void insertScoreDetail(@Param("bean") StudentScoreBean afterScore,
                           @Param("score") long score,
                           @Param("change") StudentScoreChangesEnum studentScoreChangesEnum,
                           @Param("remark") String remark);

    /**
     * 推广用户获取积分
     * @param scoreId
     * @param score
     */
    void promotionStudent(@Param("scoreId") String scoreId,
                          @Param("score") long score);
}
