package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
 * t_share_poster
 */
@Data
@Builder
public class SharePosterBean {

    private String id;              // 分享海报id
    private String posterId;              // 海报图片id
    private String name;              // 分享海报名
    private String description;              // 描述
    private Integer shareType;              // 分享类型
    private String posterSrc;              // 海报url
    private String shareUrl;             // 分享二维码url
    // 状态
    private Boolean isActive;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间
    private String keyword;

    @Tolerate
    public SharePosterBean() {
    }
}
