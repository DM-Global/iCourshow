package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.annotation.UserOperLog;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.BankPayBean;
import com.ics.cmsadmin.modules.basic.bean.WithdrawTestBean;
import com.ics.cmsadmin.modules.basic.service.PayService;
import com.jpay.ext.kit.HttpKit;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.APP_WX_PAY;
import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.APP_WX_PAY_NOTIFY;


/**
 * 微信支付接口
 */
@Api(description = "app微信支付相关接口", tags = "微信支付接口")
@RestController
@RequestMapping("app/wxpay")
@Log4j2
public class AppWxPayController {

    @Autowired
    private PayService payService;

    /**
     * 微信APP支付
     */
    @UserOperLog
    @ApiOperation(value = "微信app支付", notes = APP_WX_PAY)
    @GetMapping(value = "/appPay")
    public ApiResponse appPay(HttpServletRequest request) {
        // 不用设置授权目录域名
        // 统一下单地址 https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1#

        return payService.getWxAppPayResult(request);
    }


    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "微信app支付同步接口", notes = APP_WX_PAY_NOTIFY)
    @RequestMapping(value = "/pay_notify", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public String pay_notify(HttpServletRequest request) {
        // 支付结果通用通知文档: https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_7
        String xmlMsg = HttpKit.readData(request);
        log.info("支付通知=" + xmlMsg);
        return payService.handleOrderAfterWxAppPaid(xmlMsg);
    }


    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "微信app提现到零钱", notes = "提现测试接口,只是用来测试测试环境提现")
    @RequestMapping(value = "/wxWithdrawTest", method = {RequestMethod.POST})
    @ResponseBody
    public ApiResponse wxWithdraw(HttpServletRequest request,@RequestBody WithdrawTestBean withdrawTestBean) {
        return ApiResponse.getDefaultResponse(payService.wxWithdraw(withdrawTestBean.getAmount(),withdrawTestBean.getStudentId()));
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "微信app提现查询", notes = "提现结果测试接口,只是用来测试测试环境提现结果")
    @RequestMapping(value = "/wxWithdrawInfo/{tradeNo}", method = {RequestMethod.GET})
    @ResponseBody
    public ApiResponse transferInfo(HttpServletRequest request, @ApiParam("提现订单号") @PathVariable String tradeNo) {
        return ApiResponse.getDefaultResponse(payService.getWxWithdrawInfo(tradeNo));
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "微信app提现到银行卡", notes = "提现测试接口,只是用来测试测试环境提现")
    @RequestMapping(value = "/withdraw2BankTest", method = {RequestMethod.POST})
    @ResponseBody
    public ApiResponse withdraw2BankTest(HttpServletRequest request,@RequestBody BankPayBean bankPayBean) {
        return ApiResponse.getDefaultResponse(payService.wxPayBank(bankPayBean));
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "app提现到银行卡查询", notes = "提现结果测试接口,只是用来测试测试环境提现到银行卡结果")
    @RequestMapping(value = "/wxPayBankInfo/{tradeNo}", method = {RequestMethod.GET})
    @ResponseBody
    public ApiResponse wxPayBankInfo(HttpServletRequest request, @ApiParam("提现订单号") @PathVariable String tradeNo) {
        return ApiResponse.getDefaultResponse(payService.queryBankInfo(tradeNo));
    }


}
