//package com.ics.cmsadmin.modules.agent.export;
//
//import com.ics.cmsadmin.frame.utils.poi.CreateRow;
//import com.ics.cmsadmin.modules.agent.bean.ApplyWithdrawCashBean;
//import org.apache.poi.ss.usermodel.Row;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ApplyWithdrawCreateRow extends CreateRow<ApplyWithdrawCashBean> {
//
//    @Override
//    public void writeRow(Row row, ApplyWithdrawCashBean bean) {
//        int index = 0;
//        row.createCell(index ++).setCellValue(bean.getId());
//        row.createCell(index ++).setCellValue(bean.getAgentNo());
//        row.createCell(index ++).setCellValue(bean.getWithdrawCash().toString());
//        row.createCell(index ++).setCellValue(bean.getCreateTime());
//        row.createCell(index ++).setCellValue(bean.getStatus());
//        row.createCell(index).setCellValue(bean.getRemark());
//    }
//}
