package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

import java.util.List;

@Data
public class TopicDTO {

    private TopicVO topic;

    private List<PointVO> pointList;
}
