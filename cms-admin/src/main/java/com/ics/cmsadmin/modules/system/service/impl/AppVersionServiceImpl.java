package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.system.dao.AppVersionDao;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import com.ics.cmsadmin.modules.system.service.AppVersionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.ics.cmsadmin.modules.system.steward.SystemRepositories.appVersionDao;

/**
 * t_app_version
 * Created bylvsw on 2018-17-05 20:12:49.
 */
@Service
public class AppVersionServiceImpl implements AppVersionService {

    @Override
    public AppVersionBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return appVersionDao.queryById(id);
    }

    @Override
    public PageResult list(AppVersionBean bean, PageBean page) {
        long count = appVersionDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, appVersionDao.list(bean, page));
    }

    @Override
    public boolean insert(AppVersionBean bean) {
        if (bean == null) {
            return false;
        }
        if (appVersionDao.findOne(AppVersionBean.builder()
            .type(bean.getType())
            .version(bean.getVersion())
            .build()) != null) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "该app类型的版本号已经存在");
        }
        return appVersionDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, AppVersionBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        List<AppVersionBean> list = appVersionDao.list(AppVersionBean.builder()
            .type(bean.getType())
            .version(bean.getVersion())
            .build(), new PageBean(1, Integer.MAX_VALUE));
        if (Optional.ofNullable(list)
            .orElse(new ArrayList<>())
            .stream()
            .anyMatch(item -> !StringUtils.equalsIgnoreCase(item.getId(), id))) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该app类型的版本号已经存在");
        }
        return appVersionDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return appVersionDao.delete(id) == 1;
    }

    @Override
    public AppVersionBean checkAppVersion(AppVersionBean bean) {
        if (bean == null || StringUtils.isBlank(bean.getType()) ||
            !bean.getType().matches("^ios|android$")) {
            throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "请上传app当前版本信息");
        }
        AppVersionBean currentAppVersion = appVersionDao.findOne(AppVersionBean.builder()
            .type(bean.getType())
            .version(bean.getVersion())
            .build());
        if (currentAppVersion == null) {
            return null;
        }
        String currentAppVersionId = currentAppVersion.getId();
        AppVersionBean newestAppVersion = appVersionDao.findNewestAppVersion(bean.getType(), currentAppVersionId);
        if (newestAppVersion != null) {
            newestAppVersion.setForceUpdate(appVersionDao.checkForceUpdate(currentAppVersionId, newestAppVersion.getId()));
        }
        return newestAppVersion;
    }

    @Override
    public AppVersionBean findLastVersionByType(String type) {
        return appVersionDao.findLastVersionByType(type);
    }
}
