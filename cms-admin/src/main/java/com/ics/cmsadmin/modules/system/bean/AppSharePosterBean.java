package com.ics.cmsadmin.modules.system.bean;

                            
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
* app_share_poster
* app分享海报
*
* Created by lvsw on 2019-18-30 16:05:04.
*/
@Data
@Builder
public class AppSharePosterBean {

    private String id;              // 流水id
    private String bucketName;              // oss bucket name
    private String ossKey;              // oss key
    private String url;              // url链接
    private String isUse;              // 0 不使用, 1 使用
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    @Tolerate
    public AppSharePosterBean() {}
}
