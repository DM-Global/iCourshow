//package com.ics.cmsadmin.modules.agent.service.impl;
//
//import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
//import com.ics.cmsadmin.frame.core.exception.CmsException;
//import com.ics.cmsadmin.frame.profit.ProfitTypeEnum;
//import com.ics.cmsadmin.frame.profit.role.ProfitRole;
//import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
//import com.ics.cmsadmin.modules.agent.bean.AgentShareRuleBean;
//import com.ics.cmsadmin.modules.agent.dao.AgentInfoDao;
//import com.ics.cmsadmin.modules.agent.dao.AgentShareRuleDao;
//import com.ics.cmsadmin.modules.agent.service.AgentShareRuleService;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//
///**
// * a_agent_share_rule
// * Created bylvsw on 2018-37-29 16:09:14.
// */
//@Service
//public class AgentShareRuleServiceImplBak implements AgentShareRuleService {
//    @Resource
//    private AgentShareRuleDao agentShareRuleDao;
//    @Resource
//    private AgentInfoDao agentInfoDao;
//
//    @Override
//    public AgentShareRuleBean queryById(String id) {
//        if (StringUtils.isBlank(id)){
//            return null;
//        }
//        AgentShareRuleBean agentShareRuleBean = agentShareRuleDao.queryById(id);
//        ProfitTypeEnum profitTypeEnum = ProfitTypeEnum.valueOfProfitType(agentShareRuleBean.getType());
//        ProfitRole profitRole = profitTypeEnum.getProfitRole(agentShareRuleBean.getExpression());
//        agentShareRuleBean.setData(profitRole.getData());
//        return agentShareRuleBean;
//    }
//
//    @Override
//    public boolean insert(AgentShareRuleBean bean) {
//        if (bean == null){
//            return false;
//        }
//        AgentInfoBean agentInfoBean = agentInfoDao.queryById(bean.getAgentNo());
//        if (StringUtils.isBlank(bean.getAgentNo()) || agentInfoBean == null){
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "分销商不存在");
//        }
//        ProfitTypeEnum profitTypeEnum = ProfitTypeEnum.valueOfProfitType(bean.getType());
//        if (profitTypeEnum == null){
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "分润类型不识别");
//        }
//        String expression = profitTypeEnum.getFactory().generateExpression(bean.getData());
//        profitTypeEnum.getFactory().verifyExpression(expression);
//        bean.setExpression(expression);
//        return agentShareRuleDao.insert(bean) == 1;
//    }
//
//    @Override
//    public boolean update(String id, AgentShareRuleBean bean) {
//        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null){
//            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
//        }
//        ProfitTypeEnum profitTypeEnum = ProfitTypeEnum.valueOfProfitType(bean.getType());
//        if (profitTypeEnum == null){
//            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "分润类型不识别");
//        }
//        String expression = profitTypeEnum.getFactory().generateExpression(bean.getData());
//        profitTypeEnum.getFactory().verifyExpression(expression);
//        return agentShareRuleDao.update(id, AgentShareRuleBean.builder().expression(expression).type(bean.getType()).build()) == 1;
//    }
//
//    @Override
//    public AgentShareRuleBean queryByAgentNo(String agentNo) {
//        if (StringUtils.isBlank(agentNo)){
//            return null;
//        }
//        AgentShareRuleBean agentShareRuleBean = agentShareRuleDao.findOne(AgentShareRuleBean.builder().agentNo(agentNo).build());
//        ProfitTypeEnum profitTypeEnum = ProfitTypeEnum.valueOfProfitType(agentShareRuleBean.getType());
//        ProfitRole profitRole = profitTypeEnum.getProfitRole(agentShareRuleBean.getExpression());
//        agentShareRuleBean.setData(profitRole.getData());
//        return agentShareRuleBean;
//    }
//
//}
