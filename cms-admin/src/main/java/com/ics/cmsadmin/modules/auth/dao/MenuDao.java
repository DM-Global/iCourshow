package com.ics.cmsadmin.modules.auth.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.frame.core.dao.TreeDataDao;
import com.ics.cmsadmin.modules.auth.bean.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2018-03-29.
 */
@Repository
@Mapper
public interface MenuDao extends BaseDataDao<SysMenu>, TreeDataDao<SysMenu>, BaseDao<SysMenu> {
    /**
     * 根据登陆用户获取菜单
     *
     * @param parentId 登陆用户
     * @return
     */
    List<SysMenu> listDirectChildrenByUserId(@Param("parentId") String parentId, @Param("loginUserId") String loginUserId);
}
