package com.ics.cmsadmin.modules.agent.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.agent.bean.ShareDetailBean;
import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.dao.OrderDao;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeEnum;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.bean.StudentShareDetailBean;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.student.steward.StudentRepositories;
import com.ics.cmsadmin.modules.student.steward.StudentServices;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import static com.ics.cmsadmin.modules.agent.steward.AgentRepositories.shareDetailDao;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.registerService;

/**
 * a_share_detail
 * Created bylvsw on 2019-03-19 16:09:14.
 */
@Log4j2
@Service
public class ShareDetailServiceImpl implements ShareDetailService {

    @Resource
    private OrderDao orderDao;
    @Resource
    private UPushApiService uPushApiService;

    @Override
    public PageResult<ShareDetailBean> listByLoginUserId(ShareDetailBean bean, String loginUserId, PageBean page) {
        return listByLoginUserId(bean, loginUserId, page, shareDetailDao);
    }

    @Override
    public boolean generateShareByOrderId(String orderId) {
        if (StringUtils.isBlank(orderId)) {
            log.warn("订单id-{}不能为空", orderId);
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "订单id不能为空");
        }
        OrderBean orderBean = orderDao.findOne(OrderBean.builder().id(orderId).status(2).accStatus("0").build());
        if (orderBean == null) {
            log.warn("订单-{}未支付或已入账", orderId);
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "订单未支付或已入账");
        }
        StudentBean studentBean = StudentServices.studentService.queryById(orderBean.getUserId());
        if (studentBean == null) {
            log.warn("找不到该订单-{}的学生信息", orderId);
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "找不到该订单的学生信息");
        }
        StudentBean parentStudent = StudentServices.studentService.queryById(studentBean.getParentId());
        if (parentStudent == null) {
            log.warn("购买该订单-{}的学生不存在父级,不需要进行分润计算", orderId);
            orderDao.updateNotIntoAccount(orderId);
            return true;
        }
        if (orderBean.getPaymentMethod() == CommonEnums.PaymentMethod.FREE_TRIAL.getCode() ||
            orderBean.getPaymentMethod() == CommonEnums.PaymentMethod.APPLE_SANDBOX_PAY.getCode()) {
            log.warn("免费订单或苹果沙箱测试支付不需要分润,不需要分润", orderId);
            orderDao.updateNotIntoAccount(orderId);
            return true;
        }
//        String cityPartnerPackageId = registerService.queryRegisterValue(CITY_PARTNER_PACKAGE_ID, Function.identity());
//        if (StringUtils.equalsIgnoreCase(orderBean.getPackageId(), cityPartnerPackageId)) {
//            log.warn("城市合伙人套餐不需要分润", orderId);
//            orderDao.updateNotIntoAccount(orderId);
//            return true;
//        }
        synchronized (orderId.intern()) {
            OrderBean order = orderDao.findOne(OrderBean.builder().id(orderId).status(2).accStatus("0").build());
            if (order == null) {
                throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "订单未支付或已入账,不必重复计算");
            }

            BigDecimal parentShareRate = Optional.ofNullable(parentStudent.getShareRate())
                .orElse(registerService.queryRegisterValue(RegisterKeyValueEnum.PARENT_STUDENT_DEFAULT_SHARE_RATE, BigDecimal::new));

            // 计算直接上级的分润分润
            BigDecimal parentShareMoney = order.getOrderAmount()
                .multiply(parentShareRate)
                .multiply(new BigDecimal("0.01"))
                .setScale(2, RoundingMode.FLOOR);
            // 插入直接上级的分润明细记录
            StudentRepositories.studentShareDetailDao.insert(StudentShareDetailBean.builder()
                .orderId(orderId)
                .orderMoney(order.getOrderAmount())
                .orderStudentId(order.getUserId())
                .studentId(studentBean.getParentId())
                .shareMoney(parentShareMoney)
                .shareRate(parentShareRate)
                .shareType("1")
                .build());
            // 学生账户增加分润金额
            StudentServices.studentAccountService.accountChanges(studentBean.getParentId(), parentShareMoney,
                StudentAccountChangesEnum.PARENT_PROFIT_INCOME,
                StudentAccountDetailBean.builder()
                    .orderNo(orderId)
                    .orderStudentId(order.getUserId())
                    .build());
            uPushApiService.sendUnicast(studentBean.getParentId(),
                UPushCustomDefined.builder()
                    .uPushType(UPushTypeEnum.PARENT_SHARE)
                    .money(parentShareMoney)
                    .studentName(Optional.of(studentBean.getNickname()).orElse(studentBean.getUsername()))
                    .build());

            String grandfatherId = parentStudent.getParentId();
            if (StudentServices.studentService.queryById(grandfatherId) != null) {
                // 计算祖父的分润
                BigDecimal grandFatherShareRate = registerService.queryRegisterValue(RegisterKeyValueEnum.GRANDFATHER_STUDENT_SHARE_RATE, BigDecimal::new);
                BigDecimal grandfatherShareMoney = order.getOrderAmount()
                    .multiply(grandFatherShareRate)
                    .multiply(new BigDecimal("0.01"))
                    .setScale(2, RoundingMode.FLOOR);
                StudentRepositories.studentShareDetailDao.insert(StudentShareDetailBean.builder()
                    .orderId(orderId)
                    .orderMoney(order.getOrderAmount())
                    .orderStudentId(order.getUserId())
                    .studentId(grandfatherId)
                    .shareMoney(grandfatherShareMoney)
                    .shareRate(grandFatherShareRate)
                    .shareType("2")
                    .build());
                StudentServices.studentAccountService.accountChanges(grandfatherId, grandfatherShareMoney,
                    StudentAccountChangesEnum.GRANDFATHER_PROFIT_INCOME,
                    StudentAccountDetailBean.builder()
                        .orderNo(orderId)
                        .orderStudentId(order.getUserId())
                        .build());
                uPushApiService.sendUnicast(grandfatherId,
                    UPushCustomDefined.builder()
                        .uPushType(UPushTypeEnum.GRANDFATHER_SHARE)
                        .money(grandfatherShareMoney)
                        .studentName(Optional.of(studentBean.getNickname()).orElse(studentBean.getUsername()))
                        .build());
            }
            orderDao.updateIntoAccount(orderId);
        }
        return true;
    }

    //    /**
//    /*
//     * 代理分润
//     * @param orderId 订单id
//     */
//    @Override
//    public boolean generateShareByOrderId(String orderId) {
//        if (StringUtils.isBlank(orderId)) {
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "订单id不能为空");
//        }
//        OrderBean orderBean = orderDao.findOne(OrderBean.builder().id(orderId).status(2).accStatus("0").build());
//        if (orderBean == null) {
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "订单不存在或者未支付");
//        }
//        if (StringUtils.isBlank(orderBean.getAgentNo()) || StringUtils.equalsIgnoreCase("0", orderBean.getAgentNo())) {
//            log.warn("该代理商为顶级分销商,不需要分润");
//            orderDao.updateNotIntoAccount(orderId);
//            return true;
//        }
//        AgentInfoBean agentInfoBean = agentInfoService.queryById(orderBean.getAgentNo());
//        if (agentInfoBean == null) {
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "分销商不存在");
//        }
//        if (StringUtils.equalsIgnoreCase(agentInfoBean.getStatus(), Constants.DISABLE_STATUS)) {
//            log.warn("分销商{}是禁用状态,不予分润", agentInfoBean.getAgentNo());
//            orderDao.updateNotIntoAccount(orderId);
//            return true;
//        }
//        AgentShareRuleBean agentShareRuleBean = agentShareRuleService.queryByAgentNo(agentInfoBean.getAgentNo());
//        if (agentShareRuleBean == null) {
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "分销商分润规则不存在");
//        }
//
//        String orderPayTime = DateFormatUtils.format(Optional.ofNullable(orderBean.getPayTime()).orElse(new Date()),
//            "yyyy-MM-dd HH:mm:ss");
//        long studentNum = orderDao.countDistinctBuyOrderStudentNum(agentInfoBean.getAgentNo(), orderPayTime);
//        ProfitTypeEnum profitTypeEnum = ProfitTypeEnum.valueOfProfitType(agentShareRuleBean.getType());
//        if (profitTypeEnum == null) {
//            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "不支持该分润规则类型");
//        }
//        synchronized (agentInfoBean.getAgentNo().intern()) {
//            // 1. 计算直推分润
//            OrderBean one = orderDao.findOne(OrderBean.builder().id(orderId).status(2).accStatus("0").build());
//            if (one == null) {
//                throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "订单未支付或已入账,不必重复计算");
//            }
//            profitTypeEnum.getFactory().verifyExpression(agentShareRuleBean.getExpression());
//            ProfitInfo profitInfo = profitTypeEnum.getProfitRole(agentShareRuleBean.getExpression())
//                .getProfitInfo(orderBean.getOrderAmount(), studentNum);
//            shareDetailDao.insert(ShareDetailBean.builder()
//                .agentNo(agentInfoBean.getAgentNo())
//                .shareType("0")
//                .shareMoney(profitInfo.getShareMonery())
//                .orderMoney(profitInfo.getOrderMoney())
//                .shareRate(profitInfo.getShareRate())
//                .orderId(orderId)
//                .studentId(orderBean.getUserId())
//                .sourceAgentNo(agentInfoBean.getAgentNo())
//                .studentNum(studentNum + "")
//                .build());
//            agentAccountService.accountChanges(agentInfoBean.getAgentNo(),
//                profitInfo.getShareMonery(), AccountChangesEnum.PROFIT_INCOME);
//
//            // 2 计算上级分润
//            AgentInfoBean oneAgentInfo = agentInfoService.queryById(agentInfoBean.getParentId());
//            if (oneAgentInfo != null && StringUtils.equalsIgnoreCase(oneAgentInfo.getStatus(), Constants.ENABLE_STATUS)) {
//                BigDecimal oneAgentRate = registerService.queryRegisterValue(RegisterKeyValueEnum.ONE_AGENT_SHARE_RATE, BigDecimal::new);
//                BigDecimal oneShareMoney = orderBean.getOrderAmount().multiply(oneAgentRate).multiply(new BigDecimal("0.01"));
//                shareDetailDao.insert(ShareDetailBean.builder()
//                    .agentNo(oneAgentInfo.getAgentNo())
//                    .shareType("1")
//                    .shareMoney(oneShareMoney)
//                    .orderMoney(profitInfo.getOrderMoney())
//                    .shareRate(oneAgentRate)
//                    .orderId(orderId)
//                    .studentId(orderBean.getUserId())
//                    .sourceAgentNo(agentInfoBean.getAgentNo())
//                    .build());
//                agentAccountService.accountChanges(oneAgentInfo.getAgentNo(),
//                    oneShareMoney, AccountChangesEnum.PROFIT_INCOME);
//            }
//            orderDao.updateIntoAccount(orderId);
//        }
//        return true;
//    }
}
