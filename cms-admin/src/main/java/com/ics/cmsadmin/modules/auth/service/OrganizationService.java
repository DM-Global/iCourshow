package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.frame.core.service.TreeDataService;
import com.ics.cmsadmin.modules.auth.bean.OrganizationBean;


/**
 * sys_organization 服务类
 * Created by lvsw on 2018-29-21 10:10:05.
 */
public interface OrganizationService extends BaseDataService<OrganizationBean>, TreeDataService<OrganizationBean>,
    BaseService<OrganizationBean>, LoginBaseDataService<OrganizationBean> {
    /**
     * 查询所有的组织
     */
    OrganizationBean listAllChildren();

    /**
     * 按组织分类查询所有的用户
     */
    OrganizationBean listAllUserGroupByOrgain();
}
