package com.ics.cmsadmin.modules.pub.service;

import com.ics.cmsadmin.modules.pub.dao.TeacherMapper;
import com.ics.cmsadmin.modules.pub.bean.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("TeacherService")
public class TeacherService extends ResourceService<TeacherMapper, Teacher, Integer> {
    @Autowired
    private TeacherMapper teacherMapper;

    @Override
    public Teacher get(Integer id) {
        Teacher teacher = super.get(id);
        teacher.setSubjects(teacherMapper.getSubjectsByTeacherId(id));
        return teacher;
    }

    @Override
    public List<Teacher> getMultiple(Map<String, Object> param) {
        List<Teacher> teachers = super.getMultiple(param);
        for (Teacher teacher : teachers) {
            teacher.setSubjects(teacherMapper.getSubjectsByTeacherId(teacher.getId()));
        }
        return teachers;
    }
}
