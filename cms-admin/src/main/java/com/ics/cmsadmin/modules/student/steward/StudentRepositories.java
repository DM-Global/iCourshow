package com.ics.cmsadmin.modules.student.steward;

import com.ics.cmsadmin.modules.student.dao.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class StudentRepositories {
    public static StudentAccountDao studentAccountDao;
    public static StudentAccountDetailDao studentAccountDetailDao;
    public static StudentDao studentDao;
    public static StudentDaySummaryDao studentDaySummaryDao;
    public static StudentShareDetailDao studentShareDetailDao;
    public static StudentScoreDao studentScoreDao;

    @Resource
    public void setStudentScoreDao(StudentScoreDao studentScoreDao) {
        StudentRepositories.studentScoreDao = studentScoreDao;
    }

    @Resource
    public void setStudentAccountDao(StudentAccountDao studentAccountDao) {
        StudentRepositories.studentAccountDao = studentAccountDao;
    }

    @Resource
    public void setStudentAccountDetailDao(StudentAccountDetailDao studentAccountDetailDao) {
        StudentRepositories.studentAccountDetailDao = studentAccountDetailDao;
    }

    @Resource
    public void setStudentDao(StudentDao studentDao) {
        StudentRepositories.studentDao = studentDao;
    }

    @Resource
    public void setStudentDaySummaryDao(StudentDaySummaryDao studentDaySummaryDao) {
        StudentRepositories.studentDaySummaryDao = studentDaySummaryDao;
    }

    @Resource
    public void setStudentShareDetailDao(StudentShareDetailDao studentShareDetailDao) {
        StudentRepositories.studentShareDetailDao = studentShareDetailDao;
    }
}
