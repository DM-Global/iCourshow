package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.CourseTypeBean;
import com.ics.cmsadmin.modules.basic.service.CourseTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Api(description = "课程属性查询接口")
@RestController
@RequestMapping("/api/courseType")
public class ApiCourseTypController {

    @Autowired
    private CourseTypeService courseTypeService;

    @ApiOperation("查询所有已启用的课程属性")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody CourseTypeBean courseTypeBean,
                            @ApiParam("页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                            @ApiParam("每页条数") @RequestParam(value = "pageSize", defaultValue = "2147483647") Integer pageSize) {

        PageBean page = new PageBean(pageNo, pageSize);
        courseTypeBean.setIsActive(true);
        return ApiResponse.getDefaultResponse(courseTypeService.list(courseTypeBean, page));
    }
}
