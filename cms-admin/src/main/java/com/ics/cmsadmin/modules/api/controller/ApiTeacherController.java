package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.TeacherBean;
import com.ics.cmsadmin.modules.basic.service.TeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(description = "教师查询接口", tags = "公众号")
@RestController
@RequestMapping("/api/teacher")
public class ApiTeacherController {

    @Autowired
    private TeacherService teacherService;

    @ApiOperation(value = "分页查询教师信息")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody TeacherBean teacherBean,
                            @ApiParam("页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                            @ApiParam("每页条数") @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        PageBean pageBean = new PageBean(pageNo, pageSize);
        teacherBean.setIsActive(true);
        PageResult pageResult = teacherService.list(teacherBean, pageBean);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @ApiOperation(value = "查询教师详细信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(teacherService.teacherDetail(id));
    }
}
