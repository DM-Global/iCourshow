package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.modules.basic.bean.AnswersBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2018-09-06.
 */
@Repository
@Mapper
public interface AnswersDao {
    /**
     * 插入答案内容
     *
     * @param answersBeans 答案内容
     * @param problemId    问题id
     */
    void batchInsert(@Param("list") List<AnswersBean> answersBeans, @Param("problemId") String problemId);

    /**
     * 根据问题id删除对应的答案
     *
     * @param problemId
     */
    void deleteByProblemId(@Param("problemId") String problemId);

    /**
     * 根据问题Id查询答案列表
     *
     * @param problemId
     * @return
     */
    List<AnswersBean> queryByProblemId(@Param("problemId") String problemId);
}
