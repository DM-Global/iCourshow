package com.ics.cmsadmin.frame.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

public class QrCodeCreateUtil {

    /**
     * 生成包含字符串信息的二维码图片
     *
     * @param content      二维码携带信息
     * @param qrCodeSize   二维码图片大小
     * @throws WriterException
     * @throws IOException
     */
    public static BufferedImage createQrCode(String content, int qrCodeSize) throws WriterException, IOException {
        //设置二维码纠错级别ＭＡＰ
        Hashtable<EncodeHintType, Object> hintMap = new Hashtable<>();
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);  // 矫错级别
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        //创建比特矩阵(位矩阵)的QR码编码的字符串
        BitMatrix byteMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, qrCodeSize, qrCodeSize, hintMap);
        // 使BufferedImage勾画QRCode  (matrixWidth 是行二维码像素点)
        int matrixWidth = byteMatrix.getWidth();
        BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, BufferedImage.TYPE_INT_ARGB);
//        image.createGraphics();
        Graphics2D graphics = image.createGraphics();
//        graphics.setColor(new Color(0, 0, 0, 0));
//        graphics.fillRect(0, 0, matrixWidth, matrixWidth);
//        // 使用比特矩阵画并保存图像
//        graphics.setColor(Color.BLACK);
        for (int i = 0; i < matrixWidth; i++) {
            for (int j = 0; j < matrixWidth; j++) {
                image.setRGB(i, j, byteMatrix.get(i, j) ? Color.BLACK.getRGB() : new Color(0, 0, 0, 0).getRGB());
//                if (byteMatrix.get(i, j)) {
//                    graphics.fillRect(i, j, 1, 1);
//                }
            }
        }
        try (InputStream logoInputStream = Tools.getResourceInputStream("classpath:/static/image/cmsLogo.png")){
            BufferedImage logoBi = ImageIO.read(logoInputStream);
            BufferedImage compressImage = new BufferedImage(qrCodeSize / 5,qrCodeSize / 5, BufferedImage.TYPE_INT_ARGB);
            Graphics2D logoGraphics = (Graphics2D) compressImage.getGraphics();
            logoGraphics.setBackground(Color.WHITE);
            // 切圆角 start
            logoGraphics.setComposite(AlphaComposite.Src);
            logoGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            logoGraphics.setColor(Color.WHITE);
            logoGraphics.fill(new RoundRectangle2D.Float(0, 0, compressImage.getWidth(), compressImage.getHeight(), 30, 30));// 核心:30是圆角的大小
            logoGraphics.setComposite(AlphaComposite.SrcAtop);
            logoGraphics.drawImage(logoBi, 0,0,compressImage.getWidth(), compressImage.getHeight(),null);
            logoGraphics.dispose();// 完成缩小
            // 切圆角 end
            int x = (image.getWidth() - compressImage.getWidth()) / 2;
            int y = (image.getHeight() - compressImage.getWidth()) / 2;
            graphics.drawImage(compressImage, x, y, compressImage.getWidth(), compressImage.getHeight(), null);
            graphics.dispose();
            compressImage.flush();
        }
        return image;
    }

//    /**
//     * 读二维码并输出携带的信息
//     */
//    public static void readQrCode(InputStream inputStream) throws IOException {
//        //从输入流中获取字符串信息
//        BufferedImage image = ImageIO.read(inputStream);
//        //将图像转换为二进制位图源
//        LuminanceSource source = new BufferedImageLuminanceSource(image);
//        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//        QRCodeReader reader = new QRCodeReader();
//        Result result = null;
//        try {
//            result = reader.decode(bitmap);
//        } catch (ReaderException e) {
//            e.printStackTrace();
//        }
//        System.out.println(result.getText());
//    }

    /**
     * 测试代码
     *
     * @throws WriterException
     */
    public static void main(String[] args) throws IOException, WriterException {


    }

}
