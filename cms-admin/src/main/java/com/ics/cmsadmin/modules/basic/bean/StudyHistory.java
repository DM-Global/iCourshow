package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class StudyHistory {
    private String userId;
    private LevelEnum levelName;
    private String levelId;
    private Integer progress;

    @Tolerate
    public StudyHistory() {
    }
}
