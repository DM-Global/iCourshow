package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.constant.RedisConstants;
import com.ics.cmsadmin.frame.constant.SwaggerNoteConstants;
import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.utils.RandomValidateCodeUtils;
import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.*;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.*;

@AppLoginValid(needLogin = false)
@Api(description = "app公共接口", tags = "公共接口")
@RestController
@RequestMapping("/app/common")
public class AppCommonController {


    @ApiOperation(value = "获取图形验证码", notes = GET_IMAGE_CODE_NOTES)
    @GetMapping(value = "/getImageCode/{imageCodeId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public void getImageCode(@ApiParam("图形验证码id,随机") @PathVariable String imageCodeId,
                             HttpServletResponse response) throws IOException {
        String code = RandomStringUtils.random(4, Constants.NUMBER);
        RedisConstants.saveImageCode(imageCodeId, code);
        BufferedImage randcodeImage = RandomValidateCodeUtils.getRandCodeImage(code);
        ImageIO.write(randcodeImage, "JPEG", response.getOutputStream());
    }

    @ApiOperation(value = "获取短信验证码", notes = SwaggerNoteConstants.SMS_CODE_NOTES)
    @GetMapping(value = "/sendVerifyCodeSms/{smsType}/{phoneCode}/{phone}/{imageCodeId}/{imageCode}")
    public ApiResponse sendVerifyCodeSms(@ApiParam("短信类型") @PathVariable String smsType,
                                         @ApiParam("手机区域码") @PathVariable String phoneCode,
                                         @ApiParam("手机号") @PathVariable String phone,
                                         @ApiParam("图形验证码id") @PathVariable String imageCodeId,
                                         @ApiParam("图形验证码") @PathVariable String imageCode) {
        return smsService.sendVerifyCodeSms(smsType, phoneCode, phone, imageCodeId, imageCode);
    }

    @ApiOperation(value = "检查app版本信息", notes = CHECK_APP_VERSION_NOTES)
    @PostMapping(value = "/checkAppVersion")
    public ApiResponse checkAppVersion(@RequestBody AppVersionBean bean) {
        ApiResponse response = new ApiResponse(true);
        AppVersionBean appVersionBean = SystemServices.appVersionService.checkAppVersion(bean);
        if (appVersionBean == null) {
            response.setMessage("当前版本已经为最新版本");
        }else {
            response.setData(appVersionBean);
        }
        return response;
    }

    @ApiOperation("获取城市列表")
    @GetMapping(value = {"/listCity", "/list/{cityCode}"})
    public ApiResponse listCity(@PathVariable(required = false) String cityCode) {
        return new ApiResponse(cityService.listDirectChildren(cityCode));
    }

    @ApiOperation("获取省市列表")
    @GetMapping(value = {"/listProvinceCity"})
    public ApiResponse listProvinceCity() {
        return new ApiResponse(cityService.listProvinceCity());
    }

    @ApiOperation("查询手机区域码")
    @GetMapping(value = {"/listPhoneCode"})
    public ApiResponse listPhoneCode(@ApiParam("区域码或地区名")  @RequestParam(required = false) String keyword) {
        return new ApiResponse(phoneCodeService.list(keyword));
    }

    @ApiOperation("获取联系我们信息")
    @GetMapping(value = {"/queryContactUsInfo"})
    public ApiResponse queryContactUsInfo() {
        Map<String, String> map = new HashMap<>();
        map.put("email", registerService.queryRegisterValue(RegisterKeyValueEnum.CONTACT_US_EMAIL, Function.identity()));
        map.put("wechat", registerService.queryRegisterValue(RegisterKeyValueEnum.CONTACT_US_WECHAT, Function.identity()));
        return new ApiResponse(map);
    }


}
