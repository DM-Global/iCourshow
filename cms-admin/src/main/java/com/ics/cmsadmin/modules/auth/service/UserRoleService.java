package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.auth.bean.SysUser;

import java.util.List;

/**
 * Created by 666666 on 2018/4/4.
 */
public interface UserRoleService {

    /**
     * 统计拥有指定角色id的用户数量
     *
     * @param roleId 角色id
     */
    int countUserByRoleId(String roleId);

    /**
     * 通过角色id,删除"用户-角色"
     */
    void deleteUserRoleByRoleId(String roleId);

    /**
     * 对用户新增角色信息
     */
    boolean addRoles2User(List<String> roleIds, String userId);

    /**
     * 对用户移除角色信息
     */
    boolean removeRoles2User(List<String> roleIds, String userId);

    /**
     * 根据角色id分页查询用户列表
     */
    PageResult listUserByRoleId(String roleId, SysUser userBean, PageBean pageBean);

    /**
     * 分页查询不含该角色的用户信息
     *
     * @param roleId
     * @param userBean
     * @param pageBean
     * @return
     */
    PageResult listUserWithoutRoleId(String roleId, SysUser userBean, PageBean pageBean);
}
