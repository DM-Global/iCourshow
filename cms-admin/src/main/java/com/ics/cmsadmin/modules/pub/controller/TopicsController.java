package com.ics.cmsadmin.modules.pub.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.pub.bean.TopicDTO;
import com.ics.cmsadmin.modules.pub.bean.Topic;
import com.ics.cmsadmin.modules.pub.service.TopicService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/topics")
public class TopicsController extends ResourceController<TopicService, Topic, Integer> {
    @Resource
    private TopicService topicService;

    @GetMapping
    public ApiResponse topicsGet(String name, Integer subjectId, Integer courseId, Integer unitId,
                                 Boolean isActive, Boolean isPublic,
                                 @RequestParam(value = "page", required = false) Integer page,
                                 @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        Map<String, Object> param = new HashMap<>();
        param.put("name", name);
        param.put("subjectId", subjectId);
        param.put("courseId", courseId);
        param.put("unitId", unitId);
        param.put("isActive", isActive);
        param.put("isPublic", isPublic);
        param.put("page", page);
        param.put("pageSize", pageSize);
        return super.get(param);
    }

    @GetMapping("/{topicId}/detail")
    public ApiResponse get(@PathVariable Integer topicId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return ApiResponse.getDefaultResponse(topicService.get(topicId, userId));
    }

    @GetMapping("/listDetail")
    public ApiResponse listDetail(Integer unitId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        List<TopicDTO> topicDTOList = topicService.listDetail(unitId, userId);
        return ApiResponse.getDefaultResponse(topicDTOList);
    }

}
