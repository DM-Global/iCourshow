package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.Collect;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.basic.bean.UnitBean;
import com.ics.cmsadmin.modules.basic.dao.TopicDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.TopicBean;
import com.ics.cmsadmin.modules.basic.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by lvsw on 2018-09-01.
 */
@Service
public class TopicServiceImpl implements TopicService {
    @Resource
    private TopicDao topicDao;

    @Autowired
    private PointService pointService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private OrderService orderService;
    @Autowired
    private UnitService unitService;

    @CacheDbMember(returnClass = TopicBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public TopicBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return topicDao.queryById(id);
    }

    @CacheDbMember(returnClass = TopicBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public PageResult<TopicBean> list(TopicBean bean, PageBean page) {
        long count = topicDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, topicDao.list(bean, page));
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean insert(TopicBean bean) {
        if (bean == null) {
            return false;
        }
        return topicDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean update(String id, TopicBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return topicDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return topicDao.delete(id) == 1;
    }

    @CacheDbMember(returnClass = TopicBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public List<TopicBean> listAllTopicByUnitId(String unitId) {
        return topicDao.list(TopicBean.builder().unitId(unitId).isActive(true).build(),
            new PageBean(1, Integer.MAX_VALUE));
    }

    @Override
    public long count(TopicBean topicBean) {
        return topicDao.count(topicBean);
    }

    @Override
    public TopicBean topicDetail(String userId, String id) {
        TopicBean topicBean = queryById(id);
        if (topicBean == null) {
            return null;
        }
        if (StringUtils.isBlank(userId)) {
            topicBean.setHasBought(false);
        } else {
            topicBean.setHasBought(orderService.getPurchaseStatus(userId, topicBean.getCourseId()));
        }
        return topicBean;
    }

    @Override
    public List<TopicBean> listDetail(String userId, String unitId) {
        if (StringUtils.isBlank(unitId)) {
            return null;
        }
        UnitBean unitBean = unitService.queryById(unitId);
        if (unitBean == null) {
            return new ArrayList<>();
        }
        boolean purchaseStatus = orderService.getPurchaseStatus(userId, unitBean.getCourseId());
        List<TopicBean> topicBeans = listAllTopicByUnitId(unitId);
        Optional.ofNullable(topicBeans)
            .orElse(new ArrayList<>())
            .forEach(topic -> {
                topic.setCollected(collectionService.select(new Collect(userId, LevelEnum.topic, topic.getId())));
                topic.setHasBought(purchaseStatus);
                topic.setPointList(pointService.listAllPointDetailByTopicId(userId, topic.getId()));
            });
        return topicBeans;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public void updateProblemNum(String topicId, int testNum) {
        topicDao.updateProblemNum(topicId, testNum);
    }
}
