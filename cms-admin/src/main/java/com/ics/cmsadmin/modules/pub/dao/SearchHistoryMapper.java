package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.SearchHistory;

public interface SearchHistoryMapper extends ResourceMapper<SearchHistory, String> {
}
