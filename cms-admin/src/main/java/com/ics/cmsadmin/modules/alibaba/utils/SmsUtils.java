package com.ics.cmsadmin.modules.alibaba.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.*;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.exception.ExceptionUtils;
import com.ics.cmsadmin.frame.property.AliSmsConfig;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Component
public class SmsUtils {

    final static String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
    final static String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
    private static AliSmsConfig aliSmsConfig;

    static {
        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
    }

    @Resource
    public void setAliSmsConfig(AliSmsConfig aliSmsConfig) {
        SmsUtils.aliSmsConfig = aliSmsConfig;
    }

    /**
     * @param mobilePhone 必填:待发送手机号。
     *                    支持以逗号分隔的形式进行批量调用，
     *                    批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,
     *                    验证码类型的短信推荐使用单条调用的方式；
     *                    发送国际/港澳台消息时，接收号码格式为国际区号+号码，如“85200000000”
     * @param params      短信模板参数
     * @param smsTypeEnum 短信类型
     * @return 发送短信是否正确
     * 入参列表
     * 参数名称	参数类型	必填与否	样例取值	参数说明
     * PhoneNumbers	String	必须	15000000000	短信接收号码,支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为：国际区号+号码，如“85200000000”
     * SignName	String	必须	云通信	短信签名
     * TemplateCode	String	必须	SMS_0000	短信模板ID，发送国际/港澳台消息时，请使用国际/港澳台短信模版
     * TemplateParam	String	可选	{“code”:”1234”,”product”:”ytx”}	短信模板变量替换JSON串,友情提示:如果JSON中需要带换行符,请参照标准的JSON协议。
     * SmsUpExtendCode	String	可选	90999	上行短信扩展码,无特殊需要此字段的用户请忽略此字段
     * OutId	String	可选	abcdefgh	外部流水扩展字段
     * 出参列表
     * 出参名称	出参类型	样例取值	参数说明
     * RequestId	String	8906582E-6722	请求ID
     * Code	String	OK	状态码-返回OK代表请求成功,其他错误码详见错误码列表
     * Message	String	请求成功	状态码的描述
     * BizId	String	134523^4351232	发送回执ID,可根据该ID查询具体的发送状态
     * 错误码列表
     * OK	请求成功
     * isp.RAM_PERMISSION_DENY	RAM权限DENY
     * isv.OUT_OF_SERVICE	业务停机
     * isv.PRODUCT_UN_SUBSCRIPT	未开通云通信产品的阿里云客户
     * isv.PRODUCT_UNSUBSCRIBE	产品未开通
     * isv.ACCOUNT_NOT_EXISTS	账户不存在
     * isv.ACCOUNT_ABNORMAL	账户异常
     * isv.SMS_TEMPLATE_ILLEGAL	短信模板不合法
     * isv.SMS_SIGNATURE_ILLEGAL	短信签名不合法
     * isv.INVALID_PARAMETERS	参数异常
     * isp.SYSTEM_ERROR	系统错误
     * isv.MOBILE_NUMBER_ILLEGAL	非法手机号
     * isv.MOBILE_COUNT_OVER_LIMIT	手机号码数量超过限制
     * isv.TEMPLATE_MISSING_PARAMETERS	模板缺少变量
     * isv.BUSINESS_LIMIT_CONTROL	业务限流
     * isv.INVALID_JSON_PARAM	JSON参数不合法，只接受字符串值
     * isv.BLACK_KEY_CONTROL_LIMIT	黑名单管控
     * isv.PARAM_LENGTH_LIMIT	参数超出长度限制
     * isv.PARAM_NOT_SUPPORT_URL	不支持URL
     * isv.AMOUNT_NOT_ENOUGH	账户余额不足
     */
    public static boolean sendSms(String phoneCode, String mobilePhone, Map<String, String> params, SmsTypeEnum smsTypeEnum) {
        if (StringUtils.isBlank(mobilePhone) || !mobilePhone.matches("^(\\d+,?)+$")) {
            log.warn("手机格式不正确 : {}", mobilePhone);
            return false;
        }
        if (MapUtils.isEmpty(params)) {
            log.warn("短信参数不能空");
            return false;
        }
        String templateId;
        if (StringUtils.isBlank(phoneCode) || StringUtils.equalsIgnoreCase(phoneCode, "86")) {
            templateId = String.format("%s_%s", smsTypeEnum.name(), AliSmsConfig.INTERNAL);
        }else {
            templateId = String.format("%s_%s", smsTypeEnum.name(), AliSmsConfig.FOREIGN);
            mobilePhone = phoneCode + mobilePhone;        // 国际短信的手机号是有区号加手机号组成的
        }
//        String templateId = (StringUtils.isBlank(phoneCode) || StringUtils.equalsIgnoreCase(phoneCode, "86")) ?
//            String.format("%s_%s", smsTypeEnum.name(), AliSmsConfig.INTERNAL) :
//            String.format("%s_%s", smsTypeEnum.name(), AliSmsConfig.FOREIGN);
        String templateCode = aliSmsConfig.templateCode.get(templateId);
        if (smsTypeEnum == null || StringUtils.isBlank(templateCode)) {
            log.warn("短信模板id不能为空: {} - {}", smsTypeEnum, templateCode);
            return false;
        }
        try {
            //替换成你的AK
            final String accessKeyId = aliSmsConfig.accessKeyID;//你的accessKeyId,参考本文档步骤2
            final String accessKeySecret = aliSmsConfig.accessKeySecret;//你的accessKeySecret，参考本文档步骤2
            //初始化ascClient,暂时不支持多region（请勿修改）
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            SendSmsRequest request = new SendSmsRequest();  //组装请求对象
            request.setMethod(MethodType.POST); //使用post提交
            request.setPhoneNumbers(mobilePhone);
            //必填:短信签名-可在短信控制台中找到
            request.setSignName(aliSmsConfig.signName);
            //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
            request.setTemplateCode(templateCode);
            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
            request.setTemplateParam(GsonUtils.toJson(params));
            //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
            //request.setSmsUpExtendCode("90997");
            //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            //        request.setOutId("yourOutId");
            log.info("开始发送短信, 短信类型: {},短信模板: {}, 手机号: {}, 参数 {}",
                smsTypeEnum, templateCode, mobilePhone, params);
            SendSmsResponse acsResponse = acsClient.getAcsResponse(request);
            log.info("短信返回内容, 请求id :{}, 业务码: {}, 响应码 : {}, 响应消息: {}",
                acsResponse.getRequestId(), acsResponse.getBizId(), acsResponse.getCode(), acsResponse.getMessage());
            return StringUtils.equalsIgnoreCase(acsResponse.getCode(), "OK");
        } catch (ClientException e) {
            log.error("发送短信异常 {}", ExceptionUtils.collectExceptionStackMsg(e));
            return false;
        }
    }

    /**
     * 发送短信验证码
     *
     * @param mobilePhone 待发送手机号码
     * @param code        短信验证码
     * @param smsTypeEnum 短信类型
     * @return 是否发送成功
     */
    public static boolean sendCodeSms(String phoneCode, String mobilePhone, String code, SmsTypeEnum smsTypeEnum) {
        if (StringUtils.isBlank(code)) {
            log.warn("验证不能为空");
            return false;
        }
        Map<String, String> params = new HashMap<>();
        params.put("code", code);
        return sendSms(phoneCode, mobilePhone, params, smsTypeEnum);
    }

    /**
     * 入参列表
     * 参数名称	参数类型	必填与否	样例取值	参数说明
     * PhoneNumber	String	必须	15000000000	短信接收号码,如果需要查询国际短信,号码前需要带上对应国家的区号,区号的获取详见国际短信支持国家信息查询API接口
     * BizId	String	可选	1234^1234	发送流水号,从调用发送接口返回值中获取
     * SendDate	String	必须	20170525	短信发送日期格式yyyyMMdd,支持最近30天记录查询
     * PageSize	Number	必须	10	页大小Max=50
     * CurrentPage	Number	必须	1	当前页码
     * 出参列表
     * 出参名称	出参类型	样例取值	参数说明
     * RequestId	String	8906582E-6722	请求ID
     * Code	String	OK	状态码-返回OK代表请求成功,其他错误码详见错误码列表
     * Message	String	请求成功	状态码的描述
     * TotalCount	Number	100	发送总条数
     * smsSendDetailDTOs	Object	-	发送明细结构体,详见Demo样例
     */
    public static PageResult<QuerySendDetailsResponse.SmsSendDetailDTO> querySendDetails(String phoneNumber, Date date, PageBean pageBean) {
        if (StringUtils.isBlank(phoneNumber)) {
            log.warn("查询的手机号码不能为空 {}", phoneNumber);
            return null;
        }
        if (date == null || date.before(DateUtils.addDays(new Date(), -30))) {
            log.warn("只能查询30天內发送的短信 {}", date);
            return null;
        }
        try {
            //此处需要替换成开发者自己的AK信息
            final String accessKeyId = aliSmsConfig.accessKeyID;
            final String accessKeySecret = aliSmsConfig.accessKeySecret;
            //初始化ascClient
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            //组装请求对象
            QuerySendDetailsRequest request = new QuerySendDetailsRequest();
            //必填-号码
            request.setPhoneNumber(phoneNumber);
            //可选-调用发送短信接口时返回的BizId
//        request.setBizId("1234567^8901234");
            //必填-短信发送的日期 支持30天内记录查询（可查其中一天的发送数据），格式yyyyMMdd
            request.setSendDate(DateFormatUtils.format(date, "yyyyMMdd"));
            //必填-页大小
            request.setPageSize((long) pageBean.getPageSize());
            //必填-当前页码从1开始计数
            request.setCurrentPage((long) pageBean.getPageNo());
            //hint 此处可能会抛出异常，注意catch
            QuerySendDetailsResponse acsResponse = acsClient.getAcsResponse(request);
            //获取返回结果
            if (acsResponse.getCode() != null && acsResponse.getCode().equals("OK")) {
                return new PageResult<>(acsResponse.getSmsSendDetailDTOs(), Integer.valueOf(acsResponse.getTotalCount()));
            } else {
                log.info("查询短信内容异常, 请求id: {}, 响应码: {}, 相应消息: {}",
                    acsResponse.getRequestId(), acsResponse.getCode(), acsResponse.getMessage());
                return null;
            }
        } catch (ClientException e) {
            log.error("查询短信异常 {}", ExceptionUtils.collectExceptionStackMsg(e));
            return null;
        }
    }

    /**
     * @param phoneNumbers 待发送的手机号码
     * @param params       短信模板参数
     * @param smsTypeEnum  短信类型
     * @return 是否发送成功
     * 入参列表
     * 参数名称	参数类型	必填与否	样例取值	参数说明
     * PhoneNumberJson	String	必须	[“15000000000”,”15000000001”]	短信接收号码,JSON格式,批量上限为100个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
     * SignNameJson	String	必须	[“云通信”,”云通信”]	短信签名,JSON格式
     * TemplateCode	String	必须	SMS_0000	短信模板ID
     * TemplateParamJson	String	必须	[{“code”:”1234”,”product”:”ytx1”},{“code”:”5678”,”product”:”ytx2”}]	短信模板变量替换JSON串,友情提示:如果JSON中需要带换行符,请参照标准的JSON协议。
     * SmsUpExtendCodeJson	String	可选	[“90999”,”90998”]	上行短信扩展码,JSON格式，无特殊需要此字段的用户请忽略此字段
     * 出参列表
     * 出参名称	出参类型	样例取值	参数说明
     * RequestId	String	8906582E-6722	请求ID
     * Code	String	OK	状态码-返回OK代表请求成功,其他错误码详见错误码列表
     * Message	String	请求成功	状态码的描述
     * BizId	String	134523^4351232	发送回执ID,可根据该ID查询具体的发送状态
     */
    public static boolean sendBatchSms(String[] phoneNumbers, List<Map<String, String>> params, SmsTypeEnum smsTypeEnum) {
        if (ArrayUtils.isEmpty(phoneNumbers) || Arrays.stream(phoneNumbers).anyMatch(item -> !item.matches("^\\d+$"))) {
            log.warn("手机格式不正确 : {}", Arrays.toString(phoneNumbers));
            return false;
        }
        if (CollectionUtils.isEmpty(params) || params.size() != phoneNumbers.length) {
            log.warn("短信参数个数与手机号码个数不匹配,参数为:{}", params);
            return false;
        }
        if (smsTypeEnum == null || aliSmsConfig.templateCode.get(smsTypeEnum.name()) == null) {
            log.warn("短信模板id不能为空: {} - {}",
                smsTypeEnum, aliSmsConfig.templateCode.get(smsTypeEnum.name()));
            return false;
        }
        try {
            //替换成你的AK
            final String accessKeyId = aliSmsConfig.accessKeyID;
            final String accessKeySecret = aliSmsConfig.accessKeySecret;
            //初始化ascClient,暂时不支持多region（请勿修改）
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId,
                accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            //组装请求对象
            SendBatchSmsRequest request = new SendBatchSmsRequest();
            //使用post提交
            request.setMethod(MethodType.POST);
            //必填:待发送手机号。支持JSON格式的批量调用，批量上限为100个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
            request.setPhoneNumberJson(GsonUtils.toJson(phoneNumbers));
            //必填:短信签名-支持不同的号码发送不同的短信签名
            String[] signNames = StringUtils.repeat(aliSmsConfig.signName, "-", phoneNumbers.length).split("-");
            request.setSignNameJson(GsonUtils.toJson(signNames));
            //必填:短信模板-可在短信控制台中找到
            request.setTemplateCode(aliSmsConfig.templateCode.get(smsTypeEnum));
            //必填:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
            request.setTemplateParamJson(GsonUtils.toJson(params));
            //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
            //request.setSmsUpExtendCodeJson("[\"90997\",\"90998\"]");
            //请求失败这里会抛ClientException异常
            log.error("批量发送短信, 短信模板: {}, 发送手机号: {}, 发送参数: {}",
                aliSmsConfig.templateCode.get(smsTypeEnum), GsonUtils.toJson(phoneNumbers), GsonUtils.toJson(params));
            SendBatchSmsResponse acsResponse = acsClient.getAcsResponse(request);
            log.info("批量发送短信返回内容, 请求id :{}, 业务码: {}, 响应码 : {}, 响应消息: {}",
                acsResponse.getRequestId(), acsResponse.getBizId(), acsResponse.getCode(), acsResponse.getMessage());
            return StringUtils.equalsIgnoreCase(acsResponse.getCode(), "OK");
        } catch (ClientException e) {
            log.error("批量发送短信异常 {}", ExceptionUtils.collectExceptionStackMsg(e));
            return false;
        }
    }
}
