package com.ics.cmsadmin.frame.utils;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by 666666 on 2018/8/25.
 */
@Component
public class RedisUtils {
    @Autowired
    private StringRedisTemplate redisTemplate;
    private static RedisUtils redisUtils;
    private static final String GROUP_KEY = "%s:%s";

    @PostConstruct
    public synchronized void init() {
        if (redisUtils == null) {
            redisUtils = this;
        }
    }

    public static void set(String group, String key, String data) {
        set(String.format(GROUP_KEY, group, key), data, -1);
    }

    public static void set(String group, String key, String data, long timeoutBySeconds) {
        set(String.format(GROUP_KEY, group, key), data, timeoutBySeconds);
    }

    public static void set(String key, String data) {
        set(key, data, -1);
    }

    public static void set(String key, String data, long timeoutBySeconds) {
        if (data == null) {
            return;
        }
        if (timeoutBySeconds < 0) {
            redisUtils.redisTemplate.opsForValue()
                .set(key, data);
        } else {
            redisUtils.redisTemplate.opsForValue()
                .set(key, data, timeoutBySeconds, TimeUnit.SECONDS);
        }
    }

    public static void delete(String key) {
        redisUtils.redisTemplate.delete(key);
    }

    public static void delete(String group, String key) {
        delete(String.format(GROUP_KEY, group, key));
    }

    public static void deleteGroup(String group) {
        if (group == null) {
            return;
        }
        Set<String> keys = redisUtils.redisTemplate.opsForValue().getOperations().keys(group + ":*");
        if (CollectionUtils.isNotEmpty(keys)) {
            redisUtils.redisTemplate.delete(keys);
        }
    }

    public static String get(String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        return redisUtils.redisTemplate.opsForValue().get(key);
    }

    public static String get(String group, String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        return redisUtils.redisTemplate.opsForValue().get(group + ":" + key);
    }

    public static Boolean expire(String group, String key, long timeoutSeconds) {
        if (StringUtils.isBlank(group) || StringUtils.isBlank(key)) {
            return false;
        }
        timeoutSeconds = timeoutSeconds > 0 ? timeoutSeconds : -1;
        return expire(String.format(GROUP_KEY, group, key), timeoutSeconds);
    }

    public static Boolean expire(String key, long timeoutSeconds) {
        if (StringUtils.isBlank(key)) {
            return false;
        }
        timeoutSeconds = timeoutSeconds > 0 ? timeoutSeconds : -1;
        return redisUtils.redisTemplate.expire(key, timeoutSeconds, TimeUnit.SECONDS);
    }

}
