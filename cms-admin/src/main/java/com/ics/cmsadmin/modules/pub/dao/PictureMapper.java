package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Picture;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PictureMapper extends ResourceMapper<Picture, Integer> {

    /**
     * 获取返回总条目数
     *
     * @param picType
     * @return
     */
    Integer totalCount(@Param("picType") Integer picType);

    List<Picture> listPage(@Param("picType") Integer picType, @Param("pageSize") Integer pageSize, @Param("start") Integer start);
}
