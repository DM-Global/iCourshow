package com.ics.cmsadmin.modules.auth.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.auth.bean.SysRole;
import com.ics.cmsadmin.modules.auth.bean.SysAuthorize;
import com.ics.cmsadmin.frame.core.bean.FourTupleBean;
import com.ics.cmsadmin.frame.core.bean.ThreeTupleBean;
import com.ics.cmsadmin.modules.auth.service.AuthorizeService;
import com.ics.cmsadmin.modules.auth.service.RoleAuthorizeService;
import com.ics.cmsadmin.modules.auth.service.RoleService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Role controller
 * Created by lvsw on 2018-04-03.
 */
@Api(description = "角色管理接口")
@RestController
@RequestMapping("/role")
public class RoleController {

    @Resource
    private RoleService roleService;
    @Resource
    private AuthorizeService authorizeService;
    @Resource
    private RoleAuthorizeService roleAuthorizeService;

    @Authorize(AuthorizeEnum.ROLE_QUERY)
    @ApiOperation(value = "查询角色信息")
    @GetMapping(value = "/query/{roleId}")
    public ApiResponse queryById(@ApiParam(value = "角色id") @PathVariable String roleId) {
        return new ApiResponse(roleService.queryById(roleId));
    }

    @Authorize(AuthorizeEnum.ROLE_INSERT)
    @ApiOperation(value = "新增角色信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody SysRole roleBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(roleService.insert(roleBean));
    }

    @Authorize(AuthorizeEnum.ROLE_DELETE)
    @ApiOperation(value = "删除角色信息")
    @PostMapping(value = "/delete/{roleId}")
    public ApiResponse delete(@ApiParam("需要删除的角色id") @PathVariable String roleId) {
        return new ApiResponse(roleService.delete(roleId));
    }

    @Authorize(AuthorizeEnum.ROLE_DELETE)
    @ApiOperation(value = "强制删除角色信息")
    @PostMapping(value = "/forceDelete/{roleId}")
    public ApiResponse forceDelete(@ApiParam("需要删除的角色id") @PathVariable String roleId) {
        return new ApiResponse(roleService.forceDelete(roleId));
    }

    @Authorize(AuthorizeEnum.ROLE_UPDATE)
    @ApiOperation(value = "更新角色信息")
    @PostMapping(value = "/update/{roleId}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody SysRole roleBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的角色id") @PathVariable String roleId) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(roleService.update(roleId, roleBean));
    }

    @Authorize(AuthorizeEnum.ROLE_QUERY)
    @ApiOperation(value = "分页查询角色信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody SysRole roleBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(roleService.list(roleBean, pageBean));
    }

    @Authorize(AuthorizeEnum.AUTHORIZE_ROLE_QUERY)
    @ApiOperation(httpMethod = "POST", value = "根据菜单id查询角色相关的权限")
    @PostMapping("/queryRoleMenuAuthorizeInfo/{roleId}/{menuId}")
    public ApiResponse queryRoleMenuAuthorizeInfo(@ApiParam("角色Id") @PathVariable String roleId,
                                                  @ApiParam("菜单id") @PathVariable String menuId) {
        menuId = StringUtils.trimToEmpty(menuId);
        List<SysAuthorize> authorizeInfoBeanList = authorizeService.listAuthorizeByMenuId(menuId);
        List<String> roleHasMenuAuthorize = roleAuthorizeService.listRoleMenuAuthorize(roleId, menuId);
        List<String> roleHasMenu = roleAuthorizeService.listRoleMenu(roleId, menuId);
        return new ApiResponse(new ThreeTupleBean<>(authorizeInfoBeanList, roleHasMenuAuthorize, roleHasMenu));
    }

    @Authorize(AuthorizeEnum.AUTHORIZE_ROLE_UPDATE)
    @ApiOperation(httpMethod = "POST", value = "更新角色相关的权限")
    @RequestMapping(value = "/updateRoleMenuAuthorize/{roleId}", method = {RequestMethod.POST, RequestMethod.OPTIONS})
    public ApiResponse updateRoleMenuAuthorize(@ApiParam("角色Id") @PathVariable String roleId,
                                               @ApiParam("权限信息") @RequestBody FourTupleBean<List<String>, List<String>, List<String>, List<String>> menuAuthorize,
                                               HttpServletRequest request) {
        return new ApiResponse(roleAuthorizeService.updateRoleMenuAuthorize(roleId, menuAuthorize, SsoUtils.getLoginUserId(request)));
    }

}
