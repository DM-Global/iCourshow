package com.ics.cmsadmin.frame.core.service;

import java.util.List;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface TreeDataService<T> {
    /**
     * 默认根节点id
     */
    String DEFAULT_ROOT_ID = "__ROOT__";
    /**
     * 默认节点分隔点
     */
    String DEFAULT_SEPARATOR = "-";

    /**
     * 根据子节点获取父节点
     *
     * @param childrenId 子节点
     * @return 查询结果
     */
    T queryParent(String childrenId);

    /**
     * 根据父节点获取直接子节点
     *
     * @param parentNode 父节点
     * @return 查询结果
     */
    List<T> listDirectChildren(String parentNode);
}
