package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Sandwich on 2018/12/25
 */
@Data
@Component
@ConfigurationProperties(prefix = "app-wxpay")
public class AppWxPayConfig {
    //商户的appId
    public String appId;
    //商户的appSecret
    public String appSecret;
    //商户id
    public String mchId;
    //商户的API安全key
    public String partnerKey;
    //证书路径
    public String certPath;
    //域名
    public String domain;
    //支付加调函数地址
    public String payNotifyUrl;
    //银行卡号和用户名加字的publicKey
    public String publicKey;

}
