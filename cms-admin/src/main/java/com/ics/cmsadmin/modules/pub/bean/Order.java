package com.ics.cmsadmin.modules.pub.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order extends Resource<String> {
    private String userId;
    private String openId;
    private Integer productId;
    private BigDecimal orderAmount;
    private Integer paymentMethod;
    private Integer status;
    private Integer orderType;
    private String userName;
    private String phone;
    private String productName;
    private BigDecimal productPrice;

    @JsonSerialize(using = Date2LongSerializer.class)
    private Date createTime;

    @JsonSerialize(using = Date2LongSerializer.class)
    private Date updateTime;
}
