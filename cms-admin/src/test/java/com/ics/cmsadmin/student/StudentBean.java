package com.ics.cmsadmin.student;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.utils.KeyUtil;
import lombok.Data;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;


/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-03-20 16:40
 */
@Data
public class StudentBean extends SqlBean{

    private String id;
    private String parentId;
    private String username;
    private String realName;
    private String shareRate = "10";
    private String phoneCode = "86";
    private String phone;
    private String agentNo = "0";
    private Date createTime;
    private Date updateTime;
    private String isActive = "1";
    private String appOpenId;

    public static StudentBean newStudent(String parentId, Date date) {
        StudentBean studentBean = new StudentBean();
        studentBean.setId(KeyUtil.genUniqueKey());
        studentBean.setParentId(parentId);
        String name = "游客" + RandomStringUtils.random(4, Constants.LETTER_AND_NUMBER);
        studentBean.setUsername(name);
        studentBean.setRealName(name);
        studentBean.setPhone("1" + RandomStringUtils.random(10, Constants.NUMBER));
        studentBean.setCreateTime(date);
        studentBean.setUpdateTime(date);
        studentBean.setAppOpenId(RandomStringUtils.random(16, Constants.LETTER_AND_NUMBER));
        return studentBean;
    }

    public static StudentBean newStudent(Date date) {
        return newStudent(null, date);
    }

    @Override
    protected String tableName() {
        return "t_student";
    }
}
