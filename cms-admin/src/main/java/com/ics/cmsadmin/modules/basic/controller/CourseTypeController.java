package com.ics.cmsadmin.modules.basic.controller;


import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.modules.basic.bean.CourseTypeBean;
import com.ics.cmsadmin.modules.basic.service.CourseTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Api(description = "课程属性管理接口")
@RestController
@RequestMapping("/courseType")
public class CourseTypeController {

    @Autowired
    private CourseTypeService courseTypeService;

    @Authorize(AuthorizeEnum.COURSE_TYPE_QUERY)
    @ApiOperation(value = "查询课程属性列表")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody CourseTypeBean courseTypeBean,
                            @ApiParam("页码") @PathVariable(required = false) Integer pageNo,
                            @ApiParam("每页条数") @PathVariable(required = false) Integer pageSize) {
        if (pageNo == null || pageSize == null) {
            pageNo = 1;
            pageSize = Integer.MAX_VALUE;
        }
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return ApiResponse.getDefaultResponse(courseTypeService.list(courseTypeBean, pageBean));
    }

    @Authorize(AuthorizeEnum.COURSE_TYPE_INSERT)
    @ApiOperation(value = "新增课程属性")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@RequestBody CourseTypeBean t) {
        return ApiResponse.getDefaultResponse(courseTypeService.insert(t));
    }

    @Authorize(AuthorizeEnum.COURSE_TYPE_UPDATE)
    @ApiOperation(value = "更新课程属性")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@PathVariable String id, @RequestBody CourseTypeBean t) {
        return ApiResponse.getDefaultResponse(courseTypeService.update(id, t));
    }

    @Authorize(AuthorizeEnum.COURSE_TYPE_DELETE)
    @ApiOperation(value = "删除课程属性")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@PathVariable String id) {
        return ApiResponse.getDefaultResponse(courseTypeService.delete(id));
    }
}
