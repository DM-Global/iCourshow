package com.ics.cmsadmin.frame.core.bean;

import lombok.Data;

@Data
public class LoginSearchBean {
    protected String loginUserId;      // 登陆用户id
    protected String loginAgentNo;      // 登陆用户代理编号
    protected String loginOrgId;        // 登陆用户组织id
    protected String loginPostType;     // 登陆用户岗位
}
