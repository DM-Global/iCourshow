package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.modules.basic.bean.TipsBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.util.List;

/**
 * q_tips 服务类
 * Created by lvsw on 2018-09-06.
 */
public interface TipsService {
    /**
     * 根据问题id查询对应的提示
     *
     * @param problemId
     */
    List<TipsBean> queryByProblemId(String problemId);
}
