package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

@Mapper
public interface OrderMapper extends ResourceMapper<Order, String> {

    BigDecimal totalAmount(@Param("productName") String productName,
                           @Param("userName") String userName,
                           @Param("orderType") Integer orderType,
                           @Param("paymentMethod") Integer paymentMethod,
                           @Param("status") Integer status,
                           @Param("orderId") String orderId);

    Order getUserOrder(@Param("userId") String userId,
                       @Param("productId") Integer productId);
}
