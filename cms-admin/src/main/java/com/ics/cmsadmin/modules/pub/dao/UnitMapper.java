package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Unit;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UnitMapper extends ResourceMapper<Unit, Integer> {
    //List<Unit> selectAll(@Param("name") String name, @Param("subjectId") Integer subjectId, @Param("courseId") Integer courseId, @Param("isActive") Boolean isActive);
}
