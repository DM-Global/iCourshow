package com.ics.cmsadmin.modules.job;

import com.ics.cmsadmin.modules.agent.service.AgentDaySummaryService;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Log4j2
@Component
public class AgentStatisticsService {

    @Resource
    private AgentDaySummaryService agentDaySummaryService;

    @Scheduled(cron = "0 10 0 * * *")
    public synchronized void runDaySummary() {
        long startTime = System.currentTimeMillis();
        log.info("=====>>>>>开始执行每天汇总任务");
        agentDaySummaryService.daySummary();
        log.info("=====>>>>>结束执行每天汇总任务,耗时: {}", System.currentTimeMillis() - startTime);
    }
}
