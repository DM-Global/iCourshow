package com.ics.cmsadmin.modules.sso.handler;

import com.ics.cmsadmin.frame.constant.RedisConstants;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.app.bean.AppInfoBean;
import com.ics.cmsadmin.modules.app.utils.AppContants;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.sso.LoginHandler;
import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoContants;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by 666666 on 2018/8/25.
 */
@Component
public class AppMobileAndPasswordLoginHandler implements LoginHandler<StudentBean> {

    @Resource(name = "newStudentService")
    private StudentService studentService;

    @Override
    public StudentBean dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum) {
        String mobilePhone = request.getParameter(SsoContants.LOGIN_MOBILE_PHONE);
        String password = request.getParameter(SsoContants.LOGIN_PASSWORD);

        if (StringUtils.isBlank(mobilePhone) || StringUtils.isBlank(password)) {
            throw new CmsException(ApiResultEnum.USER_PASSWORD_ERROR);
        }

        // 判断用户是否被锁定
        if (RedisConstants.isUserLoginLock(mobilePhone)) {
            throw new CmsException(ApiResultEnum.LOGIN_USER_IS_LOCK);
        }

        StudentBean studentBean = studentService.findByMobilePhoneAndPassword(mobilePhone, password);
        if (studentBean == null) {
            RedisConstants.updateLoginErrorCount(mobilePhone);
            throw new CmsException(ApiResultEnum.USER_PASSWORD_ERROR);
        }else {
            AppInfoBean appInfoBean = SsoUtils.getAppInfo(request);
            if (null != appInfoBean && null != appInfoBean.getUMengDeviceToken()){
                if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_IOS)){
                    studentBean.setIosDeviceToken(appInfoBean.uMengDeviceToken);
                }else if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_ANDROID)){
                    studentBean.setAndroidDeviceToken(appInfoBean.uMengDeviceToken);
                }
                studentService.update(studentBean.getId(),studentBean);
            }
        }
        studentBean.setHasPassword(true);
        studentBean.setShareUrl(AppUtils.getStudentShareUrl(studentBean.getId()));
        SsoUtils.saveLoginInfo2Redis(loginTypeEnum, studentBean, 30 * 24 * 3600L);
        return studentBean;
    }

}
