package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.basic.service.VideoHistoryService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Api(description = "视频观看记录接口", tags = "公众号")
@RestController
@RequestMapping("/api/videoHistory")
public class ApiVideoHistoryController {

    @Autowired
    private VideoHistoryService videoHistoryService;

    @GetMapping("/list")
    public ApiResponse listByUserId(HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isEmpty(userId)) {
            return ApiResponse.getDefaultResponse(new ArrayList<>());
        }
        return ApiResponse.getDefaultResponse(videoHistoryService.listByUserId(userId));
    }

    @GetMapping("/{videoId}")
    public ApiResponse queryOne(@PathVariable String videoId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        VideoHistoryBean videoHistoryBean = videoHistoryService.findOne(userId, videoId);
        return ApiResponse.getDefaultResponse(videoHistoryBean);
    }

    @PostMapping("/save")
    public ApiResponse save(@RequestBody VideoHistoryBean videoHistoryBean, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        videoHistoryBean.setUserId(userId);
        videoHistoryService.save(videoHistoryBean);
        return ApiResponse.getDefaultResponse();
    }

    @GetMapping("/queryByCourseId/{courseId}")
    public ApiResponse queryByCourseId(@PathVariable String courseId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return ApiResponse.getDefaultResponse(videoHistoryService.findByCourseId(userId, courseId));
    }
}
