package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.VideoBean;

import java.util.Map;

public interface VideoService {

    PageResult list(VideoBean t, PageBean page);

    boolean insert(VideoBean t);

    boolean update(String id, VideoBean t);

    boolean delete(String id);

    Map<String, String> polyvAuth();

    /**
     * 根据知识点id获取video数组
     *
     * @param userId  登陆用户
     * @param pointId 知识点id
     * @return
     */
    VideoBean getVideoByPointId(String userId, String pointId);
}
