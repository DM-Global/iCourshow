package com.ics.cmsadmin.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Async
public class AsyncService {

    public void test1() {
        System.out.println(Thread.currentThread().getName() + " test1 ");

    }
    public void test2() {
        test1();
        System.out.println(Thread.currentThread().getName() + " test2 ");

    }
}
