package com.ics.cmsadmin.modules.app.controller;

import com.jpay.alipay.AliPayApiConfig;

public abstract class AliPayApiController {
    public abstract AliPayApiConfig getApiConfig();
}
