package com.ics.cmsadmin.modules.auth.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * Created by lvsw on 2018/8/26.
 */
@Data
@Builder
public class SysAuthorize {
    private String authCode;
    private String group;
    private String menuId;
    private String type;
    private String description;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;

    @Tolerate
    public SysAuthorize() {

    }
}
