package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.bean.StudyHistory;
import com.ics.cmsadmin.modules.basic.service.PointService;
import com.ics.cmsadmin.modules.basic.service.StudyHistoryService;
import com.ics.cmsadmin.modules.pub.dao.StudyHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudyHistoryServiceImpl implements StudyHistoryService {

    @Resource
    private StudyHistoryMapper studyHistoryMapper;

    @Autowired
    private PointService pointService;

    @Override
    public List<PointBean> getRecentStudy(StudyHistory studyHistory, PageBean page) {
        studyHistory.setLevelName(LevelEnum.point);
        List<StudyHistory> result = studyHistoryMapper.list(studyHistory, page);
        return result.stream().map(bean -> pointService.pointDetail(bean.getUserId(), bean.getLevelId())).collect(Collectors.toList());
    }

    @Override
    public int insert(StudyHistory studyHistory) {
        return studyHistoryMapper.insert(studyHistory);
    }

    @Override
    public void markFinish(StudyHistory studyHistory) {
        studyHistoryMapper.insert(studyHistory);
        PointBean pointBean = pointService.queryById(studyHistory.getLevelId());
        long pointCount = pointService.count(PointBean.builder().topicId(pointBean.getTopicId()).build());

    }

    @Override
    public StudyHistory findOne(StudyHistory studyHistory) {
        return studyHistoryMapper.findOne(studyHistory);
    }
}
