package com.ics.cmsadmin.student;

import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.student.service.StudentDayService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2018-02-22 16:12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class TestShareMain {
    @Resource
    private ShareDetailService shareDetailService;
    @Resource
    private StudentDayService studentDayService;
    @Resource
    private JdbcTemplate jdbcTemplate;
    //    private static Map<String, Date> studentOrder = StudentMap.getStudentMap();
    private static Map<String, Date> studentOrder = new HashMap<>();
    private static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void test1() {
        studentDayService.dayStatistics();
    }

    @Test
    public void test() throws Exception{
        Date startDate = yyyyMMdd.parse("2019-06-14");
        Date endDate = yyyyMMdd.parse("2019-06-22");
        studentDayService.dayStatistics();
        while (startDate.before(endDate)) {
            int maxSeconde = Contants.minutes * 60 / Contants.minutesIndex;
            for (int seconds = RandomUtils.nextInt(0, maxSeconde); seconds < Contants.minutes * 60; seconds = seconds + RandomUtils.nextInt(0, maxSeconde)){
                Date nextDate = DateUtils.addSeconds(startDate, seconds);
                changeCurrentDate(startDate, nextDate);
                makeDate(nextDate);
            }
            Date nextDate = nextDate(startDate);
            changeCurrentDate(startDate, nextDate);
            startDate = nextDate;
        }
    }

    private void changeCurrentDate(Date oldDate, Date newDate) throws Exception {
        String cmd;
        // 格式：yyyy-MM-dd
        cmd = " cmd /c date " + DateFormatUtils.format(newDate, "yyyy-MM-dd");
        Process exec = Runtime.getRuntime().exec(cmd);
        System.out.println(IOUtils.readLines(exec.getInputStream()));
        // 格式 HH:mm:ss
        cmd = " cmd /c time " + DateFormatUtils.format(newDate, "HH:mm:ss");
        Runtime.getRuntime().exec(cmd);
        Thread.sleep(500);
        System.out.println(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        String format1 = DateFormatUtils.format(oldDate, "yyyy-MM-dd");
        String format2 = DateFormatUtils.format(newDate, "yyyy-MM-dd");
        if (!StringUtils.equalsIgnoreCase(format1, format2)) {
            studentDayService.dayStatistics();
        }
    }

    private void makeDate(Date startDate) {
        EventEnums eventEnums = EventEnums.radomEvent();
        System.out.println("当前状态: " + eventEnums);
        switch (eventEnums) {
            case NEW_STUDENT:
                newStudent(startDate);
                break;
            case PROMOTION_STUDENT:
                promotionStudent(startDate);
                break;
            case NEW_ORDER:
                newOrder(startDate);
                break;
            default:
                break;
        }
    }

    private void newOrder(Date startDate) {
        shuffle(studentOrder.keySet()).stream()
            .limit(RandomUtils.nextLong(Contants.MIN_NEW_ORDER_STUDENY, Contants.MAX_NEW_ORDER_STUDENY))
            .forEach(studentId -> {
                List<OrderBean> collect = Stream.iterate(1, item -> item + 1)
                    .limit(RandomUtils.nextLong(Contants.MIN_NEW_ORDER, Contants.MAX_NEW_ORDER))
                    .map(item -> {
                        OrderBean orderBean = OrderBean.newOrder(studentId, startDate, studentOrder.get(studentId));
                        studentOrder.put(studentId, orderBean.getEndDate());
                        return orderBean;
                    })
                    .collect(Collectors.toList());
                executSql(OrderBean.class, collect);
                collect.forEach(item -> shareDetailService.generateShareByOrderId(item.getId()));
            });
    }

    private void newStudent(Date startDate) {
        List<StudentBean> collect = Stream.iterate(1, item -> item + 1)
            .limit(RandomUtils.nextLong(Contants.MIN_NEW_STUDENT, Contants.MAX_NEW_STUDENT))
            .map(item -> {
                StudentBean studentBean = StudentBean.newStudent(startDate);
                studentOrder.put(studentBean.getId(), null);
                return studentBean;
            })
            .collect(Collectors.toList());
        executSql(StudentBean.class, collect);
    }

    private void promotionStudent(Date startDate) {
        List<StudentBean> collect = shuffle(studentOrder.keySet()).stream()
            .limit(RandomUtils.nextLong(Contants.MIN_PROMOTION_STUDENT, Contants.MAX_PROMOTION_STUDENT))
            .map(item -> {
                StudentBean studentBean = StudentBean.newStudent(item, startDate);
                studentOrder.put(studentBean.getId(), null);
                return studentBean;
            })
            .collect(Collectors.toList());
        executSql(StudentBean.class, collect);
    }

    private void executSql(Class<? extends SqlBean> clazz, List<? extends SqlBean> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(SqlBean.insertSqlPrefix(clazz));
        list.stream().forEach(item -> sb.append(item.insertSqlSuffix()));
        int index = sb.lastIndexOf(",");
        sb.replace(index, index + 1, ";");
        jdbcTemplate.batchUpdate(sb.toString());
    }

    private Date nextDate(Date startDate) {
        return DateUtils.addMinutes(startDate, Contants.minutes);
    }

    private List<String> shuffle(Set<String> list){
        List<String> result = new ArrayList<>(list);
        Random random = new Random(System.currentTimeMillis());
        Collections.shuffle(result, random);
        return result;
    }

}
