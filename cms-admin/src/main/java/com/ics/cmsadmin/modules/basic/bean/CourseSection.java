package com.ics.cmsadmin.modules.basic.bean;

import lombok.Data;

import java.util.List;

@Data
public class CourseSection {

    private Header header;

    private List<CourseBean> body;

    @Data
    public static class Header {

        private String title;

        private String description;
    }
}
