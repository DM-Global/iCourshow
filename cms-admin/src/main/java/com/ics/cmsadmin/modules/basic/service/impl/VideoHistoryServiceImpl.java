package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.app.utils.SharePosterUtils;
import com.ics.cmsadmin.modules.basic.bean.VideoBean;
import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.basic.bean.VideoSumBean;
import com.ics.cmsadmin.modules.basic.dao.VideoDao;
import com.ics.cmsadmin.modules.basic.dao.VideoHistoryDao;
import com.ics.cmsadmin.modules.basic.service.PointService;
import com.ics.cmsadmin.modules.basic.service.VideoHistoryService;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.bean.StudentCertificateBean;
import com.ics.cmsadmin.modules.student.bean.StudentShareRecordBean;
import com.ics.cmsadmin.modules.student.dao.StudentShareRecordDao;
import com.ics.cmsadmin.modules.student.service.StudentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class VideoHistoryServiceImpl implements VideoHistoryService {

    @Resource
    private VideoHistoryDao videoHistoryDao;

    @Autowired
    private StudentService studentService;
    @Resource
    private PointService pointService;
    @Resource
    private StudentShareRecordDao studentShareRecordDao;
    @Resource
    private VideoDao videoDao;


    @Override
    public VideoHistoryBean findOne(String userId, String videoId) {
        return videoHistoryDao.findOne(userId, videoId);
    }

    @Override
    public VideoHistoryBean findByCourseId(String userId, String courseId) {
        if (StringUtils.isBlank(userId)) {
            throw new CmsException(ApiResultEnum.UN_LOGIN);
        }
        if (studentService.queryById(userId) == null) {
            throw new CmsException(ApiResultEnum.USER_NOT_EXIST);
        }
        return videoHistoryDao.findByCourseId(userId, courseId);
    }

    @Override
    public boolean save(VideoHistoryBean videoHistoryBean) {
        VideoHistoryBean existBean = videoHistoryDao.findOne(videoHistoryBean.getUserId(), videoHistoryBean.getVideoId());
        if (existBean != null) {
            return videoHistoryDao.update(existBean.getId(), videoHistoryBean) == 1;
        } else {
            return videoHistoryDao.insert(videoHistoryBean) == 1;
        }
    }

    @Override
    public StudentCertificateBean save4app(VideoHistoryBean historyBean) {
        if (historyBean == null || StringUtils.isBlank(historyBean.getUserId()) || StringUtils.isBlank(historyBean.getVideoId())) {
            return null;
        }
        videoHistoryDao.merge(historyBean);
        VideoHistoryBean existBean = videoHistoryDao.findOne(historyBean.getUserId(), historyBean.getVideoId());
        if (existBean != null &&
            StringUtils.isNotBlank(existBean.getPointId()) &&
            existBean.getDuration() > 0) {
            // 观看进度
            double progress = existBean.getTotalTime() * 1.0 / existBean.getDuration();
            if (progress > 0.5) {
                pointService.finish(historyBean.getUserId(), existBean.getPointId());
            }
        }
        StudentBean studentBean = studentService.queryById(historyBean.getUserId());
        if (studentBean == null) {
            return null;
        }
        // 先判断是否满足1个小时
        StudentCertificateBean studentCertificateBean = checkUserIfFullHour(historyBean, studentBean);
        if (studentCertificateBean != null) {
            return studentCertificateBean;
        }
        return checkUserIfCompleteCourse(historyBean, studentBean);
    }

    /**
     * 检查是否完成某课程
     * @param historyBean
     * @return
     */
    private StudentCertificateBean checkUserIfCompleteCourse(VideoHistoryBean historyBean,StudentBean studentBean) {
        VideoBean videoBean = videoDao.queryById(historyBean.getVideoId());
        StudentShareRecordBean shareRecordBean =
            studentShareRecordDao.findOne(StudentShareRecordBean.builder()
            .shareType(CommonEnums.ShareRecordType.COMPLETE_COURSE.getCode())
            .studentId(historyBean.getUserId())
            .scoreId(videoBean.getCourseId())
            .build());
        // 如果用户已经分享过,者跳过检查
        if (shareRecordBean != null && StringUtils.equalsIgnoreCase(shareRecordBean.getShareStatus(), "1")) {
            return null;
        }
        VideoSumBean videoSumBean = videoDao.statisticByCourse(videoBean.getCourseId());
        if (videoSumBean == null) {
            return null;
        }
        VideoSumBean videoHistorySumBean = videoHistoryDao.statisticByCourse(historyBean.getUserId(), videoBean.getCourseId());
        if (videoHistorySumBean == null) {
            return null;
        }
        // 如果所有的视频已经观看过,切观看的时间大于总视频时间的60%,则弹出完成课程海报
        if (videoHistorySumBean.getCountOfVideo() >= videoSumBean.getCountOfVideo()  &&
            videoHistorySumBean.getSumOfVideoTime() >= videoSumBean.getSumOfVideoTime() * 1000 * 0.6){
            if (shareRecordBean == null) {
                studentShareRecordDao.insert(StudentShareRecordBean.builder()
                    .shareType(CommonEnums.ShareRecordType.COMPLETE_COURSE.getCode())
                    .studentId(historyBean.getUserId())
                    .shareStatus("1")
                    .scoreId(videoBean.getCourseId())
                    .build());
            }else {
                studentShareRecordDao.update(shareRecordBean.getId(), StudentShareRecordBean.builder().shareStatus("1").build());
            }
            String userName = studentBean.getNickname();
            if (StringUtils.isBlank(userName)) {
                userName = studentBean.getUsername();
            }
            return SharePosterUtils.getCourseCompletion(userName, studentBean.getId(), videoBean.getCourseName(), videoBean.getCourseId());
        }
        return null;
    }

    /**
     * 检查用户时候达到分享 学满1,3,5,7...学时的条件
     * @param historyBean
     * @return
     */
    private StudentCertificateBean checkUserIfFullHour(VideoHistoryBean historyBean, StudentBean studentBean) {
        StudentShareRecordBean shareRecord = studentShareRecordDao.findOne(StudentShareRecordBean.builder()
            .studentId(historyBean.getUserId())
            .shareType(CommonEnums.ShareRecordType.FULL_HOUR.getCode())
            .build());
        long allStudyTime = videoHistoryDao.sumOfStudyTime(historyBean.getUserId());
        long requiredStudyTime = Optional.ofNullable(shareRecord)
            .map(StudentShareRecordBean::getStudyTime)
            .map(Long::valueOf).orElse(1L);
        // 如果没有达到要求的学习时间，则不弹出海报
        if (allStudyTime < requiredStudyTime * 3600L * 1000L) {
            return null;
        }
        // 返回相应的海报
        String userName = studentBean.getNickname();
        if (StringUtils.isBlank(userName)) {
            userName = studentBean.getUsername();
        }
        StudentCertificateBean hoursAchievementAward = SharePosterUtils.getHoursAchievementAward(userName, studentBean.getId(), requiredStudyTime);
        if (hoursAchievementAward == null) {
            return null;
        }
        // 向上取整目前已经达到的学习时间，
        requiredStudyTime = allStudyTime / 3600 / 1000 + 1;
        // 如果是偶数，则+1变成奇数
        if (requiredStudyTime % 2 == 0) {
            requiredStudyTime += 1;
        }
        // 插入或更新分享记录
        if (shareRecord == null) {
            studentShareRecordDao.insert(StudentShareRecordBean.builder()
                .studentId(historyBean.getUserId())
                .shareType(CommonEnums.ShareRecordType.FULL_HOUR.getCode())
                .studyTime(requiredStudyTime + "")
                .build());
        }else {
            studentShareRecordDao.update(shareRecord.getId(),
                StudentShareRecordBean.builder().studyTime(requiredStudyTime + "").build());
        }
        return hoursAchievementAward;
    }

    /**
     * 获取最近学习视频5个
     *
     * @param userId
     * @return
     */
    @Override
    public List<VideoHistoryBean> listByUserId(String userId) {
        return videoHistoryDao.list(VideoHistoryBean.builder().userId(userId).build(),
            new PageBean(1, 5));
    }

    @Override
    public PageResult list(VideoHistoryBean videoHistoryBean, PageBean page) {
        long count = videoHistoryDao.count(videoHistoryBean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, videoHistoryDao.list(videoHistoryBean, page));
    }

    @Override
    public VideoHistoryBean queryById(String id) {
        return videoHistoryDao.queryById(id);
    }

    @Override
    public boolean insert(VideoHistoryBean videoHistoryBean) {
        return videoHistoryDao.insert(videoHistoryBean) == 1;
    }

    @Override
    public boolean update(String id, VideoHistoryBean videoHistoryBean) {
        return videoHistoryDao.update(id, videoHistoryBean) == 1;
    }

    @Override
    public boolean delete(String id) {
        return videoHistoryDao.delete(id) == 1;
    }
}
