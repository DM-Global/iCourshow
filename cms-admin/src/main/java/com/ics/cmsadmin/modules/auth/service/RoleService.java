package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.auth.bean.SysRole;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.util.List;

/**
 * a_role 服务类
 * Created by lvsw on 2018-03-29.
 */
public interface RoleService extends BaseDataService<SysRole>, BaseService<SysRole> {
    /**
     * 强制删除角色(不管该角色是否被其他用户所拥有)
     */
    boolean forceDelete(String roleId);

    /**
     * 查询属于用户的角色信息
     */
    List<SysRole> listUserRoleByUserId(String userId);

    /**
     * 查询不属于用户的角色信息
     */
    List<SysRole> listNotBelong2UserRoles(String userId);
}
