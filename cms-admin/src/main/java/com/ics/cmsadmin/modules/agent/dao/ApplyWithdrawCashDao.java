package com.ics.cmsadmin.modules.agent.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.agent.bean.ApplyWithdrawCashBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ApplyWithdrawCashDao extends BaseDataDao<ApplyWithdrawCashBean> {
    /**
     * 拒绝代理商提现申请
     *
     * @param applyId 申请id
     * @param status  处理状态
     * @param remark  拒绝理由
     * @return
     */
    int update(@Param("applyId") String applyId,
               @Param("status") String status,
               @Param("remark") String remark);

    /**
     * 申请提现记录
     */
    int applyWithdrawCash(@Param("bean") ApplyWithdrawCashBean bean);

    /**
     * 根据id获取申请记录
     */
    ApplyWithdrawCashBean queryById(@Param("id") String id);
}
