package com.ics.cmsadmin.postman;

import com.ics.cmsadmin.frame.utils.GsonUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MainRun {
    @Resource
    private ApplicationContext context;

    @Test
    public void test() throws Exception {
        System.out.println(context);
        System.out.println("=====================");
        String[] beanNamesForAnnotation = context.getBeanNamesForAnnotation(Controller.class);
        List<PostManDomain> collect = Arrays.stream(beanNamesForAnnotation)
            .filter(item -> item.startsWith("app"))
            .map(item -> context.getBean(item))
            .map(item -> buildPostMainDomains(item))
            .collect(Collectors.toList());
        String s = GsonUtils.toJson(collect);
        System.out.println("===> \n" + s);
        System.out.println("=====================");

    }

    private PostManDomain buildPostMainDomains(Object object) {
        Method[] declaredMethods = object.getClass().getDeclaredMethods();
        Api annotation = AnnotationUtils.getAnnotation(object.getClass(), Api.class);
        List<PostManItemDomain> collect = Arrays.stream(declaredMethods)
            .filter(item -> AnnotationUtils.getAnnotation(item, RequestMapping.class) != null)
            .map(item -> buildPostMainDomain(object, item))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
        return PostManDomain.builder()
            .name(annotation == null ? object.getClass().getSimpleName() : annotation.description())
            .item(collect).build();
    }

    private PostManItemDomain buildPostMainDomain(Object object, Method item) {

        try {
            System.out.println("=======>" + object.getClass());
            RequestMapping clazzRequestMapping = AnnotationUtils.getAnnotation(object.getClass(), RequestMapping.class);
            RequestMapping methodRequestMapping = AnnotationUtils.getAnnotation(item, RequestMapping.class);
            PostMapping postMapping = AnnotationUtils.getAnnotation(item, PostMapping.class);
            GetMapping getMapping = AnnotationUtils.getAnnotation(item, GetMapping.class);
            ApiOperation methodApiOperation = item.getAnnotation(ApiOperation.class);
            Optional<Parameter> firstParam = Arrays.stream(item.getParameters())
                .filter(param -> param.getAnnotation(RequestBody.class) != null)
                .findFirst();
            String name = methodApiOperation == null ? item.getName() : methodApiOperation.value();
            String method = methodRequestMapping.method()[0].name();
            PostManHeader header = method.equals("POST") ?
                PostManHeader.builder().key("Content-Type").value("application/json").type("text").build() :
                PostManHeader.builder().build();
            PostManRequestBody body = null;
            if (firstParam.isPresent()) {
                body = PostManRequestBody.builder()
                    .mode("raw")
                    .raw(GsonUtils.toJson(GsonUtils.toJson(firstParam.get().getType().newInstance())))
                    .build();
            }
            if (body == null) {
                body = PostManRequestBody.builder().build();
            }
            String path = clazzRequestMapping.value()[0] + (method.equals("POST") ? postMapping.value()[0] : getMapping.value()[0]);
            PostManRequestUrl url = PostManRequestUrl.builder()
                .raw("{{baseUrl}}" + path + "?LOGIN_TOKEN={{loginToken}}")
                .host(Arrays.asList("{{baseUrl}}"))
                .path(path.split("/"))
                .query(Arrays.asList(PostManRequestQuery.builder()
                    .key("LOGIN_TOKEN")
                    .value("{{loginToken}}")
                    .build()))
                .build();

            return PostManItemDomain.builder()
                .name(name)
                .request(PostManRequest.builder()
                    .method(method)
                    .header(Arrays.asList(header))
                    .body(body)
                    .url(url)
                    .build())
                .response(new ArrayList<>())
                .build();
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }
}

