package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.TeacherBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * t_teacher
 * Created by lvsw on 2018-14-25 14:09:44.
 */
@Repository
@Mapper
public interface TeacherDao extends BaseDao<TeacherBean>, BaseDataDao<TeacherBean> {

    List<String> findSubjectsById(String id);
}
