package com.ics.cmsadmin.modules.agent.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.agent.bean.AgentShareRuleBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * a_agent_share_rule
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Repository
@Mapper
public interface AgentShareRuleDao {
    /**
     * 根据主键id查询bean
     *
     * @param id 主键id
     * @return config
     */
    AgentShareRuleBean queryById(@Param("id") String id);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") AgentShareRuleBean t);

    /**
     * 根据id更新数据
     *
     * @param t 更新数据
     * @return 影响条数
     */
    int update(@Param("id") String id, @Param("bean") AgentShareRuleBean t);

    /**
     * @param t
     * @return
     */
    AgentShareRuleBean findOne(@Param("bean") AgentShareRuleBean t);
}
