package com.ics.cmsadmin.frame.profit;

import com.ics.cmsadmin.frame.profit.role.ProfitRole;

/**
 * 分润规则工具类
 */
public final class ProfitUtils {
    /**
     * 获取分润计算规则
     *
     * @param profitType 分润类型
     * @param expression 分润计算表达式
     */
    public static ProfitRole getProfitRole(String profitType, String expression) {
        return getProfitRole(ProfitTypeEnum.valueOf(profitType), expression);
    }

    /**
     * 获取分润计算规则
     *
     * @param profitType 分润枚举类型
     * @param expression 分润计算表达式
     */
    public static ProfitRole getProfitRole(ProfitTypeEnum profitType, String expression) {
        return profitType.getProfitRole(expression);
    }
}
