package com.ics.cmsadmin.modules.sso.utils;

import com.ics.cmsadmin.modules.auth.bean.SysUser;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by 666666 on 2018/8/25.
 */
@AllArgsConstructor
public enum LoginTypeEnum {
    /**
     * cms-web 前端登陆
     */
    CMS_ADMIN_WEB(SysUser.class),
    /**
     * 公众号微信登陆
     */
    WECHAT_CODE_LOGIN(StudentBean.class),
    /**
     * app 手机密码登陆
     */
    APP_MOBILE_AND_PASSWORD(StudentBean.class),
    /**
     * app 手机验证码登陆
     */
    APP_MOBILE_AND_CODE(StudentBean.class),
    /**
     * app 微信登陆
     */
    APP_WECHAT_LOGIN(StudentBean.class),
    /**
     * 游客登录
     */
    TOURIST_LOGIN(StudentBean.class),
    /**
     * 注销登陆
     */
    LOGOUT(LoginInfo.class);

    @Getter
    private Class<? extends LoginInfo> userInfoClass;

    public static LoginTypeEnum valueOfType(String type) {
        try {
            return LoginTypeEnum.valueOf(type);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

}
