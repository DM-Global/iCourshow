package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.system.bean.UserOperLogBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * com_user_oper_log
 * Created by lvsw on 2018-41-11 15:10:21.
 */
@Repository
@Mapper
public interface UserOperLogDao {
    /**
     * 根据查询条件汇总
     *
     * @param t 查询条件
     * @return 查询结果
     */
    long count(@Param("bean") UserOperLogBean t);

    /**
     * 根据查询条件查询列表
     *
     * @param t 查询条件
     * @return 查询结果
     */
    List<UserOperLogBean> list(@Param("bean") UserOperLogBean t, @Param("page") PageBean page);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") UserOperLogBean t);
}
