package com.ics.cmsadmin.modules.auth.bean;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;
import java.util.List;


/**
 * sys_organization
 * 组织
 * <p>
 * Created by lvsw on 2018-29-21 10:10:05.
 */
@Data
@Builder
public class OrganizationBean extends LoginSearchBean {

    private String orgId;              // 组织id
    private String parentId;              // 父级id
    private String parentNode;              // 父级节点
    private String orgNode;              // 组织节点
    private String name;              // 组织名
    private String description;              // 描述
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间

    private List<OrganizationBean> children;

    private List<SysUser> sysUsers;

    @Tolerate
    public OrganizationBean() {
    }
}
