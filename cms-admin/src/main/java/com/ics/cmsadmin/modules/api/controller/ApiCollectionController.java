package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.basic.bean.Collect;
import com.ics.cmsadmin.modules.basic.service.CollectionService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

@Api(description = "收藏接口")
@RestController
@RequestMapping("/api/collection")
@Slf4j
public class ApiCollectionController {
    @Resource
    private CollectionService collectionService;

    @ApiOperation("查询所有收藏")
    @GetMapping("/list")
    public ApiResponse list(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        log.info("loginUserId = {}", loginUserId);
        if (StringUtils.isBlank(loginUserId)) {
            return ApiResponse.getDefaultResponse(new ArrayList<>());
        }
        return ApiResponse.getDefaultResponse(collectionService.getCollectionList(loginUserId));
    }

    @ApiOperation("开启/取消收藏")
    @PostMapping("/toggle")
    public ApiResponse toggle(@RequestBody Collect collect, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            throw new CmsException(ApiResultEnum.UN_LOGIN);
        }
        collect.setUserId(loginUserId);
        collectionService.toggleCollect(collect);
        return ApiResponse.getDefaultResponse();
    }

}
