package com.ics.cmsadmin.modules.system.emums;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.utils.Tools;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Predicate;

import static com.ics.cmsadmin.frame.constant.Constants.*;
import static com.ics.cmsadmin.frame.utils.Tools.*;

@AllArgsConstructor
@Getter
public enum RegisterKeyValueEnum {
    /**
     * 推广学生获取的积分数
     */
    PROMOTION_STUDENT_SCORE("promotion_student_score", "20", INTEGER_PREDICATE, "该配置必须为正整数"),
    /**
     * 下级推广订单
     * 一级代理商可以得到分润比例
     */
    ONE_AGENT_SHARE_RATE("one_agent_share_rate", "0",
        Between0To100_PREDICATE, "该配置必须为数字且在0~100之间."),
//    /**
//     * 代理商的分润比例
//     */
//    AGENT_SHARE_RATE("agent_share_rate", Constants.NOT_KNOWN,
//        AGENT_SHARE_RATE_PREDICATE, "该配置有误,请参考'10%<100<15%<300<20%<600<25%'"),
    /**
     * 个人代理商的分润比例
     */
    PERSONAL_AGENT_SHARE_RATE("personal_agent_share_rate", "15%<101<20%<301<25%<601<30%",
        GRADIENT_SHARE_RATE_PREDICATE, "该配置有误,请参考'15%<101<20%<301<25%<601<30%'"),
    /**
     * 企业代理商的分润比例
     */
    COMPANY_AGENT_SHARE_RATE("company_agent_share_rate", "20%<101<25%<301<30%<601<35%",
        GRADIENT_SHARE_RATE_PREDICATE, "该配置有误,请参考'20%<101<25%<301<30%<601<35%'"),
    /**
     * 最多的提现申请记录数
     * (提现申请状态为"提现中"的数量)
     * 如果分销商的提现申请大于这个数量,这不能提现
     * -1 表示没有限制
     */
    APPLY_WITHDRAW_CASH_MAX_COUNT("apply_withdraw_cash_max_count", "-1",
        INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 提现间隔时间(d)
     * -1 表示没有限制
     *
     * @see #WITHDRAW_CASH_INTERVAL_MAX_ACCEPT_COUNT
     */
    APPLY_WITHDRAW_CASH_INTERVAL_DAY("apply_withdraw_cash_interval_day", "-1",
        INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 提现间隔时间中 接受处理的最大申请次数
     * 也就说每隔多久,最多可以申请多少次提现记录
     * -1 表示没有限制
     *
     * @see #APPLY_WITHDRAW_CASH_INTERVAL_DAY
     */
    WITHDRAW_CASH_INTERVAL_MAX_ACCEPT_COUNT("withdraw_cash_interval_max_accept_count", "-1",
        INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 申请提现金额最小提现金额
     */
    APPLY_WITHDRAW_CASH_MIN_MONEY("apply_withdraw_cash_min_money", "0",
        NUMERIC_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 新增代理商时的默认用户角色
     */
    AGENT_DEFAULT_ROLE_ID("agent_default_role_id", Constants.NOT_KNOWN),
    /**
     * 新增业务员时的默认用户角色(普通用户的角色)
     */
    USER_DEFAULT_ROLE_ID("user_default_role_id", Constants.NOT_KNOWN),
    /**
     * 助理的默认角色
     */
    ASSISTANT_DEFAULT_ROLE_ID("assistant_default_role_id", Constants.NOT_KNOWN),
    /**
     * 经理的默认角色
     */
    MANAGER_DEFAULT_ROLE_ID("manager_default_role_id", Constants.NOT_KNOWN),
    /**
     * boss的默认角色
     */
    BOSS_DEFAULT_ROLE_ID("boss_default_role_id", Constants.NOT_KNOWN),
    /**
     * 导出数据的最大限制
     */
    EXPORT_MAX_NUM("export_max_num", "5000",
        INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 邮箱重置密码url路径
     */
    EMAIL_RESET_PASSWORD_URL("email_reset_password_url", ""),
    /**
     * 登陆错误最大次数
     */
    LOGIN_ERROR_MAX_COUNT("login_error_max_count", "5", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 登陆错误,用户锁定时间(分钟)
     */
    LOGIN_ERROR_USER_LOCK_TIME("Login_error_user_lock_time", "30", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 系统是否为debugger模式,填true的话为debugger模式,其余为非debugger模式
     */
    SYSTEM_IS_DEBUGGER_MODE("system_is_debugger_mode", "false", Tools.isMatch("true|false"), "只能填写true或者false"),
    /**
     * 随便看看开关
     */
    LOOK_AROUND_SWITCH("look_around_switch", "false", Tools.isMatch("true|false"), "只能填写true或者false"),

    /**
     * 游客是否能创建订单开关
     */
    TOURIST_ORDER_SWITCH("is_tourist_create_order_available", "false", Tools.isMatch("true|false"), "只能填写true或者false"),

    /**
     * 是否使用分销测试支付流程
     */
    IS_AGENT_TEST_MODE_AVAILABLE("is_agent_test_mode_available", "false", Tools.isMatch("true|false"), "只能填写true或者false"),

    /**
     * 全局价格
     */
    GLOBAL_PRICE_DISCOUNT("global_price_discount", "1", Zero_To_One_PREDICATE, NOT_CORRECT_DISCOUNT_TIPS),

    /**
     * vip订单价格
     */
    GLOBAL_VIP_PRICE("vip_price", "1.0", NUMERIC_PREDICATE, NOT_CORRECT_PRICE_FORMATE_TIPS),
    /**
     * 订单活动海报
     */
    ORDER_POSTER_IMAGE("order_poster_image", "http://ics-app.oss-cn-beijing.aliyuncs.com/image/ic_vip_entrance.png", TRUE_PREDICATE, ""),
    /**
     * 未登陆用户看到活动海报页面
     */
    UNLOGIN_ENTRANCE("unlogin_entrance", "http://ics-app.oss-cn-beijing.aliyuncs.com/image/unlogin_entrance.png"),
    /**
     * 登陆用户且已经领取免费学籍的用户看到的海报页面
     */
    LOGIN_ENTRANCE("login_entrance", "http://ics-app.oss-cn-beijing.aliyuncs.com/image/login_entrance.png"),
    /**
     * 登陆用户且还没有领取免费学籍的用户看到的海报页面
     */
    LOGIN_OLD_USER_ENTRANCE("login_old_user_entrance", "http://ics-app.oss-cn-beijing.aliyuncs.com/image/login_old_user_entrance.png"),
    /**
     * 首页科目模块最大数量
     */
    HOME_SUBJECT_MODULE_MAX_COUNT("home_subject_module_max_count", "7", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 首页每个科目模块的课程最大数量
     */
    HOME_COURSE_PER_SUBJECT_MAX_COUNT("home_course_per_subject_max_count", "7", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 首页展示教师最大数量
     */
    HOME_TEACHER_MAX_COUNT("home_teacher_max_count", "7", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    HOME_PROMOTION_MAX_COUNT("home_promotion_max_count", "7", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    /**
     * 微信首页配置
     */
    WECHAT_BASE_URL("wechat_base_url", "http://weixin.icourshow.com"),
    /**
     * 联系我们-邮箱
     */
    CONTACT_US_EMAIL("contact_us_email", "info@icourshow.com"),
    /**
     * 联系我们-微信
     */
    CONTACT_US_WECHAT("contact_us_wechat", "icourshow001"),
    /**
     * 试用订单免费天数
     */
    TRIAL_ORDER_DATE_COUNT("trial_order_date_count", "7", INTEGER_PREDICATE, TRIAL_ORDER_DATE_COUNT_TIPS),
    /**
     * 有免费订单的海报
     */
    HAVE_TRIAL_ORDER_POSTER_IMAGE("have_trial_order_poster_image", "http://ics-app.oss-cn-beijing.aliyuncs.com/image/have_trial_order.png"),
    /**
     * 没有免费订单的海报
     */
    HAVE_NOT_TRIAL_ORDER_POSTER_IMAGE("have_not_trial_order_poster_image", "http://ics-app.oss-cn-beijing.aliyuncs.com/image/have_not_trial_order.png"),
    /**
     * ios需要游客登陆的版本号
     */
    VISITOR_LANDING_IOS_VERSION("visitor_landing_ios_version", "1.0.1"),

    STUDENT_MIN_WITHDRAW_MONEY("student_min_withdraw_money", "10", NUMERIC_PREDICATE, "最小提现金额"),
    STUDENT_MAX_WITHDRAW_MONEY("student_max_withdraw_money", "5000", NUMERIC_PREDICATE, "最多提现金额"),
    STUDENT_EVERY_DAY_MAX_WITHDRAW_COUNT("student_every_day_max_withdraw_count", "5", NUMERIC_PREDICATE, "每天最多提现次数"),
    /**
     * 学生阶梯分润比例
     */
    PARENT_STUDENT_SHARE_RATE("parent_student_share_rate", "20%<30000.01<25%<100000.01<30%<300000.01<35%<900000.01<40%",
        GRADIENT_SHARE_RATE_PREDICATE, "该配置有误,请参考'20%<30000.01<25%<100000.01<30%<300000.01<35%<900000.01<40%'"),
    /**
     * 祖父级别的默认的分润比例
     */
    GRANDFATHER_STUDENT_SHARE_RATE("grandfather_student_share_rate", "10", Between0To100_PREDICATE, "该配置必须为数字且在0~100之间."),
    /**
     * 父级学生默认的分润比例
     */
    PARENT_STUDENT_DEFAULT_SHARE_RATE("parent_student_default_share_rate", "20", Between0To100_PREDICATE, "该配置必须为数字且在0~100之间."),

    H5_SHARED_VIDEO_VID("h5_shared_video_vid", "30fe77e88f71681529bb08dd80c0fb89_3",null, "这是H5分享导航页面视频的vid"),
//    /**
//     * 城市合伙人套餐
//     */
//    CITY_PARTNER_PACKAGE_ID("city_partner_package_id", "0", INTEGER_PREDICATE, REGISTER_INTEGER_TIPS),
    ;



    @Getter
    private String key;                      // 字典key 规定: key值必须是枚举name的小写形式
    private String defaultValue;            // 字典默认值
    private Predicate<String> valid;        // 新增修改字典值的检验函数
    private String description;             // 检验函数失败的提示语

    RegisterKeyValueEnum(String key, String defaultValue) {
        this(key, defaultValue, TRUE_PREDICATE, "");
    }

    public static RegisterKeyValueEnum valueOfKey(String key) {
        try {
            return RegisterKeyValueEnum.valueOf(StringUtils.upperCase(key));
        } catch (Exception e) {
            return null;
        }
    }
}
