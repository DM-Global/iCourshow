package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.TemplateBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * t_template
 */
@Repository
@Mapper
public interface TemplateDao extends BaseDao<TemplateBean>, BaseDataDao<TemplateBean> {
}
