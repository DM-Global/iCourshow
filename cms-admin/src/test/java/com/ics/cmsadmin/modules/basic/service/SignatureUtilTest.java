package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.modules.wechat.utils.SignatureUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * Created by Sandwich on 2018-11-09
 */

public class SignatureUtilTest extends CmsAdminApplicationTests {

    @Autowired
    private SignatureUtil signatureUtil;

    @Test
    public void test() {
        signatureUtil.checkSignature("", "", "");
    }

}
