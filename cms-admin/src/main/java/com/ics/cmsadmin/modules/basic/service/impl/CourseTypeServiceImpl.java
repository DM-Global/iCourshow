package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.CourseTypeBean;
import com.ics.cmsadmin.modules.basic.dao.CourseTypeDao;
import com.ics.cmsadmin.modules.basic.service.CourseTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class CourseTypeServiceImpl implements CourseTypeService {

    @Resource
    private CourseTypeDao courseTypeDao;

    @Override
    public PageResult list(CourseTypeBean bean, PageBean page) {
        long count = courseTypeDao.count(bean);
        List<CourseTypeBean> list = courseTypeDao.list(bean, page);
        return PageResult.getPage(count, list);
    }

    @Override
    public boolean insert(CourseTypeBean t) {
        return courseTypeDao.insert(t) == 1;
    }

    @Override
    public boolean update(String id, CourseTypeBean t) {
        return courseTypeDao.update(id, t) == 1;
    }

    @Override
    public boolean delete(String id) {
        return courseTypeDao.delete(id) == 1;
    }
}
