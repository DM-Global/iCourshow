package com.ics.cmsadmin.async;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class TestMain {

    @Resource
    private AsyncService asyncService;

    @Test
    public void test() {
        System.out.println(Thread.currentThread().getName() + " test");
        asyncService.test1();
        asyncService.test2();

    }
}
