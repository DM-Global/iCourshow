package com.ics.cmsadmin.modules.system.emums;


import com.ics.cmsadmin.modules.system.bean.SequenceNumberBean;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * com.kco.Enum
 * Created by swlv on 2016/10/25.
 */
@AllArgsConstructor
public enum SequenceNumberEnum {
    AGENT(new SequenceNumberBean("AGENT", "A", "代理商表自定主键", 1000L, 4L, 1L)),
    ROLE(new SequenceNumberBean("ROLE", "R", "角色表自定主键", 1000_0000L, 4L, 1L)),
    USER(new SequenceNumberBean("USER", "U", "用户表自定主键", 1000_0000L, 8L, 1L)),
    ORGANIZATION(new SequenceNumberBean("ORGANIZATION", "G", "组织自定主键", 1000L, 4L, 1L)),
    STUDENT_ACCOUNT(new SequenceNumberBean("STUDENT_ACCOUNT", "SACC", "学生账户表自定主键", 1000_0000L, 8L, 1L)),
    STUDENT_SCORE(new SequenceNumberBean("STUDENT_SCORE", "SS", "学生积分表自定主键", 1000_0000L, 8L, 1L)),
    AGENT_ACCOUNT(new SequenceNumberBean("AGENT_ACCOUNT", "ACC", "代理商账户表自定主键", 1000_0000L, 8L, 1L));

    @Getter
    private SequenceNumberBean sequenceNumberBean;

}
