package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentWithdrawAuditingBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
* t_student_withdraw_auditing
* Created by lvsw on 2019-44-31 16:05:02.
*/
@Repository
@Mapper
public interface StudentWithdrawAuditingDao extends BaseDao<StudentWithdrawAuditingBean>, BaseDataDao<StudentWithdrawAuditingBean> {

    int countToday(@Param("studentId") String studentId);

}
