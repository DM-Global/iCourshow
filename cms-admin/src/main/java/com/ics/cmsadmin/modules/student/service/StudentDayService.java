package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.modules.student.bean.StudentDaySummaryBean;

import java.util.List;

/**
 * 学生数据日统计
 */
public interface StudentDayService {
    /**
     * 执行学生日统计
     */
    void dayStatistics();

    /**
     * 按日期执行学生日统计
     * @param day 统计日期, 格式 yyyy-MM-dd
     */
    void dayStatistics(String day);

    List<StudentDaySummaryBean> list(String loginUserId, String startCreateDate, String endCreateDate);

    StudentDaySummaryBean query(String summaryDay, String studentId);
}
