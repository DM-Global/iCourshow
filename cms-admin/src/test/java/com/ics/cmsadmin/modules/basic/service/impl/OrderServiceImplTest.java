package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.utils.CalendarUtil;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.basic.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.Date;


@Slf4j
public class OrderServiceImplTest extends CmsAdminApplicationTests {
//public class OrderServiceImplTest{

    @Autowired
    private OrderService orderService;
    @Autowired
    PayService payService;


    @Test
    public void orderCreate() {
        OrderBean orderBean = orderService.create(OrderBean.builder().packageId("2").orderType(2).userId("001797c05f4440dd8d75085aa295b687").build(), "001797c05f4440dd8d75085aa295b687");
        log.info(orderBean.toString());
    }

    @Test
    public void updatePayTime(){
        OrderBean orderBean = orderService.queryById("b75be08367ae4f17823c3a48c261e591");
        orderBean.setPayTime(new Date());
        orderService.update(orderBean);

    }

    @Test
    public void orderCreateTrial() {
        OrderBean orderBean = orderService.createTrialOrderForNewUser("82bf085f2edd4e989937ca0ab27c56e8");
        log.info(orderBean.toString());
    }

    @Test
    public void testTime() {
        Date currentDate = new Date();
        log.info("current date: {}", currentDate);
        log.info("date after add 7 dates: {}", CalendarUtil.toSimpleDateStr(DateUtils.addDays(currentDate, 7)));
        log.info("date after add 1 year: {}", CalendarUtil.toSimpleDateStr(DateUtils.addYears(currentDate,1)));

    }

    @Test
    public void testOrderAfterWxAppPaid() {
        String xml = "<xml><appid><![CDATA[wx4838d74fd406436d]]></appid>\n" +
            "<attach><![CDATA[iCourshow]]></attach>\n" +
            "<bank_type><![CDATA[HXB_CREDIT]]></bank_type>\n" +
            "<cash_fee><![CDATA[1]]></cash_fee>\n" +
            "<fee_type><![CDATA[CNY]]></fee_type>\n" +
            "<is_subscribe><![CDATA[N]]></is_subscribe>\n" +
            "<mch_id><![CDATA[1520329271]]></mch_id>\n" +
            "<nonce_str><![CDATA[1546084840854]]></nonce_str>\n" +
            "<openid><![CDATA[oJmnh0rNlZP0GcRJLa4U-ZbynbmA]]></openid>\n" +
            "<out_trade_no><![CDATA[b513123b443443e9bf95cb7d9c4b1fc2]]></out_trade_no>\n" +
            "<result_code><![CDATA[SUCCESS]]></result_code>\n" +
            "<return_code><![CDATA[SUCCESS]]></return_code>\n" +
            "<sign><![CDATA[2081C6D0789C2845C7DFC0E813F0B090]]></sign>\n" +
            "<time_end><![CDATA[20181229200105]]></time_end>\n" +
            "<total_fee>1</total_fee>\n" +
            "<trade_type><![CDATA[APP]]></trade_type>\n" +
            "<transaction_id><![CDATA[4200000215201812293703045276]]></transaction_id>\n" +
            "</xml>";
        xml = payService.handleOrderAfterWxAppPaid(xml);
        log.info(xml);
    }


}
