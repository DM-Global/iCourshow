package com.ics.cmsadmin.modules.agent.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.agent.bean.StudentOrderBean;
import com.ics.cmsadmin.modules.agent.steward.AgentUtils;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.ics.cmsadmin.modules.agent.steward.AgentServices.agentDaySummaryService;
import static com.ics.cmsadmin.modules.agent.steward.AgentServices.agentInfoService;
import static com.ics.cmsadmin.modules.auth.steward.AuthServices.accessService;

@Api(description = "代理商汇总信息")
@RestController
@RequestMapping("/agentSummary")
public class AgentDaySummaryController {

    @Authorize
    @ApiOperation(value = "查询我的代理商信息")
    @GetMapping(value = "/queryMyAgentInfo")
    public ApiResponse queryMyAgentInfo(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentInfoService.queryMoreInfoByUserId(loginUserId));
    }

    @Authorize
    @ApiOperation(value = "分页查询代理商汇总信息")
    @PostMapping("/listAgentSummary/{pageNo}/{pageSize}")
    public ApiResponse listAgentSummary(@RequestBody AgentInfoBean agentBean,
                                        @ApiParam("页码") @PathVariable int pageNo,
                                        @ApiParam("每页条数") @PathVariable int pageSize,
                                        HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.listAgentSummaryByLoginUserId(agentBean, loginUserId, pageBean));
    }

    @Authorize
    @ApiOperation(value = "查询我的下级信息")
    @PostMapping("/queryMyDistributorInfo")
    public ApiResponse queryMyDistributorInfo(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.queryMyDistributorInfo(loginUserId));
    }

    @Authorize
    @ApiOperation(value = "查询我的下级汇总数据")
    @PostMapping("/listDistributorSummary")
    public ApiResponse listDistributorSummary(@RequestBody AgentInfoBean bean,
                                              HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.listDistributorSummary(bean, loginUserId));
    }

    @Authorize
    @ApiOperation(value = "查询我的收益信息")
    @PostMapping("/queryMyRevenueBaseInfo")
    public ApiResponse queryMyRevenueBaseInfo(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.queryMyRevenue(loginUserId));
    }

    @Authorize
    @ApiOperation(value = "查询我的日结汇总数据-图表")
    @PostMapping("/queryMyRevenue4Chart")
    public ApiResponse queryMyDaySummary(@RequestBody AgentInfoBean bean,
                                         HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        AgentInfoBean agentInfoBean = agentInfoService.queryByUserId(loginUserId);
        if (agentInfoBean == null) {
            return new ApiResponse(AgentUtils.optimizeResult(null, bean.getStartCreateTime(), bean.getEndCreateTime()));
        }
        bean.setAgentNo(agentInfoBean.getAgentNo());
        return new ApiResponse(agentDaySummaryService.listAgentDaySummary(bean));
    }

    @Authorize
    @ApiOperation(value = "查询我的收益信息-列表")
    @PostMapping("/queryMyRevenue4Table/{pageNo}/{pageSize}")
    public ApiResponse queryMyRevenue4Table(@RequestBody AgentInfoBean bean,
                                            @ApiParam("页码") @PathVariable int pageNo,
                                            @ApiParam("每页条数") @PathVariable int pageSize,
                                            HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.queryMyRevenue4Table(bean, loginUserId, new PageBean(pageNo, pageSize)));
    }

    @Authorize
    @ApiOperation(value = "查询我的学生信息")
    @PostMapping("/queryMyStudentInfo")
    public ApiResponse queryMyStudentInfo(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.queryMyStudentInfo(loginUserId));
    }

    @Authorize
    @ApiOperation(value = "查询我的学生信息-列表")
    @PostMapping("/queryMyStudent4Table/{pageNo}/{pageSize}")
    public ApiResponse queryMyStudent4Table(@RequestBody AgentInfoBean bean,
                                            @ApiParam("页码") @PathVariable int pageNo,
                                            @ApiParam("每页条数") @PathVariable int pageSize,
                                            HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.queryMyStudent4Table(bean, loginUserId, new PageBean(pageNo, pageSize)));
    }

    @Authorize
    @ApiOperation(value = "查询我的提现信息")
    @PostMapping("/queryMyWithdrawInfo")
    public ApiResponse queryMyWithdrawInfo(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentDaySummaryService.queryMyWithdrawInfo(loginUserId));
    }

    @Authorize
    @ApiOperation(value = "查询我的下级代理商详情信息")
    @PostMapping("/queryMyDistributorDetailInfo/{agentNo}")
    public ApiResponse queryMyDistributorDetailInfo(@PathVariable String agentNo, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (!accessService.canAccessForAgent(loginUserId, agentNo)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        return new ApiResponse(agentDaySummaryService.queryMyDistributorDetailInfo(agentNo));
    }

    @Authorize
    @ApiOperation(value = "查询我的下级代理商汇总信息")
    @PostMapping("/queryMyDistributorDetailChart/{agentNo}")
    public ApiResponse queryMyDistributorDetailChart(@ApiParam("下级代理商编号") @PathVariable String agentNo,
                                                     @RequestBody AgentInfoBean agentInfoBean,
                                                     HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (!accessService.canAccessForAgent(loginUserId, agentNo)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        agentInfoBean.setAgentNo(agentNo);
        return new ApiResponse(agentDaySummaryService.listAgentDaySummary(agentInfoBean));
    }

    @Authorize
    @ApiOperation(value = "分页查询我的下级代理商汇总信息-表格")
    @PostMapping("/queryMyDistributorDetailTable/{agentNo}/{pageNo}/{pageSize}")
    public ApiResponse queryMyDistributorDetailTable(@ApiParam("下级代理商编号") @PathVariable String agentNo,
                                                     @ApiParam("页码") @PathVariable int pageNo,
                                                     @ApiParam("每页条数") @PathVariable int pageSize,
                                                     @RequestBody AgentInfoBean agentInfoBean,
                                                     HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (!accessService.canAccessForAgent(loginUserId, agentNo)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        agentInfoBean.setAgentNo(agentNo);
        return new ApiResponse(agentDaySummaryService.pageAgentDaySummary(agentInfoBean, new PageBean(pageNo, pageSize)));
    }

    @Authorize
    @ApiOperation(value = "查询我的学生详情基本信息")
    @PostMapping("/queryMyStudentDetailInfo/{userId}")
    public ApiResponse queryMyStudentDetailInfo(@ApiParam("学生id") @PathVariable String userId,
                                                HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (!accessService.canAccessForStudent(loginUserId, userId)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该学生");
        }
        return new ApiResponse(agentDaySummaryService.queryMyStudentDetailInfo(userId));
    }

    @Authorize
    @ApiOperation(value = "查询我的学生购买订单信息")
    @PostMapping("/listMyStudentBuyOrderInfo/{userId}/{pageNo}/{pageSize}")
    public ApiResponse listMyStudentBuyOrderInfo(@ApiParam("学生id") @PathVariable String userId,
                                                 @ApiParam("页码") @PathVariable int pageNo,
                                                 @ApiParam("每页条数") @PathVariable int pageSize,
                                                 @RequestBody StudentOrderBean studentOrderBean,
                                                 HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (!accessService.canAccessForStudent(loginUserId, userId)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该学生");
        }
        studentOrderBean.setUserId(userId);
        return new ApiResponse(agentDaySummaryService.listMyStudentBuyOrderInfo(studentOrderBean, new PageBean(pageNo, pageSize)));
    }
}
