package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.SubjectBean;
import com.ics.cmsadmin.modules.basic.service.SubjectService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Subject controller
 * Created by lvsw on 2018-09-01.
 */
@Api(description = "科目管理接口")
@RestController
@RequestMapping("/subject")
public class SubjectController {

    @Resource
    private SubjectService subjectService;

    @Authorize(AuthorizeEnum.SUBJECT_QUERY)
    @ApiOperation(value = "查询科目信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(subjectService.queryById(id));
    }

    @Authorize(AuthorizeEnum.SUBJECT_INSERT)
    @ApiOperation(value = "新增科目信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody SubjectBean subjectBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(subjectService.insert(subjectBean));
    }

    @Authorize(AuthorizeEnum.SUBJECT_DELETE)
    @ApiOperation(value = "删除科目信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的科目 id") @PathVariable String id) {
        return new ApiResponse(subjectService.delete(id));
    }

    @Authorize(AuthorizeEnum.SUBJECT_UPDATE)
    @ApiOperation(value = "更新科目信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody SubjectBean subjectBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的科目Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(subjectService.update(id, subjectBean));
    }

    @Authorize(AuthorizeEnum.SUBJECT_QUERY)
    @ApiOperation(value = "分页查询科目信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody SubjectBean subjectBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(subjectService.list(subjectBean, pageBean));
    }

    @Authorize(AuthorizeEnum.SUBJECT_QUERY)
    @ApiOperation(value = "查询所有可选的无父节点科目信息")
    @GetMapping("/listOptionalSubject")
    public ApiResponse listOptionalSubject() {
        return new ApiResponse(subjectService.list(SubjectBean.builder().isActive(true).fromSubjectId("0").build(),
            new PageBean(1, Integer.MAX_VALUE)));
    }

    @Authorize(AuthorizeEnum.SUBJECT_QUERY)
    @ApiOperation(value = "查询所有科目信息")
    @GetMapping("/listAllSubject")
    public ApiResponse listAllSubject() {
        return new ApiResponse(subjectService.list(SubjectBean.builder().isActive(true).build(),
            new PageBean(1, Integer.MAX_VALUE)));
    }
}
