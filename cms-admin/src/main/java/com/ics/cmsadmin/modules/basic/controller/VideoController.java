package com.ics.cmsadmin.modules.basic.controller;


import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.modules.basic.bean.VideoBean;
import com.ics.cmsadmin.modules.basic.service.VideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(value = "知识点视频接口")
@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    private VideoService videoService;


    @Authorize(AuthorizeEnum.VIDEO_QUERY)
    @ApiOperation(value = "分页查询知识点视频信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody VideoBean bean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return ApiResponse.getDefaultResponse(videoService.list(bean, pageBean));
    }

    @Authorize(AuthorizeEnum.VIDEO_INSERT)
    @ApiOperation(value = "插入知识点视频信息")
    @PostMapping("/insert")
    public ApiResponse insert(@RequestBody VideoBean bean) {
        return ApiResponse.getDefaultResponse(videoService.insert(bean));
    }

    @Authorize(AuthorizeEnum.VIDEO_UPDATE)
    @ApiOperation(value = "更新知识点视频信息")
    @PostMapping("/update/{id}")
    public ApiResponse insert(@PathVariable String id, @RequestBody VideoBean bean) {
        return ApiResponse.getDefaultResponse(videoService.update(id, bean));
    }


    @Authorize(AuthorizeEnum.VIDEO_DELETE)
    @ApiOperation(value = "删除知识点视频信息")
    @PostMapping("/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的视频 id") @PathVariable String id) {
        return ApiResponse.getDefaultResponse(videoService.delete(id));
    }

    @ApiOperation(value = "获取保利威插件授权信息")
    @GetMapping("/auth")
    public Map<String, String> getAuth() {

        return videoService.polyvAuth();
    }
}
