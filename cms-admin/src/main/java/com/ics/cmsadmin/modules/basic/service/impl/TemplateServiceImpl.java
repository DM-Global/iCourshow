package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.basic.bean.TemplateBean;
import com.ics.cmsadmin.modules.basic.dao.TemplateDao;
import com.ics.cmsadmin.modules.basic.service.TemplateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * t_template
 */
@Service
public class TemplateServiceImpl implements TemplateService {
    @Resource
    private TemplateDao templateDao;

    @Override
    public TemplateBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return templateDao.queryById(id);
    }

    @Override
    public PageResult list(TemplateBean bean, PageBean page) {
        long count = templateDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, templateDao.list(bean, page));
    }

    @Override
    public boolean insert(TemplateBean bean) {
        if (bean == null) {
            return false;
        }
        return templateDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, TemplateBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return templateDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return templateDao.delete(id) == 1;
    }

}
