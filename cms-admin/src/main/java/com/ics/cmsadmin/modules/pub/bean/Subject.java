package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class Subject extends Resource<Integer> {

    private Integer fromSubjectId;

    private String name;

    private Boolean isActive;

    private String description;

}
