package com.ics.cmsadmin.modules.pub.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CourseOrderVO {

    /**
     * 课程id
     */
    private String courseId;

    /**
     * 课程封面
     */
    private String coverSrc;

    private String courseName;

    private String description;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 订单金额
     */
    private BigDecimal orderAmount;

    /**
     * 订单类型
     */
    private Integer orderType;

    /**
     * 完成订单事件
     */
    @JsonSerialize(using = Date2LongSerializer.class)
    private Date orderTime;
}
