package com.ics.cmsadmin;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConstants;

/**
 * Created by Sandwich on 2018-07-15
 * HTTPS请求可选HMAC-SHA256算法和MD5算法签名
 */
public class WXSignType {
    public static void main(String[] args) throws Exception {
        MyConfig config = new MyConfig();
        WXPay wxpay = new WXPay(config, WXPayConstants.SignType.HMACSHA256);
        // ......
    }
}
