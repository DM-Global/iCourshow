package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.email.MailUtils;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.basic.bean.TemplateBean;
import com.ics.cmsadmin.modules.basic.service.TemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * t_template controller
 */
@Api(description = "模板管理")
@RestController
@RequestMapping("/template")
public class TemplateController {

    @Resource
    private TemplateService templateService;
    @Autowired
    private MailUtils mailUtils;

    @Authorize(AuthorizeEnum.TEMPLATE_QUERY)
    @ApiOperation(value = "查询信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(templateService.queryById(id));
    }

    @Authorize(AuthorizeEnum.TEMPLATE_INSERT)
    @ApiOperation(value = "新增信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody TemplateBean templateBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(templateService.insert(templateBean));
    }

    @Authorize(AuthorizeEnum.TEMPLATE_DELETE)
    @ApiOperation(value = "删除信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
        return new ApiResponse(templateService.delete(id));
    }

    @Authorize(AuthorizeEnum.TEMPLATE_UPDATE)
    @ApiOperation(value = "更新信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody TemplateBean templateBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(templateService.update(id, templateBean));
    }

    @Authorize(AuthorizeEnum.TEMPLATE_SEND)
    @ApiOperation(value = "更新信息")
    @PostMapping(value = "/send")
    public ApiResponse send(@RequestBody TemplateBean templateBean,
                            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(mailUtils.sendEmail(templateBean.getEmail(), templateBean.getTitle(), templateBean.getContent()));
    }

    @Authorize(AuthorizeEnum.TEMPLATE_QUERY)
    @ApiOperation(value = "分页查询信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody TemplateBean templateBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(templateService.list(templateBean, pageBean));
    }

}
