package com.ics.cmsadmin.frame.utils;

import java.util.UUID;

public class KeyUtil {
    /**
     * 使用uuid为唯一主键
     *
     * @return
     */
    public static String genUniqueKey() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

}
