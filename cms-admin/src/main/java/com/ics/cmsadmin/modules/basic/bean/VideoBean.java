package com.ics.cmsadmin.modules.basic.bean;


import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class VideoBean {

    String id;

    private String name;
    private String duration;
    private String description;

    private String unitId;
    private String unitName;

    private String courseId;
    private String courseName;

    private String subjectId;
    private String subjectName;

    private String topicId;
    private String topicName;

    private String pointId;
    private String pointName;

    private String polyVId;

    @Tolerate
    public VideoBean() {
    }
}
