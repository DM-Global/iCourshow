CREATE TABLE `t_picture` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pic_url` varchar(512) DEFAULT NULL COMMENT '上传图标url',
  `description` varchar(64) DEFAULT NULL COMMENT '图标描述',
  `pic_type` tinyint(3) DEFAULT NULL COMMENT '图标类型: 1. 课程封面 2. 单元封面 3.老师头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='图标表';


CREATE TABLE `t_order` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) NOT NULL DEFAULT '',
  `course_id` int(11) NOT NULL COMMENT '课程id',
  `order_type` tinyint(3) DEFAULT NULL COMMENT '订单类型',
  `order_amount` decimal(8,2) NOT NULL COMMENT '订单总金额',
  `payment_method` tinyint(3) DEFAULT '1' COMMENT '支付方式: 1.微信(默认) 2.支付宝',
  `status` tinyint(3) DEFAULT '1' COMMENT '订单状态: 1. 未支付 2. 已支付 3. 已取消',
  `create_time` timestamp COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表';

# 设置触发器，添加一个新订单，自动记录当前下单时间
DELIMITER $$
CREATE TRIGGER default_datetime BEFORE INSERT ON t_order
FOR EACH ROW
BEGIN
  #IF new.`create_time` IS NULL THEN
    SET new.`create_time`=now();
  #END IF;
END $$

CREATE TABLE `t_video_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '用户id',
  `video_id` int(11) NOT NULL COMMENT '视频id',
  `duration` bigint(32) NOT NULL COMMENT '视频时长',
  `position` bigint(32) NOT NULL COMMENT '观看视频位置',
  `view_num` int(11) NOT NULL DEFAULT '0' COMMENT '观看次数',
  `total_time` bigint(32) NOT NULL DEFAULT '0' COMMENT '观看总时长',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='视频观看记录表';
DELIMITER $$
CREATE TRIGGER video_history_default_datetime BEFORE INSERT ON t_video_history
FOR EACH ROW
BEGIN
  SET new.`create_time`=now();
END $$
