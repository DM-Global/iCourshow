package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Subject;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubjectMapper extends ResourceMapper<Subject, Integer> {
    //List<Subject> selectAll(@Param("parentId") Integer parentId);
}
