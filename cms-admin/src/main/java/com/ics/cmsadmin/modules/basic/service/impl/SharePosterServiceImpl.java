package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import com.ics.cmsadmin.modules.basic.dao.SharePosterDao;
import com.ics.cmsadmin.modules.basic.service.SharePosterService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * t_share_poster
 */
@Service
public class SharePosterServiceImpl implements SharePosterService {
    @Resource
    private SharePosterDao sharePosterDao;

    @Override
    public SharePosterBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return sharePosterDao.queryById(id);
    }

    @Override
    public PageResult list(SharePosterBean bean, PageBean page) {
        long count = sharePosterDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, sharePosterDao.list(bean, page));
    }

    @Override
    public boolean insert(SharePosterBean bean) {
        if (bean == null) {
            return false;
        }
        return sharePosterDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, SharePosterBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return sharePosterDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return sharePosterDao.delete(id) == 1;
    }

}
