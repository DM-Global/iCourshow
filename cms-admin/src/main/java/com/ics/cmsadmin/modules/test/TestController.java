//package com.ics.cmsadmin.modules.test;
//
//import com.ics.cmsadmin.frame.core.bean.ApiResponse;
//import com.ics.cmsadmin.frame.property.LvswWechatConfig;
//import com.ics.cmsadmin.frame.utils.GsonUtils;
//import lombok.extern.log4j.Log4j2;
//import org.apache.commons.codec.digest.DigestUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.data.redis.core.RedisHash;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import java.util.Arrays;
//import java.util.Map;
//import java.util.stream.Stream;
//
//@Log4j2
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//    @Resource
//    private LvswWechatConfig config;
//
//    @RequestMapping("/test")
//    public String test(HttpServletRequest request){
//        String signature = request.getParameter("signature");
//        String echostr = request.getParameter("echostr");
//        String timestamp = request.getParameter("timestamp");
//        String nonce = request.getParameter("nonce");
//        String reduce = Stream.of(timestamp, nonce, config.getToken()).sorted().reduce("", (all, item) -> all += item);
//        if (StringUtils.equals(signature, DigestUtils.sha1Hex(reduce))) {
//            return echostr;
//        }else{
//            return "Error";
//        }
//    }
//
//    @RequestMapping("/wechatLogin")
//    public ApiResponse wechatLogin(HttpServletRequest request){
//        Map<String, String[]> parameterMap = request.getParameterMap();
//        log.info("wechatLogin --> " + GsonUtils.toJson(parameterMap));
//        return new ApiResponse(parameterMap);
//    }
//}
