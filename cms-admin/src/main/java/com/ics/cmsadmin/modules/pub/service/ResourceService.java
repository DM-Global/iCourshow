package com.ics.cmsadmin.modules.pub.service;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.pub.dao.ResourceMapper;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public class ResourceService<DAO extends ResourceMapper<R, T>, R, T> {

    @Autowired
    private DAO dao;

    public int insert(R o) {
        return dao.insert(o);
    }

    public int delete(T id) {
        return dao.delete(id);
    }

    public int update(R o) {
        return dao.update(o);
    }

    @Transactional
    public ApiResponse updateMultiple(List<R> list) {
        for (R o : list) {
            int effectLine = dao.update(o);
            if (effectLine == 0) {
                throw new CmsException(ApiResultEnum.BULK_UPDATE_FAIL);
            }
        }
        return ApiResponse.getDefaultResponse();
    }

    public R get(T id) {
        return dao.select(id);
    }

    public List<R> listAll() {
        return getMultiple(null);
    }

    public List<R> getMultiple(Map<String, Object> param) {
        return dao.selectAll(param);
    }

    @Transactional
    public int deleteMultiple(List<T> ids) {

        return dao.deleteMultiple(ids);
    }
}
