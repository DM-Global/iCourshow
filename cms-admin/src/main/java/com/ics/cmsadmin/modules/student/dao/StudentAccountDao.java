package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
* t_student_account
* Created by lvsw on 2019-18-11 21:03:47.
*/
@Repository
@Mapper
public interface StudentAccountDao extends BaseDao<StudentAccountBean>, BaseDataDao<StudentAccountBean> {
    /**
     * 分润收入
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void profitIncome(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 申请提现
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void applyWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 接受提现成功
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void acceptWithdrawCashSuccess(@Param("accountId") String accountId, @Param("money") BigDecimal money);
    /**
     * 接受提现失败
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void acceptWithdrawCashFailed(@Param("accountId") String accountId, @Param("money") BigDecimal money);
    /**
     * 拒绝提现
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void refuseWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 取消提现申请
     *
     * @param accountId
     * @param money
     */
    void cancelWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 购买订单
     * @param accountId
     * @param money
     */
    void buyOrder(@Param("accountId") String accountId, @Param("money") BigDecimal money);

}
