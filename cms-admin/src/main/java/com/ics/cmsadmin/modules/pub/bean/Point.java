package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class Point extends Resource<Integer> {

    private String name;

    private Boolean isActive;

    private String description;

    private Integer unitId;

    private String unitName;

    private Integer courseId;

    private String courseName;

    private Integer subjectId;

    private String subjectName;

    private Integer rank;

    private Integer topicId;

    private String topicName;

    private String pointType;

    private String pointSource;

    private Integer coverId;

    private String coverSrc;
}
