# iCourshow
> test-dev
#### 项目介绍
这是iCourshow的后台项目,为后台管理系统和app应用接供接口

#### 软件架构
软件架构说明
1.基础框架包含三个子模块,cms-service,cms-admin,cms-app. 2.cms-service为基础模块,包含model,dao,service以及其他常用的utils,其他子模块都需要依赖此模块.

cms-service
公共模块,cms-admin|cms-app,需依赖这模块.

cms-admin
后台接口服务,即controller层.

cms-app
前端接口服务,即controller层.

cms-jobs
定时任务服务


#### 安装教程

1. 初始化
git init

2.修改配置 保存账户密码
vim .git/config
  添加
[credential]
    helper = store

3. 设置远程仓库
git remote add origin https://gitee.com/DM-Global/iCourshow.git

4. 拉取代码(运行后需要输入账户密码)
git pull origin master 

#### 使用说明

1. 本地开发环境访问api接口文档地址：
http://localhost:8090/ics/swagger-ui.html
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### lombok 使用

由于项目中引入了lombok 组件，用于生成数据对象的 getter,setter, toString 等等，该组件需要一个ide插件才能支持使用，否则无法编译成功
![lombok插件安装](https://images.gitee.com/uploads/images/2018/0724/155258_b56e0c26_2045218.jpeg "Jietu20180724-155223@2x.jpg")

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

# 删除表
DROP TABLE t_state;  
DROP TABLE t_city;  
DROP TABLE t_nation;  
DROP TABLE t_school;  
DROP TABLE t_answer;  
DROP TABLE t_hint;  
DROP TABLE t_practice;  
DROP TABLE t_question;  
