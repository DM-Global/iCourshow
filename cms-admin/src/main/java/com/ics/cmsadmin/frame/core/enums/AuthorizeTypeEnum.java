package com.ics.cmsadmin.frame.core.enums;

/**
 * 权限组枚举
 * Created by 666666 on 2017/9/25.
 */
public enum AuthorizeTypeEnum {
    /**
     * 操作权限
     */
    OPERATE,
    /**
     * 查询权限
     */
    QUERY,
    /**
     * 其他权限
     */
    OTHER
}
