package com.ics.cmsadmin.modules.agent.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.agent.bean.AgentAccountBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * a_agent_account
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Repository
@Mapper
public interface AgentAccountDao extends BaseDao<AgentAccountBean>, BaseDataDao<AgentAccountBean> {
    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") AgentAccountBean t);

    /**
     * 分润收入
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void profitIncome(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 申请提现
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void applyWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 接受提现
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void acceptWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 拒绝提现
     *
     * @param accountId 账户id
     * @param money     分润金额
     * @return
     */
    void refuseWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);

    /**
     * 取消提现申请
     *
     * @param accountId
     * @param money
     */
    void cancelWithdrawCash(@Param("accountId") String accountId, @Param("money") BigDecimal money);
}
