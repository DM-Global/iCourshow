package com.ics.cmsadmin.modules.wechat.model.auth.resp;


import com.ics.cmsadmin.modules.wechat.result.ResultState;
import lombok.Data;

/**
 * jsapi_ticket是公众号用于调用微信JS接口的临时票据
 *
 * @author phil
 * @date 2017年8月21日
 */
@Data
public class JsapiTicket extends ResultState {

    private String ticket; //jsapi_ticket

    private int expires_in;
}
