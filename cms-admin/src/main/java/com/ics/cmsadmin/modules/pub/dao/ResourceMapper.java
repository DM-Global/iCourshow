package com.ics.cmsadmin.modules.pub.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ResourceMapper<Resource, T> {
    /**
     * @param record
     * @return 影响行数，1表示成功，0表示失败
     */
    int insert(Resource record);

    /**
     * @param id
     * @return 影响行数，1表示成功，0表示失败
     */
    int delete(T id);

    int deleteMultiple(List<T> ids);

    int update(Resource record);

    Resource select(T id);

    List<Resource> selectAll(Map<String, Object> map);

    Integer queryTotal(Map<String, Object> map);
}
