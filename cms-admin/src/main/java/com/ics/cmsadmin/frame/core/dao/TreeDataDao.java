package com.ics.cmsadmin.frame.core.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface TreeDataDao<T> {
    /**
     * 根据子节点获取父节点
     *
     * @param childrenNode 子节点
     * @return 查询结果
     */
    T queryParent(@Param("childrenNode") String childrenNode);

    /**
     * 根据父节点获取直接子节点
     *
     * @param parentNode 父节点
     * @return 查询结果
     */
    List<T> listDirectChildren(@Param("parentNode") String parentNode);

}
