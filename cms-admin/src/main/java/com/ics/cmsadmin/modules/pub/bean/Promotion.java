package com.ics.cmsadmin.modules.pub.bean;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import lombok.Data;

@Data
public class Promotion extends Resource<Integer> {
    private LevelEnum level;
    private Integer levelId;
    private LevelInfo levelInfo;
    private Integer imageId;
    private String imageSrc;

    private Boolean isActive;

    private String description;

    private Integer rank;
}

