package com.ics.cmsadmin.modules.pub.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ics.cmsadmin.modules.pub.dao.OrderMapper;
import com.ics.cmsadmin.frame.core.enums.OrderStatusEnum;
import com.ics.cmsadmin.modules.pub.bean.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("OrderService")
public class OrderService extends ResourceService<OrderMapper, Order, String> {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 统计查询查询到的订单的总额
     *
     * @param productName
     * @param userName
     * @param orderType
     * @param paymentMethod
     * @param status
     * @param orderId
     * @return
     */
    public BigDecimal totalAmount(String productName,
                                  String userName,
                                  Integer orderType,
                                  Integer paymentMethod,
                                  Integer status,
                                  String orderId) {
        return orderMapper.totalAmount(productName, userName, orderType, paymentMethod, status, orderId);
    }

    /**
     * 获取单个商品的购买状态
     *
     * @param userId
     * @param productId
     * @return true 表示已经下单并付款成功
     */
    public Boolean getPurchaseStatus(String userId, Integer productId) {
        Order order = getUserOrder(userId, productId);
        return order != null && order.getStatus().equals(OrderStatusEnum.SUCCESS.getCode());
    }

    /**
     * 获取用户订单
     *
     * @param userId
     * @param productId
     * @return
     */
    public Order getUserOrder(String userId, Integer productId) {
        return orderMapper.getUserOrder(userId, productId);
    }

    public List<Order> list(Map<String, Object> params, Integer page, Integer pageSize) {
        if (page != null && pageSize != null) {
            PageHelper.startPage(page, pageSize);
        }
        Page<Order> orders = new Page<>();
        List<Order> orderList = getMultiple(params);
        if (!(orderList instanceof Page)) {
            orders.addAll(orderList);
            orders.setTotal(orderList.size());
        } else {
            orders = (Page<Order>) orderList;
        }
        return orders;
    }

    public Integer getTotalNumber(Integer courseId) {
        Map<String, Object> param = new HashMap<>();
        param.put("courseId", courseId);
        return orderMapper.queryTotal(param);
    }
}
