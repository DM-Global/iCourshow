package com.ics.cmsadmin.modules.system.controller;

import com.google.gson.Gson;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.system.bean.MetaBean;
import com.ics.cmsadmin.modules.system.service.MetaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * Meta controller
 * Created by lvsw on 2018-04-03.
 */
@Api(description = "meta管理接口")
@RestController
@RequestMapping("/meta")
public class MetaController {

    @Resource
    private MetaService metaService;

    @Authorize(AuthorizeEnum.META_QUERY)
    @ApiOperation(value = "查询meta信息")
    @GetMapping(value = "/query/{metaId}")
    public ApiResponse queryById(@ApiParam(value = "metaId") @PathVariable String metaId) {
        return new ApiResponse(metaService.queryById(metaId));
    }

    @Authorize
    @ApiOperation(value = "查询meta信息")
    @GetMapping(value = "/queryByParentId/{parentId}")
    public ApiResponse queryByParentId(@ApiParam(value = "parentId") @PathVariable String parentId) {
        return new ApiResponse(metaService.listDirectChildren(parentId));
    }

    @Authorize(AuthorizeEnum.META_INSERT)
    @ApiOperation(value = "新增meta信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody MetaBean metaBean, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(metaService.insert(metaBean));
    }

    @Authorize(AuthorizeEnum.META_DELETE)
    @ApiOperation(value = "删除meta信息")
    @PostMapping(value = "/delete/{metaId}")
    public ApiResponse delete(@ApiParam("需要删除的metaId") @PathVariable String metaId) {
        return new ApiResponse(metaService.delete(metaId));
    }

    @Authorize(AuthorizeEnum.META_UPDATE)
    @ApiOperation(value = "更新meta信息")
    @PostMapping(value = "/update/{metaId}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody MetaBean metaBean,
                              @ApiParam("需要更新的metaId") @PathVariable String metaId, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(metaService.update(metaId, metaBean));
    }

    @Authorize(AuthorizeEnum.META_QUERY)
    @ApiOperation(value = "分页查询meta信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody MetaBean metaBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        ApiResponse apiResponse = new ApiResponse(metaService.list(metaBean, pageBean));
        System.out.println(new Gson().toJson(apiResponse));
        return apiResponse;
    }

}
