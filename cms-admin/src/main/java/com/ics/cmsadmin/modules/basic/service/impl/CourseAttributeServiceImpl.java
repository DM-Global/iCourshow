package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.CourseAttributeBean;
import com.ics.cmsadmin.modules.basic.dao.CourseAttributeDao;
import com.ics.cmsadmin.modules.basic.service.CourseAttributeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * t_course_attribute
 * Created bysandwich on 2018-56-10 07:12:28.
 */
@Service
public class CourseAttributeServiceImpl implements CourseAttributeService {
    @Resource
    private CourseAttributeDao courseAttributeDao;

    @CacheDbMember(returnClass = CourseAttributeBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public CourseAttributeBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return courseAttributeDao.queryById(id);
    }

    @CacheDbMember(returnClass = CourseAttributeBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public PageResult<CourseAttributeBean> list(CourseAttributeBean bean, PageBean page) {
        long count = courseAttributeDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, courseAttributeDao.list(bean, page));
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean insert(CourseAttributeBean bean) {
        if (bean == null) {
            return false;
        }
        return courseAttributeDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean update(String id, CourseAttributeBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return courseAttributeDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return courseAttributeDao.delete(id) == 1;
    }

}
