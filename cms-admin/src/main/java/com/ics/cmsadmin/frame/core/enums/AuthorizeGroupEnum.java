package com.ics.cmsadmin.frame.core.enums;

/**
 * 权限组枚举
 * Created by 666666 on 2017/9/25.
 */
public enum AuthorizeGroupEnum {
    /**
     * 用户组
     */
    USER("userManage,organManage"),
    /**
     * 角色组
     */
    ROLE("roleManage"),
    /**
     * 权限组
     */
    AUTHORIZE("authManage"),
    /**
     * 组织
     */
    ORGANIZATION("organManage"),
    /**
     * 菜单
     */
    MENU("menuManage"),
    REGISTER("registerManage"),
    META("metaManage"),

    SUBJECT("subjectManage"),
    COURSE("courseManage"),
    UNIT("unitManage"),
    TOPIC("topicManage"),
    POINT("pointManage"),
    VIDEO("pointVideoManage"),
    COURSE_ATTRIBUTE("courseAttributeManage"),

    COURSE_TYPE("courseTypeManage"),
    COURSE_ATTR("courseAttrManage"),

    PROBLEM_SET("problemSetManage"),
    PICTURE("pictureManage"),
    IMAGE(),
    FILE(),
    ORDER("orderManage"),
    PACKAGE("packageManage"),
    STUDENT("studentManage"),
    TEACHER("teacherManage"),
    PROMOTION("promotionManage"),
    TEMPLATE("templateManage"),

    ACCOUNT_DETAIL("accountDetail,myAccountHistory"),
    AGENT_ACCOUNT("agentAccount"),
    MY_AGENT_INFO("myAgentInfo"),
    AGENT("agentList"),
    SHARE_DETAIL("shareDetail"),
    APPLY_WITHDRAW_CASH("applyWithdrawCash"),
    USER_OPER_LOG("logManage"),
    SHARE_POSTER("sharePosterManage"),
//    APP_SHARE_POSTER("appSharePosterManage"),
    APP_VERION("appVersionManage"),
    APP_FEEDBACK("appFeedbackManage"),
    STUDENT_WITHDRAW_AUDITING("studentWithdrawManage"),
    UMENG_PUSH("upushHistoryManage")
    ;

    // 权限关联的菜单id
    private String menuId;

    AuthorizeGroupEnum() {
        this("other");
    }

    AuthorizeGroupEnum(String menuId) {
        this.menuId = menuId;
    }

    public String getMenuId() {
        return menuId;
    }
}
