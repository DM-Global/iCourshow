package com.kco.log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class DealWithLogs {

    public static void main(String[] args) throws Exception {
        File baseFile = new File("C:\\Users\\666666\\Desktop\\logs");
        File[] files = baseFile.listFiles();
        for (File file : files) {
            dealWithFile(file);
        }
    }

    public static void dealWithFile(File file) throws Exception {
        File outFile = new File("C:\\Users\\666666\\Desktop\\out\\out.txt");
        FileUtils.readLines(file, "utf8").stream().forEach(item -> {
            if (item.contains("请求微信api接口") || item.contains("请求微信api接口返回数据")) {
                try {
                    FileUtils.writeStringToFile(outFile, item + "\n", "utf8", true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
//          log.info("请求微信api接口:" + url);
//            String body = Jsoup.connect(url).ignoreContentType(true).execute().body();
//            log.info("请求微信api接口返回数据:" + body);
