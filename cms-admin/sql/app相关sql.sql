
ALTER TABLE t_student ADD COLUMN device_id VARCHAR(50) NULL COMMENT '设备id' AFTER  union_id;
CREATE UNIQUE INDEX uni_device_id ON t_student(device_id);

ALTER TABLE t_student ADD COLUMN regist_source VARCHAR(2) NULL DEFAULT '1' COMMENT '注册来源: 1 公众号, 2: app' AFTER  union_id;

INSERT INTO com_meta(meta_id, parent_id, NAME, VALUE, order_index, create_time)VALUES
('student', '__ROOT__', '学生相关', NULL, '0', NOW()),
('student-register', 'student', '注册来源', NULL, '0', NOW()),
('student-register_wechat', 'student-register', '公众号', '1', '1', NOW()),
('student-register_app', 'student-register', 'app', '2', '2', NOW());



ALTER TABLE app_version ADD COLUMN `status` VARCHAR(2) NOT NULL DEFAULT '1' COMMENT '状态: 0 禁用, 1 启用' AFTER remark;


ALTER TABLE t_point ADD COLUMN test_num INT DEFAULT 0 COMMENT '测试题数量' AFTER point_type;
ALTER TABLE t_point ADD COLUMN video_duration VARCHAR(20) DEFAULT NULL COMMENT '视频时长' AFTER point_type;
ALTER TABLE t_unit ADD COLUMN  test_num INT DEFAULT 0 COMMENT '测试题数量' AFTER cover_id;
ALTER TABLE t_topic ADD COLUMN  test_num INT DEFAULT 0 COMMENT '测试题数量' AFTER rank;
ALTER TABLE t_video ADD COLUMN duration VARCHAR(20) NULL COMMENT '视频时长';
ALTER TABLE com_user_oper_log ADD COLUMN cost_time INT(11) DEFAULT 0 COMMENT '执行时间' AFTER oper_ip;

UPDATE t_topic tt
JOIN (
	SELECT qps.problem_set_id, qps.level, qps.level_id, t.test_num FROM q_problem_set qps
	JOIN (
		SELECT problem_set_id, COUNT(*) test_num FROM q_problem
		GROUP BY problem_set_id
	)t ON qps.problem_set_id = t.problem_set_id
	WHERE qps.level = 'topic'
)p ON tt.id = p.level_id
SET tt.test_num = p.test_num;

UPDATE t_unit tt
JOIN (
	SELECT qps.problem_set_id, qps.level, qps.level_id, t.test_num FROM q_problem_set qps
	JOIN (
		SELECT problem_set_id, COUNT(*) test_num FROM q_problem
		GROUP BY problem_set_id
	)t ON qps.problem_set_id = t.problem_set_id
	WHERE qps.level = 'unit'
)p ON tt.id = p.level_id
SET tt.test_num = p.test_num;
UPDATE t_point tt
JOIN (
	SELECT qps.problem_set_id, qps.level, qps.level_id, t.test_num FROM q_problem_set qps
	JOIN (
		SELECT problem_set_id, COUNT(*) test_num FROM q_problem
		GROUP BY problem_set_id
	)t ON qps.problem_set_id = t.problem_set_id
	WHERE qps.level = 'point'
)p ON tt.id = p.level_id
SET tt.test_num = p.test_num;
CREATE TABLE app_version (
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
	`type` VARCHAR(10) NOT NULL COMMENT 'app类型 ios,android',
	`version` VARCHAR(20) NOT NULL COMMENT 'app版本 格式 1.0.0.0',
	`force_update` INT(11) NOT NULL DEFAULT '0' COMMENT '是否强制更新 0 不强制更新 1 强制更新',
	`url` VARCHAR(512) NOT NULL COMMENT 'app下载url',
	`remark` TEXT NULL COMMENT '更新备注',
	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`update_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
	PRIMARY KEY (`id`),
	UNIQUE KEY  (`type`, `version`)
)COMMENT 'app版本信息';

INSERT INTO sys_menu(menu_id, parent_id, label, url, icon, order_no, create_time)VALUES
('appVersionManage', 'systemConfig', 'app版本管理', '/common/appVersionManage','ios-home', '4', NOW());
INSERT INTO sys_menu(menu_id, parent_id, label, url, icon, order_no, create_time)VALUES
('appFeedbackManage', 'systemConfig', 'app意见反馈', '/common/appFeedbackManage','ios-home', '4', NOW());


INSERT INTO com_meta(meta_id, parent_id, NAME, VALUE, order_index, create_time)VALUES
('app', '__ROOT__', 'app相关配置', NULL, '0', NOW()),
('app_type', 'app', 'app类型', NULL, '0', NOW()),
('app_type_ios', 'app_type', 'ios', 'ios', '0', NOW()),
('app_type_android', 'app_type', 'android', 'android', '1', NOW()),
('app_update', 'app', 'app更新相关', NULL, '0', NOW()),
('app_update_update0', 'app_update', '非强制更新', '0', '0', NOW()),
('app_update_update1', 'app_update', '强制更新', '1', '1', NOW());

ALTER TABLE t_student ADD COLUMN app_open_id VARCHAR(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户app微信openid' AFTER open_id;
ALTER TABLE t_student DROP COLUMN register_source;
ALTER TABLE t_student ADD COLUMN `province` VARCHAR(20) DEFAULT NULL COMMENT '省份编码' AFTER `password`;
ALTER TABLE t_student ADD COLUMN `city` VARCHAR(20) DEFAULT NULL COMMENT '城市编码' AFTER province;
ALTER TABLE t_student ADD COLUMN `county` VARCHAR(20) DEFAULT NULL COMMENT '县编码' AFTER city;
ALTER TABLE t_student ADD COLUMN `city_name` VARCHAR(256) DEFAULT NULL COMMENT '省市区地址' AFTER county;
ALTER TABLE t_student ADD COLUMN `wechat_account` VARCHAR(100) DEFAULT NULL COMMENT '微信账号' AFTER nickname;
ALTER TABLE t_student ADD COLUMN `email` VARCHAR(256) DEFAULT NULL COMMENT '邮箱' AFTER wechat_account;


CREATE TABLE com_phone_code(
	`code` INT(10) NOT NULL COMMENT '手机区域码',
	`name_tw` VARCHAR(30) NULL COMMENT '中文繁体地区名',
	`name_en` VARCHAR(50) NULL COMMENT '英语地区名',
	`name_zh` VARCHAR(30) NULL COMMENT '中文简体地区名',
	`locale` VARCHAR(10) NOT NULL COMMENT '地区区域码',
	`status` VARCHAR(2) NOT NULL DEFAULT '1' COMMENT '状态: 0不启用, 1 启用',
	`order_index` int(10) NOT NULL DEFAULT '1' COMMENT '排序',
	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
	PRIMARY KEY(`code`, `locale`)
)COMMENT '手机区域码';

ALTER TABLE t_student ADD COLUMN phone_code VARCHAR(10) null DEFAULT '86' COMMENT '手机区域码' AFTER avatar;
insert into com_phone_code(code, name_tw, name_en, name_zh, locale, order_index, create_time)values
('86','中國','China', '中国','CN', '1', now()),
('244','安哥拉','Angola', '安哥拉','AO', '2', now()),
('93','阿富汗','Afghanistan', '阿富汗','AF', '3', now()),
('355','阿爾巴尼亞','Albania', '阿尔巴尼亚','AL', '4', now()),
('213','阿爾及利亞','Algeria', '阿尔及利亚','DZ', '5', now()),
('376','安道爾共和國','Andorra', '安道尔共和国','AD', '6', now()),
('1264','安圭拉島','Anguilla', '安圭拉岛','AI', '7', now()),
('1268','安提瓜和巴布達','Antigua and Barbuda', '安提瓜和巴布达','AG', '8', now()),
('54','阿根廷','Argentina', '阿根廷','AR', '9', now()),
('374','亞美尼亞','Armenia', '亚美尼亚','AM', '10', now()),
('247','阿森松','Ascension', '阿森松',' ', '11', now()),
('61','澳大利亞','Australia', '澳大利亚','AU', '12', now()),
('43','奧地利','Austria', '奥地利','AT', '13', now()),
('994','阿塞拜疆','Azerbaijan', '阿塞拜疆','AZ', '14', now()),
('1242','巴哈馬','Bahamas', '巴哈马','BS', '15', now()),
('973','巴林','Bahrain', '巴林','BH', '16', now()),
('880','孟加拉國','Bangladesh', '孟加拉国','BD', '17', now()),
('1246','巴巴多斯','Barbados', '巴巴多斯','BB', '18', now()),
('375','白俄羅斯','Belarus', '白俄罗斯','BY', '19', now()),
('32','比利時','Belgium', '比利时','BE', '20', now()),
('501','伯利茲','Belize', '伯利兹','BZ', '21', now()),
('229','貝寧','Benin', '贝宁','BJ', '22', now()),
('1441','百慕達群島','Bermuda Is.', '百慕大群岛','BM', '23', now()),
('591','玻利維亞','Bolivia', '玻利维亚','BO', '24', now()),
('267','博茨瓦納','Botswana', '博茨瓦纳','BW', '25', now()),
('55','巴西','Brazil', '巴西','BR', '26', now()),
('673','文萊','Brunei', '文莱','BN', '27', now()),
('359','保加利亞','Bulgaria', '保加利亚','BG', '28', now()),
('226','布基納法索','Burkina-faso', '布基纳法索','BF', '29', now()),
('95','緬甸','Burma', '缅甸','MM', '30', now()),
('257','布隆迪','Burundi', '布隆迪','BI', '31', now()),
('237','喀麥隆','Cameroon', '喀麦隆','CM', '32', now()),
('1','加拿大','Canada', '加拿大','CA', '33', now()),
('1345','開曼群島','Cayman Is.', '开曼群岛',' ', '34', now()),
('236','中非共和國','Central African Republic', '中非共和国','CF', '35', now()),
('235','乍得','Chad', '乍得','TD', '36', now()),
('56','智利','Chile', '智利','CL', '37', now()),
('57','哥倫比亞','Colombia', '哥伦比亚','CO', '38', now()),
('242','剛果','Congo', '刚果','CG', '39', now()),
('682','庫克群島','Cook Is.', '库克群岛','CK', '40', now()),
('506','哥斯達黎加','Costa Rica', '哥斯达黎加','CR', '41', now()),
('53','古巴','Cuba', '古巴','CU', '42', now()),
('357','塞浦路斯','Cyprus', '塞浦路斯','CY', '43', now()),
('420','捷克','Czech Republic', '捷克','CZ', '44', now()),
('45','丹麥','Denmark', '丹麦','DK', '45', now()),
('253','吉布堤','Djibouti', '吉布提','DJ', '46', now()),
('1890','多明尼加共和國共和國','Dominica Rep.', '多米尼加共和国','DO', '47', now()),
('593','厄瓜多爾','Ecuador', '厄瓜多尔','EC', '48', now()),
('20','埃及','Egypt', '埃及','EG', '49', now()),
('503','薩爾瓦多','EI Salvador', '萨尔瓦多','SV', '50', now()),
('372','愛沙尼亞','Estonia', '爱沙尼亚','EE', '51', now()),
('251','埃塞俄比亞','Ethiopia', '埃塞俄比亚','ET', '52', now()),
('679','斐濟','Fiji', '斐济','FJ', '53', now()),
('358','芬蘭','Finland', '芬兰','FI', '54', now()),
('33','法國','France', '法国','FR', '55', now()),
('594','法屬圭亞那','French Guiana', '法属圭亚那','GF', '56', now()),
('241','加蓬','Gabon', '加蓬','GA', '57', now()),
('220','岡比亞','Gambia', '冈比亚','GM', '58', now()),
('995','格魯吉亞','Georgia', '格鲁吉亚','GE', '59', now()),
('49','德國','Germany', '德国','DE', '60', now()),
('233','加納','Ghana', '加纳','GH', '61', now()),
('350','直布羅陀','Gibraltar', '直布罗陀','GI', '62', now()),
('30','希臘','Greece', '希腊','GR', '63', now()),
('1809','格林納達','Grenada', '格林纳达','GD', '64', now()),
('1671','關島','Guam', '关岛','GU', '65', now()),
('502','危地馬拉','Guatemala', '危地马拉','GT', '66', now()),
('224','幾內亞','Guinea', '几内亚','GN', '67', now()),
('592','圭亞那','Guyana', '圭亚那','GY', '68', now()),
('509','海地','Haiti', '海地','HT', '69', now()),
('504','洪都拉斯','Honduras', '洪都拉斯','HN', '70', now()),
('852','中國香港','Hongkong', '中国香港','HK', '71', now()),
('36','匈牙利','Hungary', '匈牙利','HU', '72', now()),
('354','冰島','Iceland', '冰岛','IS', '73', now()),
('91','印度','India', '印度','IN', '74', now()),
('62','印度尼西亞','Indonesia', '印度尼西亚','ID', '75', now()),
('98','伊朗','Iran', '伊朗','IR', '76', now()),
('964','伊拉克','Iraq', '伊拉克','IQ', '77', now()),
('353','愛爾蘭','Ireland', '爱尔兰','IE', '78', now()),
('972','以色列','Israel', '以色列','IL', '79', now()),
('39','意大利','Italy', '意大利','IT', '80', now()),
('225','科特迪瓦','Ivory Coast', '科特迪瓦',' ', '81', now()),
('1876','牙買加','Jamaica', '牙买加','JM', '82', now()),
('81','日本','Japan', '日本','JP', '83', now()),
('962','約旦','Jordan', '约旦','JO', '84', now()),
('855','柬埔寨','Kampuchea (Cambodia )', '柬埔寨','KH', '85', now()),
('327','哈薩克','Kazakstan', '哈萨克斯坦','KZ', '86', now()),
('254','肯雅','Kenya', '肯尼亚','KE', '87', now()),
('82','韓國','Korea', '韩国','KR', '88', now()),
('965','科威特','Kuwait', '科威特','KW', '89', now()),
('331','吉爾吉斯坦','Kyrgyzstan', '吉尔吉斯坦','KG', '90', now()),
('856','寮國','Laos', '老挝','LA', '91', now()),
('371','拉脫維亞','Latvia', '拉脱维亚','LV', '92', now()),
('961','黎巴嫩','Lebanon', '黎巴嫩','LB', '93', now()),
('266','萊索托','Lesotho', '莱索托','LS', '94', now()),
('231','利比里亞','Liberia', '利比里亚','LR', '95', now()),
('218','利比亞','Libya', '利比亚','LY', '96', now()),
('423','列支敦士登','Liechtenstein', '列支敦士登','LI', '97', now()),
('370','立陶宛','Lithuania', '立陶宛','LT', '98', now()),
('352','盧森堡','Luxembourg', '卢森堡','LU', '99', now()),
('853','中國澳門','Macao', '中国澳门','MO', '100', now()),
('261','馬達加斯加','Madagascar', '马达加斯加','MG', '101', now()),
('265','馬拉維','Malawi', '马拉维','MW', '102', now()),
('60','馬來西亞','Malaysia', '马来西亚','MY', '103', now()),
('960','馬爾代夫','Maldives', '马尔代夫','MV', '104', now()),
('223','馬里','Mali', '马里','ML', '105', now()),
('356','馬爾他','Malta', '马耳他','MT', '106', now()),
('1670','馬里亞那群島','Mariana Is', '马里亚那群岛',' ', '107', now()),
('596','馬提尼克島','Martinique', '马提尼克',' ', '108', now()),
('230','毛里裘斯','Mauritius', '毛里求斯','MU', '109', now()),
('52','墨西哥','Mexico', '墨西哥','MX', '110', now()),
('373','摩爾多瓦','Moldova, Republic of', '摩尔多瓦','MD', '111', now()),
('377','摩納哥','Monaco', '摩纳哥','MC', '112', now()),
('976','蒙古','Mongolia', '蒙古','MN', '113', now()),
('1664','蒙特塞拉特島','Montserrat Is', '蒙特塞拉特岛','MS', '114', now()),
('212','摩洛哥','Morocco', '摩洛哥','MA', '115', now()),
('258','莫桑比克','Mozambique', '莫桑比克','MZ', '116', now()),
('264','納米比亞','Namibia', '纳米比亚','NA', '117', now()),
('674','瑙魯','Nauru', '瑙鲁','NR', '118', now()),
('977','尼泊爾','Nepal', '尼泊尔','NP', '119', now()),
('599','荷屬安地列斯','Netheriands Antilles', '荷属安的列斯',' ', '120', now()),
('31','荷蘭','Netherlands', '荷兰','NL', '121', now()),
('64','新西蘭','New Zealand', '新西兰','NZ', '122', now()),
('505','尼加拉瓜','Nicaragua', '尼加拉瓜','NI', '123', now()),
('227','尼日爾','Niger', '尼日尔','NE', '124', now()),
('234','尼日利亞','Nigeria', '尼日利亚','NG', '125', now()),
('850','朝鮮','North Korea', '朝鲜','KP', '126', now()),
('47','挪威','Norway', '挪威','NO', '127', now()),
('968','阿曼','Oman', '阿曼','OM', '128', now()),
('92','巴基斯坦','Pakistan', '巴基斯坦','PK', '129', now()),
('507','巴拿馬','Panama', '巴拿马','PA', '130', now()),
('675','巴布亞新畿內亞','Papua New Cuinea', '巴布亚新几内亚','PG', '131', now()),
('595','巴拉圭','Paraguay', '巴拉圭','PY', '132', now()),
('51','秘魯','Peru', '秘鲁','PE', '133', now()),
('63','菲律賓','Philippines', '菲律宾','PH', '134', now()),
('48','波蘭','Poland', '波兰','PL', '135', now()),
('689','法屬玻利尼西亞','French Polynesia', '法属玻利尼西亚','PF', '136', now()),
('351','葡萄牙','Portugal', '葡萄牙','PT', '137', now()),
('1787','波多黎各','Puerto Rico', '波多黎各','PR', '138', now()),
('974','卡塔爾','Qatar', '卡塔尔','QA', '139', now()),
('262','留尼旺','Reunion', '留尼旺',' ', '140', now()),
('40','羅馬尼亞','Romania', '罗马尼亚','RO', '141', now()),
('7','俄羅斯','Russia', '俄罗斯','RU', '142', now()),
('1758','聖盧西亞','Saint Lueia', '圣卢西亚','LC', '143', now()),
('1784','聖文森特島','Saint Vincent', '圣文森特岛','VC', '144', now()),
('684','東薩摩亞(美)','Samoa Eastern', '东萨摩亚(美)',' ', '145', now()),
('685','西薩摩亞','Samoa Western', '西萨摩亚',' ', '146', now()),
('378','聖馬力諾','San Marino', '圣马力诺','SM', '147', now()),
('239','聖多美和普林西比','Sao Tome and Principe', '圣多美和普林西比','ST', '148', now()),
('966','沙地阿拉伯','Saudi Arabia', '沙特阿拉伯','SA', '149', now()),
('221','塞內加爾','Senegal', '塞内加尔','SN', '150', now()),
('248','塞舌爾','Seychelles', '塞舌尔','SC', '151', now()),
('232','塞拉利昂','Sierra Leone', '塞拉利昂','SL', '152', now()),
('65','新加坡','Singapore', '新加坡','SG', '153', now()),
('421','斯洛伐克','Slovakia', '斯洛伐克','SK', '154', now()),
('386','斯洛文尼亞','Slovenia', '斯洛文尼亚','SI', '155', now()),
('677','所羅門群島','Solomon Is', '所罗门群岛','SB', '156', now()),
('252','索馬里','Somali', '索马里','SO', '157', now()),
('27','南非','South Africa', '南非','ZA', '158', now()),
('34','西班牙','Spain', '西班牙','ES', '159', now()),
('94','斯裡蘭卡','Sri Lanka', '斯里兰卡','LK', '160', now()),
('249','蘇丹','Sudan', '苏丹','SD', '163', now()),
('597','蘇里南','Suriname', '苏里南','SR', '164', now()),
('268','斯威士蘭','Swaziland', '斯威士兰','SZ', '165', now()),
('46','瑞典','Sweden', '瑞典','SE', '166', now()),
('41','瑞士','Switzerland', '瑞士','CH', '167', now()),
('963','敘利亞','Syria', '叙利亚','SY', '168', now()),
('886','中國台灣','Taiwan', '中国台湾','TW', '169', now()),
('992','塔吉克','Tajikstan', '塔吉克斯坦','TJ', '170', now()),
('255','坦桑尼亞','Tanzania', '坦桑尼亚','TZ', '171', now()),
('66','泰國','Thailand', '泰国','TH', '172', now()),
('228','多哥','Togo', '多哥','TG', '173', now()),
('676','湯加','Tonga', '汤加','TO', '174', now()),
('1809','特立尼達和多巴哥','Trinidad and Tobago', '特立尼达和多巴哥','TT', '175', now()),
('216','突尼斯','Tunisia', '突尼斯','TN', '176', now()),
('90','土耳其','Turkey', '土耳其','TR', '177', now()),
('993','土庫曼','Turkmenistan', '土库曼斯坦','TM', '178', now()),
('256','烏幹達','Uganda', '乌干达','UG', '179', now()),
('380','烏克蘭','Ukraine', '乌克兰','UA', '180', now()),
('971','阿拉伯聯合酋長國','United Arab Emirates', '阿拉伯联合酋长国','AE', '181', now()),
('44','英國','United Kiongdom', '英国','GB', '182', now()),
('1','美國','United States of America', '美国','US', '183', now()),
('598','烏拉圭','Uruguay', '乌拉圭','UY', '184', now()),
('233','烏茲別克','Uzbekistan', '乌兹别克斯坦','UZ', '185', now()),
('58','委內瑞拉','Venezuela', '委内瑞拉','VE', '186', now()),
('84','越南','Vietnam', '越南','VN', '187', now()),
('967','也門','Yemen', '也门','YE', '188', now()),
('381','南斯拉夫','Yugoslavia', '南斯拉夫','YU', '189', now()),
('263','津巴布韋','Zimbabwe', '津巴布韦','ZW', '190', now()),
('243','扎伊爾','Zaire', '扎伊尔','ZR', '191', now()),
('260','贊比亞','Zambia', '赞比亚','ZM', '192', now());

CREATE TABLE app_feedback(
	`id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
	`user_id` VARCHAR(128) NULL COMMENT '用户id',
	`content` TEXT NULL COMMENT '反馈内容',
	`phone` VARCHAR(20) COMMENT '手机号',
	`wechat` VARCHAR(50) COMMENT '微信号',
	`email` VARCHAR(50) COMMENT '邮箱',
	`image1` VARCHAR(512) NULL COMMENT '反馈图片1',
	`image2` VARCHAR(512) NULL COMMENT '反馈图片1',
	`image3` VARCHAR(512) NULL COMMENT '反馈图片1',
	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	PRIMARY KEY(`id`)
)COMMENT '意见反馈';

UPDATE t_video SET duration = '00:04:10' WHERE id = '105' AND poly_vid = '30fe77e88fac6b8cc2da4f62ae417263_3';
UPDATE t_video SET duration = '00:01:26' WHERE id = '226' AND poly_vid = '30fe77e88f788bb154f2a22051095da9_3';
UPDATE t_video SET duration = '00:03:08' WHERE id = '225' AND poly_vid = '30fe77e88f1ae90bf3f4e6a9464f6199_3';
UPDATE t_video SET duration = '00:07:39' WHERE id = '120' AND poly_vid = '30fe77e88f694d7390882ea01ee87b82_3';
UPDATE t_video SET duration = '00:05:11' WHERE id = '201' AND poly_vid = '30fe77e88f2562fa6ffb052eefe003c0_3';
UPDATE t_video SET duration = '00:07:14' WHERE id = '186' AND poly_vid = '30fe77e88f33fa6fed64a79a8b40ffab_3';
UPDATE t_video SET duration = '00:06:58' WHERE id = '204' AND poly_vid = '30fe77e88fd1cc3b421dec692590ff80_3';
UPDATE t_video SET duration = '00:07:28' WHERE id = '135' AND poly_vid = '30fe77e88ff79247581fb056dfd3de53_3';
UPDATE t_video SET duration = '00:06:22' WHERE id = '238' AND poly_vid = '30fe77e88f2eca060a7920d020a25a25_3';
UPDATE t_video SET duration = '00:03:03' WHERE id = '96' AND poly_vid = '30fe77e88f81cd223383f16201cc656d_3';
UPDATE t_video SET duration = '00:07:21' WHERE id = '180' AND poly_vid = '30fe77e88f86bc41c3ab027b4c4b2b6e_3';
UPDATE t_video SET duration = '00:05:21' WHERE id = '253' AND poly_vid = '30fe77e88f7f6ae8b20f970fc6aa8c1c_3';
UPDATE t_video SET duration = '00:05:20' WHERE id = '122' AND poly_vid = '30fe77e88f7a3deba4a963e1890bd436_3';
UPDATE t_video SET duration = '00:09:03' WHERE id = '179' AND poly_vid = '30fe77e88faec279f2fd50ce2071717a_3';
UPDATE t_video SET duration = '00:05:47' WHERE id = '207' AND poly_vid = '30fe77e88f237cc8ff5faee523c4f77e_3';
UPDATE t_video SET duration = '00:03:44' WHERE id = '91' AND poly_vid = '30fe77e88f5471ae6f70a8577f900d65_3';
UPDATE t_video SET duration = '00:10:23' WHERE id = '147' AND poly_vid = '30fe77e88f0d60164afd5d758a7533e5_3';
UPDATE t_video SET duration = '00:03:27' WHERE id = '242' AND poly_vid = '30fe77e88f3b057ad4653102add27660_3';
UPDATE t_video SET duration = '00:03:37' WHERE id = '84' AND poly_vid = '30fe77e88f3cc25fd9b74f8f1ad42de2_3';
UPDATE t_video SET duration = '00:05:51' WHERE id = '260' AND poly_vid = '30fe77e88f68f95956163fc88181bea2_3';
UPDATE t_video SET duration = '00:08:18' WHERE id = '193' AND poly_vid = '30fe77e88fb462a0c5c20275798c5483_3';
UPDATE t_video SET duration = '00:01:52' WHERE id = '224' AND poly_vid = '30fe77e88f6ad86186809c443e58b244_3';
UPDATE t_video SET duration = '00:04:04' WHERE id = '162' AND poly_vid = '30fe77e88fe3cd30ad56198863bdd082_3';
UPDATE t_video SET duration = '00:10:10' WHERE id = '144' AND poly_vid = '30fe77e88f552c97f25e9cb91b831300_3';
UPDATE t_video SET duration = '00:09:01' WHERE id = '150' AND poly_vid = '30fe77e88f6368d9b12be2335be33e20_3';
UPDATE t_video SET duration = '00:06:01' WHERE id = '222' AND poly_vid = '30fe77e88ff9c990c6df304d29009bd5_3';
UPDATE t_video SET duration = '00:05:51' WHERE id = '85' AND poly_vid = '30fe77e88f7ae31b69b874d5e5a0b3e7_3';
UPDATE t_video SET duration = '00:02:00' WHERE id = '89' AND poly_vid = '30fe77e88ffa7aaf6637905e9d48a789_3';
UPDATE t_video SET duration = '00:07:25' WHERE id = '142' AND poly_vid = '30fe77e88fb0dc437b0a9bb5bde3c969_3';
UPDATE t_video SET duration = '00:11:06' WHERE id = '173' AND poly_vid = '30fe77e88f57331e46195e6b43dcac86_3';
UPDATE t_video SET duration = '00:05:05' WHERE id = '112' AND poly_vid = '30fe77e88febb347f4c04cbaa36d4c8a_3';
UPDATE t_video SET duration = '00:06:26' WHERE id = '126' AND poly_vid = '30fe77e88f68c3947f3132cfd6a86fa8_3';
UPDATE t_video SET duration = '00:06:59' WHERE id = '153' AND poly_vid = '30fe77e88fdc1992a04bed7cdbf504a7_3';
UPDATE t_video SET duration = '00:14:08' WHERE id = '181' AND poly_vid = '30fe77e88ff0d4586ec1b3e83300247f_3';
UPDATE t_video SET duration = '00:06:11' WHERE id = '121' AND poly_vid = '30fe77e88f22c446025d2ff33a588f68_3';
UPDATE t_video SET duration = '00:03:20' WHERE id = '230' AND poly_vid = '30fe77e88f8dcf1f83d783edefbb8c8c_3';
UPDATE t_video SET duration = '00:08:36' WHERE id = '202' AND poly_vid = '30fe77e88f9577109699c3352a7e8545_3';
UPDATE t_video SET duration = '00:08:12' WHERE id = '189' AND poly_vid = '30fe77e88f36ea2e2598f02c1b6099b6_3';
UPDATE t_video SET duration = '00:10:58' WHERE id = '182' AND poly_vid = '30fe77e88f9383ce162864615e3f94c0_3';
UPDATE t_video SET duration = '00:04:11' WHERE id = '245' AND poly_vid = '30fe77e88f4f1e0af295f7a7c1159d23_3';
UPDATE t_video SET duration = '00:11:00' WHERE id = '266' AND poly_vid = '30fe77e88fd11b5959e90951c9515c14_3';
UPDATE t_video SET duration = '00:06:55' WHERE id = '252' AND poly_vid = '30fe77e88fb216bb153d5e499a230f97_3';
UPDATE t_video SET duration = '00:03:38' WHERE id = '88' AND poly_vid = '30fe77e88ff8e666c8135fed1fb4e78e_3';
UPDATE t_video SET duration = '00:05:09' WHERE id = '109' AND poly_vid = '30fe77e88fc5200a2dba4ec64410a2ab_3';
UPDATE t_video SET duration = '00:05:51' WHERE id = '214' AND poly_vid = '30fe77e88f58b1595cc34b60ad0291ad_3';
UPDATE t_video SET duration = '00:04:53' WHERE id = '80' AND poly_vid = '30fe77e88f6de5ae918dc1835bedbee0_3';
UPDATE t_video SET duration = '00:04:11' WHERE id = '102' AND poly_vid = '30fe77e88fb8ca87151f896f1c5bcdb8_3';
UPDATE t_video SET duration = '00:06:55' WHERE id = '136' AND poly_vid = '30fe77e88f32349708d6abe8b1bccc1f_3';
UPDATE t_video SET duration = '00:09:27' WHERE id = '134' AND poly_vid = '30fe77e88fc5f8acec95465afc9c9c59_3';
UPDATE t_video SET duration = '00:06:26' WHERE id = '116' AND poly_vid = '30fe77e88f5b2a37c194b1bdcab80d57_3';
UPDATE t_video SET duration = '00:11:48' WHERE id = '184' AND poly_vid = '30fe77e88f57676543bca4c6d728a65e_3';
UPDATE t_video SET duration = '00:03:46' WHERE id = '227' AND poly_vid = '30fe77e88f08fad56f9080396fabc352_3';
UPDATE t_video SET duration = '00:10:52' WHERE id = '163' AND poly_vid = '30fe77e88fc03bf7af85e41f7f0d2147_3';
UPDATE t_video SET duration = '00:09:03' WHERE id = '178' AND poly_vid = '30fe77e88f76d5df7d444c17bbc8e8f6_3';
UPDATE t_video SET duration = '00:09:25' WHERE id = '191' AND poly_vid = '30fe77e88f9571eaa8aad1140c7fe1d8_3';
UPDATE t_video SET duration = '00:06:00' WHERE id = '97' AND poly_vid = '30fe77e88f62768b4aeab8e820152a91_3';
UPDATE t_video SET duration = '00:05:10' WHERE id = '128' AND poly_vid = '30fe77e88fb0b64ec0cd30b6d636bbbb_3';
UPDATE t_video SET duration = '00:06:53' WHERE id = '190' AND poly_vid = '30fe77e88fb7564235a7b4459443e517_3';
UPDATE t_video SET duration = '00:08:12' WHERE id = '199' AND poly_vid = '30fe77e88fdb0a97929e8777e30b704c_3';
UPDATE t_video SET duration = '00:05:06' WHERE id = '125' AND poly_vid = '30fe77e88faf4aa39e0802d65378ce6a_3';
UPDATE t_video SET duration = '00:04:33' WHERE id = '251' AND poly_vid = '30fe77e88f84a69ad8e84ec6228d7e23_3';
UPDATE t_video SET duration = '00:05:30' WHERE id = '213' AND poly_vid = '30fe77e88f0184e331d9a2c183310540_3';
UPDATE t_video SET duration = '00:09:33' WHERE id = '206' AND poly_vid = '30fe77e88f78c12bc3689542de4af18c_3';
UPDATE t_video SET duration = '00:07:05' WHERE id = '164' AND poly_vid = '30fe77e88fd4602dfed02489ad202ce7_3';
UPDATE t_video SET duration = '00:04:20' WHERE id = '205' AND poly_vid = '30fe77e88fe43fda0f43f182a45fc0bd_3';
UPDATE t_video SET duration = '00:07:24' WHERE id = '254' AND poly_vid = '30fe77e88fbe57118415e0657151ca51_3';
UPDATE t_video SET duration = '00:06:12' WHERE id = '166' AND poly_vid = '30fe77e88f88e506d9f2c5cad7f42236_3';
UPDATE t_video SET duration = '00:02:40' WHERE id = '103' AND poly_vid = '30fe77e88f424370c2d102962a3603ad_3';
UPDATE t_video SET duration = '00:03:05' WHERE id = '208' AND poly_vid = '30fe77e88fb77bd4dc9b0350aae8c99b_3';
UPDATE t_video SET duration = '00:10:26' WHERE id = '221' AND poly_vid = '30fe77e88fdf1d308e4bb0db18eab9c4_3';
UPDATE t_video SET duration = '00:09:47' WHERE id = '140' AND poly_vid = '30fe77e88f60d05efbdc14e7f6fb50fc_3';
UPDATE t_video SET duration = '00:02:39' WHERE id = '101' AND poly_vid = '30fe77e88f17b360e028453c0f73cff3_3';
UPDATE t_video SET duration = '00:11:22' WHERE id = '159' AND poly_vid = '30fe77e88f22479e7f2504d0c0315e52_3';
UPDATE t_video SET duration = '00:04:57' WHERE id = '235' AND poly_vid = '30fe77e88f00597239c21dc287c67ec3_3';
UPDATE t_video SET duration = '00:08:44' WHERE id = '174' AND poly_vid = '30fe77e88ff653e6b4a5fac7e7e8f002_3';
UPDATE t_video SET duration = '00:05:32' WHERE id = '237' AND poly_vid = '30fe77e88fa556d913938690c377aade_3';
UPDATE t_video SET duration = '00:04:03' WHERE id = '93' AND poly_vid = '30fe77e88f3673a1e8ea10994fae50fa_3';
UPDATE t_video SET duration = '00:03:12' WHERE id = '172' AND poly_vid = '30fe77e88fd7a088c93f328a9e5549d8_3';
UPDATE t_video SET duration = '00:05:03' WHERE id = '130' AND poly_vid = '30fe77e88f15775d48bd0652b7fa36fb_3';
UPDATE t_video SET duration = '00:05:31' WHERE id = '248' AND poly_vid = '30fe77e88f4d6fbe37cefba557291691_3';
UPDATE t_video SET duration = '00:09:15' WHERE id = '143' AND poly_vid = '30fe77e88f76cdeae330b88ea56518ca_3';
UPDATE t_video SET duration = '00:05:35' WHERE id = '183' AND poly_vid = '30fe77e88fbd4e61f33c13a740df0b1c_3';
UPDATE t_video SET duration = '00:07:09' WHERE id = '250' AND poly_vid = '30fe77e88f27422fe81074382ea3c153_3';
UPDATE t_video SET duration = '00:04:33' WHERE id = '241' AND poly_vid = '30fe77e88ffa6c8c1a3725b28580218d_3';
UPDATE t_video SET duration = '00:03:19' WHERE id = '246' AND poly_vid = '30fe77e88f8605ecc89b8c2997be1fce_3';
UPDATE t_video SET duration = '00:05:43' WHERE id = '111' AND poly_vid = '30fe77e88fa944985e4267a0d1b1b170_3';
UPDATE t_video SET duration = '00:09:49' WHERE id = '156' AND poly_vid = '30fe77e88fb4e5576d2d75030400ddc7_3';
UPDATE t_video SET duration = '00:13:59' WHERE id = '177' AND poly_vid = '30fe77e88fe8f84f51c76719b518da9f_3';
UPDATE t_video SET duration = '00:05:14' WHERE id = '113' AND poly_vid = '30fe77e88f2aa647eecae83aa80a5076_3';
UPDATE t_video SET duration = '00:05:07' WHERE id = '124' AND poly_vid = '30fe77e88fc9c75a1e0b68ec04496c37_3';
UPDATE t_video SET duration = '00:09:01' WHERE id = '149' AND poly_vid = '30fe77e88f7bdeab84cc23fb6aaa2cea_3';
UPDATE t_video SET duration = '00:04:41' WHERE id = '187' AND poly_vid = '30fe77e88faac258730ef2f84e841693_3';
UPDATE t_video SET duration = '00:09:16' WHERE id = '148' AND poly_vid = '30fe77e88f96a3bda0cb22cd5b62fd6b_3';
UPDATE t_video SET duration = '00:09:29' WHERE id = '157' AND poly_vid = '30fe77e88faf4ab6f378a93818911c8e_3';
UPDATE t_video SET duration = '00:04:50' WHERE id = '234' AND poly_vid = '30fe77e88fd772520e140466f0331e50_3';
UPDATE t_video SET duration = '00:06:01' WHERE id = '110' AND poly_vid = '30fe77e88f1dd796dc45519980772832_3';
UPDATE t_video SET duration = '00:07:34' WHERE id = '165' AND poly_vid = '30fe77e88ff49bc9810c22b0c1e95e22_3';
UPDATE t_video SET duration = '00:08:10' WHERE id = '261' AND poly_vid = '30fe77e88f57b17240fb5dffbfd34d3b_3';
UPDATE t_video SET duration = '00:06:17' WHERE id = '118' AND poly_vid = '30fe77e88f1d58c84f421aff4ba8af3b_3';
UPDATE t_video SET duration = '00:09:05' WHERE id = '151' AND poly_vid = '30fe77e88ff82b67f1b1daa97407a990_3';
UPDATE t_video SET duration = '00:01:57' WHERE id = '98' AND poly_vid = '30fe77e88fdaa94e8f29a9b7c0e726c3_3';
UPDATE t_video SET duration = '00:06:17' WHERE id = '255' AND poly_vid = '30fe77e88f0310e429e59dab53dfc59f_3';
UPDATE t_video SET duration = '00:02:46' WHERE id = '87' AND poly_vid = '30fe77e88f984c96e23707f547211d72_3';
UPDATE t_video SET duration = '00:06:08' WHERE id = '168' AND poly_vid = '30fe77e88f9692dbf69585820dad48d6_3';
UPDATE t_video SET duration = '00:05:26' WHERE id = '129' AND poly_vid = '30fe77e88f0b0619f1806b291ff2bdb6_3';
UPDATE t_video SET duration = '00:05:08' WHERE id = '131' AND poly_vid = '30fe77e88fa5481f3f5970bb612d4c1c_3';
UPDATE t_video SET duration = '00:08:23' WHERE id = '155' AND poly_vid = '30fe77e88feee37c1c9b97966bd63516_3';
UPDATE t_video SET duration = '00:05:49' WHERE id = '231' AND poly_vid = '30fe77e88fc024ebbe04fd9186d2df75_3';
UPDATE t_video SET duration = '00:04:15' WHERE id = '90' AND poly_vid = '30fe77e88ffe2525df1e6cfb0f2d3c6e_3';
UPDATE t_video SET duration = '00:11:01' WHERE id = '176' AND poly_vid = '30fe77e88fc11ae26c91581428bb356e_3';
UPDATE t_video SET duration = '00:03:25' WHERE id = '228' AND poly_vid = '30fe77e88f53f810d0ca233251ecff0a_3';
UPDATE t_video SET duration = '00:05:17' WHERE id = '117' AND poly_vid = '30fe77e88f8564efc9cdaa814105953b_3';
UPDATE t_video SET duration = '00:08:13' WHERE id = '258' AND poly_vid = '30fe77e88f6540c4c456c62931ff7e9d_3';
UPDATE t_video SET duration = '00:05:06' WHERE id = '215' AND poly_vid = '30fe77e88fc785257b03e82c0fc3dc8f_3';
UPDATE t_video SET duration = '00:03:34' WHERE id = '244' AND poly_vid = '30fe77e88f1a1b5063622bd7fadb2e56_3';
UPDATE t_video SET duration = '00:05:00' WHERE id = '171' AND poly_vid = '30fe77e88f1d4b0c490982fb98323a34_3';
UPDATE t_video SET duration = '00:06:03' WHERE id = '119' AND poly_vid = '30fe77e88fb06ded1096fa9d4b22251b_3';
UPDATE t_video SET duration = '00:07:14' WHERE id = '247' AND poly_vid = '30fe77e88fca34dc8c9d12e02e0c577e_3';
UPDATE t_video SET duration = '00:03:37' WHERE id = '161' AND poly_vid = '30fe77e88fee14238156ce9c859b83e4_3';
UPDATE t_video SET duration = '00:06:56' WHERE id = '104' AND poly_vid = '30fe77e88f4da37943f82d18912e2162_3';
UPDATE t_video SET duration = '00:07:09' WHERE id = '137' AND poly_vid = '30fe77e88ffcf052b5cb5e5cd2681be9_3';
UPDATE t_video SET duration = '00:05:52' WHERE id = '167' AND poly_vid = '30fe77e88f502e90f5b31bc2b5af5d11_3';
UPDATE t_video SET duration = '00:05:57' WHERE id = '232' AND poly_vid = '30fe77e88f2508f007443834492786f4_3';
UPDATE t_video SET duration = '00:05:57' WHERE id = '194' AND poly_vid = '30fe77e88fcc962822b5ad694fcdc11d_3';
UPDATE t_video SET duration = '00:08:32' WHERE id = '219' AND poly_vid = '30fe77e88f2ece14d3279d1ff5ccb77e_3';
UPDATE t_video SET duration = '00:08:02' WHERE id = '209' AND poly_vid = '30fe77e88f8fb84d11c1dd79334bc1aa_3';
UPDATE t_video SET duration = '00:03:21' WHERE id = '229' AND poly_vid = '30fe77e88f35132bfeed609b047fec51_3';
UPDATE t_video SET duration = '00:02:05' WHERE id = '86' AND poly_vid = '30fe77e88fb7488f993c7dfdf00db41a_3';
UPDATE t_video SET duration = '00:03:01' WHERE id = '100' AND poly_vid = '30fe77e88f0668a2eab5bf8a0546ce30_3';
UPDATE t_video SET duration = '00:05:13' WHERE id = '123' AND poly_vid = '30fe77e88f452c92d5cd50f6efcf4c48_3';
UPDATE t_video SET duration = '00:05:46' WHERE id = '127' AND poly_vid = '30fe77e88fbdea838413fba3142b37ee_3';
UPDATE t_video SET duration = '00:04:58' WHERE id = '240' AND poly_vid = '30fe77e88f9e1be6867b84ff09325291_3';
UPDATE t_video SET duration = '00:10:40' WHERE id = '262' AND poly_vid = '30fe77e88f737008b5c9f9bc080eddee_3';
UPDATE t_video SET duration = '00:08:33' WHERE id = '210' AND poly_vid = '30fe77e88fa128df5dc6821e647df2cc_3';
UPDATE t_video SET duration = '00:05:58' WHERE id = '239' AND poly_vid = '30fe77e88fe3618ea213a4a0609b79b2_3';
UPDATE t_video SET duration = '00:04:37' WHERE id = '175' AND poly_vid = '30fe77e88f3f694123a1acb89000c139_3';
UPDATE t_video SET duration = '00:06:14' WHERE id = '198' AND poly_vid = '30fe77e88f110d30ca9296820e36f232_3';
UPDATE t_video SET duration = '00:07:58' WHERE id = '108' AND poly_vid = '30fe77e88f9fc0b2c56749d60f64c70a_3';
UPDATE t_video SET duration = '00:08:23' WHERE id = '145' AND poly_vid = '30fe77e88f3eb8ceec3be6ca12b20b08_3';
UPDATE t_video SET duration = '00:03:02' WHERE id = '99' AND poly_vid = '30fe77e88f66bb233d8b888805024ca3_3';
UPDATE t_video SET duration = '00:12:37' WHERE id = '192' AND poly_vid = '30fe77e88f1e270e6c31eb82f56db725_3';
UPDATE t_video SET duration = '00:04:46' WHERE id = '211' AND poly_vid = '30fe77e88f3255997210814a1baa9c85_3';
UPDATE t_video SET duration = '00:03:18' WHERE id = '92' AND poly_vid = '30fe77e88f35438ee7274157ce350359_3';
UPDATE t_video SET duration = '00:05:21' WHERE id = '265' AND poly_vid = '30fe77e88f092737f00289386c3504e7_3';
UPDATE t_video SET duration = '00:08:36' WHERE id = '200' AND poly_vid = '30fe77e88fd7421db74be505d2cf1d1e_3';
UPDATE t_video SET duration = '00:08:03' WHERE id = '169' AND poly_vid = '30fe77e88f5b5103b4d1a961b0df815c_3';
UPDATE t_video SET duration = '00:05:40' WHERE id = '196' AND poly_vid = '30fe77e88f87232184b350d0c1a0ec7d_3';
UPDATE t_video SET duration = '00:05:56' WHERE id = '257' AND poly_vid = '30fe77e88fc2042947c10e1a9210ef03_3';
UPDATE t_video SET duration = '00:08:10' WHERE id = '158' AND poly_vid = '30fe77e88fdb8de8ae1630e000533f78_3';
UPDATE t_video SET duration = '00:06:29' WHERE id = '263' AND poly_vid = '30fe77e88f7bbb96fffbe14dbeb01f97_3';
UPDATE t_video SET duration = '00:07:10' WHERE id = '152' AND poly_vid = '30fe77e88f4db905086b9e898624a666_3';
UPDATE t_video SET duration = '00:06:12' WHERE id = '212' AND poly_vid = '30fe77e88f09c0d972e50ade2f48914b_3';
UPDATE t_video SET duration = '00:07:45' WHERE id = '233' AND poly_vid = '30fe77e88fc3a8b36d9a88763f5ebc86_3';
UPDATE t_video SET duration = '00:03:12' WHERE id = '95' AND poly_vid = '30fe77e88f29bafbbca87a25c43ef308_3';
UPDATE t_video SET duration = '00:08:34' WHERE id = '139' AND poly_vid = '30fe77e88f47ec2f99296101ce33f310_3';
UPDATE t_video SET duration = '00:07:09' WHERE id = '216' AND poly_vid = '30fe77e88fac83d3e14e5d6af92cdda0_3';
UPDATE t_video SET duration = '00:09:37' WHERE id = '170' AND poly_vid = '30fe77e88fb0530c902dde732c7f7d54_3';
UPDATE t_video SET duration = '00:05:42' WHERE id = '195' AND poly_vid = '30fe77e88f44eec3445642d9ba899954_3';
UPDATE t_video SET duration = '00:05:04' WHERE id = '236' AND poly_vid = '30fe77e88f04e669485ad3e3bfd8a16f_3';
UPDATE t_video SET duration = '00:02:47' WHERE id = '82' AND poly_vid = '30fe77e88f933438ccf0f836f5a5fd36_3';
UPDATE t_video SET duration = '00:05:54' WHERE id = '185' AND poly_vid = '30fe77e88f7d3c6753cc40402e9174af_3';
UPDATE t_video SET duration = '00:05:02' WHERE id = '256' AND poly_vid = '30fe77e88f68a71bf96ddca4dfc3911f_3';
UPDATE t_video SET duration = '00:06:33' WHERE id = '141' AND poly_vid = '30fe77e88f8b25410ac8f8431913b0f3_3';
UPDATE t_video SET duration = '00:02:31' WHERE id = '106' AND poly_vid = '30fe77e88f674d3ae7ba65bcd0fa36ac_3';
UPDATE t_video SET duration = '00:12:35' WHERE id = '259' AND poly_vid = '30fe77e88fbc92da3012daf9745e25dc_3';
UPDATE t_video SET duration = '00:03:24' WHERE id = '94' AND poly_vid = '30fe77e88f51f9a8e4720c4e97d0a1aa_3';
UPDATE t_video SET duration = '00:03:48' WHERE id = '217' AND poly_vid = '30fe77e88fe33a0350f314f37af29582_3';
UPDATE t_video SET duration = '00:05:19' WHERE id = '249' AND poly_vid = '30fe77e88fa21af323c897608951944e_3';
UPDATE t_video SET duration = '00:08:12' WHERE id = '203' AND poly_vid = '30fe77e88f82db9ba22127ed52608107_3';
UPDATE t_video SET duration = '00:08:46' WHERE id = '223' AND poly_vid = '30fe77e88f3dc62c43bcbff600fcbe29_3';
UPDATE t_video SET duration = '00:10:25' WHERE id = '197' AND poly_vid = '30fe77e88f3ab66e33df506f6c367673_3';
UPDATE t_video SET duration = '00:04:55' WHERE id = '83' AND poly_vid = '30fe77e88f409ca287c10ddd5f8413c3_3';
UPDATE t_video SET duration = '00:05:59' WHERE id = '220' AND poly_vid = '30fe77e88fa9793d8f995edd6b457e6f_3';
UPDATE t_video SET duration = '00:06:29' WHERE id = '264' AND poly_vid = '30fe77e88fc1d37b8adc4ff2d4b082a8_3';
UPDATE t_video SET duration = '00:07:33' WHERE id = '114' AND poly_vid = '30fe77e88fb5cc686eb1c43c6662cd3b_3';
UPDATE t_video SET duration = '00:05:55' WHERE id = '218' AND poly_vid = '30fe77e88f0c883b71e2feede22b6875_3';
UPDATE t_video SET duration = '00:05:06' WHERE id = '115' AND poly_vid = '30fe77e88fe0527b4b63acbc40d314df_3';
UPDATE t_video SET duration = '00:02:52' WHERE id = '132' AND poly_vid = '30fe77e88fce03cac64501b1528a1de8_3';
UPDATE t_video SET duration = '00:11:50' WHERE id = '188' AND poly_vid = '30fe77e88fc511ae33d28f0c177adaf8_3';
UPDATE t_video SET duration = '00:13:16' WHERE id = '107' AND poly_vid = '30fe77e88f65e8e358cdb8373ad24827_3';
UPDATE t_video SET duration = '00:08:35' WHERE id = '154' AND poly_vid = '30fe77e88fbea406bca9044335af8fbd_3';


UPDATE t_point p
JOIN t_video v ON v.point_id = p.id
SET p.video_duration = v.duration;
