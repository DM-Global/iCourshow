package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.PictureBean;
import com.ics.cmsadmin.modules.basic.dao.PictureDao;
import com.ics.cmsadmin.modules.basic.service.PictureService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class PictureServiceImpl implements PictureService {

    @Resource
    private PictureDao pictureDao;

    @Override
    public PictureBean queryById(Long id) {
        return pictureDao.queryById(id);
    }

    @Override
    public boolean save(PictureBean t) {
        if (t.getId() == null) {
            return pictureDao.insert(t) == 1;
        } else {
            return pictureDao.update(t.getId(), t) == 1;
        }
    }

    @Override
    public PageResult list(PictureBean t, PageBean page) {
        long count = pictureDao.count(t);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        List<PictureBean> dataList = pictureDao.list(t, page);
        return PageResult.getPage(count, dataList);
    }

    @Override
    public boolean delete(Long id) {
        return pictureDao.delete(id) == 1;
    }
}
