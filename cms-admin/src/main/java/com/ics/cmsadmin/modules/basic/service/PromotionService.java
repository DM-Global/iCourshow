package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.basic.bean.PromotionBean;


/**
 * t_promotion 服务类
 * Created by sandwich on 2018-12-06 21:12:37.
 */
public interface PromotionService extends BaseService<PromotionBean>, BaseDataService<PromotionBean> {
}
