package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.agent.bean.AgentShareRuleBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;


/**
 * a_agent_share_rule 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface AgentShareRuleService {
    /**
     * 通过代理商编号获取代理商分润规则
     */
    AgentShareRuleBean queryByAgentNo(String agentNo);

    /**
     * 根据主键id查询bean
     *
     * @param id 主键id
     * @return config
     */
    AgentShareRuleBean queryById(String id);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    boolean insert(AgentShareRuleBean t);

    /**
     * 根据id更新数据
     *
     * @param t 更新数据
     * @return 影响条数
     */
    boolean update(String id, AgentShareRuleBean t);
}
