package com.ics.cmsadmin.frame.api;

import lombok.Data;

@Data
public class WechatApiResponse {
    private String accessToken;
    private String expiresIn;
    private String refreshToken;
    private String openid;
    private String scope;
    private int errcode;
    private String errmsg;
    private String nickname;
    private int sex;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private String[] privilege;
    private String unionid;
    private int subscribe;
}
