package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.basic.service.impl.VideoHistoryServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=dev"})
public class VideoHistoryServiceTest {

    @Autowired
    VideoHistoryServiceImpl videoHistoryService;

    @Test
    public void listByUserId() {
        List<VideoHistoryBean> list = videoHistoryService.listByUserId("d86f39a318cb451689e71ddefcccdedb");
    }
}
