package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.CourseAttributeBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * t_course_attribute
 * Created by sandwich on 2018-56-10 07:12:28.
 */
@Repository
@Mapper
public interface CourseAttributeDao extends BaseDao<CourseAttributeBean>, BaseDataDao<CourseAttributeBean> {
}
