insert  into `com_register`(`register_id`,`value`,`description`,`create_time`,`update_time`) values
('agent_default_role_id','R10000002','新增代理商时默认的用户角色','2018-10-11 08:44:27','2018-10-11 08:44:42'),
('apply_withdraw_cash_interval_day','15','提现间隔时间(d)','2018-10-10 14:15:46','2018-10-10 14:32:41'),
('apply_withdraw_cash_max_count','1','最多的提现申请记录数','2018-10-10 14:15:31','2018-10-10 14:32:36'),
('apply_withdraw_cash_min_money','100','申请提现最小金额','2018-10-10 14:30:38','2018-10-10 14:34:09'),
('one_agent_share_rate','1','一级代理商可以得到分润比例(%)','2018-10-10 14:29:28','2018-10-10 14:32:31'),
('withdraw_cash_interval_max_accept_count','1','提现时间间隔中 最大的接收提现数量','2018-10-10 14:16:24','2018-10-10 14:32:26'),
('wechat_base_url','http://weixin.icourshow.com/#/home','公众号首页url','2018-10-08 17:21:23','2018-10-09 08:26:19');
