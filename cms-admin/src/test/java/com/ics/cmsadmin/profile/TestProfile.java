package com.ics.cmsadmin.profile;

import com.ics.cmsadmin.frame.profit.ExpressionConstant;
import com.ics.cmsadmin.frame.profit.ProfitTypeEnum;
import com.ics.cmsadmin.frame.profit.ProfitUtils;
import com.ics.cmsadmin.frame.profit.role.ProfitRole;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * 测试分润规则
 */
public class TestProfile {

    private String expression = "5%<100<50.0%<200<99.1%";

    @Test
    public void test2() {
        boolean matches = expression.matches(ExpressionConstant.GRADIENT_RATE_EXPRESSION_EXCLUDE_RIGHT);
        System.out.println(matches);
    }
    @Test
    public void test() {
        ProfitRole profitRole = ProfitUtils.getProfitRole(ProfitTypeEnum.GRADIENT_RATE, expression);
        String[] moneys = {
            "100", "99", "199", "500", "1000"
        };
        long[] studentNum = {
            0, 99, 100, 101, 199, 200, 201
        };

        for (long student : studentNum) {
            for (String money : moneys) {
                System.out.println(profitRole.getProfitInfo(new BigDecimal(money), BigDecimal.valueOf(student)));
            }
        }

    }

    @Test
    public void test1() {
        ProfitRole profitRole = ProfitUtils.getProfitRole(ProfitTypeEnum.GRADIENT_RATE, "5%");
        Object data = profitRole.getData();
        System.out.println(data);
    }
}
