package com.ics.cmsadmin.modules.sso.handler;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.modules.app.bean.AppInfoBean;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.sso.LoginHandler;
import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * android app 游客登陆
 * Created by 666666 on 2018/8/25.
 */
@Log4j2
@Component
public class TouristLoginHandler implements LoginHandler<StudentBean> {

    @Resource(name = "newStudentService")
    private StudentService studentService;

    @Override
    public StudentBean dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum) {
        AppInfoBean appInfo = SsoUtils.getAppInfo(request);
        if (appInfo == null || StringUtils.isBlank(appInfo.getDeviceId())) {
            return null;
        }
        StudentBean student = StudentBean.builder()
            .agentNo("0")
            .registerSource(Constants.REGISTER_SOURCE_TOURIST)
            .nickname("游客")
            .username("游客")
            .deviceId(appInfo.getDeviceId())
            .build();
        StudentBean existStudent = studentService.refreshStudentInfoByTourist(student);
        existStudent.setHasPassword(StringUtils.isNotBlank(existStudent.getPassword()));
        existStudent.setPassword(null);
        SsoUtils.saveLoginInfo2Redis(loginTypeEnum, existStudent, 30 * 24 * 3600L);
        return existStudent;
    }
}
