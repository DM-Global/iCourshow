package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.UnitBean;
import com.ics.cmsadmin.modules.basic.service.UnitService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/unit")
public class ApiUnitController {

    @Autowired
    private UnitService unitService;

    @ApiOperation(value = "分页查询单元信息")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody UnitBean unitBean,
                            @ApiParam("页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                            @ApiParam("每页条数") @RequestParam(value = "pageSize", defaultValue = "2147483647") Integer pageSize) {
        PageBean page = new PageBean(pageNo, pageSize);
        unitBean.setIsActive(true);
        PageResult pageResult = unitService.list(unitBean, page);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @GetMapping("/query/{id}")
    public ApiResponse detail(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        UnitBean unit = unitService.unitDetail(id);
        return new ApiResponse(unitService.unitDetailAdditional(userId, unit));
    }
}
