package com.ics.cmsadmin.modules.auth.service.impl;

import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.modules.auth.bean.SysAuthorize;
import com.ics.cmsadmin.modules.auth.bean.SysControllerAuthorize;
import com.ics.cmsadmin.modules.auth.service.AuthorizeService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ics.cmsadmin.modules.auth.steward.AuthRepositories.authorizeDao;

/**
 * Created by lvsw on 2017-10-06.
 */
@Service
public class AuthorizeServiceImpl implements AuthorizeService {

    @CacheDbMember(returnClass = SysAuthorize.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public PageResult<SysAuthorize> list(SysAuthorize bean, PageBean page) {
        long count = authorizeDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;

        return PageResult.getPage(count, authorizeDao.list(bean, page));
    }

    @CacheDbMember(returnClass = String.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<String> listAuthorizesByUserId(String userId) {
        if (StringUtils.isBlank(userId)) {
            return null;
        }
        return authorizeDao.listAuthorizesByUserId(userId);
    }

    @CacheDbMember(returnClass = SysAuthorize.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<SysAuthorize> listUserAuthorizesByUserId(String userId) {
        if (StringUtils.isBlank(userId)) {
            return null;
        }
        return authorizeDao.listUserAuthorizesByUserId(userId);
    }

    @Override
    public void batchAddControllerAuthorize(List<SysControllerAuthorize> list) {
        authorizeDao.deleteAllControllerAuthorize();
        if (!CollectionUtils.isEmpty(list)) {
            authorizeDao.batchAddControllerAuthorize(list);
        }
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public void batchAddAuthorizeInfo(List<SysAuthorize> list) {
        authorizeDao.deleteAllAuthorizeInfo();
        if (!CollectionUtils.isEmpty(list)) {
            authorizeDao.batchAddAuthorizeInfo(list);
        }
    }

    @CacheDbMember(returnClass = SysAuthorize.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<SysAuthorize> listAuthorizeByRoleId(String roleId) {
        if (StringUtils.isBlank(roleId)) {
            return null;
        }
        return authorizeDao.listAuthorizeByRoleId(roleId);
    }

    @CacheDbMember(returnClass = SysAuthorize.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<SysAuthorize> listNotBelong2RoleAuthorizes(String roleId) {
        if (StringUtils.isBlank(roleId)) {
            return null;
        }
        return authorizeDao.listNotBelong2RoleAuthorizes(roleId);
    }

    @CacheDbMember(returnClass = SysAuthorize.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<SysAuthorize> listAuthorizeByMenuId(String menuId) {
        return authorizeDao.listAuthorizeByMenuId(menuId);
    }

    @Override
    public Map<String, List<SysAuthorize>> listByMenuIds(List<String> menuIds) {
        List<SysAuthorize> authorizeBeans = authorizeDao.listByMenuIds(menuIds);
        Map<String, List<SysAuthorize>> map = new HashMap<>();
        for (SysAuthorize bean : authorizeBeans) {
            List<SysAuthorize> orDefault = map.getOrDefault(bean.getMenuId(), new ArrayList<>());
            orDefault.add(bean);
            map.put(bean.getMenuId(), orDefault);
        }
        return map;
    }
}
