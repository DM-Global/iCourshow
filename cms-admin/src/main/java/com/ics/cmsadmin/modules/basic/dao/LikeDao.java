package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.Like;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LikeDao {

    int insert(Like like);

    Boolean select(@Param("userId") String userId,
                   @Param("levelName") LevelEnum levelName,
                   @Param("levelId") Integer levelId);

    int delete(@Param("userId") String userId,
               @Param("levelName") LevelEnum levelName,
               @Param("levelId") Integer levelId);

    Integer queryTotal(@Param("levelName") LevelEnum levelName,
                       @Param("levelId") String levelId);

    Boolean select(Like like);

    int delete(Like like);
}
