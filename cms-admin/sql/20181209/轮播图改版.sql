--轮播图改版
ALTER TABLE `t_promotion`
DROP COLUMN `image_src`;

ALTER TABLE `t_promotion`
ADD COLUMN `level_node` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '等级节点' AFTER `level_id`;


--属性管理改版
ALTER TABLE `t_course_attribute`
ADD COLUMN `image_id` int(32) NULL DEFAULT NULL COMMENT '图片id' AFTER `name`;

ALTER TABLE t_picture
MODIFY COLUMN `pic_type` tinyint(3) NULL DEFAULT NULL COMMENT '图标类型: 1. 课程封面 2. 单元封面 3.老师头像 4. 问题图片 5. 走马灯图片 6.课程介绍图文 7.课程海报 8.知识点封面 9. 单元图标 10.科目封面 11.科目图标 12.分享海报 13.课程属性图片' AFTER `description`;

COMMIT;

