package com.ics.cmsadmin.modules.auth.service.impl;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import com.ics.cmsadmin.modules.auth.service.AccessService;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.ics.cmsadmin.modules.agent.steward.AgentServices.agentInfoService;
import static com.ics.cmsadmin.modules.auth.steward.AuthServices.userService;

@Service
public class AccessServiceImpl implements AccessService {

    @Resource(name = "newStudentService")
    private StudentService studentService;

    @Override
    public boolean canAccessForUser(String loginUserId, String userId) {
        PageResult pageResult = userService.listByLoginUserId(
            SysUser.builder().userId(userId).build(), loginUserId, new PageBean()
        );
        return pageResult != null && pageResult.getTotalCount() >= 1;
    }

    @Override
    public boolean canAccessForAgent(String loginUserId, String agentNo) {
        PageResult<AgentInfoBean> pageResult = agentInfoService.listByLoginUserId(
            AgentInfoBean.builder().agentNo(agentNo).build(), loginUserId, new PageBean());
        return pageResult != null && pageResult.getTotalCount() >= 1;
    }

    @Override
    public boolean canAccessForStudent(String loginUserId, String studentId) {
        PageResult pageResult = studentService.listByLoginUserId(StudentBean.builder()
            .id(studentId).build(), loginUserId, new PageBean());
        return pageResult != null && pageResult.getTotalCount() >= 1;
    }

    @Override
    public LoginSearchBean queryLoginInfoByLoginUserId(String loginUserId) {
        LoginSearchBean loginSearchBean = new LoginSearchBean();
        SysUser sysUser = userService.queryById(loginUserId);
        // 如果登陆用户查不到,或者用户类型为空,则返回null
        if (sysUser == null || StringUtils.isBlank(sysUser.getUserType())) {
            return null;
        }
        loginSearchBean.setLoginPostType(sysUser.getPostType());
        loginSearchBean.setLoginUserId(loginUserId);
        // 根据登陆用户类型设置查询相关信息
        switch (sysUser.getUserType()) {
            case Constants.USER_TYPE_AGENT:     // 如果登陆用户是代理商,则查询登陆代理商的agentNo
                AgentInfoBean agentInfoBean = agentInfoService.queryByUserId(loginUserId);
                if (agentInfoBean == null) {
                    return null;
                } else {
                    loginSearchBean.setLoginAgentNo(agentInfoBean.getAgentNo());
                }
                break;
            case Constants.USER_TYPE_ADMIN:     // 如果登陆用户是普通管理员,则查询登陆用户的所属组织
                if (StringUtils.isBlank(sysUser.getOrgId())) {
                    return null;
                }
                loginSearchBean.setLoginOrgId(sysUser.getOrgId());
                break;
            default:                            // 其他登陆用户类型,则返回空
                return null;
        }
        return loginSearchBean;
    }


}
