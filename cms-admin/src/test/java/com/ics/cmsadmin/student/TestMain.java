//package com.ics.cmsadmin.student;
//
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.RandomUtils;
//import org.apache.commons.lang3.time.DateUtils;
//
//import java.io.File;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
///**
// * @author kco1989
// * @email kco1989@qq.com
// * @date 2019-03-20 16:34
// */
//public class TestMain {
//
//    private static List<StudentBean> studentBeanList = new ArrayList<>();
//    private static List<OrderBean> orderBeanList = new ArrayList<>();
//    private static File saveFile = new File("d:\\sql-test.sql");
//
//    public static void main(String[] args) throws Exception {
//        saveFile.delete();
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date startDate = simpleDateFormat.parse("2019-04-20");
//        Date endDate = simpleDateFormat.parse("2019-05-27");
//        while (startDate.before(endDate)) {
//            int maxSeconde = Contants.minutes * 60 / Contants.minutesIndex;
//            for (int seconds = RandomUtils.nextInt(0, maxSeconde); seconds < Contants.minutes * 60; seconds = seconds + RandomUtils.nextInt(0, maxSeconde)){
//                makeDate(DateUtils.addSeconds(startDate, seconds));
//            }
//            startDate = nextDate(startDate);
//        }
//        makeSql(StudentBean.class, studentBeanList);
//        makeSql(OrderBean.class, orderBeanList);
//    }
//
//    private static void makeDate(Date startDate) {
//        shuffle(startDate);
//        switch (EventEnums.radomEvent()) {
//            case NEW_STUDENT:
//                newStudent(startDate);
//                break;
//            case PROMOTION_STUDENT:
//                promotionStudent(startDate);
//                break;
//            case NEW_ORDER:
//                newOrder(startDate);
//                break;
//            default:
//                break;
//        }
//    }
//
//    private static void newOrder(Date startDate) {
//        studentBeanList.stream()
//            .limit(RandomUtils.nextLong(Contants.MIN_NEW_ORDER_STUDENY, Contants.MAX_NEW_ORDER_STUDENY))
//            .forEach(student ->
//                orderBeanList.addAll(Stream.iterate(1, item -> item + 1)
//                    .limit(RandomUtils.nextLong(Contants.MIN_NEW_ORDER, Contants.MAX_NEW_ORDER))
//                    .map(item -> OrderBean.newOrder(student.getId(), startDate))
//                    .collect(Collectors.toList())
//                )
//            );
//    }
//
//    private static void promotionStudent(Date startDate) {
//        studentBeanList.addAll(
//            studentBeanList.stream()
//                .limit(RandomUtils.nextLong(Contants.MIN_PROMOTION_STUDENT, Contants.MAX_PROMOTION_STUDENT))
//                .map(item -> StudentBean.newStudent(item.getId(), startDate))
//                .collect(Collectors.toList())
//        );
//    }
//
//    private static void newStudent(Date startDate) {
//        studentBeanList.addAll(
//            Stream.iterate(1, item -> item + 1)
//                .limit(RandomUtils.nextLong(Contants.MIN_NEW_STUDENT, Contants.MAX_NEW_STUDENT))
//                .map(item -> StudentBean.newStudent(startDate))
//                .collect(Collectors.toList())
//        );
//    }
//
//    private static void makeSql(Class<? extends SqlBean> clazz, List<? extends SqlBean> list) throws IOException {
//        int size = list.size();
//        int maxInsert = 2000;
//        for (int i = 0; i < size / maxInsert + 1; i++) {
//            List<? extends SqlBean> sqlBeans = list.subList(i * maxInsert, Math.min((i + 1) * maxInsert, size));
//            StringBuilder sb = new StringBuilder();
//            sb.append(SqlBean.insertSqlPrefix(clazz));
//            sqlBeans.stream().forEach(item -> sb.append(item.insertSqlSuffix()));
//            int index = sb.lastIndexOf(",");
//            sb.replace(index, index + 1, ";");
//            FileUtils.writeStringToFile(saveFile, sb.toString(), "utf-8", true);
//        }
//    }
//
//    private static Date nextDate(Date startDate) {
//        return DateUtils.addMinutes(startDate, Contants.minutes);
//    }
//
//    private static void shuffle(Date date){
//        Random random = new Random(date.getTime());
//        Collections.shuffle(studentBeanList, random);
////        Collections.shuffle(orderBeanList, random);
//    }
//}
