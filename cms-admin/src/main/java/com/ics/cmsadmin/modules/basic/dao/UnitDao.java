package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.UnitBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-09-01.
 */
@Repository
@Mapper
public interface UnitDao extends BaseDao<UnitBean>, BaseDataDao<UnitBean> {
    void updateProblemNum(@Param("id") String unitId, @Param("testNum") int testNum);
}
