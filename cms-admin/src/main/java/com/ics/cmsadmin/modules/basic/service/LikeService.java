package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.Like;

public interface LikeService {

    int insert(Like like);

    int delete(Like like);

    Boolean select(Like like);

    Integer queryTotal(LevelEnum levelName, String levelId);

    boolean toggleLike(Like like);
}
