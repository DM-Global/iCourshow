ALTER TABLE `t_order`
MODIFY COLUMN `payment_method` tinyint(3) NULL DEFAULT 1 COMMENT '支付方式: 1.微信公众号 2微信app  3.支付宝 4.免费试用 5.苹果支付 6.苹果沙箱支付 7.账户余额支付' AFTER `order_amount`;
