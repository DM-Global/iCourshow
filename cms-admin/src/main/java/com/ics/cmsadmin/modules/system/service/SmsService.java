package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-01-22 09:19
 */
public interface SmsService {
    /**
     * 获取短信验证码
     */
    ApiResponse sendVerifyCodeSms(String smsType, String phoneCode, String phone, String imageCodeId, String imageCode);
}
