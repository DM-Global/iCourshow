package com.ics.cmsadmin.frame.core.enums;

/**
 * Created by Sandwich on 2018-07-14
 */
public enum ApiResultEnum {
    SUCCESS(0, "成功"),

    FAILURE(400, "失败"),
    LOGIN_PAST_DUE(401, "未登录"),
    FAILURE_UPDATE(402, "数据库操作,操作失败"),
    OPERATION_FORBIDDEN(403, "无权限操作"),
    NOT_FOUND(404, "接口不存在"),
    USER_FORBIDDEN(405, "用户被禁用"),
    INTERNAL_SERVER_ERROR(500, "服务器内部错误"),
    SQL_EXCEPTION(501, "sql异常"),

    INSERT_TABLE_ITEM_FAIL(100, "插入表数据失败"),
    INSERT_TABLE_NULL_PRIMARY(101, "插入表数据失败,主键为空"),
    INSERT_TABLE_REPEAT_PRIMARY(102, "插入表数据失败,主键重复"),
    UPDATE_TABLE_ITEM_FAIL(103, "更新表数据失败"),
    DELETE_TABLE_ITEM_FAIL(104, "删除表数据失败"),
    BULK_DELETE_FAIL(105, "数据批量删除失败"),
    BULK_UPDATE_FAIL(106, "数据批量更新失败"),
    BULK_INSERT_FAIL(107, "数据批量插入失败"),

    PARAM_ERROR(1000, "参数错误"),
    DATA_NOT_EXIST(1001, "数据不存在"),
    VALIDATE_ERROR(1002, "数据校验失败"),
    UNEXPECTED_TYPE_EXCEPTION(1003, "校验方法异常"),
    HTTP_MESSAGE_NOT_READABLE_EXCEPTION(1004, "请求参数不匹配"),
    IMAGE_VERIFY_CODE_ERROR(1005, "图形验证码验证错误"),
    SMS_VERIFY_CODE_TYPE_ERROR(1006, "短信验证码类型错误"),
    SEND_SMS_ERROR(1007, "发送短信失败"),
    SMS_VERIFY_CODE_ALREAD_SEND(1008, "短信验证码已经发送,请稍等"),
    SMS_VERIFY_CODE_ERROR(1008, "短信验证码有误"),

    USER_EXIT(2000, "用户已经存在"),
    USER_NOT_EXIT(2001, "用户不存在"),
    USER_NAME_DUPLICATE(2002, "该用户名已存在!"),
    USER_STATUS_INVALID(2003, "用户状态:禁用!"),
    USER_CODE_ERROR(2004, "验证码错误!"),
    SEND_CODE_INCORRECT(2005, "验证码发送失败"),
    OLD_PWD_ERROR(2006, "原密码错误"),
    USER_NO_BIND_WX(2007, "用户还没绑定微信"),
    NOT_ENOUGH_REWARD(2008, "用户积点不够"),
    NOT_ENOUGH_ELITE_COIN(2009, "精英币不够"),
    LOGIN_TYPE_ERROR(2010, "登陆类型不正确!"),
    LOGIN_PHONE_OR_CODE_ERROR(2011, "登陆手机或验证码为空!"),
    LOGIN_CODE_NON_EXISTENT(2012, "登陆验证码不存在"),
    LOGIN_CODE_ERROR(2013, "登陆验证码不正确"),
    LOGIN_USER_IS_LOCK(2014, "登陆用户被锁定"),
    USER_PASSWORD_ERROR(2015, "账号或密码错误!"),


    WECHAT_PAY_ERROR(3000, "微信配置出错"),
    ALIPAY_ERROR(3001, "支付宝配置出错"),

    SIGNATURE_VALID_FAILED(4000, "接口签名验证失败"),
    FAILURE_REDIRECT(4001, "数据有误"),
    LOGIN_FAILED(4002, "登录失败!"),
    LOGOUT_SUCCESS(4003, "登出成功!"),
    UN_LOGIN(4004, "未登陆!"),
    INCORRECT_SESSION_KEY(4005, "sessionKey错误或无效"),
    LOGIN_OUT_BY_OTHER(4006, "被挤下线了!"),

    ADMYWITHDRAW_IS_NULL(5000, "提现记录为空"),
    ADMYREWARDRECORD_IS_NULL(5001, "积点记录为空"),
    APPUSER_IS_NULL(5002, "用户记录为空"),


    PRODUCT_NOT_EXIST(6000, "商品不存在或商品已下架"),
    REPEAT_PURCHASE(6001, "商品已购买，无需重复购买"),
    USER_NOT_EXIST(6002, "用户不存在"),
    USER_NOT_ALLOW(6003, "用户被封号了"),
    CREATE_ORDER_FAIL(6004, "创建订单失败"),
    COURSE_NOT_EXIST(6005, "课程不存在或商品已下架"),
    UNIT_NOT_EXIST(6006, "单元不存在或商品已下架"),
    TOPIC_NOT_EXIST(6007, "主题不存在或商品已下架"),
    POINT_NOT_EXIST(6008, "知识点不存在或商品已下架"),
    UPDATE_SUBJECT_FAILED(6009, "主题更新失败"),
    GLOBAL_DISCOUNT_DISABLED(6010, "全场折扣异常"),

    EMPTY_QUESTION_LIST(7000, "问题列表不能为空"),
    EMPTY_PRACTICE_LIST(7001, "题集不能为空"),
    CHARSET_CONVERT_FAILED(7002, "字符转码失败"),


    FILE_TOO_BIG(8001, "文件太大"),
    FILE_MOVE_FAIL(8002, "文件移动失败!"),
    FILE_IS_NULL(8003, "文件不能为空!"),
    FILE_FORMAT_ERR(8004, "文件格式不正确!"),
    FILE_UPLOAD_ERR(8005, "文件上传失败!"),

    ORDER_NOT_EXIST(9001, "订单不存在"),
    WXPAY_NOTIFY_MONEY_VERIFY_ERROR(9002, "微信支付金额验证失败"),
    WX_APP_PREPAY_FAILED(9003, "微信app预支付失败"),
    WX_APP_WITHDRAW_FAILED(9006, "微信app提现失败"),
    WX_APP_WITHDRAW_WITHOUT_OPENID(9007, "您的app还没有绑定微信，不能提现到微信"),
    ALREADY_EXISTED_PACKAGE_ORDER(9004, "新用户已经有包年包月订单"),
    ALREADY_EXISTED_TRIAL_ORDER(9005, "用户已经有试用订单"),
    WITHDRAW_LIMIT(9006, "提现限制"),
    CREATE_ORDER_ERROR(9007, "新建订单有误"),
    WITHDRAW_LIMIT_MUST_BUY_ORDER(9008, "您至少有一次购买会员消费记录"),

    SEND_ANDROID_BROADCAST_FAILED(10001, "发送安卓广播推送失败"),
    SEND_ANDROID_UNICAST_FAILED(10002, "发送安卓单播推送失败"),
    SEND_IOS_BROADCAST_FAILED(10003, "发送IOS广播推送失败"),
    SEND_IOS_UNICAST_FAILED(10004, "发送IOS单播推送失败"),

    ;

    private int code;
    private String detail;

    ApiResultEnum(int code, String detail) {
        this.code = code;
        this.detail = detail;
    }

    public String getDetail() {
        return this.detail;
    }

    public int getCode() {
        return this.code;
    }
}
