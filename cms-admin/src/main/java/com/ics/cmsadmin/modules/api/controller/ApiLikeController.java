package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.basic.bean.Like;
import com.ics.cmsadmin.modules.basic.service.LikeService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/like")
public class ApiLikeController {

    @Autowired
    private LikeService likeService;

    @PostMapping("/toggle")
    public ApiResponse toggle(@RequestBody Like like, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            throw new CmsException(ApiResultEnum.UN_LOGIN);
        }
        like.setUserId(loginUserId);
        likeService.toggleLike(like);
        return ApiResponse.getDefaultResponse();
    }
}
