package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.system.bean.RegisterBean;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.function.Function;

import static com.ics.cmsadmin.modules.system.steward.SystemRepositories.registerDao;

/**
 * Created by lvsw on 2018-03-29.
 */
@Service
public class RegisterServiceImpl implements RegisterService {

    @Override
    public RegisterBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return registerDao.queryById(id);
    }

    @Override
    public PageResult<RegisterBean> list(RegisterBean bean, PageBean page) {
        long count = registerDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, registerDao.list(bean, page));
    }

    @Override
    public boolean insert(RegisterBean bean) {
        if (bean == null) {
            return false;
        }
        if (StringUtils.isBlank(bean.getRegisterId())) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_NULL_PRIMARY, "主键id不能为空");
        }
        if (queryById(bean.getRegisterId()) != null) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_REPEAT_PRIMARY, "该配置id已经存在.");
        }
        RegisterKeyValueEnum registerKeyValueEnum = RegisterKeyValueEnum.valueOfKey(bean.getRegisterId());
        if (registerKeyValueEnum != null &&
            !registerKeyValueEnum.getValid().test(bean.getValue())) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, registerKeyValueEnum.getDescription());
        }
        return registerDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, RegisterBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        RegisterKeyValueEnum registerKeyValueEnum = RegisterKeyValueEnum.valueOfKey(bean.getRegisterId());
        if (registerKeyValueEnum != null &&
            !registerKeyValueEnum.getValid().test(bean.getValue())) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, registerKeyValueEnum.getDescription());
        }
        return registerDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return registerDao.delete(id) == 1;
    }

    @Override
    public <T> T queryRegisterValue(RegisterKeyValueEnum keyValueEnum, Function<String, T> function) {
        return translate(function,
            registerDao.queryById(keyValueEnum.getKey()),
            function.apply(keyValueEnum.getDefaultValue()));
    }


}
