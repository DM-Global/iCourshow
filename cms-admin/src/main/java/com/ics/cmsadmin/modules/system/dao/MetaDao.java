package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.system.bean.MetaBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2018-03-29.
 */
@Repository
@Mapper
public interface MetaDao extends BaseDao<MetaBean>, BaseDataDao<MetaBean> {
    /**
     * 获取meta直接下级
     *
     * @param parentId 父级id
     * @return
     */
    List<MetaBean> listDirectChildren(@Param("parentId") String parentId);
}
