package com.ics.cmsadmin.modules.wechat.model.auth.resp;

import lombok.Data;

/**
 * 微信通用接口凭证
 *
 * @author phil
 * @date 2017年7月2日
 */
@Data
public class AccessToken {
    // 获取到的凭证
    private String access_token;
    // 凭证有效时间，单位：秒
    private int expires_in;
}
