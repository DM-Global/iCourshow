package com.ics.cmsadmin.modules.auth.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-03-29.
 */
@Repository
@Mapper
public interface UserDao extends BaseDataDao<SysUser>, BaseDao<SysUser> {
    /**
     * 恢复用户信息
     *
     * @param userId 用户id
     */
    int recovery(@Param("userId") String userId);

    /**
     * 重置密码
     *
     * @param userId      需要重置的用户id
     * @param newPassword 新密码
     */
    int resetPassword(@Param("userId") String userId, @Param("newPassword") String newPassword);

    /**
     * 修改密码
     *
     * @param userId      用户id
     * @param newPassword 新密码
     */
    int modifyPassword(@Param("userId") String userId, @Param("newPassword") String newPassword);
}
