package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Dao
 *
 * @author Sandwich
 * @date 2018-07-06 02:09:50
 */
@Mapper
public interface TeacherMapper extends ResourceMapper<Teacher, Integer> {
    List<String> getSubjectsByTeacherId(Integer teacherId);
}
