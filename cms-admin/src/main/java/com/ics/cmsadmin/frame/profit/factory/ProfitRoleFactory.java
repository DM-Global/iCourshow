package com.ics.cmsadmin.frame.profit.factory;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.profit.ProfitTypeEnum;
import com.ics.cmsadmin.frame.profit.role.ProfitRole;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 分润实现类抽象工厂类
 * Created by Administrator on 2017/4/19.
 */
public abstract class ProfitRoleFactory {

//    protected void commonVerifExpression(ProfitTypeEnum profitType, String expression) {
//        if (StringUtils.isBlank(expression)) {
//            throw new CmsException(ApiResultEnum.FAILURE_REDIRECT, "分润表示时不符合" + profitType.name() + "的规则.");
//        }
//        Pattern pattern = Pattern.compile(profitType.getRegular());
//        Matcher matcher = pattern.matcher(expression);
//        if (!matcher.matches()) {
//            throw new CmsException(ApiResultEnum.FAILURE_REDIRECT, "分润表示时不符合" + profitType.name() + "的规则.");
//        }
//    }

    public ProfitRole createProfitRole(String expression) {
        verifyExpression(expression);
        return newProfitRole(expression);
    }

    protected abstract ProfitRole newProfitRole(String expression);

    public abstract void verifyExpression(String expression);

    public abstract String generateExpression(Object data);

}
