package com.ics.cmsadmin.modules.system.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * c_register
 * 注册(用于配置不同环境的变量值)
 * Created by lvsw on 2018-07-03.
 */
@Data
@Builder
public class RegisterBean {
    @NotNull(message = "registerId不能为空", groups = InsertGroup.class)
    @Length(message = "registerId长度必须在{min}~{max}之间", min = 2, max = 50, groups = InsertGroup.class)
    @Pattern(message = "registerId只能包含数字和字母,且以字母开头", regexp = "^[a-zA-Z][_a-zA-Z0-9]+$", groups = InsertGroup.class)
    private String registerId;
    private String value;
    @NotNull(message = "描述不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "描述长度必须在{min}~{max}之间", min = 2, max = 150, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public RegisterBean() {
    }
}
