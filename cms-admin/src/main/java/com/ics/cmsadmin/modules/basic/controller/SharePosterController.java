package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import com.ics.cmsadmin.modules.basic.service.SharePosterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * t_share_poster controller
 */
@Api(description = "")
@RestController
@RequestMapping("/sharePoster")
public class SharePosterController {

    @Resource
    private SharePosterService sharePosterService;

    @Authorize(AuthorizeEnum.SHARE_POSTER_QUERY)
    @ApiOperation(value = "查询信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(sharePosterService.queryById(id));
    }

    @Authorize(AuthorizeEnum.SHARE_POSTER_INSERT)
    @ApiOperation(value = "新增信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody SharePosterBean sharePosterBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(sharePosterService.insert(sharePosterBean));
    }

    @Authorize(AuthorizeEnum.SHARE_POSTER_DELETE)
    @ApiOperation(value = "删除信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
        return new ApiResponse(sharePosterService.delete(id));
    }

    @Authorize(AuthorizeEnum.SHARE_POSTER_UPDATE)
    @ApiOperation(value = "更新信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody SharePosterBean sharePosterBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(sharePosterService.update(id, sharePosterBean));
    }

    @Authorize(AuthorizeEnum.SHARE_POSTER_QUERY)
    @ApiOperation(value = "分页查询信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody SharePosterBean sharePosterBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(sharePosterService.list(sharePosterBean, pageBean));
    }

}
