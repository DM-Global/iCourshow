package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * t_student
 * Created by lvsw on 2018-51-22 19:09:31.
 */
@Repository
@Mapper
public interface StudentDao extends BaseDao<StudentBean>, BaseDataDao<StudentBean> {
    /**
     * 根据代理商查询最近关注的学生信息
     */
    StudentBean findLatelyStudentByAgentNo(@Param("agentNo") String agentNo);

    List<StudentBean> queryByIds(@Param("studentIds") List<String> studentIds);
}
