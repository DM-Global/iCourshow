package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.modules.system.bean.RegisterBean;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

@Slf4j
public class RegisterServiceImplTest extends CmsAdminApplicationTests {

    @Autowired
    RegisterService registerService;

    @Test
    public void test() {
        RegisterBean registerBean = registerService.queryById("global_price_discount");
        log.info(registerBean.toString());
    }

}
