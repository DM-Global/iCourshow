package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.WeChatSession;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface WeChatSessionMapper extends ResourceMapper<WeChatSession, String> {
    int delete(String openId);

    WeChatSession select(String openId);
}
