package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.pub.bean.OrderForm;

import java.util.List;

public interface StudentOrderService {

    OrderBean create(OrderForm orderForm, String userId);

    PageResult list(String userId, PageBean page);
}
