package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PostManHeader {
    private String key;
    private String value;
    private String type = "text";
}
