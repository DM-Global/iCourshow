package com.ics.cmsadmin.modules.umeng.upush;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.property.UmengPushConfig;
import org.json.JSONObject;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Created by Sandwich on 2019/6/10
 */
public class PushClientTest extends CmsAdminApplicationTests {

    @Autowired
    private UmengPushConfig umengPushConfig;

    @Test
    public void testSendPush() throws Exception {

        UmengNotification umengNotification = new UmengNotification() {
            @Override
            public boolean setPredefinedKeyValue(String key, Object value) throws Exception {
                return false;
            }
        };
        umengNotification.setExpireTime("2019-06-11 23:08:16");
        umengNotification.setDescription("推送单元测试Description");
        umengNotification.setProductionMode(true);
        umengNotification.setAppMasterSecret(umengPushConfig.getAndroidMap().get("appMasterSecret"));
        umengNotification.setPredefinedKeyValue("display_type","notification");
//        umengNotification.setPredefinedKeyValue("appkey", umengPushConfig.getAndroidMap().get("appKey"));
        umengNotification.rootJson = new JSONObject().put("appKey",umengPushConfig.getAndroidMap().get("appKey"))
            .put("timestamp",System.currentTimeMillis())
            .put("type","broadcast").put("payload",new JSONObject()
                    .put("display_type",AndroidNotification.DisplayType.NOTIFICATION.getValue())
                    .put("body",new JSONObject().put("title","推送测试title")
                            .put("ticker","推送测试ticker")
                            .put("after_open",AndroidNotification.AfterOpenAction.go_app)
                            .put("text","推送测试Content"))
                                                                    );

        PushClient.send(umengNotification);
    }

    /***
     * 测试成功
     * @throws Exception
     */
    @Test
    public void testUploadContents() throws Exception {

        PushClient.uploadContents(umengPushConfig.getAndroidMap().get("appKey"),umengPushConfig.getAndroidMap().get("appMasterSecret"),"testUploadContents");

    }

}
