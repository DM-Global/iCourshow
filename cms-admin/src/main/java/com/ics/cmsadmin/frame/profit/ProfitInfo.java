package com.ics.cmsadmin.frame.profit;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;

@Data
@Builder
public class ProfitInfo {
    private ProfitTypeEnum profitTypeEnum;
    private BigDecimal limitValue;
    private BigDecimal orderMoney;
    private BigDecimal shareRate;
    private BigDecimal shareMonery;

    @Tolerate
    public void ProfitInfo() {
    }

}
