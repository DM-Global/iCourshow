package com.ics.cmsadmin.modules.pub.service;

import com.ics.cmsadmin.modules.pub.dao.SubjectMapper;
import com.ics.cmsadmin.modules.pub.bean.Subject;
import org.springframework.stereotype.Service;

@Service("SubjectService")
public class SubjectService extends ResourceService<SubjectMapper, Subject, Integer> {
}
