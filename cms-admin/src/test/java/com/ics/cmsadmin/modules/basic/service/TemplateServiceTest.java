package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.email.MailUtils;
import com.ics.cmsadmin.modules.basic.bean.TemplateBean;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Sandwich on 2018-11-01
 */
public class TemplateServiceTest extends CmsAdminApplicationTests {
    @Autowired
    private MailUtils mailUtils;

    @Autowired
    private TemplateService templateService;

    @Test
    public void sendMailWithTemplate() {
        TemplateBean templateBean = templateService.queryById("test");
        mailUtils.sendEmail("550141610@qq.com", "邮件测试", templateBean.getContent());
    }

    @Test
    public void sendMailWithTemplateWithParams() {
        TemplateBean templateBean = templateService.queryById("test1");
        String content = templateBean.getContent().replace("{username}", "陈上明");
        mailUtils.sendEmail("550141610@qq.com", "邮件测试", content);
    }


}
