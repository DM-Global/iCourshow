package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.student.dao.StudentShareRecordDao;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.student.bean.StudentShareRecordBean;
import com.ics.cmsadmin.modules.student.service.StudentShareRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * t_student_share_record
 * Created bylvsw on 2019-03-03 00:06:16.
 */
@Service
public class StudentShareRecordServiceImpl implements StudentShareRecordService {
    @Resource
    private StudentShareRecordDao studentShareRecordDao;

    @Override
    public StudentShareRecordBean queryById(String id) {
        if (StringUtils.isBlank(id)){
            return null;
        }
        return studentShareRecordDao.queryById(id);
    }

    @Override
    public PageResult list(StudentShareRecordBean bean, PageBean page) {
        long count = studentShareRecordDao.count(bean);
        if (count == 0){
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, studentShareRecordDao.list(bean, page));
    }

    @Override
    public boolean insert(StudentShareRecordBean bean) {
        if (bean == null){
            return false;
        }
        return studentShareRecordDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, StudentShareRecordBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null){
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return studentShareRecordDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null){
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return studentShareRecordDao.delete(id) == 1;
    }

}
