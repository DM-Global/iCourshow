package com.ics.cmsadmin.frame.utils;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.pub.bean.*;
import com.ics.cmsadmin.modules.pub.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class LevelInfoUtil {

    @Autowired
    private SubjectMapper subjectMapper;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private UnitMapper unitMapper;

    @Autowired
    private TopicMapper topicMapper;

    @Autowired
    private PointMapper pointMapper;

    public LevelInfo setLevelInfo(LevelEnum level, Integer levelId) {
        LevelInfo levelInfo = new LevelInfo();
        if (StringUtils.isEmpty(level)) {
            return levelInfo;
        }
        Integer preLevelId = levelId;
        switch (level) {
            case point:
                Point point = pointMapper.select(preLevelId);
                if (point == null) {
                    return levelInfo;
                }
                levelInfo.setPointId(point.getId());
                levelInfo.setPointName(point.getName());
                preLevelId = point.getTopicId();
            case topic:
                Topic topic = topicMapper.select(preLevelId);
                if (topic == null) {
                    return levelInfo;
                }
                levelInfo.setTopicId(topic.getId());
                levelInfo.setTopicName(topic.getName());
                preLevelId = topic.getUnitId();
            case unit:
                Unit unit = unitMapper.select(preLevelId);
                if (unit == null) {
                    return levelInfo;
                }
                levelInfo.setUnitId(unit.getId());
                levelInfo.setUnitName(unit.getName());
                preLevelId = unit.getCourseId();
            case course:
                Course course = courseMapper.select(preLevelId);
                if (course == null) {
                    return levelInfo;
                }
                levelInfo.setCourseId(course.getId());
                levelInfo.setCourseName(course.getName());
                preLevelId = course.getSubjectId();
            case subject:
                Subject subject = subjectMapper.select(preLevelId);
                if (subject == null) {
                    return levelInfo;
                }
                levelInfo.setSubjectId(subject.getId());
                levelInfo.setSubjectName(subject.getName());
        }
        return levelInfo;
    }
}
