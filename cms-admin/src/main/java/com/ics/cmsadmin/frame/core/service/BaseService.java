package com.ics.cmsadmin.frame.core.service;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface BaseService<T> {
    /**
     * 根据主键id查询bean
     *
     * @param id 主键id
     * @return config
     */
    T queryById(String id);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    boolean insert(T t);

    /**
     * 根据id更新数据
     *
     * @param t 更新数据
     * @return 影响条数
     */
    boolean update(String id, T t);

    /**
     * 根据id删除数据
     *
     * @param id 主键id
     * @return 影响条数
     */
    boolean delete(String id);
}
