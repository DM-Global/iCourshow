package com.ics.cmsadmin.frame.config;

import com.ics.cmsadmin.frame.init.AfterSpringStarted;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Log4j2
public class ApplicationEventListener implements ApplicationListener {

    @Resource
    private AfterSpringStarted afterSpringStarted;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        // 在这里可以监听到Spring Boot的生命周期
        // 初始化环境变量
//        if (event instanceof ApplicationEnvironmentPreparedEvent) {
//            System.out.println("~~~~ ApplicationEventListener -> ApplicationEnvironmentPreparedEvent ");
//        }
//        // 初始化完成
//        else if (event instanceof ApplicationPreparedEvent) {
//            System.out.println("~~~~ ApplicationEventListener -> ApplicationPreparedEvent ");
//        }
//        // 应用刷新
//        else if (event instanceof ContextRefreshedEvent) {
//            System.out.println("~~~~ ApplicationEventListener -> ContextRefreshedEvent ");
//        }
        // 应用已启动完成
        if (event instanceof ApplicationReadyEvent) {
            log.info("ApplicationEventListener -> ApplicationReadyEvent");
            afterSpringStarted.InitializationTask();
        }
        // 应用启动，需要在代码动态添加监听器才可捕获
//        else if (event instanceof ContextStartedEvent) {
//            System.out.println("~~~~ ApplicationEventListener -> ContextStartedEvent ");
//        }
//        // 应用停止
//        else if (event instanceof ContextStoppedEvent) {
//            System.out.println("~~~~ ApplicationEventListener -> ContextStoppedEvent ");
//        }
//        // 应用关闭
//        else if (event instanceof ContextClosedEvent) {
//            System.out.println("~~~~ ApplicationEventListener -> ContextClosedEvent ");
//        } else {
//            System.out.println("~~~~ ApplicationEventListener -> else ");
//        }
    }

}
