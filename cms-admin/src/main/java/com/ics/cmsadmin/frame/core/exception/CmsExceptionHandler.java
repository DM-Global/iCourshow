package com.ics.cmsadmin.frame.core.exception;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.UnexpectedTypeException;
import java.sql.SQLException;

@Slf4j
@ResponseBody
@ControllerAdvice
public class CmsExceptionHandler {


    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(UnexpectedTypeException.class)
    public ApiResponse unexpectedType(UnexpectedTypeException exception) {
        log.error("校验方法太多，不确定合适的校验方法。{}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(ApiResultEnum.UNEXPECTED_TYPE_EXCEPTION);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ApiResponse messageNotReadable(HttpMessageNotReadableException exception) {
        log.error("请求参数不匹配。{}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(ApiResultEnum.HTTP_MESSAGE_NOT_READABLE_EXCEPTION);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(DataIntegrityViolationException.class)
    public ApiResponse dataIntegrityViolationException(DataIntegrityViolationException exception) {
        log.error("新增或更新sql异常: {}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(ApiResultEnum.SQL_EXCEPTION);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(value = CmsException.class)
    public ApiResponse handleCmsException(CmsException exception) {
        log.error("自定义错误: {}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(exception);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NoHandlerFoundException.class)
    public ApiResponse handleNoHandlerFoundException(NoHandlerFoundException exception) {
        log.error("没找到请求: {}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(ApiResultEnum.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(Exception.class)
    public ApiResponse handleException(Exception exception) {
        log.error("其他异常: {}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(ApiResultEnum.INTERNAL_SERVER_ERROR);
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(SQLException.class)
    public ApiResponse handleSQLException(SQLException exception) {
        log.error("sql异常: {}", ExceptionUtils.collectExceptionStackMsg(exception));
        return new ApiResponse(ApiResultEnum.SQL_EXCEPTION);
    }

}
