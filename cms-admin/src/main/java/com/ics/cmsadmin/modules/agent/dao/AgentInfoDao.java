package com.ics.cmsadmin.modules.agent.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * a_agent_info
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Repository
@Mapper
public interface AgentInfoDao extends BaseDataDao<AgentInfoBean>, BaseDao<AgentInfoBean> {

}
