package com.kco.generate.app;

import com.kco.generate.config.ColumnsBean;
import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateListComponentHtml {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildBarMenu(tableBean))
            .append(buildSearch(tableBean))
            .append(buildListTable(tableBean))
            .append(buildAddOrUpdateModal(tableBean));
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + "-list.component.html", sb.toString());
    }

    private static String buildAddOrUpdateModal(TableBean tableBean) {
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();
        sb.append("<p-dialog [(visible)]=\"diaLog.display\" [width]=\"300\">\n" +
            "  <p-header>\n" +
            "    {{diaLog.title}}\n" +
            "  </p-header>\n" +
            "  <div class=\"ui-g \">\n");
        sb.append("    <div class=\"ui-g-12\">\n" +
            "      <label class=\"ui-g-4\" >" + tableBean.getPrimaryPropertyKeyColumnComment() + "</label>\n" +
            "      <input class=\"ui-g-8\" pInputText [(ngModel)]=\"theOperation" + className + "." + tableBean.getPrimaryPropertyKey() + "\" [disabled]=\"!diaLog.isAddModal\"/>\n" +
            "    </div>\n");
        for (ColumnsBean columns : tableBean.getColumnsBeans()) {
            if (columns.getPropertyName().equals(tableBean.getPrimaryPropertyKey())) {
                continue;
            }
            sb.append("    <div class=\"ui-g-12\">\n" +
                "      <label class=\"ui-g-4\" >" + columns.getColumnComment() + "</label>\n" +
                "      <input class=\"ui-g-8\" pInputText [(ngModel)]=\"theOperation" + className + "." + columns.getPropertyName() + "\"/>\n" +
                "    </div>\n");
        }
        sb.append(" </div>\n" +
            "  <p-footer>\n" +
            "    <button type=\"text\" (click)=\"insertOrUpdate(selected" + className + ")\" pButton label=\"确认\"></button>\n" +
            "    <button type=\"text\" (click)=\"diaLog.display = false\" pButton label=\"取消\"></button>\n" +
            "  </p-footer>\n" +
            "</p-dialog>\n");
        return sb.toString();
    }

    private static String buildListTable(TableBean tableBean) {
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();
        sb.append("<p-panel header=\"查询列表\" [toggleable]=\"true\">\n" +
            "  <p-dataTable\n" +
            "     [value]=\"" + tempName + "List\" [totalRecords]=\"totalRecords\"\n" +
            "     [rows]=\"pageSize\" [paginator]=\"true\"\n" +
            "     [lazy]=\"true\" [alwaysShowPaginator]=\"true\"\n" +
            "     (onPage)=\"list" + className + "OnPage($event)\" [rowsPerPageOptions]=\"[5,10,20]\"\n" +
            "     scrollable=\"true\"  scrollHeight=\"300px\" scrollWidth=\"100%\"\n" +
            "     selectionMode=\"single\" [(selection)]=\"selected" + className + "\" dataKey=\"" + tableBean.getPrimaryPropertyKey() + "\"\n" +
            "  >\n");
        for (ColumnsBean columns : tableBean.getColumnsBeans()) {
            sb.append("    <p-column field=\"" + columns.getPropertyName() + "\" header=\"" + columns.getColumnComment() + "\" [style]=\"{'width':'150px'}\"></p-column>\n");
        }
        sb.append("    <p-column [style]=\"{'width':'270px'}\">\n" +
            "      <ng-template pTemplate=\"header\">\n" +
            "        操作\n" +
            "      </ng-template>\n" +
            "      <ng-template let-" + tempName + "=\"rowData\" pTemplate=\"body\">\n" +
            "        <button type=\"button\" pButton (click)=\"to" + className + "DetailsPage(" + tempName + "." + tableBean.getPrimaryPropertyKey() + ")\" label=\"详情\"></button>\n" +
            "      </ng-template>\n" +
            "    </p-column>\n" +
            "  </p-dataTable>\n" +
            "</p-panel>\n");
        return sb.toString();
    }

    private static String buildSearch(TableBean tableBean) {
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();
        sb.append("<p-panel header=\"查询条件\" [toggleable]=\"true\" >\n" +
            "  <div class=\"ui-g\">\n");
        for (ColumnsBean columns : tableBean.getColumnsBeans()) {
            sb.append("    <div class=\"ui-g-6 ui-sm-4\">\n" +
                "      <div class=\"ui-g-4\">\n" +
                "        " + columns.getColumnComment() + ":\n" +
                "      </div>\n" +
                "      <div class=\"ui-g-8\">\n" +
                "        <input pInputText type=\"text\" [(ngModel)]=\"searchParam." + columns.getPropertyName() + "\">\n" +
                "      </div>\n" +
                "    </div>\n");
        }
        sb.append("    <div class=\"ui-g-12\">\n" +
            "      <div class=\"ui-g-4 ui-g-offset-4\">\n" +
            "        <button pButton type=\"button\" (click)=\"list" + className + "OnPage(null)\" label=\"查询\"></button>\n" +
            "        <button pButton type=\"button\" (click)=\"resetSearchParam()\" label=\"重置\" class=\"ui-button-secondary\"></button>\n" +
            "      </div>\n" +
            "    </div>\n" +
            "  </div>\n" +
            "</p-panel>\n");
        return sb.toString();
    }

    private static String buildBarMenu(TableBean tableBean) {
        return "<p-menubar [model]=\"menubarItems\"></p-menubar>\n";
    }

    private static String build(TableBean tableBean) {
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();

        return sb.toString();
    }
}
