package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.agent.bean.AgentAccountBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.agent.bean.AgentShareRuleBean;
import com.ics.cmsadmin.modules.agent.enums.AccountChangesEnum;

import java.math.BigDecimal;


/**
 * totalIncome;         // 总收益 A = B + C
 * withdrawCash;        // 提现金额 B
 * balance;              // 余额 C = D + E
 * availableBalance;    // 可用余额 D
 * frozenBalance;       // 冻结余额 E
 * a_agent_account 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface AgentAccountService extends LoginBaseDataService<AgentAccountBean> {

    /**
     * 查询代理商账户信息
     */
    AgentAccountBean queryByAgentNo(String agentNo);

    /**
     * 账户变动
     *
     * @param agentNo            变动代理商
     * @param money              变动金额
     * @param accountChangesEnum 变动类型
     */
    boolean accountChanges(String agentNo, BigDecimal money, AccountChangesEnum accountChangesEnum);

    /**
     * @param bean
     * @return
     */
    boolean insert(AgentAccountBean bean);
}
