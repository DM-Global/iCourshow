package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.modules.system.bean.CityBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2017-12-02.
 */
@Repository
@Mapper
public interface CityDao {
    /**
     * 根据父code获取下级节点
     *
     * @param parentCode 父级code
     * @return
     */
    List<CityBean> listDirectChildren(@Param("parentCode") String parentCode);
}
