import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.modules.app.bean.AppInfoBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.client.methods.HttpHead;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test3 {
    public static void main(String[] args) throws Exception{
//        AppInfoBean appInfoBean = new AppInfoBean();
//        appInfoBean.setPlatform("ios");
//        String body = Jsoup.connect("http://127.0.0.1:8090/ics/app/home/getEntranceImage")
//            .header("user-agent", GsonUtils.toJson(appInfoBean))
//            .ignoreContentType(true)
//            .execute()
//            .body();
//        System.out.println(body);
//        20%<1000<25%<3000<30%<6000<35%
//        String str = "20%<1000<25%<3000<30%<6000<35%";
//        System.out.println(str.replaceAll("\\d+%<|<\\d+%", "")
//            .replaceAll("<", ","));
//        System.out.println(str.replaceAll("<\\d+<", ","));
        String summaryDate = "2018-01-31";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date today = DateUtils.addDays(simpleDateFormat.parse(summaryDate), 1);
        String currentMonthFirstDay = DateFormatUtils.format(today, "yyyy-MM-01");
        Date currentMonthLastDate = DateUtils.addDays(DateUtils.addMonths(simpleDateFormat.parse(currentMonthFirstDay), 1), -1);
        String currentMonthLastDay = DateFormatUtils.format(currentMonthLastDate, "yyyy-MM-dd");
        System.out.println(currentMonthFirstDay);
        System.out.println(currentMonthLastDay);


    }

}
