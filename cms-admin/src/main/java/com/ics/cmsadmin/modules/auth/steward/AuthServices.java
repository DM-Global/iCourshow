package com.ics.cmsadmin.modules.auth.steward;

import com.ics.cmsadmin.modules.auth.service.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AuthServices {
    public static AccessService accessService;
    public static AuthServices authServices;
    public static MenuService menuService;
    public static OrganizationService organizationService;
    public static RoleAuthorizeService roleAuthorizeService;
    public static RoleMenuService roleMenuService;
    public static RoleService roleService;
    public static UserRoleService userRoleService;
    public static UserService userService;

    @Resource
    public void setAccessService(AccessService accessService) {
        AuthServices.accessService = accessService;
    }

    @Resource
    public void setAuthServices(AuthServices authServices) {
        AuthServices.authServices = authServices;
    }

    @Resource
    public void setMenuService(MenuService menuService) {
        AuthServices.menuService = menuService;
    }

    @Resource
    public void setOrganizationService(OrganizationService organizationService) {
        AuthServices.organizationService = organizationService;
    }

    @Resource
    public void setRoleAuthorizeService(RoleAuthorizeService roleAuthorizeService) {
        AuthServices.roleAuthorizeService = roleAuthorizeService;
    }

    @Resource
    public void setRoleMenuService(RoleMenuService roleMenuService) {
        AuthServices.roleMenuService = roleMenuService;
    }

    @Resource
    public void setRoleService(RoleService roleService) {
        AuthServices.roleService = roleService;
    }

    @Resource
    public void setUserRoleService(UserRoleService userRoleService) {
        AuthServices.userRoleService = userRoleService;
    }

    @Resource
    public void setUserService(UserService userService) {
        AuthServices.userService = userService;
    }
}
