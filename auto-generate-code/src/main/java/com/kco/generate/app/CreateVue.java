package com.kco.generate.app;

import com.kco.generate.config.ColumnsBean;
import com.kco.generate.config.TableBean;
import org.apache.commons.lang3.StringUtils;

import static com.kco.generate.config.Utils.fristLower;
import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateVue {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildTemplate(tableBean));
        sb.append(buildScript(tableBean));
        String fileName = tableBean.getClassName() + "Manage.vue";
        saveFile("vue/" + fileName, sb.toString());
    }

    private static String buildTemplate(TableBean tableBean) {
        StringBuffer sb = new StringBuffer();
        sb.append("<template>\n")
            .append("    <div>\n")
            .append("        <Row>\n" +
                "            <Col><h2 class=\"page-title\">" + tableBean.getTableComment() + "管理</h2></Col>\n" +
                "        </Row>\n")
            .append("        <Card>\n")
            .append("            <Form label-position=\"right\" :label-width=\"100\">\n")
            .append("                <Row>\n");
        for (ColumnsBean bean : tableBean.getColumnsBeans()) {
            sb.append("                    <Col span=\"8\">\n")
                .append("                        <FormItem label=\"" + bean.getColumnComment() + "\">\n")
                .append("                            <Input v-model=\"searchData." + bean.getPropertyName() + "\" ></Input>\n")
                .append("                        </FormItem>\n")
                .append("                    </Col>\n");
        }
        sb.append("                </Row>\n")
            .append("                <Row>\n")
            .append("                    <Col>\n")
            .append("                       <FormItem>\n")
            .append("                           <Button type=\"primary\"  icon=\"ios-search\" @click=\"queryData()\">查询</Button>\n")
            .append("                           <Button type=\"primary\" ghost icon=\"ios-trash\" @click=\"clearQueryData()\">清空</Button>\n")
            .append("                       </FormItem>\n")
            .append("                    </Col>\n")
            .append("                </Row>\n")
            .append("            </Form>\n")
            .append("        </Card>\n")
            .append("        <Row>\n" +
                "                <Button type=\"primary\"  icon=\"plus-round\" @click=\"showAdd()\">新增" + tableBean.getTableComment() + "</Button>\n" +
                "        </Row>\n")

            .append("        <Table size=\"small\" border stripe :loading=\"loading\" :columns=\"columns\" :data=\"data\">\n")
            .append("            <p slot=\"loading\">数据正在加载中...</p>\n")
            .append("        </Table>\n")
            .append("        <div class=\"ics-page\">\n")
            .append("                <Page ref='page' :total=\"count\"\n" +
                "                      @on-change=\"queryData\"\n" +
                "                      @on-page-size-change=\"queryData\"\n" +
                "                      transfer show-total show-elevator show-sizer></Page>\n")
            .append("        </div>\n")
            .append("        <Modal\n" +
                "            v-model=\"showModal\"\n" +
                "            :title=\"modalTitle\"\n" +
                "            :mask-closable=\"false\">\n")
            .append("            <Form label-position=\"right\" :label-width=\"100\">\n");
        for (ColumnsBean bean : tableBean.getColumnsBeans()) {
            String disabled = StringUtils.equals(tableBean.getPrimaryPropertyKey(), bean.getPropertyName()) ? "disabled" : "";
            String vIf = StringUtils.equals(tableBean.getPrimaryPropertyKey(), bean.getPropertyName()) ? "v-if=\"!isAddModal\"" : "";
            sb.append("                <FormItem label=\"" + bean.getColumnComment() + "\" " + vIf + ">\n" +
                "                    <Input v-model=\"operData." + bean.getPropertyName() + "\" " + disabled + " ></Input>\n" +
                "                </FormItem>\n");
        }
        sb.append("            </Form>\n" +
            "            <div slot=\"footer\">\n" +
            "                <Button type=\"text\" size=\"large\" @click=\"modalCancel\">取消</Button>\n" +
            "                <Button type=\"primary\" size=\"large\" @click=\"addOrUpdate\">确定</Button>\n" +
            "            </div>\n" +
            "        </Modal>\n" +
            "    </div>\n" +
            "</template>\n");
        return sb.toString();
    }

    private static String buildScript(TableBean tableBean) {
        StringBuffer sb = new StringBuffer();
        String className = tableBean.getClassName();
        sb.append("<script>\n")
            .append("    import MetaSelect from '@/components/MetaSelect'\n" +
                "    import MetaTextField from '@/components/MetaTextField'\n" +
                "    import Api from \"@/common/api\";\n" +
                "    import Utils from '@/common/Utils';\n")
            .append("    export default {\n")
            .append("        name: '" + className + "Manage',\n" +
                "        components: {\n" +
                "            MetaSelect,\n" +
                "            MetaTextField\n" +
                "        },\n")
            .append("        data () {\n" +
                "            return {\n" +
                "                showModal: false,\n" +
                "                modalTitle: '新增" + tableBean.getTableComment() + "',\n" +
                "                isAddModal: true,\n" +
                "                loading: false,\n" +
                "                operData: {},\n" +
                "                searchData:{},\n" +
                "                columns: [");

        for (ColumnsBean bean : tableBean.getColumnsBeans()) {
            sb.append("{\n" +
                "                    title: '" + bean.getColumnComment() + "',\n" +
                "                    key: '" + bean.getPropertyName() + "',\n" +
                "                    width: 150\n" +
                "                },");
        }
        sb.append("{\n" +
            "                    title: '操作',\n" +
            "                    key: 'action',\n" +
            "                    fixed: 'right',\n" +
            "                    width: 120,\n" +
            "                    render: (h, params) => {\n" +
            "                        return h('div', [h('Button', {\n" +
            "                            props: {\n" +
            "                                type: 'primary',\n" +
            "                                size: 'small'\n" +
            "                            },\n" +
            "                            style: {\n" +
            "                                marginRight: '5px'\n" +
            "                            },\n" +
            "                            on:{\n" +
            "                                click: () => this.showUpdate(params.index)\n" +
            "                            }\n" +
            "                        }, '修改'), h('Button', {\n" +
            "                            props: {\n" +
            "                                type: 'error',\n" +
            "                                size: 'small'\n" +
            "                            },\n" +
            "                            style: {\n" +
            "                                marginRight: '5px'\n" +
            "                            },\n" +
            "                            on:{\n" +
            "                                click: () => this.showDelete(params.index)\n" +
            "                            }\n" +
            "                        }, '删除')]);\n" +
            "                    }\n" +
            "                }],\n" +
            "                data: [],\n" +
            "                count: 100,\n" +
            "            };\n" +
            "        },\n")
            .append("        mounted:function () {\n" +
                "            this.queryData();\n" +
                "        },\n")
            .append("        methods: {\n" +
                "            async queryData(){\n" +
                "                this.loading = true;\n" +
                "                let pageSize = this.$refs.page.currentPageSize;\n" +
                "                let pageNo = this.$refs.page.currentPage;\n" +
                "                let data = await Api.list" + tableBean.getClassName() + "(pageNo, pageSize, this.searchData);\n" +
                "                this.data = data.data;\n" +
                "                this.count = data.count;\n" +
                "                this.loading = false;\n" +
                "            },\n" +
                "            clearQueryData(){\n" +
                "                this.searchData = {};\n" +
                "                this.queryData();\n" +
                "            },\n" +
                "            showUpdate(index){\n" +
                "                this.showModal = true;\n" +
                "                this.isAddModal = false;\n" +
                "                this.modalTitle = \"修改" + tableBean.getTableComment() + "\";\n" +
                "                this.operData = Object.assign({},this.data[index]);\n" +
                "            },\n" +
                "            showAdd(){\n" +
                "                this.showModal = true;\n" +
                "                this.isAddModal = true;\n" +
                "                this.modalTitle = \"新增" + tableBean.getTableComment() + "\";\n" +
                "                this.operData = {};\n" +
                "            },\n" +
                "            showDelete(index){\n" +
                "                let " + fristLower(tableBean.getClassName()) + " = this.data[index];\n" +
                "                let self = this;\n" +
                "                this.$Modal.confirm({\n" +
                "                    title: '删除提示',\n" +
                "                    content: `<p>确定删除\"${" + fristLower(tableBean.getClassName()) + "." + tableBean.getPrimaryPropertyKey() + "}(${" + fristLower(tableBean.getClassName()) + "." + tableBean.getPrimaryPropertyKey() + "})\"</p>`,\n" +
                "                    okText: '确定删除',\n" +
                "                    cancelText: '取消',\n" +
                "                    onOk: function () {\n" +
                "                        Api.delete" + tableBean.getClassName() + "ById(" + fristLower(tableBean.getClassName()) + "." + tableBean.getPrimaryPropertyKey() + ").then(data => {\n" +
                "                            Utils.showMessage(self, data, \"删除成功\", \"删除失败\");\n" +
                "                            self.queryData();\n" +
                "                        });\n" +
                "                    }\n" +
                "                });\n" +
                "            },\n" +
                "            modalCancel: function () {\n" +
                "                this.showModal = false;\n" +
                "            },\n" +
                "            addOrUpdate: function () {\n" +
                "                let operResult;\n" +
                "                let self = this;\n" +
                "                if (this.isAddModal){\n" +
                "                    operResult = Api.insert" + tableBean.getClassName() + "(this.operData);\n" +
                "                }else{\n" +
                "                    operResult = Api.update" + tableBean.getClassName() + "(this.operData." + tableBean.getPrimaryPropertyKey() + ", this.operData);\n" +
                "                }\n" +
                "                operResult.then(data => {\n" +
                "                    Utils.showMessage(self, data, \"操作成功\", \"操作失败\");\n" +
                "                    if (data.success){\n" +
                "                        this.showModal = false;\n" +
                "                        self.queryData();\n" +
                "                    }\n" +
                "                });\n" +
                "            }\n" +
                "        }\n" +
                "    };\n");
        sb.append("</script>\n");
        return sb.toString();
    }
}
