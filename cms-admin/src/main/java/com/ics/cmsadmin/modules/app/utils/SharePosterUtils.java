package com.ics.cmsadmin.modules.app.utils;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectRequest;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.property.AliOssConfig;
import com.ics.cmsadmin.frame.utils.QrCodeCreateUtil;
import com.ics.cmsadmin.frame.utils.Tools;
import com.ics.cmsadmin.modules.student.bean.StudentCertificateBean;
import com.ics.cmsadmin.modules.student.dao.StudentCertificateDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.util.Optional;

@Component
@Slf4j
public final class SharePosterUtils {

    private static final int TITLE_FONT_SIZE = 48;
    private static final int CONTENT_FONT_SIZE = 36;
    private static final int QR_CODE_SIZE = 300;
    private static final int Y_TITLE = 450;
    private static final int X_CONTENT = 100;
    private static final int Y_CONTENT = 590;
    private static final int Y_QR_CODE = 710;

    private static final int COURSE_QR_CODE_SIZE = 200;
    private static final int Y_COURSE_TITLE = 650;
    private static final int Y_COURSE_CONTENT = 770;
    private static final int X_COURSE_CONTENT = 50;


    private static AliOssConfig aliOssConfig;
    private static StudentCertificateDao studentCertificateDao;
    @Resource
    public void setAliOssConfig(AliOssConfig aliOssConfig) {
        SharePosterUtils.aliOssConfig = aliOssConfig;
    }

    @Resource
    public void setStudentCertificateDao(StudentCertificateDao studentCertificateDao) {
        SharePosterUtils.studentCertificateDao = studentCertificateDao;
    }

    /**
     * 获取课程结业证书相关信息
     */
    public static StudentCertificateBean getCourseCompletion(String userName, String studentId, String courseName, String courseId) {
        String fileName = String.format("course_%s_%s.png", studentId, courseId);
        String picUrl = uploadAliOss(generateCourseCompletionImage(userName, studentId, courseName), fileName);
        if (StringUtils.isBlank(picUrl)) {
            return null;
        }
        StudentCertificateBean build = StudentCertificateBean.builder()
            .studentId(studentId)
            .studentName(userName)
            .pictureName(fileName)
            .pictureUrl(picUrl)
            .shareType(CommonEnums.SharePosterType.COMPLETE_COURSE.getCode() + "")
            .courseId(courseId)
            .courseName(courseName)
            .build();
        studentCertificateDao.merge(build);
        return build;
    }

    /**
     * 获取入学通知书相关信息
     */
    public static StudentCertificateBean getAdmissionNotice(String userName, String studentId) {
        String fileName = String.format("admission_%s.png", studentId);
        StudentCertificateBean studentCertificateBean = studentCertificateDao.queryByPictureName(fileName);
        if (studentCertificateBean != null && StringUtils.equalsIgnoreCase(userName, studentCertificateBean.getStudentName())) {
            return studentCertificateBean;
        }
        String picUrl = uploadAliOss(generateAdmissionNoticeImage(userName, studentId), fileName);
        if (StringUtils.isBlank(picUrl)) {
            return null;
        }
        StudentCertificateBean build = StudentCertificateBean.builder()
            .studentId(studentId)
            .studentName(userName)
            .pictureName(fileName)
            .pictureUrl(picUrl)
            .shareType(CommonEnums.SharePosterType.First_purchase.getCode() + "")
            .build();
        studentCertificateDao.merge(build);
        return build;
    }

    /**
     * 获取 学时成就奖 相关信息
     */
    public static StudentCertificateBean getHoursAchievementAward(String userName, String studentId, long hours) {
        String fileName = String.format("hours_%s_%s.png", studentId, hours + "");
        String picUrl = uploadAliOss(generateHoursAchievementAwardImage(userName, studentId, hours), fileName);
        if (StringUtils.isBlank(picUrl)) {
            return null;
        }
        StudentCertificateBean build = StudentCertificateBean.builder()
            .studentId(studentId)
            .studentName(userName)
            .pictureName(fileName)
            .pictureUrl(picUrl)
            .shareType(CommonEnums.SharePosterType.FULL_HOUR.getCode() + "")
            .hours(hours + "")
            .build();
        studentCertificateDao.merge(build);
        return build;
    }
    /**
     * 生成课程结业证书
     * @param userName   用户名
     * @param studentId  用户id
     */
    public static BufferedImage generateCourseCompletionImage(String userName, String studentId, String courseName) {
        try (InputStream imageInputStream = Tools.getResourceInputStream("classpath:/static/image/courseCompletion.png");
             InputStream fontTtfInputStream = Tools.getResourceInputStream("classpath:/ttf/msyh.ttf")) {
            Font baseFont = Font.createFont(Font.TRUETYPE_FONT, fontTtfInputStream);
            BufferedImage read = ImageIO.read(imageInputStream);
            Graphics2D  graphics = (Graphics2D) read.getGraphics();
            int imageWidth = read.getWidth();
            drawCourseTitle(graphics, imageWidth, userName, baseFont);
            drawCourseCompletionContent(graphics, imageWidth, courseName, baseFont);
            BufferedImage qrCode = QrCodeCreateUtil.createQrCode(AppUtils.getStudentShareUrl(studentId), COURSE_QR_CODE_SIZE);
            graphics.drawImage(qrCode, null, imageWidth / 2 + 85, 900);
            return read;
        } catch (Exception e) {
            log.error("绘制结业证书异常{}", e);
            return null;
        }
    }

    /**
     * 获取入学通知书
     * @param userName   用户名
     * @param studentId  用户id
     */
    public static BufferedImage generateAdmissionNoticeImage(String userName, String studentId) {
        try (InputStream imageInputStream = Tools.getResourceInputStream("classpath:/static/image/admissionNotice.png");
             InputStream fontTtfInputStream = Tools.getResourceInputStream("classpath:/ttf/msyh.ttf")) {
            Font baseFont = Font.createFont(Font.TRUETYPE_FONT, fontTtfInputStream);
            BufferedImage read = ImageIO.read(imageInputStream);
            Graphics2D  graphics = (Graphics2D) read.getGraphics();
            int imageWidth = read.getWidth();
            drawTitle(graphics, imageWidth, userName, baseFont);
            drawAdmissNoticeContent(graphics, imageWidth, baseFont);
            BufferedImage qrCode = QrCodeCreateUtil.createQrCode(AppUtils.getStudentShareUrl(studentId), QR_CODE_SIZE);
            graphics.drawImage(qrCode, null, (imageWidth - qrCode.getWidth()) / 2, Y_QR_CODE);
            return read;
        } catch (Exception e) {
            log.error("绘制入学通知书异常{}", e);
            return null;
        }
    }

    /**
     * 学时成就奖
     * @param userName  用户名
     * @param studentId 用户id
     * @param hours     完成学时
     */
    public static BufferedImage generateHoursAchievementAwardImage(String userName, String studentId, long hours) {
        try (InputStream imageInputStream = Tools.getResourceInputStream("classpath:/static/image/hoursAchievementAward.png");
             InputStream fontTtfInputStream = Tools.getResourceInputStream("classpath:/ttf/msyh.ttf")) {
            Font baseFont = Font.createFont(Font.TRUETYPE_FONT, fontTtfInputStream);

            BufferedImage read = ImageIO.read(imageInputStream);
            Graphics2D  graphics = (Graphics2D) read.getGraphics();
            int imageWidth = read.getWidth();

            drawTitle(graphics, imageWidth, userName, baseFont);
            drawHoursAchievementAwardContent(graphics, imageWidth, hours, baseFont);
            BufferedImage qrCode = QrCodeCreateUtil.createQrCode(AppUtils.getStudentShareUrl(studentId), QR_CODE_SIZE);
            graphics.drawImage(qrCode, null, (imageWidth - qrCode.getWidth()) / 2, Y_QR_CODE);
            return read;
        } catch (Exception e) {
            log.error("绘制学时成就奖异常{}", e);
            return null;
        }
    }

    private static void drawCourseCompletionContent(Graphics2D graphics, int imageWidth, String courseName, Font baseFont) {
        courseName = "「" + prune(courseName, 8, "...") + "」";
        Font courseFont = baseFont.deriveFont(Font.ITALIC, CONTENT_FONT_SIZE);
        FontMetrics courseFm = graphics.getFontMetrics(courseFont);
        Font contentFont = baseFont.deriveFont(Font.PLAIN, CONTENT_FONT_SIZE);
        FontMetrics contentFm = graphics.getFontMetrics(contentFont);

        int oneHanziChineseWidth = contentFm.stringWidth("一");
        String fristLeftContent = "恭喜您顺利完成了";
        String fristRightContent = "课程";
        int fristLeftContentWidth = contentFm.stringWidth(fristLeftContent);
        int hoursTextWidth = courseFm.stringWidth(courseName);
        int fristRightContentWidth = contentFm.stringWidth(fristRightContent);

        graphics.setFont(contentFont);
        graphics.drawString(fristLeftContent, X_COURSE_CONTENT + oneHanziChineseWidth * 2 , Y_COURSE_CONTENT);
        graphics.setFont(courseFont);
        graphics.drawString(courseName, X_COURSE_CONTENT + oneHanziChineseWidth * 2 + fristLeftContentWidth, Y_COURSE_CONTENT);
        graphics.setFont(contentFont);
        graphics.drawString(fristRightContent, X_COURSE_CONTENT + oneHanziChineseWidth * 2 + fristLeftContentWidth + hoursTextWidth, Y_COURSE_CONTENT);

        String content = "掌握了STEAM课程核心技能，继续加油。";
        int contentWidth = contentFm.stringWidth(content);
        graphics.setFont(contentFont);
        graphics.drawString(content, X_COURSE_CONTENT, Y_COURSE_CONTENT + contentFm.getHeight() * 5 / 4);

    }

    private static void drawHoursAchievementAwardContent(Graphics2D graphics, int imageWidth, long hours, Font baseFont) {

        String hoursText = hours <= 0 ? " 1 " : " " + hours + " ";
        Font hoursFont = baseFont.deriveFont(Font.ITALIC, CONTENT_FONT_SIZE);

        // 绘制主体内容
        Font contentFont = baseFont.deriveFont(Font.PLAIN, CONTENT_FONT_SIZE);
        graphics.setColor(new Color(51, 51, 51));
        FontMetrics hoursFm = graphics.getFontMetrics(hoursFont);
        FontMetrics contentFm = graphics.getFontMetrics(contentFont);

        int oneHanziChineseWidth = contentFm.stringWidth("一");
        String fristLeftContent = "恭喜您完成了";
        String fristRightContent = "小时的STEAM课程学习，";
        int fristLeftContentWidth = contentFm.stringWidth(fristLeftContent);
        int hoursTextWidth = hoursFm.stringWidth(hoursText);
        int fristRightContentWidth = contentFm.stringWidth(fristRightContent);

        graphics.setFont(contentFont);
        graphics.drawString(fristLeftContent, X_CONTENT + oneHanziChineseWidth * 2, Y_CONTENT );
        graphics.setFont(hoursFont);
        graphics.drawString(hoursText, X_CONTENT + oneHanziChineseWidth * 2 + fristLeftContentWidth, Y_CONTENT);
        graphics.setFont(contentFont);
        graphics.drawString(fristRightContent, X_CONTENT + oneHanziChineseWidth * 2 + fristLeftContentWidth + hoursTextWidth, Y_CONTENT );

        String content = "坚持就会有所收获，继续加油!";
        int contentWidth = contentFm.stringWidth(content);
        graphics.setFont(contentFont);
        graphics.drawString(content, X_CONTENT, Y_CONTENT + contentFm.getHeight() * 5 / 4);

    }

    /**
     * 绘制入学通知书主体内容
     */
    private static void drawAdmissNoticeContent(Graphics2D graphics, int imageWidth, Font baseFont) {
        // 绘制主体内容
        Font contentFont = baseFont.deriveFont(Font.PLAIN, CONTENT_FONT_SIZE);
        FontMetrics contentFm = graphics.getFontMetrics(contentFont);
        graphics.setColor(new Color(51, 51, 51));

        int oneHanziChineseWidth = contentFm.stringWidth("一");
        graphics.setFont(contentFont);
        String content = "欢迎入学，祝您在爱科塾的学习旅途有";
        int contentHeight = contentFm.getHeight();
        graphics.drawString(content, X_CONTENT + oneHanziChineseWidth * 2, Y_CONTENT);

        content = "所收获，成绩不断提高。";
        graphics.drawString(content, X_CONTENT, Y_CONTENT + contentHeight * 5 / 4);
    }
    /**
     * 绘制标题
     */
    private static void drawTitle(Graphics2D graphics, int imageWidth, String userName, Font baseFont) {
        userName = prune(userName, 10, "...");
        // 获取用户名的宽度
        Font userNameFont = baseFont.deriveFont(Font.ITALIC, TITLE_FONT_SIZE);
        int userNameWidth = graphics.getFontMetrics(userNameFont).stringWidth(userName);

        // 获取"同学"的宽度
        String content = "同学";
        Font studentFont = baseFont.deriveFont(Font.BOLD, TITLE_FONT_SIZE);
        int studentWidth = graphics.getFontMetrics(studentFont).stringWidth(content);

        // 绘制标题 "用户名 同学"
        graphics.setFont(userNameFont);
//            graphics.setColor(Color.RED);
        graphics.setColor(Color.BLACK);
        graphics.drawString(userName, (imageWidth  - userNameWidth - studentWidth) / 2, Y_TITLE);
        graphics.setFont(studentFont);
        graphics.setColor(Color.BLACK);
        graphics.drawString(content, (imageWidth - userNameWidth - studentWidth) / 2 + userNameWidth, Y_TITLE);

    }

    private static void drawCourseTitle(Graphics2D graphics, int imageWidth, String userName, Font baseFont) {
        userName = prune(userName, 10, "...");
        // 获取用户名的宽度
        Font userNameFont = baseFont.deriveFont(Font.ITALIC, TITLE_FONT_SIZE);
        int userNameWidth = graphics.getFontMetrics(userNameFont).stringWidth(userName);

        // 获取"同学"的宽度
        String content = "同学";
        Font studentFont = baseFont.deriveFont(Font.BOLD, TITLE_FONT_SIZE);
        int studentWidth = graphics.getFontMetrics(studentFont).stringWidth(content);

        // 绘制标题 "用户名 同学"
        graphics.setFont(userNameFont);
//            graphics.setColor(Color.RED);
        graphics.setColor(Color.BLACK);
        graphics.drawString(userName, (imageWidth  - userNameWidth - studentWidth) / 2, Y_COURSE_TITLE);
        graphics.setFont(studentFont);
        graphics.setColor(Color.BLACK);
        graphics.drawString(content, (imageWidth - userNameWidth - studentWidth) / 2 + userNameWidth, Y_COURSE_TITLE);

    }
//    /**
//     * 绘制二维码
//     */
//    private static void drawQrCode(Graphics2D graphics, int imageWidth, String studentId) throws Exception {
//        BufferedImage qrCode = QrCodeCreateUtil.createQrCode(AppUtils.getStudentShareUrl(studentId), QR_CODE_SIZE);
//        graphics.drawImage(qrCode, null, (imageWidth - qrCode.getWidth()) / 2, Y_QR_CODE);
//    }

    private static String prune(String text, int maxLength, String exceedSuffix) {
        text = Optional.ofNullable(text).orElse("").trim();
        if (text.length() > maxLength) {
            text = text.substring(0, maxLength) + exceedSuffix;
        }
        text += " ";
        return text;
    }

    private static String uploadAliOss(BufferedImage bufferedImage, String fileName) {
        if (bufferedImage == null) {
            return null;
        }
        try {
            File tempFile = File.createTempFile("uploadAliOss", "png");
            OSSClient ossClient = new OSSClient("https://" + aliOssConfig.endpoint, aliOssConfig.accessKeyId, aliOssConfig.accessKeySecret);
            ImageIO.write(bufferedImage, "png", tempFile);
            String baseFilePath = "image/certificate/";
            ossClient.putObject(new PutObjectRequest(aliOssConfig.bucketName, baseFilePath + fileName, tempFile));
            return "https://" + aliOssConfig.bucketName + "." + aliOssConfig.endpoint + "/" + baseFilePath + fileName;
        } catch (Exception e) {
            log.error("上传阿里云oss异常 {}", e);
            return null;
        }
    }

    public static void main(String[] args) throws Exception {
        BufferedImage image1 = generateAdmissionNoticeImage("风动风动风动风动风动风动", "123456789abcdefghijk");
        BufferedImage image2 = generateHoursAchievementAwardImage("风动", "123456789abcdefghijk", 1);
        BufferedImage image3 = generateCourseCompletionImage("风动", "123456789abcdefghijk", "语文语文语文语文语文语文");
        ImageIO.write(image1, "png", new File("d:/1.png"));
        ImageIO.write(image2, "png", new File("d:/2.png"));
        ImageIO.write(image3, "png", new File("d:/3.png"));
    }
}

