package com.ics.cmsadmin.modules.auth.steward;

import com.ics.cmsadmin.modules.auth.dao.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AuthRepositories {
    public static AuthorizeDao authorizeDao;
    public static MenuDao menuDao;
    public static OrganizationDao organizationDao;
    public static RoleAuthorizeDao roleAuthorizeDao;
    public static RoleMenuDao roleMenuDao;
    public static UserDao userDao;
    public static UserRoleDao userRoleDao;
    public static RoleDao roleDao;

    @Resource
    public void setAuthorizeDao(AuthorizeDao authorizeDao) {
        AuthRepositories.authorizeDao = authorizeDao;
    }

    @Resource
    public void setMenuDao(MenuDao menuDao) {
        AuthRepositories.menuDao = menuDao;
    }

    @Resource
    public void setOrganizationDao(OrganizationDao organizationDao) {
        AuthRepositories.organizationDao = organizationDao;
    }

    @Resource
    public void setRoleAuthorizeDao(RoleAuthorizeDao roleAuthorizeDao) {
        AuthRepositories.roleAuthorizeDao = roleAuthorizeDao;
    }

    @Resource
    public void setRoleMenuDao(RoleMenuDao roleMenuDao) {
        AuthRepositories.roleMenuDao = roleMenuDao;
    }

    @Resource
    public void setUserDao(UserDao userDao) {
        AuthRepositories.userDao = userDao;
    }

    @Resource
    public void setUserRoleDao(UserRoleDao userRoleDao) {
        AuthRepositories.userRoleDao = userRoleDao;
    }

    @Resource
    public void setRoleDao(RoleDao roleDao) {
        AuthRepositories.roleDao = roleDao;
    }
}
