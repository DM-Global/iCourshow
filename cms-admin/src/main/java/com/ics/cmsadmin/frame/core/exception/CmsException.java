package com.ics.cmsadmin.frame.core.exception;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import lombok.Getter;

@Getter
public class CmsException extends RuntimeException {

    private ApiResultEnum apiResultEnum;

    public CmsException(ApiResultEnum resultEnum) {
        super(resultEnum.getDetail());
        this.apiResultEnum = resultEnum;
    }

    public CmsException(ApiResultEnum resultEnum, String message) {
        super(message);
        this.apiResultEnum = resultEnum;
    }
}
