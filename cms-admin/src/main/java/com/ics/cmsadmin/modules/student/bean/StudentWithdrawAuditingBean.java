package com.ics.cmsadmin.modules.student.bean;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * t_student_withdraw_auditing
 * 学生提现审核记录
 * <p>
 * Created by lvsw on 2019-44-31 16:05:02.
 */
@Data
@Builder
public class StudentWithdrawAuditingBean {

    private String id;              // 申请流水id
    private String studentId;              // 学生编号
    private String username;              // 学生编号
    private String nickname;              // 学生编号
    private BigDecimal withdrawCash;     // 申请提现金额
    private String status;              // 状态, 0
    private String remark;              // 备注
    private String withdrawType;        // 提现类型
    private String relationOrderNo;     // 关联的订单
    private String openId;
    private String paymentNo;           // 关联的订单
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间

    @Tolerate
    public StudentWithdrawAuditingBean() {
    }
}
