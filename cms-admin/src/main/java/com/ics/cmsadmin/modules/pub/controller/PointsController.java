package com.ics.cmsadmin.modules.pub.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.pub.bean.Point;
import com.ics.cmsadmin.modules.pub.bean.PointVO;
import com.ics.cmsadmin.modules.pub.service.PointService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/points")
public class PointsController extends ResourceController<PointService, Point, Integer> {
    @Resource
    private PointService pointService;

    @GetMapping
    public ApiResponse get(String name, Integer subjectId, Integer courseId, Integer unitId, Integer topicId, Boolean isActive,
                           @RequestParam(value = "page", required = false) Integer page,
                           @RequestParam(value = "pageSize", required = false) Integer pageSize,
                           HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        Map<String, Object> param = new HashMap<>();
        param.put("name", name);
        param.put("subjectId", subjectId);
        param.put("courseId", courseId);
        param.put("unitId", unitId);
        param.put("topicId", topicId);
        param.put("isActive", isActive);
        if (page != null && pageSize != null) {
            PageHelper.startPage(page, pageSize);
        } else {
            List<PointVO> data = pointService.getMultiple(param, userId);
            long total = data.size();
            return ApiResponse.getDefaultResponse(PageResult.getPage(total, data));
        }
        List<PointVO> data = pointService.getMultiple(param, userId);
        long total = ((Page) data).getTotal();
        PageResult pageResult = PageResult.getPage(total, data);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @GetMapping("/{pointId}/detail")
    public ApiResponse get(@PathVariable Integer pointId,
                           HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return ApiResponse.getDefaultResponse(pointService.get(pointId, userId));
    }

}
