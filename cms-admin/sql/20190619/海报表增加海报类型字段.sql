ALTER TABLE `t_share_poster`
ADD COLUMN `share_type` tinyint(1) NOT NULL DEFAULT 1 COMMENT '分享类型：1购买分享 2满一小时分享 3完成课程分享 4分享赚奖学金' AFTER `is_active`;

INSERT INTO `com_meta`(`meta_id`, `parent_id`, `name`, `value`, `order_index`, `create_time`, `update_time`) VALUES ('common-share-type', 'common', '分享类型', NULL, 0, '2019-06-21 00:14:05', NULL);
INSERT INTO `com_meta`(`meta_id`, `parent_id`, `name`, `value`, `order_index`, `create_time`, `update_time`) VALUES ('common-share-type-buy', 'common-share-type', '购买分享', '1', 1, '2019-06-21 00:16:51', NULL);
INSERT INTO `com_meta`(`meta_id`, `parent_id`, `name`, `value`, `order_index`, `create_time`, `update_time`) VALUES ('common-share-type-finish', 'common-share-type', '完成课程分享', '3', 3, '2019-06-21 00:18:54', NULL);
INSERT INTO `com_meta`(`meta_id`, `parent_id`, `name`, `value`, `order_index`, `create_time`, `update_time`) VALUES ('common-share-type-profit', 'common-share-type', '分享赚奖学金', '4', 4, '2019-06-21 00:19:56', NULL);
INSERT INTO `com_meta`(`meta_id`, `parent_id`, `name`, `value`, `order_index`, `create_time`, `update_time`) VALUES ('common-share-type-time', 'common-share-type', '满一小时分享', '2', 2, '2019-06-21 00:18:01', NULL);
