package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.property.WechatConfig;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.basic.service.PayService;
import com.lly835.bestpay.model.PayResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * Created by Sandwich on 2018-08-18
 */
@Controller
@RequestMapping("api/pay")
@Log4j2
public class ApiPayController {

    @Autowired
    private WechatConfig wechatConfig;

    @Autowired
    private OrderService orderService;

    @Autowired
    private PayService payService;

    @GetMapping("/create")
    public ModelAndView create(@RequestParam("orderId") String orderId,@RequestParam("openid") String openid,
                               @RequestParam("returnUrl") String returnUrl,
                               Map<String, Object> map) {
        log.info("Going to pay for openId: {}",openid);
        if (StringUtils.isBlank(openid)){
            throw new CmsException(ApiResultEnum.DATA_NOT_EXIST);
        }
        //1. 查询订单
        OrderBean order = orderService.queryById(orderId);
        if (order == null) {
            throw new CmsException(ApiResultEnum.ORDER_NOT_EXIST);
        }
        //2. 发起支付
        PayResponse payResponse = payService.create(order,openid);
        map.put("payResponse", payResponse);
        map.put("returnUrl", returnUrl);
        map.put("payFailUrl", wechatConfig.payFailRedirectUrl);
        return new ModelAndView("/pay/create", map);
    }

    /**
     * 微信异步通知
     *
     * @param notifyData
     */
    @PostMapping("/notify")
    public ModelAndView orderNotify(@RequestBody String notifyData) {
        payService.orderNotify(notifyData);
        return new ModelAndView("/pay/success");
    }

}
