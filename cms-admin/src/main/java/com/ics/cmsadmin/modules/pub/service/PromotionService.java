package com.ics.cmsadmin.modules.pub.service;

import com.ics.cmsadmin.modules.pub.dao.PromotionMapper;
import com.ics.cmsadmin.modules.pub.bean.Promotion;
import com.ics.cmsadmin.frame.utils.LevelInfoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("PromotionService")
public class PromotionService extends ResourceService<PromotionMapper, Promotion, Integer> {
    @Autowired
    private LevelInfoUtil levelInfoUtil;

    @Override
    public List<Promotion> getMultiple(Map<String, Object> param) {
        List<Promotion> promotions = super.getMultiple(param);
        for (Promotion promotion : promotions) {
            promotion.setLevelInfo(levelInfoUtil.setLevelInfo(promotion.getLevel(), promotion.getLevelId()));
        }
        return promotions;
    }

    @Override
    public Promotion get(Integer id) {
        Promotion promotion = super.get(id);
        promotion.setLevelInfo(levelInfoUtil.setLevelInfo(promotion.getLevel(), promotion.getLevelId()));
        return promotion;
    }
}
