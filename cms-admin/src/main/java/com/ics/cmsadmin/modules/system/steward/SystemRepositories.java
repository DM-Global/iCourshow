package com.ics.cmsadmin.modules.system.steward;

import com.ics.cmsadmin.modules.system.dao.*;
import com.ics.cmsadmin.modules.system.service.AppFeedbackService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class SystemRepositories {
    public static CityDao cityDao;
    public static MetaDao metaDao;
    public static RegisterDao registerDao;
    public static SequenceNumberDao sequenceNumberDao;
    public static UserOperLogDao userOperLogDao;
    public static AppVersionDao appVersionDao;
    public static PhoneCodeDao phoneCodeDao;
    public static AppFeedbackDao appFeedbackDao;

    @Resource
    public void setPhoneCodeDao(PhoneCodeDao phoneCodeDao) {
        SystemRepositories.phoneCodeDao = phoneCodeDao;
    }

    @Resource
    public void setAppFeedbackDao(AppFeedbackDao appFeedbackDao) {
        SystemRepositories.appFeedbackDao = appFeedbackDao;
    }

    @Resource
    public void setCityDao(CityDao cityDao) {
        SystemRepositories.cityDao = cityDao;
    }

    @Resource
    public void setMetaDao(MetaDao metaDao) {
        SystemRepositories.metaDao = metaDao;
    }

    @Resource
    public void setRegisterDao(RegisterDao registerDao) {
        SystemRepositories.registerDao = registerDao;
    }

    @Resource
    public void setSequenceNumberDao(SequenceNumberDao sequenceNumberDao) {
        SystemRepositories.sequenceNumberDao = sequenceNumberDao;
    }

    @Resource
    public void setUserOperLogDao(UserOperLogDao userOperLogDao) {
        SystemRepositories.userOperLogDao = userOperLogDao;
    }

    @Resource
    public void setAppVersionDao(AppVersionDao appVersionDao) {
        SystemRepositories.appVersionDao = appVersionDao;
    }
}
