package com.ics.cmsadmin.frame.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ics.cmsadmin.frame.api.response.PolyvResponse;
import com.ics.cmsadmin.frame.api.response.PolyvVideoInfo;
import com.ics.cmsadmin.frame.core.exception.ExceptionUtils;
import com.ics.cmsadmin.frame.property.PolyvConfig;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Component
@Log4j2
public class PolyvApi {
    private static PolyvConfig polyvConfig;

    @Resource
    public void setPolyvConfig(PolyvConfig polyvConfig) {
        PolyvApi.polyvConfig = polyvConfig;
    }

    /**
     * {
     * "code": 200,
     * "status": "success",
     * "message": "success",
     * "data": [{
     * "images_b": ["30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_0_b.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1_b.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2_b.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_3_b.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_4_b.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_5_b.jpg"],
     * "images": ["30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_0.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_3.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_4.jpg",
     * "30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_5.jpg"],
     * "imageUrls": ["http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_0.jpg",
     * "http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1.jpg",
     * "http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2.jpg",
     * "http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_3.jpg",
     * "http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_4.jpg",
     * "http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_5.jpg"],
     * "tag": "",
     * "mp4": "http://mpv.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1.mp4",
     * "title": "计数（一）",
     * "df": 3,
     * "times": "97",
     * "vid": "30fe77e88f6de5ae918dc1835bedbee0_3",
     * "mp4_1": "http://mpv.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1.mp4",
     * "mp4_2": "http://mpv.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2.mp4",
     * "mp4_3": "http://mpv.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_3.mp4",
     * "cataid": "1",
     * "swf_link": "http://player.polyv.net/videos/30fe77e88f6de5ae918dc1835bedbee0_3.swf",
     * "status": "61",
     * "seed": 0,
     * "flv1": "http://plvod01.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1.flv",
     * "flv2": "http://plvod01.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2.flv",
     * "flv3": "http://plvod01.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_3.flv",
     * "sourcefile": "",
     * "playerwidth": "600",
     * "default_video": "http://plvod01.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2.flv",
     * "duration": "00:04:53",
     * "first_image": "http://img.videocc.net/uimage/3/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_0.jpg",
     * "original_definition": "2560x1440",
     * "context": "",
     * "playerheight": "337",
     * "ptime": "2018-10-08 07:58:18",
     * "source_filesize": 46296080,
     * "filesize": [8426874,
     * 21443407,
     * 44592619],
     * "md5checksum": "d1d2f3666afc96802eb245380a39140d",
     * "hls": ["http://hls.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_1.m3u8",
     * "http://hls.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_2.m3u8",
     * "http://hls.videocc.net/30fe77e88f/0/30fe77e88f6de5ae918dc1835bedbee0_3.m3u8"],
     * "previewVid": "i60nc44c55n3kc8lc915km1568pckpcc0_6"
     * }]
     * }
     *
     * @param vid
     * @throws IOException
     */
    public static PolyvVideoInfo getVideoMsg(String vid) {
        try {
            if (StringUtils.isBlank(vid)) {
                return null;
            }
            Map<String, String> param = new TreeMap<>();
            param.put("vid", vid);
            param.put("ptime", System.currentTimeMillis() + "");
            param.put("format", "json");
            StringBuilder sb = new StringBuilder();
            for (Map.Entry<String, String> entry : param.entrySet()) {
                sb.append(entry.getKey() + "=" + entry.getValue() + "&");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(polyvConfig.getSecretKey());
            param.put("sign", DigestUtils.sha1Hex(sb.toString()).toUpperCase());
            String url = String.format("http://api.polyv.net/v2/video/%s/get-video-msg", polyvConfig.getUserId());

            String body = Jsoup.connect(url)
                .method(Connection.Method.POST)
                .data(param)
                .ignoreContentType(true)
                .execute()
                .body();
            log.info("请求保利威api url = {}, 请求参数为: {}, 返回值为{}", url, param, body);
            Gson gson = new Gson();
            PolyvResponse<List<PolyvVideoInfo>> result = gson.fromJson(body, new TypeToken<PolyvResponse<List<PolyvVideoInfo>>>() {
            }.getType());
            if (result.getCode() == 200) {
                return result.getData().get(0);
            } else {
                return null;
            }
        } catch (Exception e) {
            log.info("请求保利威api异常{}", ExceptionUtils.collectExceptionStackMsg(e));
            return null;
        }
    }
}
