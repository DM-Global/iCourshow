package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseMapper extends ResourceMapper<Course, Integer> {
    int setCourseType(@Param("courseId") Integer courseId,
                      @Param("courseTypeId") Integer courseTypeId);

    int unsetCourseType(@Param("courseId") Integer courseId,
                        @Param("courseTypeId") Integer courseTypeId);

    @Override
    Course select(Integer id);

    Course select(@Param("id") Integer id, @Param("userId") String userId);

    List<Course> findByIdIn(@Param("list") List<Integer> ids, @Param("userId") String userId);
}
