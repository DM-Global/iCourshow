drop table t_upush_history;
CREATE TABLE `t_upush_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `student_id` varchar(40)  NULL COMMENT '单播给指定用户时保留用户id',
  `type` varchar(32)  NULL COMMENT '推送类型',
  `title` varchar(128) NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4  NULL COMMENT '内容',
  `details` text CHARACTER SET utf8mb4  NULL COMMENT '详情',
  `extra` text NULL COMMENT '额外参数',
  `is_read` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否已读：0.未读 1.已读',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
);


ALTER TABLE `t_student`
ADD COLUMN `android_device_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '安卓友盟device_token' AFTER `umeng_device_token`,
ADD COLUMN `ios_device_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'ios友盟device_token' AFTER `android_device_token`;
COMMIT;
