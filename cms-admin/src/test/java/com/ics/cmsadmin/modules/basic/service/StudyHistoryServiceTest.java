package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.bean.StudyHistory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;


@Slf4j
public class StudyHistoryServiceTest extends CmsAdminApplicationTests {

    @Autowired
    private StudyHistoryService studyHistoryService;

    @Test
    public void getRecentStudy() {
        List<PointBean> recentStudy = studyHistoryService.getRecentStudy(StudyHistory.builder().userId("123456").build(), new PageBean(1, Integer.MAX_VALUE));
        log.info("result = {}", recentStudy);
    }

    @Test
    public void insert() {
        StudyHistory studyHistory = StudyHistory.builder().userId("123456").levelName(LevelEnum.point).levelId("12").progress(1).build();
        studyHistoryService.insert(studyHistory);
    }

    @Test
    public void findOne() {
        StudyHistory studyHistory = StudyHistory.builder().userId("123456").levelName(LevelEnum.point).levelId("12").progress(1).build();
        StudyHistory one = studyHistoryService.findOne(studyHistory);
        log.info("one = {}", one);
    }
}
