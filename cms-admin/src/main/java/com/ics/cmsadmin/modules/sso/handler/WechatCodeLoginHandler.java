package com.ics.cmsadmin.modules.sso.handler;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.property.WechatConfig;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.frame.api.WechatApi;
import com.ics.cmsadmin.frame.api.WechatApiResponse;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.sso.LoginHandler;
import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoContants;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 微信code登陆
 * Created by 666666 on 2018/8/25.
 */
@Log4j2
@Component
public class WechatCodeLoginHandler implements LoginHandler<StudentBean> {

    @Resource
    private WechatConfig config;
    @Resource(name = "newStudentService")
    private StudentService studentService;

    @Override
    public StudentBean dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum) {
        String code = request.getParameter(SsoContants.LOGIN_CODE);
        String agentNo = request.getParameter(SsoContants.LOGIN_AGENT_NO);
        String recommenderStudent = request.getParameter(SsoContants.LOGIN_RECOMMEND_STUDENT_ID);
        log.info("获取的所有参数:" + GsonUtils.toJson(request.getParameterMap()));
        log.info("获取到的code " + code);
        log.info("-------------> agentNo = " + agentNo);
        WechatApiResponse accessTokenByCode = WechatApi.getAccessTokenByCode(config.getAppId(), config.getAppSecret(), code);
        if (accessTokenByCode == null || accessTokenByCode.getErrcode() != 0) {
            return null;
        }
        WechatApiResponse userinfo = WechatApi.getUserinfo(accessTokenByCode.getAccessToken(), accessTokenByCode.getOpenid());
        if (userinfo == null || userinfo.getErrcode() != 0) {
            return null;
        }
        WechatApiResponse newAccessToken = WechatApi.getAccessToken(config.getAppId(), config.getAppSecret());
        WechatApiResponse subscribeUserinfo = WechatApi.getSubscribeUserinfo(newAccessToken.getAccessToken(), accessTokenByCode.getOpenid());
        userinfo.setSubscribe(subscribeUserinfo.getSubscribe());
        StudentBean student = StudentBean.builder()
            .agentNo(agentNo)
            .parentId(recommenderStudent)
            .registerSource(Constants.REGISTER_SOURCE_WECHAT)
            .openId(userinfo.getOpenid())
            .unionId(userinfo.getUnionid())
            .nickname(userinfo.getNickname())
            .isFollowed(userinfo.getSubscribe() > 0)
            .avatar(userinfo.getHeadimgurl())
            .gender(userinfo.getSex() + "")
            .build();
        StudentBean existStudent = studentService.refreshStudentInfoByWechat(student, false);
        existStudent.setShareUrl(AppUtils.getStudentShareUrl(existStudent.getId()));
        SsoUtils.saveLoginInfo2Redis(loginTypeEnum, existStudent, 30 * 24 * 3600L);
        existStudent.setAccessToken(accessTokenByCode.getAccessToken());
        return existStudent;
    }

}
