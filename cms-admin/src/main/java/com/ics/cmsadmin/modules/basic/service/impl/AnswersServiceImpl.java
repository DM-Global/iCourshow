package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.modules.basic.dao.AnswersDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.modules.basic.bean.AnswersBean;
import com.ics.cmsadmin.modules.basic.service.AnswersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lvsw on 2018-09-06.
 */
@Service
public class AnswersServiceImpl implements AnswersService {
    @Resource
    private AnswersDao answersDao;

    @CacheDbMember(returnClass = AnswersBean.class, group = CacheGroupEnum.CACHE_PROBLEM_GROUP)
    @Override
    public List<AnswersBean> queryByProblemId(String problemId) {
        return answersDao.queryByProblemId(problemId);
    }
}
