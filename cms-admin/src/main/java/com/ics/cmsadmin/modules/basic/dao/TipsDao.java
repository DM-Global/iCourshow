package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.modules.basic.bean.TipsBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2018-09-06.
 */
@Repository
@Mapper
public interface TipsDao {
    /**
     * 插入提示内容
     *
     * @param tipsBeans 提示内容
     * @param problemId 问题id
     */
    void batchInsert(@Param("list") List<TipsBean> tipsBeans, @Param("problemId") String problemId);

    /**
     * 根据问题id删除对应的提示
     *
     * @param problemId
     */
    void deleteByProblemId(@Param("problemId") String problemId);

    /**
     * 根据问题id查询对应的提示
     *
     * @param problemId
     */
    List<TipsBean> queryByProblemId(@Param("problemId") String problemId);
}
