package com.ics.cmsadmin.modules.student.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-07-02 11:19
 */
@Data
@Builder
public class StudentCertificateBean {
    private String id;
    private String studentId;
    private String studentName;
    private String pictureName;
    private String pictureUrl;
    private String shareType;
    private String hours;
    private String courseId;
    private String courseName;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;         // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;        // 修改时间

    @Tolerate
    public StudentCertificateBean(){}
}
