package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.bean.TwoTupleBean;
import com.ics.cmsadmin.modules.agent.bean.AgentAccountBean;
import com.ics.cmsadmin.modules.agent.bean.AgentDaySummaryBean;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.agent.bean.StudentOrderBean;

import java.util.List;
import java.util.Map;

/**
 * 代理商各种数据日结汇总
 */
public interface AgentDaySummaryService {

    boolean daySummary();

    boolean daySummary(String summaryDate);

    /**
     * 查询代理商汇总数据
     */
    PageResult<AgentDaySummaryBean> listAgentSummaryByLoginUserId(AgentInfoBean agentBean, String loginUserId, PageBean pageBean);

    /**
     * 查询我的下级信息
     */
    AgentDaySummaryBean queryMyDistributorInfo(String loginUserId);

    /**
     * 查询下级汇总数据
     */
    List<AgentDaySummaryBean> listDistributorSummary(AgentInfoBean bean, String loginUserId);

    /**
     * 查询我的收益
     */
    TwoTupleBean<AgentDaySummaryBean, AgentAccountBean> queryMyRevenue(String loginUserId);

    /**
     * 查询代理商的日结汇总数据
     */
    List<AgentDaySummaryBean> listAgentDaySummary(AgentInfoBean bean);

    /**
     * 查询我的收益信息-列表
     */
    PageResult<AgentDaySummaryBean> queryMyRevenue4Table(AgentInfoBean bean, String loginUserId, PageBean pageBean);

    /**
     * 查询我的学生信息
     */
    AgentDaySummaryBean queryMyStudentInfo(String loginUserId);

    /**
     * 查询我的学生信息-列表
     */
    PageResult<Map<String, Object>> queryMyStudent4Table(AgentInfoBean bean, String loginUserId, PageBean pageBean);

    /**
     * 查询我的提现信息
     *
     * @param loginUserId
     * @return
     */
    Map<String, String> queryMyWithdrawInfo(String loginUserId);

    /**
     * 查询我的下级代理商详情信息
     */
    AgentDaySummaryBean queryMyDistributorDetailInfo(String agentNo);

    PageResult<AgentDaySummaryBean> pageAgentDaySummary(AgentInfoBean agentInfoBean, PageBean pageBean);

    /**
     * 查询学生基本信息
     *
     * @param userId 学生id
     * @return
     */
    AgentDaySummaryBean queryMyStudentDetailInfo(String userId);

    /**
     * 分页学生购买订单列表
     *
     * @param studentOrderBean
     * @param pageBean
     * @return
     */
    PageResult<StudentOrderBean> listMyStudentBuyOrderInfo(StudentOrderBean studentOrderBean, PageBean pageBean);

}
