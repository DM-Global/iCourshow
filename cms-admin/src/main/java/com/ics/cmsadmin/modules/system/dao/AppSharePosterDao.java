//package com.ics.cmsadmin.modules.system.dao;
//
//import com.ics.cmsadmin.frame.core.dao.BaseDao;
//import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
//import com.ics.cmsadmin.modules.system.bean.AppSharePosterBean;
//import org.apache.ibatis.annotations.Mapper;
//import org.springframework.stereotype.Repository;
//
///**
//* app_share_poster
//* Created by lvsw on 2019-18-30 16:05:04.
//*/
//@Repository
//@Mapper
//public interface AppSharePosterDao extends BaseDao<AppSharePosterBean>, BaseDataDao<AppSharePosterBean> {
//
//}
