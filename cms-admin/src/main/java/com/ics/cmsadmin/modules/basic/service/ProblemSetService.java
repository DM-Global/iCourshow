package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.ProblemSetBean;
import com.ics.cmsadmin.modules.basic.bean.ProblemSetDto;
import com.ics.cmsadmin.modules.basic.bean.ScoreBean;

/**
 * q_problem_set 服务类
 * Created by lvsw on 2018-09-06.
 */
public interface ProblemSetService extends BaseDataService<ProblemSetBean>, BaseService<ProblemSetBean> {
    /**
     * 根据问题id查询题集信息
     */
    ProblemSetBean queryByProblemId(String problemId);

    /**
     * 获取题集详情(包含问题等)
     *
     * @param problemSetBean 查询参数
     */
    ProblemSetDto findProblemSetDetails(ProblemSetBean problemSetBean);

    /**
     * 提交分数
     *
     * @param scoreBean
     * @return
     */
    boolean submitScore(ScoreBean scoreBean);
}

