package com.ics.cmsadmin.modules.system.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.system.bean.CityBean;
import com.ics.cmsadmin.modules.system.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static com.ics.cmsadmin.modules.system.steward.SystemServices.cityService;

@Api(description = "城市相关接口")
@RestController
@RequestMapping("/city")
public class CityController {

    @Authorize
    @ApiOperation("获取城市列表")
    @GetMapping(value = {"/list", "/list/{cityCode}"})
    public ApiResponse listCity(@PathVariable(required = false) String cityCode) {
        return new ApiResponse(cityService.listDirectChildren(cityCode));
    }
}
