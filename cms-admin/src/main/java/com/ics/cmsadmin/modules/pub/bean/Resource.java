package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class Resource<T> {
    private T id;
}
