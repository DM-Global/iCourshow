package com.ics.cmsadmin.modules.app.bean;

import lombok.Data;

@Data
public class AppInfoBean {
    // 系统版本 比如ios5 或者android7
    private String systemVersion;
    // 设备类型 iphone7 或者 小米5
    private String deviceType;
    // 平台 ios 或者 android
    private String platform;
    // app的版本号 例如: 1.0.1
    private String appVersion;
    // 设备id,
    private String deviceId;
    // app渠道
    private String appChannel;
    //友盟deviceToken
    public String uMengDeviceToken;
}
