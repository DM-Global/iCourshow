package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.basic.bean.CourseAttributeBean;
import com.ics.cmsadmin.modules.basic.service.CourseAttributeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_course_attribute controller
 * Created by sandwich on 2018-12-10 07:12:28.
 */
@Api(description = "")
@RestController
@RequestMapping("/courseAttribute")
public class CourseAttributeController {

    @Resource
    private CourseAttributeService courseAttributeService;

    @Authorize(AuthorizeEnum.COURSE_ATTRIBUTE_QUERY)
    @ApiOperation(value = "查询信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(courseAttributeService.queryById(id));
    }

    @Authorize(AuthorizeEnum.COURSE_ATTRIBUTE_INSERT)
    @ApiOperation(value = "新增信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody CourseAttributeBean courseAttributeBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(courseAttributeService.insert(courseAttributeBean));
    }

    @Authorize(AuthorizeEnum.COURSE_ATTRIBUTE_DELETE)
    @ApiOperation(value = "删除信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
        return new ApiResponse(courseAttributeService.delete(id));
    }

    @Authorize(AuthorizeEnum.COURSE_ATTRIBUTE_UPDATE)
    @ApiOperation(value = "更新信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody CourseAttributeBean courseAttributeBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(courseAttributeService.update(id, courseAttributeBean));
    }

    @Authorize(AuthorizeEnum.COURSE_ATTRIBUTE_QUERY)
    @ApiOperation(value = "分页查询信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody CourseAttributeBean courseAttributeBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(courseAttributeService.list(courseAttributeBean, pageBean));
    }

}
