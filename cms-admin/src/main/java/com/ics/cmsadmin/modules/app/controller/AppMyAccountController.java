package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.bean.TwoTupleBean;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;
import com.ics.cmsadmin.modules.basic.service.PackageService;
import com.ics.cmsadmin.modules.student.bean.*;
import com.ics.cmsadmin.modules.student.service.*;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.LIST_STUDENT_DAY_SUMMARY_NOTES;
import static com.ics.cmsadmin.frame.profit.ExpressionConstant.NUMBER_PATTERN;

@AppLoginValid
@Api(description = "我的账户相关接口", tags = "我的账户相关接口")
@RestController
@RequestMapping("/app/myAccount")
public class AppMyAccountController {

    @Resource
    private StudentAccountDetailService studentAccountDetailService;
    @Resource
    private StudentAccountService studentAccountService;
    @Resource
    private StudentShareDetailService studentShareDetailService;
    @Resource
    private StudentDayService studentDayService;
    @Resource
    private StudentWithdrawAuditingService studentWithdrawAuditingService;
    @Resource
    private PackageService packageService;
    @Resource
    private RegisterService registerService;

    @ApiOperation(value = "获取账户明细列表")
    @PostMapping("/listMyAccountDetail/{pageNo}/{pageSize}")
    public ApiResponse listMyAccountDetail(@RequestBody StudentAccountDetailBean studentAccountDetailBean,
                                           @ApiParam("页码") @PathVariable Integer pageNo, @ApiParam("每页条数") @PathVariable Integer pageSize,
                                           HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN);
        }
        studentAccountDetailBean.setStudentId(loginUserId);
        return new ApiResponse(studentAccountDetailService.list(studentAccountDetailBean, new PageBean(pageNo, pageSize)));
    }

    @ApiOperation(value = "获取分润明细列表")
    @PostMapping("/listMyShareDetail/{pageNo}/{pageSize}")
    public ApiResponse listMyShareDetail(@RequestBody StudentShareDetailBean bean,
                                         @ApiParam("页码") @PathVariable Integer pageNo, @ApiParam("每页条数") @PathVariable Integer pageSize,
                                         HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN);
        }
        bean.setStudentId(loginUserId);
        return new ApiResponse(studentShareDetailService.list(bean, new PageBean(pageNo, pageSize)));
    }

    @ApiOperation(value = "获取学生日结数据", notes = LIST_STUDENT_DAY_SUMMARY_NOTES)
    @PostMapping("/listStudentDaySummary/{startCreateDate}/{endCreateDate}")
    public ApiResponse listStudentDaySummary(@ApiParam("开始日期(yyyy-MM-dd)") @PathVariable String startCreateDate,
                                             @ApiParam("结束日期(yyyy-MM-dd)") @PathVariable String endCreateDate,
                                             HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN);
        }
        return new ApiResponse(studentDayService.list(loginUserId,
            Optional.ofNullable(startCreateDate).orElse("").trim(),
            Optional.ofNullable(endCreateDate).orElse("").trim()));
    }

    @ApiOperation(value = "获取我的账户信息")
    @PostMapping("/queryMyAccount")
    public ApiResponse queryMyAccount(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        StudentAccountBean studentAccountBean = Optional.ofNullable(studentAccountService.queryByStudentId(loginUserId)).orElse(new StudentAccountBean());
        Map<String, Object> result = new HashMap<>();
        result.put("totalIncome", studentAccountBean.getTotalIncome());
        result.put("withdrawCash", studentAccountBean.getWithdrawCash());
        result.put("balance", studentAccountBean.getBalance());
        result.put("availableBalance", studentAccountBean.getAvailableBalance());
        result.put("frozenBalance", studentAccountBean.getFrozenBalance());
        result.put("todayProfit", Optional.ofNullable(studentShareDetailService.getTodayProfit(loginUserId)).orElse(BigDecimal.ZERO));
        return new ApiResponse(result);
    }

    @ApiOperation(value = "申请提现到微信零钱")
    @PostMapping("/applyStudentWithdrawAuditingToWechat/{openId}/{money}")
    public ApiResponse applyStudentWithdrawAuditingToWechat(
        @PathVariable String openId,
        @PathVariable BigDecimal money, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(studentWithdrawAuditingService.applyStudentWithdrawAuditing(openId, money, "1", loginUserId));
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "列举学生提现规则")
    @PostMapping("/listStudentWithdrawRule")
    public ApiResponse listStudentWithdrawRule() {
        List<String> result = new ArrayList<>();
        String minWithdrawMoney = registerService.queryRegisterValue(RegisterKeyValueEnum.STUDENT_MIN_WITHDRAW_MONEY, Function.identity());
        String maxWithdrawMoney = registerService.queryRegisterValue(RegisterKeyValueEnum.STUDENT_MAX_WITHDRAW_MONEY, Function.identity());
        String everyDayMaxWithdrawCount = registerService.queryRegisterValue(RegisterKeyValueEnum.STUDENT_EVERY_DAY_MAX_WITHDRAW_COUNT, Function.identity());
        result.add(String.format("1. 单次提现不可低于%s元，不能超过%s元", minWithdrawMoney, maxWithdrawMoney));
        result.add(String.format("2. 每天提现不能超过%s次", everyDayMaxWithdrawCount));
        result.add("3. 24小时后到账（手动审核）");
        result.add("4. 您至少有一次购买会员消费记录");
        return new ApiResponse(result);
    }

    @ApiOperation(value = "取消提现申请")
    @PostMapping("/cancelStudentWithdrawAuditing/{applyId}")
    public ApiResponse cancelStudentWithdrawAuditing(
        @PathVariable String applyId, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(studentWithdrawAuditingService.cancelStudentWithdrawAuditing(applyId, loginUserId));
    }

    @ApiOperation(value = "查询提现申请记录")
    @PostMapping("/listStudentWithdrawAuditing/{pageNo}/{pageSize}")
    public ApiResponse listStudentWithdrawAuditing(
        @ApiParam("页码") @PathVariable Integer pageNo, @ApiParam("每页条数") @PathVariable Integer pageSize, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        PageResult<StudentWithdrawAuditingBean> list = studentWithdrawAuditingService.list(
            StudentWithdrawAuditingBean.builder().studentId(loginUserId).build(), new PageBean(pageNo, pageSize));
        return new ApiResponse(list);
    }


    @ApiOperation(value = "获取当前分润比例")
    @PostMapping("/queryCurrentShareRate")
    public ApiResponse queryCurrentShareRate(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        StudentDaySummaryBean studentDaySummaryBean = studentDayService.query(LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")), loginUserId);
        TwoTupleBean<List<BigDecimal>, List<BigDecimal>> parse = parseShareRate();
        Map<String, Object> result = new HashMap<>();
        result.put("rates", parse.one);
        result.put("limitMoney", parse.two);
        result.put("current", studentDaySummaryBean);
//        result.put("remark", "说明：\n1. 折线图显示的数据为昨日的数据\n" +
//            "2. 相关规则，爱科塾有最终解释权\n");
        result.put("remark", "说明：\n" +
            "1. 分润采用累进递增的方式，按自然月结算；\n" +
            "2. 折线图显示数据为昨天的数据；\n" +
            "3. 爱科塾有分润相关规则的最终解释权。\n");
        return new ApiResponse(result);
    }

    public TwoTupleBean<List<BigDecimal>, List<BigDecimal>> parseShareRate() {
        String shareRate = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.PARENT_STUDENT_SHARE_RATE, Function.identity());
        List<BigDecimal> rates = new ArrayList<>();
        List<BigDecimal> limits = new ArrayList<>();
        Matcher numberMatcher = NUMBER_PATTERN.matcher(shareRate);
        for (int i = 0; numberMatcher.find(); i++) {
            if (i % 2 == 0) {
                rates.add(new BigDecimal(numberMatcher.group()));
            } else {
                limits.add(new BigDecimal(numberMatcher.group()));
            }
        }
        return new TwoTupleBean<>(rates, limits);
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "获取分润示例")
    @PostMapping("/getShareMoneyDemo")
    public ApiResponse getShareMoneyDemo() {
        PageResult<PackageBean> pageResult = packageService.list(PackageBean.builder().isActive(true).build(), new PageBean(1, Integer.MAX_VALUE));
        TwoTupleBean<List<BigDecimal>, List<BigDecimal>> shareRate = parseShareRate();

        BigDecimal minRate = BigDecimal.ZERO;
        BigDecimal maxRate = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(shareRate.one)) {
            minRate = shareRate.one.get(0);
            maxRate = shareRate.one.get(shareRate.one.size() - 1);
        }
        BigDecimal finalMinRate = minRate;
        BigDecimal finalMaxRate = maxRate;
        List<Map<String, String>> collect = Optional.ofNullable(pageResult.getDataList())
            .orElse(new ArrayList<>())
            .stream()
            .map(item -> {
                Map<String, String> map = new HashMap<>();
                BigDecimal price = Optional.ofNullable(item.getPrice()).orElse(BigDecimal.ZERO).setScale(0, RoundingMode.DOWN);
                map.put("name", String.format("%s%s元", item.getTitle(), price));
                BigDecimal minShareMoney = price.multiply(finalMinRate).divide(BigDecimal.valueOf(100)).setScale(0, RoundingMode.DOWN);
                BigDecimal maxShareMoney = price.multiply(finalMaxRate).divide(BigDecimal.valueOf(100)).setScale(0, RoundingMode.DOWN);
                map.put("shareMoney", String.format("%s-%s元", minShareMoney, maxShareMoney));
                return map;
            }).collect(Collectors.toList());
        return new ApiResponse(collect);
    }

    public static void main(String[] args) {
        List<BigDecimal> rates = new ArrayList<>();
        System.out.println(rates.get(0));
    }
}
