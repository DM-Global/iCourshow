package com.kco.generate.app;

import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateComponentTs {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildTs(tableBean));
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + ".component.ts", sb.toString());
    }

    private static String buildTs(TableBean tableBean) {
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);

        StringBuilder sb = new StringBuilder();
        sb.append("import { Component, OnInit } from '@angular/core';\n\n")
            .append("@Component({\n")
            .append("  selector: 'bz-" + tempName + "',\n")
            .append("  template: '<router-outlet></router-outlet>'\n")
            .append("})\n")
            .append("export class " + className + "Component  {\n\n")
            .append("}\n");
        return sb.toString();
    }
}

//import { Component, OnInit } from '@angular/core';
//
//@Component({
//        selector: 'bz-user',
//        template: '<router-outlet></router-outlet>'
//        })
//export class UserComponent  {
//
//}
