package com.ics.cmsadmin.sms;

import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import com.ics.cmsadmin.modules.alibaba.utils.SmsUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw", "app.init-authorize=false"})
public class TestSms {

    @Test
    public void testSendSms() {
        // 6812034686
//        SmsUtils.sendCodeSms("16812034686", "123456", SmsTypeEnum.loginVerify);
        SmsUtils.sendCodeSms("86", "13590387457", "1263", SmsTypeEnum.loginVerify);
    }
}
