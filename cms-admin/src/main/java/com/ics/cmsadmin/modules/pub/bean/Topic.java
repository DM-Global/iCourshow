package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class Topic extends Resource<Integer> {

    private String name;

    private Boolean isActive;

    private Boolean isPublic;

    private String description;

    private Integer unitId;

    private String unitName;

    private Integer courseId;

    private String courseName;

    private Integer subjectId;

    private String subjectName;

    private Integer rank;
}
