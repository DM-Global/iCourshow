package com.ics.cmsadmin.frame.interceptor;

import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Sandwich on 2018-08-13
 */
@Component
@Log4j2
public class AppLoginInterceptor implements HandlerInterceptor {

    @Resource(name = "newStudentService")
    private StudentService studentService;
    /**
     * 在请求被处理之前调用
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;

        AppLoginValid appMethodAppLoginValid = handlerMethod.getMethodAnnotation(AppLoginValid.class);
        // 1. 如果方法体上有 AppLoginValid
        if (appMethodAppLoginValid != null) {
            // 则判断是否需要登陆校验
            return !appMethodAppLoginValid.needLogin() || loginValid(request, response);
        }
        // 2. 如果方法体上没有注解 AppLoginValid, 则判断该方法的类上有没有这个注解 AppLoginValid
        AppLoginValid appBeanLoginValid = handlerMethod.getBeanType().getAnnotation(AppLoginValid.class);
        if (appBeanLoginValid != null) {
            // 如果有判读时候需要登陆校验
            return !appBeanLoginValid.needLogin() || loginValid(request, response);
        }
        return loginValid(request, response);
    }

    private boolean loginValid(HttpServletRequest request, HttpServletResponse response) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            HttpUtils.setJsonDataResponse(response, ApiResponse.getResponse(ApiResultEnum.UN_LOGIN), 401);
            return false;
        }else {
            StudentBean studentBean = studentService.queryById(loginUserId);
            String requestUrl  = request.getRequestURI();
            if (requestUrl.equals("/ics/app/order/create")){
                boolean isTouristCreateOrderAvailable=SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.TOURIST_ORDER_SWITCH, Boolean::valueOf);
                if (isTouristCreateOrderAvailable){
                    return true;
                }
            }
            if (requestUrl.equals("/ics/app/iap/iapProcess")
                || requestUrl.equals("/ics/app/order/packages")
                || requestUrl.equals("/ics/app/order/latestOrder")){
                return true;
            }

            if (!StringUtils.isBlank(studentBean.getDeviceId())){
                log.info("This is a tourist, trying to request without permission user id is：{}", loginUserId);
                HttpUtils.setJsonDataResponse(response, ApiResponse.getResponse(ApiResultEnum.UN_LOGIN), 401);
                return false;
            }
        }
        return true;
    }

    /**
     * 在请求被处理后，视图渲染之前调用
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    /**
     * 在整个请求结束后调用
     *
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}
