package com.ics.cmsadmin.modules.pub.service.impl;

import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.dao.CollectionDao;
import com.ics.cmsadmin.modules.basic.dao.LikeDao;
import com.ics.cmsadmin.modules.pub.bean.Course;
import com.ics.cmsadmin.modules.pub.dao.CourseMapper;
import com.ics.cmsadmin.modules.pub.dao.TeacherMapper;
import com.ics.cmsadmin.modules.pub.dao.TopicMapper;
import com.ics.cmsadmin.modules.pub.dao.UnitMapper;
import com.ics.cmsadmin.modules.pub.service.OrderService;
import com.ics.cmsadmin.modules.pub.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("CourseService")
@Primary
public class CourseServiceImpl extends ResourceService<CourseMapper, Course, Integer> {
    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private UnitMapper unitMapper;
    @Autowired
    private TopicMapper topicMapper;
    @Autowired
    private CollectionDao collectionDao;

    @Autowired
    private TeacherMapper teacherMapper;
    @Autowired
    private LikeDao likeDao;

    @Autowired
    private OrderService orderService;

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    public int setCourseType(Integer courseId, Integer courseTypeId) {
        return courseMapper.setCourseType(courseId, courseTypeId);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    public int unsetCourseType(Integer courseId, Integer courseTypeId) {
        return courseMapper.unsetCourseType(courseId, courseTypeId);
    }

    @Override
    public Course get(Integer id) {
        Course course = super.get(id);
        buildCourse(course, null);
        return course;
    }

    @Override
    public List<Course> getMultiple(Map<String, Object> param) {
        List<Course> ret = super.getMultiple(param);
        for (Course course : ret) {
            buildCourse(course, null);
        }
        return ret;
    }

    public Course get(Integer id, String userId) {
        Course course = courseMapper.select(id, userId);
        buildCourse(course, userId);
        return course;
    }

    public List<Course> getMultiple(Map<String, Object> param, String userId) {
        List<Course> courses = getMultiple(param);
        for (Course course : courses) {
            buildCourse(course, userId);
        }
        return courses;
    }

    private void buildCourse(Course course, String userId) {
        if (course == null) return;
        Map<String, Object> tp = new HashMap<>();
        tp.put("courseId", course.getId());
        course.setTeachers(teacherMapper.selectAll(tp));
        course.setUnitNumber(unitMapper.queryTotal(tp));
        course.setTopicNumber(topicMapper.queryTotal(tp));
        course.setCollectCount(collectionDao.queryTotal(LevelEnum.course, String.valueOf(course.getId())));
        course.setThumbUpCount(likeDao.queryTotal(LevelEnum.course, String.valueOf(course.getId())));
        course.setCollected(collectionDao.select(userId, LevelEnum.course, String.valueOf(course.getId())));
        course.setLiked(likeDao.select(userId, LevelEnum.course, course.getId()));
        course.setHasBought(orderService.getPurchaseStatus(userId, course.getId()));
        course.setBoughtNumber(orderService.getTotalNumber(course.getId()));
    }

    public List<Course> findByIdIn(List<Integer> ids, String userId) {
        return courseMapper.findByIdIn(ids, userId);
    }

    /**
     * 记录课程点击量
     *
     * @param courseId
     */
    public void increaseViewNum(Integer courseId) {
        Course course = courseMapper.select(courseId);
        Integer clickNumber = course.getClickNumber();
        clickNumber = clickNumber != null ? clickNumber : 0;
        course.setClickNumber(clickNumber + 1);
        courseMapper.update(course);
    }
}
