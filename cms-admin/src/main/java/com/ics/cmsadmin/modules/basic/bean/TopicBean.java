package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import net.bytebuddy.implementation.bind.annotation.Empty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

/**
 * t_topic
 * 主题
 * Created by lvsw on 2018-09-01.
 */
@Data
@Builder
public class TopicBean {

    // 主题id
    private String id;
    // 主题名称
    @NotEmpty(message = "主题名称不能为空",  groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "主题名称长度不能超过{max}个字符", max = 10, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    // 单元id
    @NotEmpty(message = "请选择单元", groups = {InsertGroup.class, UpdateGroup.class})
    private String unitId;
    // 单元名
    private String unitName;
    // 课程id
    private String courseId;
    // 课程名
    private String courseName;
    // 科目id
    private String subjectId;
    // 科目名
    private String subjectName;
    // 状态：true 启用，false 禁用
    private Boolean isActive;
    // 公开
    private Boolean isPublic;
    // 描述
    @Length(message = "主题描述长度不能超过{max}个字符", max = 300, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    // 排序
    private String rank;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 修改时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;
    private Date startTime;
    private Date endTime;
    private int testNum;

    @Tolerate
    public TopicBean() {
    }

    // 主题下知识点
    private List<PointBean> pointList;

    // 主题是否已收藏
    private Boolean collected;
    // 课程是否已经购买
    private Boolean hasBought;
}
