package com.ics.cmsadmin.modules.sso;

import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登陆处理器
 * Created by 666666 on 2018/8/25.
 */
public interface LoginHandler<T extends LoginInfo> {
    /**
     * 处理登陆逻辑
     */
    T dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum);
}
