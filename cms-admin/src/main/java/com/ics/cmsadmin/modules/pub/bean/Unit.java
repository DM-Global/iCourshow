package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class Unit extends Resource<Integer> {

    private String name;

    private Boolean isActive;

    private String description;

    private Integer courseId;

    private String courseName;

    private Integer subjectId;

    private String subjectName;

    private Integer teacherId;

    private String teacherName;

    private Integer rank;

    private Integer coverId;

    private String coverSrc;

    private Integer iconId;

    private String iconSrc;
}
