package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-09-01.
 */
@Repository
@Mapper
public interface PointDao extends BaseDataDao<PointBean>, BaseDao<PointBean> {
    void updateProblemNum(@Param("id") String pointId, @Param("testNum") int testNum);

    void updateVideoInfo(@Param("id") String pointId, @Param("videoDuration") String videoDuration, @Param("coverSrc") String coverSrc);
}
