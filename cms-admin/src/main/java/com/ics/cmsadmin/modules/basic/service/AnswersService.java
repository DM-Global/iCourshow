package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.modules.basic.bean.AnswersBean;

import java.util.List;

/**
 * q_answers 服务类
 * Created by lvsw on 2018-09-06.
 */
public interface AnswersService {
    /**
     * 根据问题id查询对应的提示
     *
     * @param problemId
     */
    List<AnswersBean> queryByProblemId(String problemId);
}
