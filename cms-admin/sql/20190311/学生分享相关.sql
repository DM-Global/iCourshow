ALTER TABLE t_student DROP COLUMN parent_id;
ALTER TABLE t_student ADD COLUMN parent_id VARCHAR(40) NULL COMMENT '上级用户' AFTER id;
ALTER TABLE t_student DROP COLUMN share_rate;
ALTER TABLE t_student ADD COLUMN share_rate DECIMAL(8,2) DEFAULT 10 COMMENT '分润比例' AFTER birthday;
ALTER TABLE `t_student` ADD INDEX idx_parent_id ( `parent_id` );
alter table t_course_attribute add column sort int(10) not null default '0' after is_active;

ALTER TABLE sys_menu ADD COLUMN `active` VARCHAR(2) NOT NULL DEFAULT '1' COMMENT '状态: 0 禁用, 1启用';
INSERT INTO com_register(register_id, `value`, description)
        VALUES('contact_us_wechat', 'iCourshow_service', '联系我们的微信id');
ALTER TABLE t_point ADD COLUMN video_duration_second INT(10) DEFAULT 0 COMMENT '视频时长s' AFTER video_duration;
UPDATE t_point SET video_duration_second = SUBSTR(video_duration, 4, 2) * 60 + SUBSTR(video_duration, 7, 2);

INSERT INTO com_meta(meta_id, parent_id, NAME, VALUE, order_index)VALUES
('studentAccountDetail', '__ROOT__', '学生账户明细', '', 0),
('studentAccountDetail-type', 'studentAccountDetail', '学生账户明细类型', '', 0),
('studentAccountDetail-type-type1', 'studentAccountDetail-type', '一级分润', 'PARENT_PROFIT_INCOME', 0),
('studentAccountDetail-type-type2', 'studentAccountDetail-type', '二级分润', 'GRANDFATHER_PROFIT_INCOME', 0),
('studentAccountDetail-type-type3', 'studentAccountDetail-type', '购买订单', 'BUY_ORDER', 0),
('studentAccountDetail-type-type4', 'studentAccountDetail-type', '申请提现', 'APPLY_WITHDRAW_CASH', 0),
('studentAccountDetail-type-type5', 'studentAccountDetail-type', '接受提现成功', 'ACCEPT_WITHDRAW_CASH_SUCCESS', 0),
('studentAccountDetail-type-type6', 'studentAccountDetail-type', '接受提现失败', 'ACCEPT_WITHDRAW_CASH_FAILED', 0),
('studentAccountDetail-type-type7', 'studentAccountDetail-type', '拒绝提现', 'REFUSE_WITHDRAW_CASH', 0),
('studentAccountDetail-type-type8', 'studentAccountDetail-type', '取消提现', 'CANCEL_WITHDRAW_CASH', 0);

INSERT INTO com_register(register_id, VALUE, description)VALUES
('parent_student_share_rate', '20%<30000.01<25%<100000.01<30%<300000.01<35%<900000.01<40%', '学生阶梯分润比例'),
('parent_student_default_share_rate', '20', '父级学生默认的分润比例'),
('grandfather_student_share_rate', '10', '祖父级分润比例'),
('student_min_withdraw_money', '10', '学生最小提现金额'),
('student_max_withdraw_money', '5000', '学生最大提现金额'),
('student_every_day_max_withdraw_count', '5', '每天最多提现次数');



INSERT INTO com_meta(meta_id, parent_id, NAME, VALUE, order_index)VALUES
('studentWithdraw', '__ROOT__', '学生提现', '', 0),
('studentWithdraw-type', 'studentWithdraw', '提现方式', '', 0),
('studentWithdraw-type-type1', 'studentWithdraw-type', 'app微信到零钱', '1', 0),
('studentWithdraw-type-type2', 'studentWithdraw-type', '公众号微信到零钱', '2', 0),
('studentWithdraw-type-type3', 'studentWithdraw-type', 'app微信到银行卡', '3', 0);

-- INSERT INTO sys_menu(menu_id, parent_id, label, url, icon,order_no,create_time)
-- VALUE('appSharePosterManage', 'systemConfig', 'app分享海报', '/common/appSharePosterManage', 'ios-home', 5, NOW());

INSERT INTO sys_menu(menu_id, parent_id, label, url, icon,order_no,create_time)
VALUE('studentWithdrawManage', 'memberManage', '学生提现申请', '/basic/studentWithdrawManage', 'ios-home', 5, NOW());

ALTER TABLE t_order ADD KEY idx_user_id_order_amount(user_id,order_amount);

DROP TABLE IF EXISTS t_student_account;
CREATE TABLE `t_student_account` (
  `account_id` VARCHAR(20) NOT NULL COMMENT '账户id',
  `student_id` VARCHAR(40) NOT NULL COMMENT '学生编号',
  `total_income` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '总收益: A = B + C',
  `withdraw_cash` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '提现金额: B',
  `balance` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '余额: C = D + E',
  `available_balance` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '可用余额: D',
  `frozen_balance` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '冻结余额: E',
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `student_id` (`student_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='学生账户';

DROP TABLE IF EXISTS t_student_account_detail;
CREATE TABLE `t_student_account_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `student_id` VARCHAR(40) NOT NULL COMMENT '学生编号',
  `account_id` VARCHAR(20) NOT NULL COMMENT '学生账户编号',
  `money` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '操作金额',
  `type` VARCHAR(50) NOT NULL COMMENT '类型: 查看meta(studentAccoutDetail-type)',
  `total_income` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '总收益: A = B + C',
  `withdraw_cash` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '提现金额: B',
  `balance` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '余额: C = D + E',
  `available_balance` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '可用余额: D',
  `frozen_balance` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '冻结余额: E',
  `order_no` VARCHAR(100) NULL COMMENT '关联订单号',
  `order_student_id` VARCHAR(40) NULL COMMENT '关联订单号的学生id',
  `withdraw_id` INT(10) DEFAULT NULL COMMENT '提现id',
  `remark` VARCHAR(100) DEFAULT NULL COMMENT '备注',
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  key ind_student_id(`student_id`),
  key ind_order_no(`order_no`),
  key ind_order_student_id(`order_student_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='学生账户明细表';


DROP TABLE IF EXISTS t_student_withdraw_auditing;
CREATE TABLE `t_student_withdraw_auditing` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '申请流水id',
  `student_id` VARCHAR(40) NOT NULL COMMENT '学生编号',
  `student_name` VARCHAR(80) NOT NULL COMMENT '学生名称',
  `withdraw_cash` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '申请提现金额',
  `status` VARCHAR(10) NOT NULL DEFAULT '0' COMMENT '状态, 0: 申请中, 1: 处理成功, 2: 处理失败， 3：已拒绝 4取消提现',
  `withdraw_type` VARCHAR(2) NULL COMMENT '提现方式: 1 app微信到零钱, 2, 公众号微信到零钱 3. app微信到银行卡',
  `relation_order_no` VARCHAR(50) NULL COMMENT '关联订单',
  payment_no     VARCHAR(50) NULL COMMENT 'payment_no',
  open_id  varchar(50) null comment '提现的微信open_id',
  `remark` VARCHAR(512) DEFAULT NULL COMMENT '备注',
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`id`),
    key ind_student_id(`student_id`)
) ENGINE=INNODB AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='学生提现审核记录';


DROP TABLE IF EXISTS t_student_share_detail;
CREATE TABLE `t_student_share_detail` (
    `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
    `student_id` VARCHAR(40) NOT NULL COMMENT '收益学生编号',
    `share_type` VARCHAR(20) NOT NULL COMMENT '分润类型: 1 直接上级分润, 2, 上二级的分润',
    `share_money` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '分润金额',
    `order_money` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
    `share_rate` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '分润比例',
    `order_id` VARCHAR(40) NOT NULL COMMENT '分润订单id',
    `order_student_id` VARCHAR(40) NOT NULL COMMENT '交易订单的学生id',
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY ind_student_id_share_money(`student_id`, share_money)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='学生分润明细表';

DROP TABLE IF EXISTS t_student_day_summary;
CREATE TABLE `t_student_day_summary` (
    `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `student_id` VARCHAR(40) NOT NULL COMMENT '学生编号',
    `parent_id` VARCHAR(40) NULL COMMENT '父级编号',
    `old_share_rate` DECIMAL(8,2) null comment '旧分润比例',
    `new_share_rate` DECIMAL(8,2) null comment '新分润比例',
    `current_month_child_order_money_cusm` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当月直接下级交易总金额',
    `summary_day` DATE NOT NULL COMMENT '汇总日期',
    `children_cusm` INT(11) NOT NULL DEFAULT '0' COMMENT '下级学生累积数量',
    `children_newly` INT(11) NOT NULL DEFAULT '0' COMMENT '下级学生新增数量',
    `child_order_money_cusm` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '下级订单金额累积数',
    `child_order_money_newly` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '下级当日订单金额新增数',
    `share_money_cusm` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '分润金额累积数',
    `share_money_newly` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日分润金额新增数',
    `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `student_id_summary_day` (`student_id`,`summary_day`),
    KEY idx_parent_id(parent_id)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='学生日结汇总数据';

drop table if exists t_student_share_record ;
CREATE TABLE t_student_share_record(
       `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
       `student_id` VARCHAR(40) NOT NULL COMMENT '学生编号',
       `share_type` INT(10) NOT NULL DEFAULT '0' COMMENT '分享类型,2满一小时分享 3完成课程分享',
       `score_id` INT(10) NULL COMMENT '课程id',
       `share_status` INT(10) NULL COMMENT '完成课程分享是否分享标记, 0 未分享, 1 已经分享',
       `study_time` INT(10) NULL COMMENT '学习时间,单位h',
       `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
       `update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
       PRIMARY KEY (`id`)
)COMMENT '学生分享记录';

drop table if exists t_student_certificate;
CREATE TABLE t_student_certificate(
       `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
       `student_id` VARCHAR(40) NOT NULL COMMENT '学生编号',
       `student_name` VARCHAR(80) NOT NULL COMMENT '学生名称',
       `picture_name` varchar(80) NOT NULL COMMENT '图片名字',
       `picture_url` varchar(500) not NULL COMMENT '图片url',
       `share_type` INT(10) NOT NULL DEFAULT '0' COMMENT '分享类型,1入学通知书, 2满一小时证书 3完成课程证书',
       `hours` INT(10) NULL COMMENT '完成小时数',
       `course_id` INT(10) NULL COMMENT '课程id',
       `course_name` varchar(100) NULL COMMENT '课程名字',
       `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
       `update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
       PRIMARY KEY (`id`),
       unique key (picture_name)
)COMMENT '学生证书';
