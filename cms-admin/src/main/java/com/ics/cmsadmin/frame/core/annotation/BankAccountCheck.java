package com.ics.cmsadmin.frame.core.annotation;

import com.ics.cmsadmin.frame.api.AliBabaApi;
import com.ics.cmsadmin.frame.api.CcdcapiResponse;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 校验银行卡是否正确
 */
public class BankAccountCheck implements ConstraintValidator<BankAccount, String> {
    @Override
    public boolean isValid(String accoutNo, ConstraintValidatorContext context) {
        if (StringUtils.isBlank(accoutNo)) {
            return true;
        }
        if (!accoutNo.matches("\\d{15,19}")) {
            return false;
        }
        CcdcapiResponse ccdcapiResponse = AliBabaApi.validateAndCacheCardInfo(accoutNo);
        if (ccdcapiResponse == null) {
            return false;
        }
        return ccdcapiResponse.isValidated();
    }
}
