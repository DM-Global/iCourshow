package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;


/**
 * t_package 服务类
 * Created by sandwich on 2018-18-14 21:12:28.
 */
public interface PackageService extends BaseService<PackageBean>, BaseDataService<PackageBean> {
}
