package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentShareRecordBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
* t_student_share_record
* Created by lvsw on 2019-03-03 00:06:16.
*/
@Repository
@Mapper
public interface StudentShareRecordDao extends BaseDao<StudentShareRecordBean>, BaseDataDao<StudentShareRecordBean> {

    void updateCourseIsNotComplete(@Param("courseId") String courseId);
}
