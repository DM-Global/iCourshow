package com.ics.cmsadmin.frame.constant;

/**
 * swagger部分notes的说明
 * Created by Sandwich on 2018-08-09
 */
public interface SwaggerNoteConstants {

    String LOGIN_NOTES = "统一登录方式\n" +
        "1. cms前端登录\n" +
        "   - loginType: CMS_ADMIN_WEB\n" +
        "   - username: 用户名\n" +
        "   - password: 密码\n" +
        "2. 公众号微信登陆\n" +
        "   - loginType: WECHAT_CODE_LOGIN\n" +
        "   - code: 微信授权码\n" +
        "   - agentNo: 分销商编号, 没有填0\n" +
        "   - recommendStudentId: 推荐学生id, 没有不填\n" +
        "3. app微信登陆\n" +
        "   - loginType: APP_WECHAT_LOGIN\n" +
        "   - code: 微信授权码\n" +
        "   - agentNo: 分销商编号, 没有填0\n" +
        "   - recommendStudentId: 推荐学生id, 没有不填\n" +
        "   - uMengDeviceToken: 有盟device_token, 没有不填\n" +
        "4. app手机加短信验证码登陆\n" +
        "   - loginType: APP_MOBILE_AND_CODE\n" +
        "   - code: 短信验证码\n" +
        "   - mobilePhone: 手机\n" +
        "   - phoneCode: 手机区域码\n" +
        "   - uMengDeviceToken: 有盟device_token, 没有不填\n" +
        "   - 注意: 短信验证码类型为: loginVerify\n" +
        "   - 如果手机号码不存在,就相当于注册加登陆的功能\n" +
        "   - 通过该接口会返回hasPassword, 根据这个值判断是否要进入绑定密码接口\n" +
        "5. app手机加密码登陆\n" +
        "   - loginType: APP_MOBILE_AND_PASSWORD\n" +
        "   - password: 密码\n" +
        "   - mobilePhone: 手机\n" +
        "   - uMengDeviceToken: 有盟device_token, 没有不填\n" +
        "6. 游客登陆(仅提供给ios用)" +
        "   - loginType: TOURIST_LOGIN" +
        "   - app信息放到请求头user-agent中" +
        "6. 注销登陆\n" +
        "   - loginType: LOGOUT\n";

    String SMS_CODE_NOTES = "短信验证码相关说明:\n" +
        "- 如果配置表 system_is_debugger_mode 设置为true,则是debugger模式,系统不需要真实的发送短信,默认验证始终为1234\n" +
        "- 如果配置表 system_is_debugger_mode 不是设置为true,则为生产模式,系统会真实的发送短信 \n" +
        "- 目前验证a码有以下几种类型\n" +
        "  - forgotPasswordVerify\n" +
        "    - 忘记密码/重置密码时使用这个类型\n" +
        "  - bindPhoneVerify\n" +
        "    - 首次绑定手机号时使用这个类型\n" +
        "    - 通过短信验证码修改手机号码时,新手机号码使用这个类型\n" +
        "  - modifyPhoneVerify\n" +
        "    - 通过密码修改手机号码时使用这个类型\n" +
        "    - 通过短信验证码修改手机号码时,旧手机号码使用这个类型\n" +
        "  - loginVerify\n" +
        "    - 通过短信验证登陆时使用这个类型\n" +
        "    - 手机加验证码注册也是这个类型,调用'手机号加验证登陆接口'\n";

    String UPDATE_MY_USER_NOTES = "该方法目前只能更新以下字段:\n" +
        "  - wechatAccount: 微信号\n" +
        "  - province: 省份编码\n" +
        "  - city: 城市编号\n" +
        "  - county: 县区编码\n" +
        "  - cityName: 省市区中文名, 冗余字段\n" +
        "  - email: 电子邮箱\n" +
        "  - 除上的其他字段的值将被忽略\n";

    String GET_IMAGE_CODE_NOTES = "imageCodeId: 图像验证码id,可以是uuid, 也可以是时间戳, 保证每次调用不同即可";

    String BIND_PASSWORD_NOTES = "绑定密码是在用户第一次设置密码的才调用的\n  - [注意] 密码需要md5之后再上传";

    String BIND_WECHAT_NOTES = "该登陆帐号必须还没有绑定过微信, 且被绑定的微信还没有在系统登陆过,才能绑定成功.";

    String FORGOT_PASSWORD_NOTES = "忘记密码,需要先获取短信验证码,验证码类型为:forgotPasswordVerify";

    String BIND_PHONE_NOTES = "只有在用微信登陆之后,如果返回信息的手机号码为空,则需要调用该接口\n" +
        "被绑定的用户此时手机号必须为空,短信验证码类型: bindPhoneVerify";

    String MODIFY_PASSWORD_NOTES = "修改密码,需要提供新密码和旧密码\n" +
        " - [注意]: 新密码和旧密码需要md5之后在上传给服务端";

    String MODIFY_PHONE_BY_SMS_CODE_NOTES = "- oldCode短信验证码类型: modifyPhoneVerify;\n" +
        "- newCode短信验证码类型: bindPhoneVerify;\n";

    String CHECK_SMS_VERIFY_CODE_NODES = "用于校验短信验证接口,目前有两个用途\n" +
        "1. 通过手机号码修改密码前先调用该接口来校验短信验证码\n" +
        "   - codeType 为forgotPasswordVerify\n" +
        "2. 通过旧手机号码修改新手机号码前先调用该接口来校验短信验证码\n" +
        "   -codeType 为modifyPhoneVerify";

    String CHECK_APP_VERSION_NOTES = "需要上传当前app类型和app版本号" +
        "  - app类型支持ios/android\n" +
        "  - app版本号定为'1.0.0.0'这种格式\n";

    String APP_HOME_GET_COURSES = "可以获取所有课程/推荐课程/科学/英语/生物等，" +
        "取决于courseTypeId，可以添加课程其他字段作为索引过滤条件\n" +
        "- 如果是模块询对应的课程信息,courseTypeId传对应的模块id\n" +
        "- 如果科目查询对应的课程信息,subject_id传对应的科目id\n" +
        "- collected: 为用户收藏标志\n";

    String APP_CREATE_ORDER = "创建订单需要登录和以下参数：\n" +
        "- packageId 套餐id\n" +
        "- orderType 订单类型：订单类型: 1.购买课程（这个目前是公众号的下单方式） 2.包年包月\n";
    String APP_LATEST_ORDER = "获取最新的订单数据" +
        "- studyStatus: 如果最新的订单是3.已到期，说明前面所有的订单都已经到期，如果最新的订单是1.未入学，那么前面肯定会有已入学的订单，如果最新的订单是2.已入学，那么就是入学状态\n" +
        "- 总结：1和2都显示学生是已入学状态，3.显示已到期，到期时间就取最新订单的endDate\n" +
        "- hasTrialOrder true用户已经领取过免费学籍订单, false用户还没有领取过免费学籍订单";

    String PROBLEM_SET_DETAILS = "参数需要level 和levelId\n" +
        "- level可取值 'subject','course','unit','topic','point' \n" +
        "- 返回值说明\n" +
        "   - problemSet是问题集的基本信息\n" +
        "   - problems 是问题列表\n" +
        "       - problem 问题信息\n" +
        "           - type 目前只有fillin(填空题,目前有且只有一个空)和selection(选择题 可能有多选)\n" +
        "           - content 问题内容\n" +
        "       - answers 答案列表\n" +
        "           - content 答案内容,填空题需要忽略大小写,去左右空格然后再比较\n" +
        "           - correct 该答案是否正确\n" +
        "       - tips 提示列表\n" +
        "           - type: 0 文本提示 1 知识点提示; 文本提示就直接展示文本即可,知识点提示,需要能点击跳到对应的页面\n" +
        "           - content 提示内容\n";

    String APP_WX_PAY = "获取预支付参数已经简化到只需要orderId,请拿到返回值去调用公共支付接口：\n" +
        "   - orderId 订单id（格式：http://api-test.icourshow.com/ics/app/wxpay/appPay?orderId=039fea9588df4644abd1d4ddc010d2fd）\n" +
        "- 返回值格式：{\n" +
        "    \"code\": 0,\n" +
        "    \"message\": null,\n" +
        "    \"data\": \"{\\\"package\\\":\\\"Sign=WXPay\\\",\\\"appid\\\":\\\"wx4838d74fd406436d\\\",\\\"sign\\\":\\\"05CF7309FE780FF4B8005315D239B966\\\",\\\"partnerid\\\":\\\"1520329271\\\",\\\"prepayid\\\":\\\"wx3013533513408826a81098151455421957\\\",\\\"noncestr\\\":\\\"1546149215327\\\",\\\"timestamp\\\":\\\"1546149215\\\"}\"\n" +
        "       }\n" +
        "- 请拿到返回值后再去调用统一下单接口，参考文档：https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_1#\n";

    String APP_WX_PAY_NOTIFY = "支付回调函数不需要app端调用，是供给微信后台调用我们的后台的";

    String LIST_STUDENT_DAY_SUMMARY_NOTES = "获取学生日结统计数据,为折线图准备\n" +
        "- summaryDay 统计日期\n" +
        "- childrenCusm 累计新增下级学生数量\n" +
        "- childrenNewly 统计当天新增学生数量\n" +
        "- childOrderMoneyCusm 累计下级学生购买订单金额\n" +
        "- childOrderMoneyNewly 统计当天下级学生购买订单金额\n" +
        "- shareMoneyCusm 累计获取的分润金额\n" +
        "- shareMoneyNewly 统计当天获取的分润金额\n";
    String SAVE_VIDEO_HISTORY = "保存观看视频记录\n" +
        "- 参数\n" +
        "   - position 当前视频播放时长\n" +
        "   - totalTime 本次视频观看时长\n" +
        "       - 如果是暂停之后又播放再暂停的话，则取两次暂停之间的差值\n" +
        "   - videoId 视频id\n" +
        "   - pointId 知识点id\n" +
        "   - duration 视频时长\n" +
        "- 返回值， 如果返回值data不为空，则需要弹出分享海报\n" +
        "   - shareType 海报类型\n" +
        "       - 2：每观看视频时长累计满1，3，5，7.。。个小时，弹出分享海报\n" +
        "       - 3: 该课程下所有视频都点击过，且观看记录超过全部课程下视频的60%，弹出分享海报\n" +
        "   - posterSrc 海报url\n" +
        "   - shareUrl: 分享二维码链接地址\n" +
        "";
}
