package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.TopicBean;
import com.ics.cmsadmin.modules.basic.service.TopicService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/topic")
public class ApiTopicController {

    @Autowired
    private TopicService topicService;

    @ApiOperation(value = "分页查询主题信息")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody TopicBean topicBean,
                            @ApiParam("页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                            @ApiParam("每页条数") @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        PageBean page = new PageBean(pageNo, pageSize);
        topicBean.setIsActive(true);
        PageResult pageResult = topicService.list(topicBean, page);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @GetMapping("/query/{id}")
    public ApiResponse detail(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        TopicBean topicBean = topicService.topicDetail(userId, id);
        return ApiResponse.getDefaultResponse(topicBean);
    }

    @GetMapping("/listDetail")
    public ApiResponse listDetail(String unitId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        List<TopicBean> topicList = topicService.listDetail(userId, unitId);
        return ApiResponse.getDefaultResponse(topicList);
    }
}
