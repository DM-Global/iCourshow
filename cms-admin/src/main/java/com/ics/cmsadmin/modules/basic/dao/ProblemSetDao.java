package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.ProblemSetBean;
import com.ics.cmsadmin.modules.basic.bean.ScoreBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-09-06.
 */
@Repository
@Mapper
public interface ProblemSetDao extends BaseDao<ProblemSetBean>, BaseDataDao<ProblemSetBean> {
    /**
     * 根据问题id查询题集信息
     *
     * @param problemId
     * @return
     */
    ProblemSetBean queryByProblemId(@Param("problemId") String problemId);

    /**
     * 插入分数
     *
     * @param scoreBean
     */
    void mergeIntoScore(@Param("bean") ScoreBean scoreBean);

    /**
     * 统计该题集下测试题的数量
     *
     * @param problemSetId 题集id
     * @return
     */
    int countProblem(@Param("problemSetId") String problemSetId);
}
