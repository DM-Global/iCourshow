package com.ics.cmsadmin.frame.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum LevelTypeEnum {
    SUBJECT("subject"),     // 科目
    COURSE("course"),       // 课程
    UNIT("unit"),           // 单元
    TOPIC("topic"),         // 主题
    POINT("point");         // 知识点

    @Getter
    private String typeName;

    public static LevelTypeEnum vauleByIndex(int index) {
        try {
            return LevelTypeEnum.values()[index];
        } catch (Exception e) {
            return null;
        }
    }

}
