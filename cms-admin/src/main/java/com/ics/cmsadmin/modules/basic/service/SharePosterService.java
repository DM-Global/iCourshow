package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;


/**
 * t_share_poster 服务类
 */
public interface SharePosterService extends BaseService<SharePosterBean>, BaseDataService<SharePosterBean> {
}
