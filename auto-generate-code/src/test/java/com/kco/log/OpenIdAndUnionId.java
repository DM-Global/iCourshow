package com.kco.log;

import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class OpenIdAndUnionId {

    public static void main(String[] args) throws IOException {
        String filePath = "C:\\Users\\pc\\Desktop\\美国\\用户union.txt";
        String sql = "UPDATE t_student SET union_id = '%s' WHERE open_id = '%s';";
        List<String> lines = FileUtils.readLines(new File(filePath), "utf8");
        for (String line : lines) {
            Gson gson = new Gson();
            DataBean dataBean = gson.fromJson(line, DataBean.class);
            List<UserInfoBean> user_info_list = dataBean.getUser_info_list();
//            System.out.println(user_info_list);
            for (UserInfoBean userInfoBean : user_info_list) {
                if (StringUtils.isNotBlank(userInfoBean.getOpenid()) && StringUtils.isNotBlank(userInfoBean.getUnionid())) {
                    System.out.println(String.format(sql, userInfoBean.getUnionid(), userInfoBean.getOpenid()));
                }
            }
        }
    }
}
