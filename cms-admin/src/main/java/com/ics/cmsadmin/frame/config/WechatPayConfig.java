package com.ics.cmsadmin.frame.config;

import com.ics.cmsadmin.frame.property.WechatConfig;
import com.lly835.bestpay.config.WxPayH5Config;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Sandwich on 2018-08-18
 */
@Configuration
public class WechatPayConfig {
    @Autowired
    private WechatConfig wechatConfig;

    @Bean
    public BestPayServiceImpl bestPayService() {
        BestPayServiceImpl bestPayService = new BestPayServiceImpl();
        bestPayService.setWxPayH5Config(wxPayH5Config());
        return bestPayService;
    }

    @Bean
    public WxPayH5Config wxPayH5Config() {
        WxPayH5Config wxPayH5Config = new WxPayH5Config();
        wxPayH5Config.setAppId(wechatConfig.getAppId());
        wxPayH5Config.setAppSecret(wechatConfig.getAppSecret());
        wxPayH5Config.setMchId(wechatConfig.getMchId());
        wxPayH5Config.setMchKey(wechatConfig.getMchKey());
        wxPayH5Config.setKeyPath(wechatConfig.getKeyPath());
        wxPayH5Config.setNotifyUrl(wechatConfig.getNotifyUrl());
        return wxPayH5Config;
    }
}
