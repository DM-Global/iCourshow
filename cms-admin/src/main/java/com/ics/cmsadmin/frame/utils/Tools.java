package com.ics.cmsadmin.frame.utils;

import com.ics.cmsadmin.frame.profit.ProfitTypeEnum;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * 工具类
 */
public final class Tools {
    public final static Predicate<String> TRUE_PREDICATE = item -> true;
    public final static Predicate<String> Between0To100_PREDICATE = Tools::isBetween0To100;
    public final static Predicate<String> Zero_To_One_PREDICATE = Tools::isFromZero2One;
    public final static Predicate<String> GRADIENT_SHARE_RATE_PREDICATE = Tools::gradientShareRateValid;
    public final static Predicate<String> NUMERIC_PREDICATE = Tools::isNumeric;
    public final static Predicate<String> INTEGER_PREDICATE = Tools::isInteger;

    /**
     * 判断字符串是否为数字
     *
     * @param value
     * @return
     */
    public static boolean isNumeric(String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }
        return value.matches("^[-\\+]?([1-9]\\d*|0)(\\.\\d+)?$");
    }

    /**
     * 阶梯分润比例校验规则
     */
    public static boolean gradientShareRateValid(String value) {
        try {
            ProfitTypeEnum.GRADIENT_RATE.getFactory().verifyExpression(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 少于100的
     *
     * @param value
     * @return
     */
    public static boolean isBetween0To100(String value) {
        try {
            BigDecimal bigDecimal = new BigDecimal(value);
            return bigDecimal.compareTo(BigDecimal.ZERO) >= 0 && bigDecimal.compareTo(new BigDecimal(100)) < 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static void main(String[] args) {
        System.out.println(isBetween0To100("0"));
    }
    /**
     * 0< value <=1?
     *
     * @param value
     * @return
     */
    public static boolean isFromZero2One(String value) {
        try {
            BigDecimal bigDecimal = new BigDecimal(value);
            if (bigDecimal.compareTo(new BigDecimal(1)) > 0) {
                return false;
            }
            if (bigDecimal.compareTo(new BigDecimal(0)) <= 0){
                return  false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 判断字符串是否为整数
     */
    public static boolean isInteger(String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }
        try {
            Integer.valueOf(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 生成随机code
     */
    public static String randomString(int count) {
        if (count <= 0) {
            count = 4;
        }
        return RandomStringUtils.random(count, "123456789QWERTYUIOPLKJHGFDSAZXCVBNMqwertyuioplkjhgfdsazxcvbnm");
    }

    public static String getValueFromMap(Map<String, Object> map, String key) {
        if (map == null) {
            return "";
        }
        return Optional.ofNullable(map.get(key)).orElse("").toString();
    }

    /**
     * 正则校验
     *
     * @return
     */
    public static Predicate<String> isMatch(String pattern) {
        return s -> StringUtils.isNotBlank(s) && s.matches(pattern);
    }

    /**
     * 获取资源文件流
     * @param location  class:/foo/baz.png
     */
    public static InputStream getResourceInputStream(String location) throws IOException {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        return resourceLoader.getResource(location).getInputStream();
    }
}
