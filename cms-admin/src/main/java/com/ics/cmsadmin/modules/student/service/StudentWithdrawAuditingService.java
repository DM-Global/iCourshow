package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.modules.agent.bean.ApplyWithdrawCashBean;
import com.ics.cmsadmin.modules.student.bean.StudentWithdrawAuditingBean;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.math.BigDecimal;


/**
* t_student_withdraw_auditing 服务类
* Created by lvsw on 2019-44-31 16:05:02.
*/
public interface StudentWithdrawAuditingService extends BaseService<StudentWithdrawAuditingBean>, BaseDataService<StudentWithdrawAuditingBean> {

    /**
     * 接受学生提现申请
     * @param applyId       申请id
     * @param loginUserId   登录用户
     */
    boolean acceptStudentWithdrawAuditing(String applyId, String loginUserId);

    /**
     * 拒绝学生提现申请
     * @param applyId       申请id
     * @param loginUserId   登录用户
     * @return
     */
    boolean rejectStudentWithdrawAuditing(String applyId, String remark, String loginUserId);

    /**
     * 申请提现记录
     * @param money 申请金额
     * @param loginUserId 登录用户即申请人
     * @return
     */
    boolean applyStudentWithdrawAuditing(String openId, BigDecimal money, String withdrawType, String loginUserId);
    /**
     * 用户自行取消提现申请
     * @param applyId       申请id
     * @param loginUserId   登录用户
     */
    boolean cancelStudentWithdrawAuditing(String applyId, String loginUserId);
}
