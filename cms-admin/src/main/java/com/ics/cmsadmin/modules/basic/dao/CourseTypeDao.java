package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.CourseTypeBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CourseTypeDao {

    List<CourseTypeBean> list(@Param("bean") CourseTypeBean t, @Param("page") PageBean page);

    long count(@Param("bean") CourseTypeBean t);

    int insert(@Param("bean") CourseTypeBean t);

    int update(@Param("id") String id, @Param("bean") CourseTypeBean t);

    int delete(@Param("id") String id);
}
