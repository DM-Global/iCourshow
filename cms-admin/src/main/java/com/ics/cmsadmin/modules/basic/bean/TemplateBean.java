package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
 * t_template
 */
@Data
@Builder
public class TemplateBean {

    private String id;              // id
    private String content;              // 模版内容
    private String title;              // 标题
    private String type;              // 1.邮件模版 2.公众号模版
    private String description;              // 描述
    private String email;              // 邮箱地址
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间

    @Tolerate
    public TemplateBean() {
    }
}
