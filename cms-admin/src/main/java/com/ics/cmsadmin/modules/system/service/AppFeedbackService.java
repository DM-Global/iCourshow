package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.system.bean.AppFeedbackBean;

public interface AppFeedbackService {
    /**
     * 插入意见反馈信息
     */
    boolean insert(AppFeedbackBean appFeedbackBean);

    /**
     * 根据id查询意见反馈
     */
    AppFeedbackBean queryById(String id);

    /**
     * 分页查询意见反馈
     */
    PageResult<AppFeedbackBean> list(AppFeedbackBean appFeedbackBean, PageBean pageBean);
}
