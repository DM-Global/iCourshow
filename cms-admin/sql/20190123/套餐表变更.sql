ALTER TABLE `t_package`
ADD COLUMN `ios_purch_id` varchar(50) NULL COMMENT 'ios内购产品id' AFTER `id`;


-- 删除订单表的order_id
ALTER TABLE `t_order`
DROP COLUMN `open_id`;

ALTER TABLE `zh_order`
DROP COLUMN `open_id`;

DROP TRIGGER `tri_order_insert`;

DROP TRIGGER `tri_order_update`;

DROP TRIGGER `tri_order_delete`;

CREATE TRIGGER `tri_order_insert` AFTER INSERT ON `t_order` FOR EACH ROW insert into zh_order(id,user_id,course_id,order_type,course_price,order_amount,payment_method,status,acc_status,acc_time,agent_no,salesman,create_time,
pay_time,update_time,is_deleted,start_date,end_date,package_id,study_status,operate_type,operate_ts) values(new.id,new.user_id,new.course_id,new.order_type,new.course_price,new.order_amount,new.payment_method,new.status,new.acc_status,new.acc_time,new.agent_no,new.salesman,new.create_time,
new.pay_time,new.update_time,new.is_deleted,new.start_date,new.end_date,new.package_id,new.study_status,'I',CURRENT_TIMESTAMP);

CREATE TRIGGER `tri_order_update` AFTER UPDATE ON `t_order` FOR EACH ROW insert into zh_order(id,user_id,course_id,order_type,course_price,order_amount,payment_method,status,acc_status,acc_time,agent_no,salesman,create_time,
pay_time,update_time,is_deleted,start_date,end_date,package_id,study_status,operate_type,operate_ts) values(new.id,new.user_id,new.course_id,new.order_type,new.course_price,new.order_amount,new.payment_method,new.status,new.acc_status,new.acc_time,new.agent_no,new.salesman,new.create_time,
new.pay_time,new.update_time,new.is_deleted,new.start_date,new.end_date,new.package_id,new.study_status,'U',CURRENT_TIMESTAMP);

CREATE TRIGGER `tri_order_delete` AFTER DELETE ON `t_order` FOR EACH ROW insert into zh_order(id,user_id,course_id,order_type,course_price,order_amount,payment_method,status,acc_status,acc_time,agent_no,salesman,create_time,
pay_time,update_time,is_deleted,start_date,end_date,package_id,study_status,operate_type,operate_ts) values(old.id,old.user_id,old.course_id,old.order_type,old.course_price,old.order_amount,old.payment_method,old.status,old.acc_status,old.acc_time,old.agent_no,old.salesman,old.create_time,
old.pay_time,old.update_time,old.is_deleted,old.start_date,old.end_date,old.package_id,old.study_status,'D',CURRENT_TIMESTAMP);


ALTER TABLE `t_order`
MODIFY COLUMN `payment_method` tinyint(3) NULL DEFAULT 1 COMMENT '支付方式: 1.微信公众号 2微信app  3.支付宝 4.免费试用 5.苹果支付' AFTER `order_amount`;


