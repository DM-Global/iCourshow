package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.service.PointService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Api(description = "知识点api接口")
@RestController
@RequestMapping("/api/point")
public class ApiPointController {

    @Autowired
    private PointService pointService;

    @ApiOperation(value = "根据主题Id查询所有知识点信息")
    @RequestMapping("/listAllPointByTopicId/{topicId}")
    public ApiResponse listAllPointByTopicId(@PathVariable String topicId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return ApiResponse.getDefaultResponse(pointService.listAllPointDetailByTopicId(userId, topicId));
    }

    @GetMapping("/query/{id}")
    public ApiResponse detail(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        PointBean pointBean = pointService.pointDetail(userId, id);
        return ApiResponse.getDefaultResponse(pointBean);
    }

    @GetMapping("/finish/{id}")
    public ApiResponse finish(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        pointService.finish(userId, id);
        return ApiResponse.getDefaultResponse();
    }
}
