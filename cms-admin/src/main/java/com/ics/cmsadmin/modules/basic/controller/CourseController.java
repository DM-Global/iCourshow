package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.CourseBean;
import com.ics.cmsadmin.modules.basic.service.CourseService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Course controller
 * Created by lvsw on 2018-09-01.
 */
@Api(description = "课程管理接口")
@RestController
@RequestMapping("/course")
public class CourseController {

    @Resource
    private CourseService courseService;

    @Authorize(AuthorizeEnum.COURSE_QUERY)
    @ApiOperation(value = "查询课程信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(courseService.queryById(id));
    }

    @Authorize(AuthorizeEnum.COURSE_INSERT)
    @ApiOperation(value = "新增课程信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody CourseBean courseBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(courseService.insert(courseBean));
    }

    @Authorize(AuthorizeEnum.COURSE_DELETE)
    @ApiOperation(value = "删除课程信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的课程 id") @PathVariable String id) {
        return new ApiResponse(courseService.delete(id));
    }

    @Authorize(AuthorizeEnum.COURSE_UPDATE)
    @ApiOperation(value = "更新课程信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody CourseBean courseBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的课程Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(courseService.update(id, courseBean));
    }

    @Authorize(AuthorizeEnum.COURSE_QUERY)
    @ApiOperation(value = "分页查询课程信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody CourseBean courseBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(courseService.list(courseBean, pageBean));
    }

    @Authorize(AuthorizeEnum.COURSE_QUERY)
    @ApiOperation(value = "根据科目Id查询所有课程信息")
    @GetMapping("/listAllCourseBySubjectId/{subjectId}")
    public ApiResponse listAllCourseBySubjectId(@PathVariable String subjectId) {
        return new ApiResponse(courseService.listAllCourseBySubjectId(subjectId));
    }


    @Authorize(AuthorizeEnum.COURSE_ATTR_BIND)
    @ApiOperation(value = "绑定课程到课程类型上")
    @GetMapping("/bindCourseType")
    public ApiResponse bindCourseType(@RequestParam String courseId, @RequestParam String courseTypeId) {

        return ApiResponse.getDefaultResponse(courseService.bindCourseType(courseId, courseTypeId));
    }

    @Authorize(AuthorizeEnum.COURSE_ATTR_UNBIND)
    @ApiOperation(value = "解绑课程与课程类型关系")
    @GetMapping("/unbindCourseType")
    public ApiResponse unbindCourseType(@RequestParam String courseId, @RequestParam String courseTypeId) {

        return ApiResponse.getDefaultResponse(courseService.unbindCourseType(courseId, courseTypeId));
    }

    @Authorize(AuthorizeEnum.COURSE_ATTR_UNBIND)
    @ApiOperation(value = "批量解绑课程与课程类型关系")
    @PostMapping("/bulkUnbindCourseType")
    public ApiResponse bulkUnbindCourseType(@RequestBody List<String> ids, @RequestParam String courseTypeId) {

        return ApiResponse.getDefaultResponse(courseService.bulkUnbindCourseType(ids, courseTypeId));
    }
}
