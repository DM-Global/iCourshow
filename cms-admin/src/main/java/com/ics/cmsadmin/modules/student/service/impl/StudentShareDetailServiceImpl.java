package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.student.bean.StudentShareDetailBean;
import com.ics.cmsadmin.modules.student.dao.StudentShareDetailDao;
import com.ics.cmsadmin.modules.student.service.StudentShareDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.math.BigDecimal;

import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentShareDetailDao;

@Service
public class StudentShareDetailServiceImpl implements StudentShareDetailService {

    @Override
    public PageResult<StudentShareDetailBean> list(StudentShareDetailBean studentShareDetailBean, PageBean page) {
        long count = studentShareDetailDao.count(studentShareDetailBean);
        if (count == 0) {
            return new PageResult<>();
        }
        return new PageResult<>(studentShareDetailDao.list(studentShareDetailBean, page), count);
    }

    @Override
    public BigDecimal getTodayProfit(String studentId) {
        return studentShareDetailDao.getTodayProfit(studentId);
    }

}
