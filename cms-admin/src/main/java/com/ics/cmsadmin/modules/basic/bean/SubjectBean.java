package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * t_subject
 * 科目
 * Created by lvsw on 2018-09-01.
 */
@Data
@Builder
public class SubjectBean {

    // 科目id
    private String id;
    // 科目名称
    @Length(message = "科目名称必须在{min}~{max}之间", min = 1, max = 10, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    // 描述
    @Length(message = "科目描述必须在小于{max}个字符", max = 1000, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    // 父科目id
    private String fromSubjectId;
    //父科目名
    private String fromSubjectName;

    // icon id
    private String iconId;
    // icon src
    private String iconSrc;

    // cover id
    private String coverId;
    // cover src
    private String coverSrc;

    // 状态：true 启用，false 禁用
    private Boolean isActive;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 修改时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public SubjectBean() {
    }
}
