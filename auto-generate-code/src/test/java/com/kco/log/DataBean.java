package com.kco.log;

import java.util.List;

public class DataBean {

    private List<UserInfoBean> user_info_list;

    public List<UserInfoBean> getUser_info_list() {
        return user_info_list;
    }

    public void setUser_info_list(List<UserInfoBean> user_info_list) {
        this.user_info_list = user_info_list;
    }
}
