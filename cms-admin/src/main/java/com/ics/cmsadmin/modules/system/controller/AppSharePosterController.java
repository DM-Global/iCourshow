//package com.ics.cmsadmin.modules.system.controller;
//
//import com.aliyun.oss.OSSClient;
//import com.aliyun.oss.model.PutObjectRequest;
//import com.ics.cmsadmin.frame.core.annotation.Authorize;
//import com.ics.cmsadmin.frame.core.bean.ApiResponse;
//import com.ics.cmsadmin.frame.core.bean.PageBean;
//import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
//import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
//import com.ics.cmsadmin.frame.property.AliOssConfig;
//import com.ics.cmsadmin.modules.system.bean.AppSharePosterBean;
//import com.ics.cmsadmin.modules.system.service.AppSharePosterService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.annotation.Resource;
//import java.util.Arrays;
//import java.util.UUID;
//
///**
// * app_share_poster controller
// * Created by lvsw on 2019-18-30 16:05:04.
// */
//@Api(description = "app分享海报")
//@RestController
//@RequestMapping("/appSharePoster")
//public class AppSharePosterController {
//
//    @Resource
//    private AppSharePosterService appSharePosterService;
//    @Resource
//    private AliOssConfig aliOssConfig;
//
//    @Authorize(AuthorizeEnum.APP_SHARE_POSTER_QUERY)
//    @ApiOperation(value = "查询app分享海报信息")
//    @GetMapping(value = "/query/{id}")
//    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
//        return new ApiResponse(appSharePosterService.queryById(id));
//    }
//
//    @Authorize(AuthorizeEnum.APP_SHARE_POSTER_INSERT)
//    @ApiOperation(value = "新增app分享海报信息")
//    @PostMapping(value = "/insert")
//    public ApiResponse insert(@RequestParam("file") MultipartFile file) {
//        if (file.isEmpty()) {
//            return ApiResponse.getResponse(ApiResultEnum.PARAM_ERROR);
//        }
//        if (file.getSize() > 1024 * 1024 * 10) {
//            return ApiResponse.getResponse(ApiResultEnum.FILE_TOO_BIG);
//        }
//        String fileName = file.getOriginalFilename();
//        String suffixName = fileName.substring(fileName.lastIndexOf("."));
//        if (!Arrays.asList("png", "jpg", "gif", "jpeg").contains(suffixName.toLowerCase())) {
//            return ApiResponse.getResponse(ApiResultEnum.FILE_FORMAT_ERR);
//        }
//        try {
//            OSSClient ossClient = new OSSClient("https://" + aliOssConfig.endpoint, aliOssConfig.accessKeyId, aliOssConfig.accessKeySecret);
//            String key = String.format("image/%s.%s", UUID.randomUUID().toString(), suffixName);
//            ossClient.putObject(new PutObjectRequest(aliOssConfig.bucketName, key, file.getInputStream()));
//            AppSharePosterBean appSharePosterBean = AppSharePosterBean.builder()
//                .bucketName(aliOssConfig.bucketName)
//                .ossKey(key)
//                .url(String.format("https://%s.%s/%s", aliOssConfig.bucketName, aliOssConfig.endpoint, key))
//                .build();
//            return new ApiResponse(appSharePosterService.insert(appSharePosterBean));
//        } catch (Exception e) {
//            return ApiResponse.getResponse(ApiResultEnum.FILE_UPLOAD_ERR);
//        }
//    }
//
//    @Authorize(AuthorizeEnum.APP_SHARE_POSTER_DELETE)
//    @ApiOperation(value = "删除app分享海报信息")
//    @PostMapping(value = "/delete/{id}")
//    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
//        AppSharePosterBean appSharePosterBean = appSharePosterService.queryById(id);
//        if (appSharePosterBean != null) {
//            OSSClient ossClient = new OSSClient("https://" + aliOssConfig.endpoint, aliOssConfig.accessKeyId, aliOssConfig.accessKeySecret);
//            ossClient.deleteObject(aliOssConfig.bucketName, appSharePosterBean.getOssKey());
//        }
//        return new ApiResponse(appSharePosterService.delete(id));
//    }
//
//    @Authorize(AuthorizeEnum.APP_SHARE_POSTER_UPDATE)
//    @ApiOperation(value = "禁用海报")
//    @PostMapping(value = "/disable/{id}")
//    public ApiResponse disable(@ApiParam("需要更新的Id") @PathVariable String id) {
//        return new ApiResponse(appSharePosterService.update(id, AppSharePosterBean.builder().isUse("0").build()));
//    }
//
//    @Authorize(AuthorizeEnum.APP_SHARE_POSTER_UPDATE)
//    @ApiOperation(value = "启用海报")
//    @PostMapping(value = "/enable/{id}")
//    public ApiResponse enable(@ApiParam("需要更新的Id") @PathVariable String id) {
//        return new ApiResponse(appSharePosterService.update(id, AppSharePosterBean.builder().isUse("1").build()));
//    }
//
//    @Authorize(AuthorizeEnum.APP_SHARE_POSTER_QUERY)
//    @ApiOperation(value = "分页查询app分享海报信息")
//    @PostMapping("/list/{pageNo}/{pageSize}")
//    public ApiResponse list(@RequestBody AppSharePosterBean appSharePosterBean,
//                            @ApiParam("页码") @PathVariable int pageNo,
//                            @ApiParam("每页条数") @PathVariable int pageSize) {
//        PageBean pageBean = new PageBean(pageNo, pageSize);
//        return new ApiResponse(appSharePosterService.list(appSharePosterBean, pageBean));
//    }
//
//}
