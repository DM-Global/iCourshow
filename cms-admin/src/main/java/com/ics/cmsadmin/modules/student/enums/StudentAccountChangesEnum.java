package com.ics.cmsadmin.modules.student.enums;

import com.google.common.base.Enums;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.EnumUtils;

/**
 * 账户变动明细类型
 * totalIncome 总收益(A): A = B + C
 * withdrawCash 提现金额(B)
 * balance 余额(C): C = D + E
 * availableBalance 可用余额(D)
 * frozenBalance 冻结余额(E)
 */
@AllArgsConstructor
@Getter
public enum StudentAccountChangesEnum {
    /**
     * 父级/祖父分润账户收入
     * totalIncome = totalIncome + money
     * withdrawCash = withdrawCash
     * balance = balance + money
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance
     */
    PARENT_PROFIT_INCOME("一级分润", true),
    GRANDFATHER_PROFIT_INCOME("二级分润", true),
    /**
     * 申请提现
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance - money
     * frozenBalance = frozenBalance + money
     */
    APPLY_WITHDRAW_CASH("申请提现", false),
    /**
     * 接受提现成功
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash + money
     * balance = balance - money
     * availableBalance = availableBalance
     * frozenBalance = frozenBalance - money
     */
    ACCEPT_WITHDRAW_CASH_SUCCESS("提现成功", false),
    /**
     * 接受提现失败
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance - money
     *
     * @see #REFUSE_WITHDRAW_CASH
     */
    ACCEPT_WITHDRAW_CASH_FAILED("提现失败", true),
    /**
     * 拒绝提现 （管理员拒绝）
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance - money
     */
    REFUSE_WITHDRAW_CASH("拒绝提现", true),
    /**
     * 取消提现申请，(代理商主动取消)
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance - money
     *
     * @see #REFUSE_WITHDRAW_CASH
     */
    CANCEL_WITHDRAW_CASH("取消提现", true),
    /**
     * 购买订单
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash + money
     * balance = balance - money
     * availableBalance = availableBalance - money
     * frozenBalance = frozenBalance
     */
    BUY_ORDER("购买订单", false);

    private String name;
    private boolean income;
}
