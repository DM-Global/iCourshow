package com.ics.cmsadmin.student;

import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.student.service.StudentAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.math.BigDecimal;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-03-21 09:04
 */
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class TestStudentShare {
    @Resource
    private ShareDetailService shareDetailService;
    @Resource
    private StudentAccountService studentAccountService;

    @Resource
    private JdbcTemplate jdbcTemplate;
    @Resource
    private DataSource dataSource;

    @Test
    public void test() throws Exception {
        String cmd = " cmd /c date " + "2018-02-22";
//        String cmd = " cmd /c start d:";
        Process exec = Runtime.getRuntime().exec(cmd);
        Thread.sleep(1000);
//        studentAccountService.accountChanges("e9080e18999e4518b1a2b6cf45d0521b", new BigDecimal("50"), StudentAccountChangesEnum.BUY_ORDER);
    }

    @Test
    public void testShare() {
        shareDetailService.generateShareByOrderId("80ba71e48820456b83b613b0a9f86d36");
    }

    @Test
    public void testBatchShare() {
        while (true) {
            String sql = "SELECT * FROM t_order WHERE STATUS=2 AND acc_status = 0 ORDER BY create_time ASC limit 1";
            String id = jdbcTemplate.query(sql, resultSet ->
                resultSet.first() ? resultSet.getString("id") : null
            );
            if (id == null) {
                break;
            }
            shareDetailService.generateShareByOrderId(id);
        }

    }
}
