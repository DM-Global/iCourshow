package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.SubjectBean;
import com.ics.cmsadmin.modules.basic.service.SubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(description = "科目查询接口", tags = "公众号")
@RestController
@RequestMapping("/api/subject")
public class ApiSubjectController {

    @Autowired
    private SubjectService subjectService;

    @ApiOperation(value = "查询所有已启用的科目")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody SubjectBean subject) {
        PageBean page = new PageBean(1, Integer.MAX_VALUE);
        subject.setIsActive(true);
        return ApiResponse.getDefaultResponse(subjectService.list(subject, page));
    }
}
