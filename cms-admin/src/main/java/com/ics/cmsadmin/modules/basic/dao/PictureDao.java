package com.ics.cmsadmin.modules.basic.dao;


import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.PictureBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PictureDao {

    PictureBean queryById(@Param("id") Long id);

    int insert(@Param("bean") PictureBean t);

    int update(@Param("id") Long id, @Param("bean") PictureBean t);

    long count(@Param("bean") PictureBean t);

    List<PictureBean> list(@Param("bean") PictureBean t, @Param("page") PageBean page);

    int delete(@Param("id") Long id);
}
