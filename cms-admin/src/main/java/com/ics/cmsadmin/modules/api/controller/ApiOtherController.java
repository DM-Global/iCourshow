package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sandwich on 2019/7/10
 */
@Api(description = "公众号额外接口", tags = "公众号")
@RestController
@RequestMapping("api/extra")
@Log4j2
public class ApiOtherController {
    @Resource
    private RegisterService registerService;

    @ApiOperation(value = "H5视频vid", notes = "H5导航页面视频的vid")
    @GetMapping("/video")
    public ApiResponse detail() {
        String sharedVid = registerService.queryRegisterValue(RegisterKeyValueEnum.H5_SHARED_VIDEO_VID, String::new);
        Map map = new HashMap();
        map.put("vid",sharedVid);
        return ApiResponse.getDefaultResponse(map);
    }

}
