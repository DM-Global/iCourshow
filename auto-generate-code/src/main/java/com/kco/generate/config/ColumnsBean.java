package com.kco.generate.config;

import lombok.Builder;
import lombok.Data;

/**
 * Created by 666666 on 2017/9/26.
 */
@Data
@Builder
public class ColumnsBean {
    private String columnName;
    private String propertyName;
    private String columnComment;
    private String jdbcType;
    private String javaType;
    private int maxSize;
    private boolean isNullAble;
    private String defaultValue;
}
