package com.ics.cmsadmin.frame.core.bean;

/**
 * 两位的元组
 * Created by 666666 on 2017/9/26.
 */
public class FiveTupleBean<ONE, TWO, THREE, FOUR, FIVE> extends FourTupleBean<ONE, TWO, THREE, FOUR> {
    public FIVE five;

    public FiveTupleBean() {
        super();
    }

    public FiveTupleBean(ONE one, TWO two, THREE three, FOUR four, FIVE five) {
        super(one, two, three, four);
        this.five = five;
    }

}
