package com.kco.generate.app;

import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateServiceTs {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(build(tableBean));
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + ".service.ts", sb.toString());
    }

    private static String build(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        sb.append("import {Injectable} from \"@angular/core\";\n" +
            "import {ResponseBean} from \"../utils/response_result\";\n" +
            "import {HttpService} from \"../utils/http.service\";\n" +
            "\n" +
            "\n" +
            "/**\n" +
            " * " + tableBean.getPrimaryPropertyKeyColumnComment() + "服务\n" +
            " */\n" +
            "@Injectable()\n" +
            "export class " + className + "Service {\n" +
            "\n" +
            "  constructor(private httpService: HttpService){\n" +
            "\n" +
            "  }\n" +
            "\n" +
            "  baseUrl: string = \"/" + tempName + "\";\n" +
            "\n" +
            "  // 查询" + tableBean.getPrimaryPropertyKeyColumnComment() + "列表\n" +
            "  list" + className + "(searchParam:object, pageNo:number, pageSize:number):Promise<ResponseBean> {\n" +
            "      let url = `${this.baseUrl}/list/${pageNo}/${pageSize}`;\n" +
            "      return this.httpService.post(url, searchParam);\n" +
            "  }\n" +
            "\n" +
            "  // 查询单个" + tableBean.getPrimaryPropertyKeyColumnComment() + "信息\n" +
            "  query" + className + "(" + tableBean.getPrimaryPropertyKey() + ":string){\n" +
            "    let url = `${this.baseUrl}/query/${" + tableBean.getPrimaryPropertyKey() + "}`;\n" +
            "    return this.httpService.get(url);\n" +
            "  }\n" +
            "\n" +
            "  // 删除角色\n" +
            "  delete" + className + "(" + tableBean.getPrimaryPropertyKey() + ": string) {\n" +
            "    let url = `${this.baseUrl}/delete/${" + tableBean.getPrimaryPropertyKey() + "}`;\n" +
            "    return this.httpService.delete(url);\n" +
            "  }\n" +
            "\n" +
            "  // 新增角色\n" +
            "  insert" + className + "(" + tempName + ": any) {\n" +
            "    let url = `${this.baseUrl}/insert`;\n" +
            "    return this.httpService.put(url, " + tempName + ");\n" +
            "  }\n" +
            "\n" +
            "  // 更新角色\n" +
            "  update" + className + "(" + tableBean.getPrimaryPropertyKey() + ": string, " + tempName + ": any) {\n" +
            "    let url = `${this.baseUrl}/update/${" + tableBean.getPrimaryPropertyKey() + "}`;\n" +
            "    return this.httpService.post(url, " + tempName + ");\n" +
            "  }\n" +
            "\n" +
            "}\n");
        return sb.toString();
    }
}
