-- 2018年11月10日
CREATE TABLE `t_share_poster` (
`id`  int(11) NOT NULL AUTO_INCREMENT COMMENT '分享海报id' ,
`poster_id`  int(11) NOT NULL COMMENT '海报图片id' ,
`name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分享海报名' ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述' ,
`create_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间' ,
`update_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
PRIMARY KEY (`id`)
)

-- 图片表类型增加分享海报类型
ALTER TABLE `t_picture`
MODIFY COLUMN `pic_type`  tinyint(3) NULL DEFAULT NULL COMMENT '图标类型: 1. 课程封面 2. 单元封面 3.老师头像 4. 问题图片 5. 走马灯图片 6.课程介绍图文 7.课程海报 8.知识点封面 9. 单元图标 10.科目封面 11.科目图标 12.分享海报 ' AFTER `description`;

-- 海报增加激活状态
ALTER TABLE `t_share_poster`
ADD COLUMN `is_active`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '状态:0 禁用,1激活' AFTER `description`;

COMMIT;
