package com.ics.cmsadmin.modules.pub.service;/*
 *    created by mengyang ${date}
 */

import com.ics.cmsadmin.modules.pub.dao.ScoreMapper;
import com.ics.cmsadmin.modules.pub.bean.Score;
import org.springframework.stereotype.Service;

@Service
public class ScoreService extends ResourceService<ScoreMapper, Score, Integer> {
}
