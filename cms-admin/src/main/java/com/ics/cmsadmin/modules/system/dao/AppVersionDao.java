package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * t_app_version
 * Created by lvsw on 2018-17-05 20:12:49.
 */
@Repository
@Mapper
public interface AppVersionDao extends BaseDao<AppVersionBean>, BaseDataDao<AppVersionBean> {
    /**
     * 获取最新app版本信息
     *
     * @param type                app类型
     * @param currentAppVersionId 当前版本id
     * @return
     */
    AppVersionBean findNewestAppVersion(@Param("type") String type,
                                        @Param("currentAppVersionId") String currentAppVersionId);

    /**
     * 检查是否需要强制更新
     * @param minVersion
     * @param maxVersion
     * @return
     */
    String checkForceUpdate(@Param("minVersion")String minVersion, @Param("maxVersion") String maxVersion);

    /**
     * 获取最新的版本信息
     * @param type
     * @return
     */
    AppVersionBean findLastVersionByType(@Param("type") String type);
}
