package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
 * t_course_attribute
 * <p>
 * <p>
 * Created by sandwich on 2018-12-10 07:12:28.
 */
@Data
@Builder
public class CourseAttributeBean {

    private String id;              // id
    private String description;              // 描述
    private String name;              // 名字
    private String imageId;              // 图片id
    private String imageSrc;              // 图片链接
    private Boolean isActive;              // 状态
    private Integer sort;                  // 排序
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间

    @Tolerate
    public CourseAttributeBean() {
    }
}
