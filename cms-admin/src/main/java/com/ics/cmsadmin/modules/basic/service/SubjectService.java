package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.SubjectBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

/**
 * t_subject 服务类
 * Created by lvsw on 2018-09-01.
 */
public interface SubjectService extends BaseDataService<SubjectBean>, BaseService<SubjectBean> {

    /**
     * 根据类型节点获取节点名字
     *
     * @param levelNode 类型节点,以逗号分隔
     */
    String findNameByLevelNode(String levelNode);
}
