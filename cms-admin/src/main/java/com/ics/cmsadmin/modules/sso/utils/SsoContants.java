package com.ics.cmsadmin.modules.sso.utils;

/**
 * Created by 666666 on 2018/8/25.
 */
public final class SsoContants {
    public static final String LOGIN_TYPE = "loginType";
    public static final String LOGIN_USERNAME = "username";
    public static final String LOGIN_PASSWORD = "password";

    public static final String LOGIN_MOBILE_PHONE = "mobilePhone";
    public static final String LOGIN_PHONE_CODE = "phoneCode";
    public static final String LOGIN_CODE = "code";
    public static final String LOGIN_AGENT_NO = "agentNo";
    public static final String LOGIN_RECOMMEND_STUDENT_ID = "recommendStudentId";   // 推荐学生
    public static final String LOGIN_APP_TYPE = "appType";
}
