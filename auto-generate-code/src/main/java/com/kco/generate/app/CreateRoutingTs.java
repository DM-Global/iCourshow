package com.kco.generate.app;

import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateRoutingTs {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildImport(tableBean))
            .append(buildRoutes(tableBean))
            .append(buildNgModule(tableBean));
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + "-routing.module.ts", sb.toString());
    }

    private static String buildNgModule(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        sb.append("@NgModule({\n" +
            "  imports: [\n" +
            "    RouterModule.forChild(" + tempName + "Routes)\n" +
            "  ],\n" +
            "  exports: [\n" +
            "    RouterModule\n" +
            "  ]\n" +
            "})\n" +
            "export class " + className + "RoutingModule { }");
        return sb.toString();
    }

    private static String buildRoutes(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        sb.append("const " + tempName + "Routes: Routes = [\n" +
            "    {\n" +
            "        path: '', component: " + className + "Component,\n" +
            "        children: [\n" +
            "            {\n" +
            "                path:'" + tempName + "List',\n" +
            "                component:" + className + "ListComponent\n" +
            "            },{\n" +
            "                path:'" + tempName + "Details/:" + tableBean.getPrimaryPropertyKey() + "',\n" +
            "                component:" + className + "DetailsComponent\n" +
            "            }" +
            "        ]\n" +
            "    }\n" +
            "];\n");
        return sb.toString();
    }

    private static String buildImport(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        sb.append("import { NgModule }   from '@angular/core';\n" +
            "import { RouterModule, Routes } from '@angular/router';\n" +
            "\n" +
            "import { " + className + "Component } from './" + tempName + ".component';\n" +
            "import { " + className + "ListComponent } from './" + tempName + "-list.component';\n" +
            "import { " + className + "DetailsComponent} from './" + tempName + "-details.component';\n");
        return sb.toString();
    }
}
