package com.ics.cmsadmin.frame.profit.bean;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GradientRateData {
    private String minStudent;
    private String maxStudent;
    private String rate;
}
