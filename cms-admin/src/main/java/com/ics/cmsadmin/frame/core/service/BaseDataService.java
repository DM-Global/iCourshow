package com.ics.cmsadmin.frame.core.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface BaseDataService<T> {
    /**
     * 根据查询条件查询列表并汇总
     *
     * @param t 查询条件
     * @return 查询结果
     */
    PageResult<T> list(T t, PageBean page);

}
