package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Point;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PointMapper extends ResourceMapper<Point, Integer> {
}
