package com.ics.cmsadmin.modules.auth.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

/**
 * Created by lvsw on 2018/8/26.
 */
@Data
@Builder
public class SysUser extends LoginInfo {

    @Getter
    private String userId;
    //    @NotEmpty(message = "{User.name.NotEmpty}",  groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "{User.name.Length}", min = 2, max = 50, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    @Getter
    @NotEmpty(message = "{User.loginName.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "{User.loginName.Length}", min = 2, max = 50, groups = {InsertGroup.class, UpdateGroup.class})
    private String loginName;
    private String password;
    @Length(message = "{User.mobilephone.Length}", min = 2, max = 11, groups = {InsertGroup.class, UpdateGroup.class})
    @Pattern(message = "{User.mobilephone.Pattern}", regexp = "^1\\d{10}$", groups = {InsertGroup.class, UpdateGroup.class})
    private String mobilephone;
    @Length(message = "{User.email.Length}", min = 2, max = 50, groups = {InsertGroup.class, UpdateGroup.class})
    @Email(message = "{User.email.Email}", groups = {InsertGroup.class, UpdateGroup.class})
    private String email;
    private String orgId;
    private String orgName;
    private String userType;
    private String status;
    private String postType;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;
    private List<String> authCodes; // 用户拥有的权限集合

    public void setUserId(String userId) {
        super.setLoginId(userId);
        this.userId = userId;
    }

    public void setLoginName(String loginName) {
        super.setLoginUserName(loginName);
        this.loginName = loginName;
    }

    @Tolerate
    public SysUser() {
    }

}
