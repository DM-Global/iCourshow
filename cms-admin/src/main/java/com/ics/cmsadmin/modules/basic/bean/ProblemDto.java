package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * q_problem_set
 * 题集
 * Created by lvsw on 2018-09-06.
 */
@Data
@Builder
public class ProblemDto {
    private ProblemBean problem;
    private List<AnswersBean> answers;
    private List<TipsBean> tips;

}
