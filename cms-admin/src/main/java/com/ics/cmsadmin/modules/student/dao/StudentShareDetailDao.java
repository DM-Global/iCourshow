package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.student.bean.StudentShareDetailBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
* t_student_share_detail
* Created by lvsw on 2019-02-12 11:03:21.
*/
@Repository
@Mapper
public interface StudentShareDetailDao extends BaseDataDao<StudentShareDetailBean> {
    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") StudentShareDetailBean t);

    /**
     * 获取今日分润
     */
    BigDecimal getTodayProfit(@Param("studentId") String studentId);
}
