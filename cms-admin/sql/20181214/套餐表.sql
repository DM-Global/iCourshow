
DROP TABLE IF EXISTS `t_package`;
CREATE TABLE `t_package`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `second_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `price` decimal(10, 2) NOT NULL COMMENT '价格',
  `month_count` int(11) NOT NULL COMMENT '月数',
  `tag` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:0 禁用,1激活',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单套餐' ROW_FORMAT = Dynamic;



ALTER TABLE `t_order`
MODIFY COLUMN `order_type` tinyint(3) NULL DEFAULT NULL COMMENT '订单类型: 1.课程 2.包年包月' AFTER `course_id`,
ADD COLUMN `start_date` datetime(0) NULL COMMENT '生效日期' AFTER `is_deleted`,
ADD COLUMN `end_date` datetime(0) NULL COMMENT '结束日期' AFTER `start_date`,
ADD COLUMN `package_id` int(11) NULL DEFAULT NULL COMMENT '套餐id' AFTER `end_date`;

ALTER TABLE `t_order`
ADD COLUMN `study_status` tinyint(1) NULL COMMENT '学习状态：1.未入学 2.已入学 3.已到期' AFTER `package_id`;

ALTER TABLE `t_order`
MODIFY COLUMN `course_id` int(11) NULL COMMENT '课程id' AFTER `open_id`;

ALTER TABLE `t_order`
MODIFY COLUMN `course_price` decimal(8, 2) NULL COMMENT '交易时课程原价' AFTER `order_type`;

ALTER TABLE `t_order`
MODIFY COLUMN `agent_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '代理商编号' AFTER `acc_time`;

ALTER TABLE `t_order`
MODIFY COLUMN `payment_method` tinyint(3) NULL DEFAULT 1 COMMENT '支付方式: 1.微信公众号 2微信app  3.支付宝 4.免费试用' AFTER `order_amount`;
