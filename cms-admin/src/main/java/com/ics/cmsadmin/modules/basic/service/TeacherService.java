package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.TeacherBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.util.List;


/**
 * t_teacher 服务类
 * Created by lvsw on 2018-14-25 14:09:44.
 */
public interface TeacherService extends BaseDataService<TeacherBean>, BaseService<TeacherBean> {

    List<TeacherBean> findByCourseId(String courseId);

    TeacherBean teacherDetail(String id);
}
