package com.ics.cmsadmin.modules.basic.service;


import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.PictureBean;

public interface PictureService {

    PictureBean queryById(Long id);

    boolean save(PictureBean t);

    PageResult list(PictureBean t, PageBean page);

    boolean delete(Long id);
}
