package com.ics.cmsadmin.modules.auth.service;

/**
 * Created by 666666 on 2018/4/4.
 */
public interface RoleMenuService {
    /**
     * 根据菜单id删除"角色-菜单"
     *
     * @param menuId 菜单id
     */
    void deleteRoleMenuByMenuId(String menuId);

    /**
     * 根据角色id删除 "角色-菜单"
     *
     * @param roleId 角色id
     */
    void deleteRoleMenuByRoleId(String roleId);
}
