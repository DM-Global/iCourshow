package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CourseResource extends Resource<Integer> {
    private String name;

    private Boolean isActive;

    private Boolean isCompleted;

    private String description;

    private Integer subjectId;

    private String subjectName;

    private Integer rank;

    private Integer coverId;

    private String coverSrc;

    private Integer longPosterId;

    private String longPosterSrc;

    private BigDecimal price;

    private BigDecimal discountPrice;

    /**
     * 课程的学员数量
     */
    private Integer learnerCount;

    /**
     * 点赞数量
     */
    private Integer thumbUpCount;

    /**
     * 收藏数量
     */
    private Integer collectCount;

    /**
     * 分享数量
     */
    private Integer shareCount;
}
