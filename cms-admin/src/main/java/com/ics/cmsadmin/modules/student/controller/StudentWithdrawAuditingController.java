package com.ics.cmsadmin.modules.student.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.agent.bean.ApplyWithdrawCashBean;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.student.bean.StudentWithdrawAuditingBean;
import com.ics.cmsadmin.modules.student.service.StudentWithdrawAuditingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_student_withdraw_auditing controller
 * Created by lvsw on 2019-44-31 16:05:02.
 */
@Api(description = "学生提现审核记录")
@RestController
@RequestMapping("/studentWithdrawAuditing")
public class StudentWithdrawAuditingController {

    @Resource
    private StudentWithdrawAuditingService studentWithdrawAuditingService;

    @Authorize(AuthorizeEnum.ACCEPT_STUDENT_WITHDRAW_AUDITING)
    @ApiOperation(value = "接受学生提现申请")
    @PostMapping("/accept/{applyId}")
    public ApiResponse acceptStudentWithdrawAuditing(@PathVariable String applyId, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(studentWithdrawAuditingService.acceptStudentWithdrawAuditing(applyId, loginUserId));
    }

    @Authorize(AuthorizeEnum.REJECT_STUDENT_WITHDRAW_AUDITING)
    @ApiOperation(value = "拒绝学生提现申请")
    @PostMapping("/reject/{applyId}")
    public ApiResponse rejectAcceptStudentWithdrawAuditing(@PathVariable String applyId,
                                                           @RequestBody ApplyWithdrawCashBean bean,
                                                           HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(bean.getRemark())) {
            throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "请填写拒绝原因");
        }
        return new ApiResponse(studentWithdrawAuditingService.rejectStudentWithdrawAuditing(applyId, bean.getRemark(), loginUserId));
    }


    @Authorize(AuthorizeEnum.STUDENT_WITHDRAW_AUDITING_QUERY)
    @ApiOperation(value = "分页查询学生提现审核记录信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody StudentWithdrawAuditingBean studentWithdrawAuditingBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(studentWithdrawAuditingService.list(studentWithdrawAuditingBean, pageBean));
    }

}
