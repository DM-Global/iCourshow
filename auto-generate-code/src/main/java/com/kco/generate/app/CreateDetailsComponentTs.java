package com.kco.generate.app;

import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateDetailsComponentTs {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildClass(tableBean));
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + "-details.component.ts", sb.toString());
    }

    private static String buildClass(TableBean tableBean) {
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();
        sb.append("import {Component, Input} from \"@angular/core\";\n" +
            "import {ActivatedRoute, Router} from \"@angular/router\";\n" +
            "import {" + className + "Service} from \"./" + fileName + ".service\";\n" +
            "import \"rxjs/add/operator/map\";\n" +
            "import {MainService} from \"../main/main-service\";\n" +
            "import {CommonService} from \"../utils/common.service\";\n" +
            "\n" +
            "@Component({\n" +
            "  selector: 'bz-" + fileName + "-info',\n" +
            "  templateUrl: './" + fileName + "-details.component.html'\n" +
            "})\n" +
            "export class " + className + "DetailsComponent {\n" +
            "\n" +
            "  // =============== start 成员变量 =======================\n" +
            "  // 传递过来的" + tableBean.getPrimaryPropertyKeyColumnComment() + "id\n" +
            "  @Input() " + tableBean.getPrimaryPropertyKey() + ": string;\n" +
            "  // " + tableBean.getPrimaryPropertyKeyColumnComment() + "信息\n" +
            "  " + fileName + ": any;\n" +
            "  // =============== end 成员变量 =========================\n" +
            "\n" +
            "\n" +
            "  // =============== start 构造器和初始化 =================\n" +
            "  constructor(\n" +
            "    private route: ActivatedRoute,\n" +
            "    private router: Router,\n" +
            "    private " + fileName + "Service: " + className + "Service,\n" +
            "    private mainService: MainService,\n" +
            "    private commonService: CommonService\n" +
            "  ){}\n" +
            "  ngOnInit(): void {\n" +
            "    this.mainService.menuItemNext(this.commonService.breadcrumb." + fileName + "Details);\n" +
            "    this.route.paramMap.map(param => param.get(\"" + tableBean.getPrimaryPropertyKey() + "\"))\n" +
            "      .subscribe(" + tableBean.getPrimaryPropertyKey() + " => {\n" +
            "        this." + tableBean.getPrimaryPropertyKey() + " = " + tableBean.getPrimaryPropertyKey() + ";\n" +
            "        this.query" + className + "(" + tableBean.getPrimaryPropertyKey() + ");\n" +
            "      });\n" +
            "  }\n" +
            "  // =============== end 构造器和初始化 ===================\n" +
            "\n" +
            "\n" +
            "  // =============== start 成员方法  ======================\n" +
            "  /**\n" +
            "   * 查询" + tableBean.getPrimaryPropertyKeyColumnComment() + "信息\n" +
            "   * @param " + tableBean.getPrimaryPropertyKey() + "\n" +
            "   */\n" +
            "  query" + className + "(" + tableBean.getPrimaryPropertyKey() + ":string){\n" +
            "    this." + fileName + "Service.query" + className + "(" + tableBean.getPrimaryPropertyKey() + ")\n" +
            "      .then(data => {\n" +
            "        this." + fileName + " = data.data\n" +
            "      })\n" +
            "  }\n" +
            "\n" +
            "  // =============== end 成员方法  ========================\n" +
            "\n" +
            "\n" +
            "}\n");
        return sb.toString();
    }
}
