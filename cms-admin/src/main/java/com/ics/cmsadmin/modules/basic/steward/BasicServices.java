package com.ics.cmsadmin.modules.basic.steward;

import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.student.service.StudentAccountService;
import com.ics.cmsadmin.modules.student.service.StudentService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class BasicServices {
    public static OrderService orderService;

    @Resource
    public void setOrderService(OrderService orderService) {
        BasicServices.orderService = orderService;
    }

}
