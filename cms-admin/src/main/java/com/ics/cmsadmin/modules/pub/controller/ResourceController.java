package com.ics.cmsadmin.modules.pub.controller;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.pub.bean.Resource;
import com.ics.cmsadmin.modules.pub.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

public class ResourceController<RS extends ResourceService, R extends Resource, T> {

    @Autowired
    private RS resourceService;

    public ApiResponse get(Map<String, Object> params) {
        if (params.get("page") != null && params.get("pageSize") != null) {
            PageHelper.startPage((int) params.get("page"), (int) params.get("pageSize"));
        } else {
            List<R> data = resourceService.getMultiple(params);
            long total = data.size();
            return ApiResponse.getDefaultResponse(PageResult.getPage(total, data));
        }
        List<R> data = resourceService.getMultiple(params);
        long total = ((Page) data).getTotal();
        PageResult pageResult = PageResult.getPage(total, data);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @GetMapping("/{id}")
    public ApiResponse gets(@PathVariable T id) {
        return ApiResponse.getDefaultResponse(resourceService.get(id));
    }

    @PostMapping
    public ApiResponse post(@RequestBody R r) {
        int result = resourceService.insert(r);
        if (result != 0) {
            return ApiResponse.getDefaultResponse();
        } else {
            return ApiResponse.getResponse(ApiResultEnum.INSERT_TABLE_ITEM_FAIL);
        }
    }

    @PutMapping
    public ApiResponse put(@RequestBody List<R> rs) {
        return resourceService.updateMultiple(rs);
    }

    @PutMapping("/{id}")
    public ApiResponse put(@PathVariable T id, @RequestBody R r) {
        r.setId(id);
        int result = resourceService.update(r);
        if (result != 0) {
            return ApiResponse.getDefaultResponse();
        } else {
            return ApiResponse.getResponse(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL);
        }
    }

    @DeleteMapping
    public ApiResponse delete(@RequestBody List<Integer> ids) {
        for (Integer id : ids) {
            int effectLine = resourceService.delete(id);
            if (effectLine == 0) {
                throw new CmsException(ApiResultEnum.BULK_DELETE_FAIL);
            }
        }

        return ApiResponse.getDefaultResponse();
    }

    @DeleteMapping("/{id}")
    public ApiResponse delete(@PathVariable T id) {
        int result = resourceService.delete(id);
        if (result != 0) {
            return ApiResponse.getDefaultResponse();
        } else {
            return ApiResponse.getResponse(ApiResultEnum.DELETE_TABLE_ITEM_FAIL);
        }
    }
}
