package com.ics.cmsadmin.modules.basic.utils;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public enum UPushTypeEnum {
    /**
     * 需要以下参数
     * studentName
     */
    NEW_SUBORDINATE("新增下级", "您新增下级伙伴%s！人多力量大，继续推广吧~"){
        @Override
        public UPushTypeBean apply(UPushCustomDefined defined){
            if (defined == null) {
                return super.apply(defined);
            }
            String content = String.format(this.content, defined.getStudentName());
            String details = String.format(this.details, defined.getStudentName());
            return new UPushTypeBean(this.title, content, details);
        }
    },
    /**
     * 需要以下参数
     * studentName
     * money
     */
    PARENT_SHARE("分润收入", "您新增一笔来自下级伙伴%s的分润%s元，前去查看"){
        @Override
        public UPushTypeBean apply(UPushCustomDefined defined){
            if (defined == null) {
                return super.apply(defined);
            }
            String content = String.format(this.content, defined.getStudentName(), defined.getMoney());
            String details = String.format(this.details, defined.getStudentName(), defined.getMoney());
            return new UPushTypeBean(this.title, content, details);
        }
    },
    /**
     * 需要以下参数
     * studentName
     * money
     */
    GRANDFATHER_SHARE("分润收入", "您新增一笔来自下级伙伴%s的分润%s元，前去查看"){
        @Override
        public UPushTypeBean apply(UPushCustomDefined defined){
            if (defined == null) {
                return super.apply(defined);
            }
            String content = String.format(this.content, defined.getStudentName(), defined.getMoney());
            String details = String.format(this.details, defined.getStudentName(), defined.getMoney());
            return new UPushTypeBean(this.title, content, details);
        }
    },
    /**
     * 需要以下参数
     * money,
     * withdrawId
     */
    APPLY_WITHDRAY_SUCCESS("提现成功", "您的%s元分润提现成功！",
        "您的%s元分润提现成功！（提现ID：%s）"){
        @Override
        public UPushTypeBean apply(UPushCustomDefined defined){
            if (defined == null) {
                return super.apply(defined);
            }
            String content = String.format(this.content, defined.getMoney());
            String details = String.format(this.details, defined.getMoney(), defined.getWithdrawId());
            return new UPushTypeBean(this.title, content, details);
        }
    },
    /**
     * 需要以下参数
     * money,
     * withdrawId,
     * remark
     */
    APPLY_WITHDRAY_FAILED("提现失败", "您的%s元分润提现失败，查看原因",
        "您%s元分润提现失败。原因是%s。（提现ID：%s)"){
        @Override
        public UPushTypeBean apply(UPushCustomDefined defined){
            if (defined == null) {
                return super.apply(defined);
            }
            String content = String.format(this.content, defined.getMoney());
            String details = String.format(this.details, defined.getMoney(), defined.getRemark(), defined.getWithdrawId());
            return new UPushTypeBean(this.title, content, details);
        }
    }
    ;

    protected String title;
    protected String content;
    protected String details;

    public UPushTypeBean apply(UPushCustomDefined uPushCustomDefined){
        return new UPushTypeBean(this.title, this.content, this.details);
    }

    UPushTypeEnum(String title, String content) {
        this(title, content, content);
    }

    UPushTypeEnum(String title, String content, String details) {
        this.title = title;
        this.content = content;
        this.details = details;
    }


    public static void main(String[] args) {
        UPushCustomDefined asdfasd = UPushCustomDefined.builder()
            .uPushType(APPLY_WITHDRAY_FAILED)
            .money(BigDecimal.valueOf(25))
            .remark("asdfasd")
            .build();
        asdfasd.getUPushType().apply(asdfasd);
        System.out.println(asdfasd.getUPushType().getContent());
        System.out.println(asdfasd.getUPushType().getDetails());
    }
}
