//package com.ics.cmsadmin.frame.aop;
//
//import com.ics.cmsadmin.frame.core.bean.ApiResponse;
//import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
//import com.ics.cmsadmin.modules.sso.LoginInfo;
//import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
//import com.ics.cmsadmin.utils.HttpUtils;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * Created by Sandwich on 2018-08-13
// */
//@Component
//public class LoginInterceptor implements HandlerInterceptor {
//    /**
//     * 在请求被处理之前调用
//     * @param request
//     * @param response
//     * @param handler
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        LoginInfo loginInfo = SsoUtils.loadLoginInfoFromRedis(request);
//        if (loginInfo == null){
//            HttpUtils.setJsonDataResponse(response, ApiResponse.getResponse(ApiResultEnum.SIGNATURE_VALID_FAILED), 401);
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 在请求被处理后，视图渲染之前调用
//     * @param request
//     * @param response
//     * @param handler
//     * @param modelAndView
//     * @throws Exception
//     */
//    @Override
//    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//
//    }
//
//    /**
//     * 在整个请求结束后调用
//     * @param request
//     * @param response
//     * @param handler
//     * @param ex
//     * @throws Exception
//     */
//    @Override
//    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//
//    }
//
//}
