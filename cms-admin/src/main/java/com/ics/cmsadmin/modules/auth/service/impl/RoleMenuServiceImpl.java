package com.ics.cmsadmin.modules.auth.service.impl;

import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.modules.auth.service.RoleMenuService;
import org.springframework.stereotype.Service;

import static com.ics.cmsadmin.modules.auth.steward.AuthRepositories.roleMenuDao;

/**
 * Created by 666666 on 2018/4/4.
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService {

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public void deleteRoleMenuByMenuId(String menuId) {
        roleMenuDao.deleteRoleMenuByMenuId(menuId);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public void deleteRoleMenuByRoleId(String roleId) {
        roleMenuDao.deleteRoleMenuByRoleId(roleId);
    }
}
