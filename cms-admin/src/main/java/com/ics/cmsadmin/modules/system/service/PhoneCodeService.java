package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.modules.system.bean.PhoneCodeBean;

import java.util.List;

public interface PhoneCodeService {
    /**
     * 查询手机区域吗
     */
    List<PhoneCodeBean> list(String keyword);
}
