package com.ics.cmsadmin.modules.student.bean;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
* t_student_account
* 学生账户
*
* Created by lvsw on 2019-18-11 21:03:47.
*/
@Data
@Builder
public class StudentAccountDetailBean {

    private String id;
    private String accountId;              // 账户id
    private String studentId;              // 学生编号
    private BigDecimal money;
    private String type;
    private BigDecimal totalIncome;              // 总收益
    private BigDecimal withdrawCash;              // 提现金额
    private BigDecimal balance;                     // 余额
    private BigDecimal availableBalance;              // 可用余额
    private BigDecimal frozenBalance;              // 冻结余额
    private String orderNo;
    private String orderStudentId;
    private String withdrawId;              // 提现id
    private String remark;                  // 备注
    private String orderStudentName;

    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    private String startCreateDate;
    private String endCreateDate;
    private String typeName;

    @Tolerate
    public StudentAccountDetailBean() {}
}
