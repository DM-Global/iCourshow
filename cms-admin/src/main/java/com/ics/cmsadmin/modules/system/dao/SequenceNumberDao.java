package com.ics.cmsadmin.modules.system.dao;


import com.ics.cmsadmin.modules.system.bean.SequenceNumberBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * com.kco.dao
 * Created by swlv on 2016/10/25.
 */
@Mapper
public interface SequenceNumberDao {
    /**
     * 根据前缀生成一个序列号信息
     *
     * @param name 主键名 唯一
     * @return 新的序列号信息
     */
    SequenceNumberBean newSequenceNumber(@Param("name") String name);

    /**
     * 将生成的序列号信息更新到数据库中
     *
     * @param sequenceNumberBean 需要更新信息
     */
    void updateSequenceNumber(@Param("bean") SequenceNumberBean sequenceNumberBean);

}
