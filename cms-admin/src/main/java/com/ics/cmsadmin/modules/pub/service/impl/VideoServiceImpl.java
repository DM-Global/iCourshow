package com.ics.cmsadmin.modules.pub.service.impl;

import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.dao.PointDao;
import com.ics.cmsadmin.modules.pub.dao.PointMapper;
import com.ics.cmsadmin.modules.pub.dao.VideoMapper;
import com.ics.cmsadmin.modules.pub.bean.Point;
import com.ics.cmsadmin.modules.pub.bean.Video;
import com.ics.cmsadmin.modules.pub.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

@Service("VideoService")
public class VideoServiceImpl extends ResourceService<VideoMapper, Video, Integer> {
    @Autowired
    private PointDao pointMapper;
    @Autowired
    private VideoMapper videoMapper;

    @Transactional
    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public int insert(Video video) {
        int result = super.insert(video);
        Map<String, Object> param = new HashMap<>();
        param.put("polyVId", video.getPloyVId());
        video = videoMapper.selectAll(param).get(0);
        String pointId = video.getPointId().toString();
        pointMapper.update(pointId, PointBean.builder().id(pointId).pointSource(video.getId().toString()).build());
        return result;
    }

    @Transactional
    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public int update(Video video) {
        int result = super.update(video);
        String pointId = video.getPointId().toString();
        pointMapper.update(pointId, PointBean.builder().id(pointId).pointSource(video.getId().toString()).build());
        return result;
    }
}
