package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.TopicBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.util.List;

/**
 * t_topic 服务类
 * Created by lvsw on 2018-09-01.
 */
public interface TopicService extends BaseDataService<TopicBean>, BaseService<TopicBean> {
    /**
     * 根据单元Id查询所有主题信息
     *
     * @param unitId 单元Id
     */
    List<TopicBean> listAllTopicByUnitId(String unitId);

    long count(TopicBean topicBean);

    TopicBean topicDetail(String userId, String id);

    List<TopicBean> listDetail(String userId, String unitId);

    void updateProblemNum(String topicId, int testNum);
}
