package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * t_package
 * 订单套餐
 * <p>
 * Created by sandwich on 2018-18-14 21:12:28.
 */
@Data
@Builder
public class PackageBean {

    private String id;              // id
    private String title;              // 标题
    private String iosPurchId;              // ios内购产品id
    private String secondTitle;              // 标题
    private String tag;              // 标签
    private String description;              // 标题
    private BigDecimal price;              // 价格
    private Integer monthCount;              // 月数
    private Boolean isActive;              // 状态
    private Boolean isDeleted;              // 是否删除
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间

    @Tolerate
    public PackageBean() {
    }
}
