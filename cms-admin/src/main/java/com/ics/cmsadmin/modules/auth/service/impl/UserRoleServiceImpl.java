package com.ics.cmsadmin.modules.auth.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import com.ics.cmsadmin.modules.auth.dao.RoleDao;
import com.ics.cmsadmin.modules.auth.dao.UserRoleDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.auth.service.UserRoleService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.modules.auth.steward.AuthRepositories.roleDao;
import static com.ics.cmsadmin.modules.auth.steward.AuthRepositories.userRoleDao;

/**
 * Created by 666666 on 2018/4/4.
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @CacheDbMember(returnClass = Integer.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public int countUserByRoleId(String roleId) {
        return userRoleDao.countUserByRoleId(new SysUser(), roleId);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public void deleteUserRoleByRoleId(String roleId) {
        userRoleDao.deleteUserRoleByRoleId(roleId);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean addRoles2User(List<String> roleIds, String userId) {
        if (CollectionUtils.isEmpty(roleIds) || StringUtils.isBlank(userId)) {
            return false;
        }
        Set<String> tempRoleIds = new HashSet<>(roleIds);
        List<String> nonExistRole = tempRoleIds.stream()
            .filter(roleId -> roleDao.queryById(roleId) == null)
            .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(nonExistRole)) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "新增的角色含有不存在的角色: " + nonExistRole);
        }
        userRoleDao.addRoles2User(tempRoleIds, userId);
        return true;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean removeRoles2User(List<String> roleIds, String userId) {
        userRoleDao.removeRoles2User(roleIds, userId);
        return true;
    }

    @CacheDbMember(returnClass = SysUser.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public PageResult<SysUser> listUserByRoleId(String roleId, SysUser userBean, PageBean page) {
        long count = userRoleDao.countUserByRoleId(userBean, roleId);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return new PageResult(userRoleDao.listUserByRoleId(userBean, roleId, page), count);
    }

    @Override
    public PageResult<SysUser> listUserWithoutRoleId(String roleId, SysUser userBean, PageBean page) {
        long count = userRoleDao.countUserWithoutRoleId(userBean, roleId);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return new PageResult(userRoleDao.listUserWithoutRoleId(userBean, roleId, page), count);
    }
}
