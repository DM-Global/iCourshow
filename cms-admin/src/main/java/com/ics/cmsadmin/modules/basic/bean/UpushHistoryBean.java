package com.ics.cmsadmin.modules.basic.bean;

                                            
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
* t_upush_history
* 
*
* Created by Sandwich on 2019-15-13 22:06:38.
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UpushHistoryBean {

    private String id;                  // id
    private String studentId;           // 单播给指定用户时保留用户id
    private String type;                // 推送类型
    private String title;               // 标题
    private String content;              // 内容
    private String details;              // 详情
    private String extra;                // 自定义字符串
        @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
        @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间
    private Boolean isRead;           //是否已读

}
