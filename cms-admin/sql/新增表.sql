CREATE TABLE `t_template` (
`id` varchar(50) NOT NULL COMMENT 'id' ,
`content`  text NOT NULL COMMENT '模版内容' ,
`title`  varchar(255) NOT NULL COMMENT '标题' ,
`type`  tinyint(1) NULL DEFAULT 1 COMMENT '1.邮件模版 2.公众号模版' ,
`description`  varchar(255) NULL COMMENT '描述' ,
`create_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间' ,
`update_time`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间' ,
PRIMARY KEY (`id`)
)
;
