package com.ics.cmsadmin.student;

import com.ics.cmsadmin.frame.utils.KeyUtil;
import lombok.Data;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-03-20 16:41
 */
@Data
public class OrderBean extends SqlBean{
    private String id;
    private String userId;
    private String orderType;
    private BigDecimal coursePrice;
    private BigDecimal orderAmount;
    private String paymentMethod;
    private String status;
    private String accStatus = "0";
    private Date createTime;
    private Date payTime;
    private Date updateTime;
    private Date startDate;
    private Date endDate;

    public static OrderBean newOrder(String studentId, Date date, Date startDate) {
        OrderBean orderBean = new OrderBean();
        orderBean.setId(KeyUtil.genUniqueKey());
        orderBean.setUserId(studentId);
        orderBean.setOrderType(RandomStringUtils.random(1, "12"));
        BigDecimal amount = new BigDecimal(RandomUtils.nextInt(Contants.MIN_ORDER_MONEY, Contants.MAX_ORDER_MONEY) * 1000);
        orderBean.setOrderAmount(amount.setScale(2, RoundingMode.CEILING));
        orderBean.setCoursePrice(amount.setScale(2, RoundingMode.CEILING));
        orderBean.setPaymentMethod(RandomStringUtils.random(1, "123456"));
        orderBean.setStatus("2");
        orderBean.setAccStatus("0");
        orderBean.setCreateTime(date);
        orderBean.setPayTime(date);
        orderBean.setUpdateTime(date);
        if (startDate == null || startDate.before(date)) {
            startDate = date;
        }
        orderBean.setStartDate(startDate);
        orderBean.setEndDate(DateUtils.addMonths(startDate, 1));
        return orderBean;
    }
    @Override
    protected String tableName() {
        return "t_order";
    }
}
