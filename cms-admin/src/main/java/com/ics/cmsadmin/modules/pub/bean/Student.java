package com.ics.cmsadmin.modules.pub.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongSerializer;
import lombok.Data;

import java.util.Date;

@Data
public class Student extends Resource<String> {

    private String token;
    private String username;

    private String realName;

    private String avatar;

    private String phone;

    @JsonSerialize(using = Date2LongSerializer.class)
    private Date birthday;

    private String gender;

    private String unionId;

    private String openId;

    private String nickname;

    private String password;

    private Boolean isActive;

    @JsonSerialize(using = Date2LongSerializer.class)
    private Date createTime;
}
