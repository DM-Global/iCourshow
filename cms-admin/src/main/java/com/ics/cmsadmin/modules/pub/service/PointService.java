package com.ics.cmsadmin.modules.pub.service;

import com.github.pagehelper.Page;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.basic.dao.CollectionDao;
import com.ics.cmsadmin.modules.basic.dao.LikeDao;
import com.ics.cmsadmin.modules.pub.bean.Point;
import com.ics.cmsadmin.modules.pub.bean.PointVO;
import com.ics.cmsadmin.modules.pub.bean.Score;
import com.ics.cmsadmin.modules.pub.bean.Topic;
import com.ics.cmsadmin.modules.pub.dao.PointMapper;
import com.ics.cmsadmin.modules.pub.dao.ScoreMapper;
import com.ics.cmsadmin.modules.pub.dao.TopicMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("PointService")
public class PointService extends ResourceService<PointMapper, Point, Integer> {

    @Autowired
    private ScoreMapper scoreMapper;
    @Autowired
    private TopicMapper topicMapper;
    @Autowired
    private CollectionDao collectionDao;

    @Autowired
    private LikeDao likeDao;

    @Autowired
    private OrderService orderService;

    public PointVO get(Integer id, String userId) {
        Point point = super.get(id);
        return buildPoint(point, userId);
    }

    public List<PointVO> getMultiple(Map<String, Object> param, String userId) {
        List<Point> pointList = super.getMultiple(param);
        Page<PointVO> page = new Page<>();
        page.addAll(pointList.stream().map(unit -> buildPoint(unit, userId)).collect(Collectors.toList()));
        if (pointList instanceof Page) {
            page.setTotal(((Page) pointList).getTotal());
        } else {
            page.setTotal(pointList.size());
        }
        return page;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public int update(Point point) {
        return super.update(point);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public int insert(Point point) {
        return super.insert(point);
    }

    private PointVO buildPoint(Point point, String userId) {
        if (point == null) {
            throw new CmsException(ApiResultEnum.POINT_NOT_EXIST);
        }
        PointVO pointVO = new PointVO();
        BeanUtils.copyProperties(point, pointVO);
        pointVO.setId(point.getId());
        if (point.getPointType().equals("exercise")) {
            Map<String, Object> param = new HashMap<>();
            param.put("studentId", userId);
            param.put("practiceId", point.getPointSource());
            List<Score> scores = scoreMapper.selectAll(param);
            if (scores != null && !scores.isEmpty())
                pointVO.setScore(scores.get(0).getScore());
        }
        Topic topic = topicMapper.select(point.getTopicId());
        if (topic != null) {
            pointVO.setIsPublic(topic.getIsPublic());
        }
        pointVO.setCollected(collectionDao.select(userId, LevelEnum.point, String.valueOf(point.getId())));
        pointVO.setCollectCount(collectionDao.queryTotal(LevelEnum.point, String.valueOf(point.getId())));
        pointVO.setLiked(likeDao.select(userId, LevelEnum.point, point.getId()));
        pointVO.setThumbUpCount(likeDao.queryTotal(LevelEnum.point, String.valueOf(point.getId())));
        pointVO.setHasBought(orderService.getPurchaseStatus(userId, point.getCourseId()));
        return pointVO;
    }
}
