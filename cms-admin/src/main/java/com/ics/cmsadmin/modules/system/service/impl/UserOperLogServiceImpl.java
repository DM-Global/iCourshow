package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.system.bean.UserOperLogBean;
import com.ics.cmsadmin.modules.system.service.UserOperLogService;
import org.springframework.stereotype.Service;

import static com.ics.cmsadmin.modules.system.steward.SystemRepositories.userOperLogDao;

/**
 * com_user_oper_log
 * Created bylvsw on 2018-41-11 15:10:21.
 */
@Service
public class UserOperLogServiceImpl implements UserOperLogService {

    @Override
    public PageResult<UserOperLogBean> list(UserOperLogBean bean, PageBean page) {
        long count = userOperLogDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, userOperLogDao.list(bean, page));
    }

    @Override
    public boolean insert(UserOperLogBean bean) {
        if (bean == null) {
            return false;
        }
        return userOperLogDao.insert(bean) == 1;
    }

}
