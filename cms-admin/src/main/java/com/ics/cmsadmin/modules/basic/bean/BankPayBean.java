package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by Sandwich on 2019/6/7
 */
@Data
@Builder
public class BankPayBean {

    String bankCode;
    String bankCardNo;
    String bankCardName;
    BigDecimal amount;

}
