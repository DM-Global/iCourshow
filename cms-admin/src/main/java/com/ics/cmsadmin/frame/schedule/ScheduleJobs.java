package com.ics.cmsadmin.frame.schedule;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sandwich on 2019/1/4
 */

@Log4j2
@Component
public class ScheduleJobs {

    @Autowired
    private OrderService orderService;

    /**
     * 每10分钟执行一次自动任务
     */
    @Scheduled(cron = "0 0/10 * * * ?")
    public void cronJob() {
        log.info(" ~~~ >>订单扫描任务开始执行 ~~~ ");
        List<OrderBean> onStudyList = orderService.list(OrderBean.builder().status(CommonEnums.PayStatus.PAID.getCode())
            .isDeleted(false).orderType(CommonEnums.OrderType.PACKAGE.getCode()).studyStatus(CommonEnums.StudyStatus.STARTED.getCode())
            .build(), new PageBean(1, Integer.MAX_VALUE)).getDataList();
        Date currentDate = new Date();
        if (!CollectionUtils.isEmpty(onStudyList)) {
            onStudyList.forEach(orderBean -> {
                Date endDate = orderBean.getEndDate();
                int compareResult = currentDate.compareTo(endDate);
                log.info("compare result:{}", compareResult);
                if (compareResult > 0) {
                    orderBean.setStudyStatus(CommonEnums.StudyStatus.EXPIRED.getCode());
                    orderService.update(orderBean);
                    log.info("The order is expired：{} ", orderBean.getId());
                }
            });
        }
        List<OrderBean> notStudyList = orderService.list(OrderBean.builder().status(CommonEnums.PayStatus.PAID.getCode())
            .isDeleted(false).orderType(CommonEnums.OrderType.PACKAGE.getCode()).studyStatus(CommonEnums.StudyStatus.NOT_STARTED.getCode())
            .build(), new PageBean(1, Integer.MAX_VALUE)).getDataList();
        if (!CollectionUtils.isEmpty(notStudyList)) {
            notStudyList.forEach(orderBean -> {
                Date startDate = orderBean.getStartDate();
                int compareResult = currentDate.compareTo(startDate);
                if (compareResult > 0) {
                    orderBean.setStudyStatus(CommonEnums.StudyStatus.STARTED.getCode());
                    orderService.update(orderBean);
                    log.info("The order is started：{} ", orderBean.getId());
                }
            });
        }
        log.info(" ~~~ >>订单扫描任务执行完成 ~~~ ");
    }

}

