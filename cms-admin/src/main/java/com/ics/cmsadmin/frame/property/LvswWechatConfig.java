//package com.ics.cmsadmin.frame.property;
//
//import lombok.Data;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Component;
//
//import java.util.Map;
//
///**
// * Created by Sandwich on 2018-08-10
// */
//@Data
//@Component
//@ConfigurationProperties(prefix = "lvswwechat")
//public class LvswWechatConfig {
//    private String appId;
//    private String secret;
//    private String token;
//}
