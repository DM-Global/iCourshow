package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.system.bean.MetaBean;
import com.ics.cmsadmin.modules.system.service.MetaService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.modules.system.steward.SystemRepositories.metaDao;

/**
 * Created by lvsw on 2018-03-29.
 */
@Service
public class MetaServiceImpl implements MetaService {

    @Override
    public MetaBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return metaDao.queryById(id);
    }

    @Override
    public PageResult<MetaBean> list(MetaBean bean, PageBean page) {
        long count = metaDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, metaDao.list(bean, page));
    }

    @Override
    public boolean insert(MetaBean bean) {
        if (bean == null) {
            return false;
        }
        if (StringUtils.isBlank(bean.getParentId()) || StringUtils.equals(DEFAULT_ROOT_ID, bean.getParentId())) {
            bean.setParentId(DEFAULT_ROOT_ID);
        } else {
            MetaBean parentMenuInfo = queryById(bean.getParentId());
            if (parentMenuInfo == null) {
                throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "父节点没有找到");
            }
            bean.setMetaId(bean.getParentId() + DEFAULT_SEPARATOR + bean.getMetaId());
        }
        MetaBean metaBean = metaDao.queryById(bean.getMetaId());
        if (metaBean != null) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_REPEAT_PRIMARY, "该Meta id已经存在");
        }
        return metaDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, MetaBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return metaDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        List<MetaBean> metaBeans = listDirectChildren(id);
        if (CollectionUtils.isNotEmpty(metaBeans)) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "该meta存在子节点,不能删除");
        }
        return metaDao.delete(id) == 1;
    }

    @Override
    public List<MetaBean> listDirectChildren(String parentNode) {
        return metaDao.listDirectChildren(parentNode);
    }

    @Override
    public Map<String, String> listDirectChildren2Map(String parentNode) {
        return Optional.ofNullable(this.listDirectChildren(parentNode))
            .orElse(new ArrayList<>())
            .stream()
            .collect(Collectors.toMap(MetaBean::getValue, MetaBean::getName));
    }

    @Override
    public MetaBean queryParent(String childrenId) {
        MetaBean metaBean = queryById(childrenId);
        if (metaBean == null) {
            return null;
        }
        return queryById(metaBean.getParentId());
    }

}
