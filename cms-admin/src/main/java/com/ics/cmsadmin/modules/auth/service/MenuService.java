package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.TreeDataService;
import com.ics.cmsadmin.modules.auth.bean.SysMenu;

import java.util.List;

/**
 * a_menu 服务类
 * Created by lvsw on 2018-03-29.
 */
public interface MenuService extends BaseDataService<SysMenu>, TreeDataService<SysMenu>, BaseService<SysMenu> {

    /**
     * 根据登陆用户获取菜单
     *
     * @param loginUserId 登陆用户
     * @return
     */
    List<SysMenu> listMenuTreeByUserId(String loginUserId);

    /**
     * 获取所有的菜单
     *
     * @return
     */
    List<SysMenu> listAllChildren();
}
