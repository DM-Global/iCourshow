package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.TopicBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-09-01.
 */
@Repository
@Mapper
public interface TopicDao extends BaseDao<TopicBean>, BaseDataDao<TopicBean> {
    void updateProblemNum(@Param("id") String topicId, @Param("testNum") int testNum);
}
