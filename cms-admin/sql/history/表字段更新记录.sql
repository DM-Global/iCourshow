
# 2018-10-11
ALTER TABLE `t_point` ADD `readable` TINYINT(1)  NULL  DEFAULT '1'  COMMENT '在公众号是否可以查看详情，1:是 0:否'  AFTER `is_active`;

# 2018-10-06
ALTER TABLE `t_course`
ADD COLUMN `course_slogan`  varchar(30) NULL AFTER `is_completed`;

# 2018-10-12
ALTER TABLE `t_course`
MODIFY COLUMN `is_completed`  tinyint(1) NULL DEFAULT 0 AFTER `long_poster_id`;

# 2018-10-13
ALTER TABLE `t_student`
ADD `special_vip` TINYINT(1)  NULL  DEFAULT '0'  COMMENT '是否为特殊vip用户，1:是 0:否'  AFTER `is_active`;

ALTER TABLE `t_order`
ADD `pay_time` timestamp  NULL  DEFAULT NULL  COMMENT '支付时间'  AFTER `create_time`;

# 2018-10-31
ALTER TABLE t_subject
ADD icon_id int(32) NULL COMMENT '图表id' AFTER from_subject_id;
ALTER TABLE t_subject
ADD cover_id int(32) NULL COMMENT '封面图片id' AFTER icon_id;
ALTER TABLE t_picture
MODIFY pic_type tinyint(3) COMMENT '图标类型: 1. 课程封面 2. 单元封面 3.老师头像 4. 问题图片 5. 走马灯图片 6.课程介绍图文 7.课程海报 8.知识点封面 9. 单元图标 10.科目封面 11.科目图标 ';
