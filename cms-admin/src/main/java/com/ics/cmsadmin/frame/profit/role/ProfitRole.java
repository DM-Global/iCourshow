package com.ics.cmsadmin.frame.profit.role;

import com.ics.cmsadmin.frame.profit.ProfitInfo;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/4/19.
 */
public interface ProfitRole {
    /**
     * 计算分润
     *
     * @param orderMoney 订单金额
     * @param limitValue 分界值
     * @return 分润
     */
    ProfitInfo getProfitInfo(BigDecimal orderMoney, BigDecimal limitValue);

    Object getData();
}
