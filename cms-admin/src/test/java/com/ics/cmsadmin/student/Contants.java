package com.ics.cmsadmin.student;

public class Contants {
    public static int minutes = 240;
    public static int minutesIndex = 5;

    public static long MIN_NEW_STUDENT = 1L;
    public static long MAX_NEW_STUDENT = 3L;

    public static long MIN_PROMOTION_STUDENT = 1L;
    public static long MAX_PROMOTION_STUDENT = 5L;

    public static long MIN_NEW_ORDER_STUDENY = 1L;
    public static long MAX_NEW_ORDER_STUDENY = 10L;

    public static long MIN_NEW_ORDER = 1L;
    public static long MAX_NEW_ORDER = 2L;

    public static int MIN_ORDER_MONEY = 5;
    public static int MAX_ORDER_MONEY = 30;


}
