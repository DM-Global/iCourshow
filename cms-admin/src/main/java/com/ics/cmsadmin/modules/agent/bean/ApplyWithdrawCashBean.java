package com.ics.cmsadmin.modules.agent.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class ApplyWithdrawCashBean extends LoginSearchBean {

    private String id;
    private String agentNo;
    private BigDecimal withdrawCash;
    private String status;
    private String remark;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    private String startUpdateTime;

    @Tolerate
    public ApplyWithdrawCashBean() {
    }
}
