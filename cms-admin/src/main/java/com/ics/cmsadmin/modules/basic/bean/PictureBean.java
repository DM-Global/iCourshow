package com.ics.cmsadmin.modules.basic.bean;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PictureBean {

    private Long id;    // 图片id

    private String picUrl;  // 图片url

    private String description;  // 图片描述

    private Integer picType;   //  图片类型
}
