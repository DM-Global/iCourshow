package com.ics.cmsadmin.modules.student.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Data
@Builder
public class StudentBean extends LoginInfo {

    private String id;              // 用户id
    private String showId;          // 展示id
    private String parentId;        // 上级id, 推荐者id
    private String username;        // 用户名
    private String realName;        // 真实名字
    private String avatar;          // 头像
    private String phone;           // 手机号
    private String phoneCode;           // 手机号区域码
    private String birthday;        // 生日
    private BigDecimal shareRate;
    private String gender;          // 性别
    private String openId;          // 用户微信公众号openid
    private String appOpenId;       // 用户app微信openid
    private String accessToken;
    private String unionId;         // 微信unionid
    private String registerSource;         // 注册来源 1 公众号, 2 app
    private String nickname;        // 昵称,默认时微信昵称
    private String wechatAccount;   // 微信账号
    private String email;           // 邮箱地址
    private String password;        // 登陆密码
    private String province;        // 省份编码
    private String city;            // 城市编码
    private String county;          // 县编码
    private String cityName;        // 省市区地址regist_source
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;         // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;        // 修改时间
    private Boolean isActive;       // 账号状态 1
    private Boolean isFollowed;     //是否关注
    private Boolean specialVip;     // 特殊vip用户 0
    private String agentNo;         // 分销商编号
    private boolean hasPassword;    // 是否有密码

    private String startCreateTime;
    private String endCreateTime;
    private String salesman;
    private String deviceId;
    private String uMengDeviceToken;
    private String androidDeviceToken;
    private String iosDeviceToken;
    private String shareUrl;

    public void setId(String id) {
        setLoginId(id);
        this.id = id;
        try {
            BigInteger bigInteger = new BigInteger(id, 16);
            this.showId = bigInteger.toString(Character.MAX_RADIX);
        } catch (Exception e) {
            this.showId = id;
        }
    }

    public void setNickname(String nickname) {
        setLoginUserName(nickname);
        this.nickname = nickname;
    }

    @Tolerate
    public StudentBean() {
    }

}
