package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.Like;
import com.ics.cmsadmin.modules.basic.dao.LikeDao;
import com.ics.cmsadmin.modules.basic.service.LikeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.jsf.FacesContextUtils;

@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    private LikeDao likeDao;

    @Override
    public int insert(Like like) {
        return likeDao.insert(like);
    }

    @Override
    public int delete(Like like) {
        return likeDao.delete(like);
    }

    @Override
    public Boolean select(Like like) {
        return likeDao.select(like);
    }

    @Override
    public Integer queryTotal(LevelEnum levelName, String levelId) {
        return likeDao.queryTotal(levelName, levelId);
    }

    @Override
    public boolean toggleLike(Like like) {
        if (like == null ||
            like.getLevelName() == null ||
            StringUtils.isBlank(like.getUserId()) ||
            StringUtils.isBlank(like.getLevelId())
        ) {
            return false;
        }
        Boolean collected = likeDao.select(like);
        if (collected) {
            return likeDao.delete(like) > 0;
        } else {
            return likeDao.insert(like) > 0;
        }
    }
}
