package com.ics.cmsadmin.modules.system.steward;

import com.ics.cmsadmin.modules.system.service.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class SystemServices {
    public static CityService cityService;
    public static MetaService metaService;
    public static RegisterService registerService;
    public static SequenceNumberService sequenceNumberService;
    public static UploadService uploadService;
    public static UserOperLogService userOperLogService;
    public static AppVersionService appVersionService;
    public static PhoneCodeService phoneCodeService;
    public static AppFeedbackService appFeedbackService;
    public static SmsService smsService;

    @Resource
    public void setSmsService(SmsService smsService) {
        SystemServices.smsService = smsService;
    }

    @Resource
    public void setAppFeedbackService(AppFeedbackService appFeedbackService) {
        SystemServices.appFeedbackService = appFeedbackService;
    }

    @Resource
    public void setPhoneCodeService(PhoneCodeService phoneCodeService) {
        SystemServices.phoneCodeService = phoneCodeService;
    }


    @Resource
    public void setCityService(CityService cityService) {
        SystemServices.cityService = cityService;
    }

    @Resource
    public void setMetaService(MetaService metaService) {
        SystemServices.metaService = metaService;
    }

    @Resource
    public void setRegisterService(RegisterService registerService) {
        SystemServices.registerService = registerService;
    }

    @Resource
    public void setSequenceNumberService(SequenceNumberService sequenceNumberService) {
        SystemServices.sequenceNumberService = sequenceNumberService;
    }

    @Resource
    public void setUploadService(UploadService uploadService) {
        SystemServices.uploadService = uploadService;
    }

    @Resource
    public void setUserOperLogService(UserOperLogService userOperLogService) {
        SystemServices.userOperLogService = userOperLogService;
    }

    @Resource
    public void setAppVersionService(AppVersionService appVersionService) {
        SystemServices.appVersionService = appVersionService;
    }
}
