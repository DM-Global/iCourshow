package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * q_tips
 * 提示
 * Created by lvsw on 2018-09-06.
 */
@Data
@Builder
public class TipsBean {

    // 提示id
    private String tipsId;
    // 问题id
    private String problemId;
    private String type;
    private String pointId;
    private String levelNode;
    private String pointType;
    private String pointName;
    // 知识点对应的视频时长
    private String videoDuration;
    private String pointCoverUrl;
    // 描述
    private String content;
    private String orderNo;
    // 创建时间
    private Date createTime;
    // 更新时间
    private Date updateTime;



    @Tolerate
    public TipsBean() {
    }
}
