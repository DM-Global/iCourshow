package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.app.bean.AppInfoBean;
import com.ics.cmsadmin.modules.app.utils.AppContants;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.basic.bean.*;
import com.ics.cmsadmin.modules.basic.service.*;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.APP_HOME_GET_COURSES;

@AppLoginValid(needLogin = false)
@Api(description = "app主页接口", tags = "首页相关接口")
@RestController
@RequestMapping("/app/home")
public class AppHomeController {

    @Resource
    private CourseService courseService;
    @Resource
    private CourseAttributeService courseAttributeService;
    @Resource
    private TeacherService teacherService;
    @Resource
    private PromotionService promotionService;
    @Resource
    private SubjectService subjectService;
    @Resource
    private CollectionService collectionService;
    @Resource
    private VideoService videoService;
    @Resource
    private TemplateService templateService;
    @Resource
    private SharePosterService sharePosterService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private LikeService likeService;
    @Autowired
    private StudentService studentService;
    @Resource
    private RegisterService registerService;


    @ApiOperation(value = "获取随便看看开关")
    @GetMapping("/getLookAroundSwitch")
    public ApiResponse getLookAroundSwitch() {
        Map<String, Object> result = new HashMap<>();
        result.put("lookAroundSwitch", SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.LOOK_AROUND_SWITCH, Boolean::valueOf));
        return new ApiResponse(result);
    }

    @ApiOperation(value = "获取登陆页面相关信息")
    @GetMapping("/queryLoginPageInfo")
    public ApiResponse queryLoginPageInfo(HttpServletRequest request) {
        String trialOrderDateCount = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.TRIAL_ORDER_DATE_COUNT, Function.identity());
        Map<String, Object> result = new HashMap<>();
        result.put("trialOrderDateCount", trialOrderDateCount);
//        result.put("trialOrderTip", String.format("首次登录赠送%s天学籍", trialOrderDateCount));
        result.put("trialOrderTip", "");
        result.put("lookAroundSwitch", SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.LOOK_AROUND_SWITCH, Boolean::valueOf));
        AppInfoBean appInfo = SsoUtils.getAppInfo(request);
        if (appInfo == null) {
            result.put("visitorLanding", false);
        } else if (StringUtils.equalsIgnoreCase(appInfo.getPlatform(), AppContants.PLATFORM_IOS)) {
            String appVersion = appInfo.getAppVersion();
            String iosVersion = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.VISITOR_LANDING_IOS_VERSION, Function.identity());
            result.put("visitorLanding", StringUtils.equalsIgnoreCase(appVersion, iosVersion));
        } else {
            result.put("visitorLanding", false);
        }
        return new ApiResponse(result);
    }

    @ApiOperation(value = "获取订单海报接口", notes = "首页的主页用一个接口获取完要用的所有数据")
    @GetMapping(value = "/getEntranceImage")
    public ApiResponse getEntranceImage(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        Map<String, String> collectionMap = new HashMap<>();
//        String orderPosterImage = "";
//        if (StringUtils.isBlank(loginUserId)) {
//            orderPosterImage = registerService.queryRegisterValue(RegisterKeyValueEnum.UNLOGIN_ENTRANCE, Function.identity());
//        }else if (orderService.hasTrialOrder(loginUserId)) {
//            orderPosterImage = registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ENTRANCE, Function.identity());
//        }else {
//            orderPosterImage = registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_OLD_USER_ENTRANCE, Function.identity());
//        }
//        collectionMap.put("orderPosterImage", orderPosterImage);
        collectionMap.put("orderPosterImage", registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ENTRANCE, Function.identity()));
        return new ApiResponse(collectionMap);
    }

    @ApiOperation(value = "获取首页的所有数据", notes = "首页的主页用一个接口获取完要用的所有数据")
    @GetMapping(value = "/collections")
    public ApiResponse getCollections(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        Integer homeSubjectModuleMaxCount = registerService.queryRegisterValue(RegisterKeyValueEnum.HOME_SUBJECT_MODULE_MAX_COUNT, Integer::new);
        Integer homeCoursePerSubjectMaxCount = registerService.queryRegisterValue(RegisterKeyValueEnum.HOME_COURSE_PER_SUBJECT_MAX_COUNT, Integer::new);
        Integer homeTeacherMaxCount = registerService.queryRegisterValue(RegisterKeyValueEnum.HOME_TEACHER_MAX_COUNT, Integer::new);
        Integer homePromotionMaxCount = registerService.queryRegisterValue(RegisterKeyValueEnum.HOME_PROMOTION_MAX_COUNT, Integer::new);
        PageResult<CourseAttributeBean> courseTypeBeanPageResult = courseAttributeService.list(CourseAttributeBean.builder().isActive(Boolean.TRUE).build(),
            new PageBean(1, homeSubjectModuleMaxCount));
        List<CourseAttributeBean> courseAttributeBeans = courseTypeBeanPageResult.getDataList();
        Map<String, Object> collectionMap = new HashMap<>();
        List<Map<String, Object>> courseList = new ArrayList<>();
        for (CourseAttributeBean courseAttributeBean : courseAttributeBeans) {
            Map<String, Object> map = new HashMap<>();
            PageResult<CourseBean> courseBeanPageResult = courseService.list(CourseBean.builder().isActive(true).courseTypeId(courseAttributeBean.getId()).build(),
                new PageBean(1, homeCoursePerSubjectMaxCount));
            List<CourseBean> courseBeanList = courseBeanPageResult.getDataList();
            map.put("type", courseAttributeBean);
            map.put("list", courseBeanList);
            courseList.add(map);
        }
        collectionMap.put("courses", courseList);
//        PageResult subjectBeanPageResult = subjectService.list(SubjectBean.builder().isActive(Boolean.TRUE).fromSubjectId("0").build(),
//            new PageBean(1, maxHomeListCount));
//        List<SubjectBean> subjectBeanList = subjectBeanPageResult.getDataList();
//        collectionMap.put("subjects", subjectBeanList);
        collectionMap.put("subjects", new ArrayList<>());
        PageResult teacherBeanPageResult = teacherService.list(TeacherBean.builder().isActive(true).build(),
            new PageBean(1, homeTeacherMaxCount));
        collectionMap.put("teachers", teacherBeanPageResult.getDataList());
        PageResult promotionBeanPageResult = promotionService.list(PromotionBean.builder().isActive(Boolean.TRUE).build(),
            new PageBean(1, homePromotionMaxCount));
        List<PromotionBean> promotions = promotionBeanPageResult.getDataList();
        collectionMap.put("promotions", promotions);
//        String orderPosterImage = "";
//        if (StringUtils.isBlank(loginUserId)) {
//            orderPosterImage = registerService.queryRegisterValue(RegisterKeyValueEnum.UNLOGIN_ENTRANCE, Function.identity());
//        }else if (orderService.hasTrialOrder(loginUserId)) {
//            orderPosterImage = registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ENTRANCE, Function.identity());
//        }else {
//            orderPosterImage = registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_OLD_USER_ENTRANCE, Function.identity());
//        }
//        collectionMap.put("orderPosterImage", orderPosterImage);
        collectionMap.put("orderPosterImage", registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ENTRANCE, Function.identity()));
        return new ApiResponse(collectionMap);
    }

    @ApiOperation(value = "分页获取所有课程", notes = APP_HOME_GET_COURSES)
    @PostMapping(value = "/courses/{pageNo}/{pageSize}")
    public ApiResponse getCourses(@RequestBody CourseBean courseBean,
                                  @ApiParam("页码") @PathVariable Integer pageNo,
                                  @ApiParam("每页条数") @PathVariable Integer pageSize,
                                  HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        courseBean.setIsActive(true);
        PageResult result = courseService.list(courseBean, new PageBean(pageNo, pageSize));
        List<CourseBean> courseList = result.getDataList();
        if (CollectionUtils.isNotEmpty(courseList)) {
            if (StringUtils.isBlank(loginUserId)) {
                courseList.forEach(item -> item.setCollected(false));
            } else {
                List<String> courseIds = courseList.stream()
                    .map(CourseBean::getId)
                    .collect(Collectors.toList());
                List<String> collectList = collectionService.list(loginUserId, LevelEnum.course, courseIds);
                if (CollectionUtils.isNotEmpty(collectList)) {
                    courseList.forEach(item -> item.setCollected(collectList.contains(item.getId())));
                }
            }
        }
        return new ApiResponse(result);
    }

    @ApiOperation(value = "查询课程完整详细信息", notes = "单元列表通过hasTestSet判断是否有单元测试")
    @GetMapping("/coursesDetail/{id}")
    public ApiResponse coursesDetail(@PathVariable(value = "id") String id,
                                     HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        CourseBean courseBean = courseService.courseAllDetail(id);
        courseBean = courseService.courseAllDetailAdditional(userId, courseBean);
        return new ApiResponse(courseBean);
    }

    @ApiOperation(value = "根据知识点id获取video")
    @GetMapping("/getVideoByPointId/{pointId}")
    public ApiResponse getVideoByPointId(@PathVariable String pointId,
                                         HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(videoService.getVideoByPointId(userId, pointId));
    }


    @ApiOperation(value = "分页获取老师")
    @PostMapping(value = "/teachers/{pageNo}/{pageSize}")
    public ApiResponse getTeachers(@RequestBody TeacherBean teacherBean,
                                   @ApiParam("页码") @PathVariable Integer pageNo,
                                   @ApiParam("每页条数") @PathVariable Integer pageSize) throws IOException {

        teacherBean.setIsActive(true);
        PageResult teacherBeanPageResult = teacherService.list(teacherBean, new PageBean(pageNo, pageSize));
        List<TeacherBean> teacherBeanList = teacherBeanPageResult.getDataList();
        return new ApiResponse(teacherBeanList);
    }

    @ApiOperation(value = "查询教师详细信息")
    @GetMapping(value = "/queryTeacherDetail/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(teacherService.teacherDetail(id));
    }

    @ApiOperation(value = "查询课程详细信息")
    @GetMapping("/queryCourseDetail/{id}")
    public ApiResponse queryCourseDetail(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        CourseBean courseBean = courseService.courseDetail(id);
        courseBean = courseService.courseDetailAdditional(userId, courseBean);
        if (courseBean != null) {
            if (StringUtils.isBlank(userId)) {
                courseBean.setHasBought(false);
                courseBean.setLiked(false);
                courseBean.setCollected(false);
            } else {
                courseBean.setHasBought(orderService.getPurchaseStatus(userId, id));
                courseBean.setLiked(likeService.select(new Like(userId, LevelEnum.course, id)));
                courseBean.setCollected(collectionService.select(new Collect(userId, LevelEnum.course, id)));
                /* special vip start **/
                StudentBean studentBean = studentService.queryById(userId);
                if (studentBean != null) {
                    Boolean specialVip = studentBean.getSpecialVip();
                    if (specialVip != null && studentBean.getSpecialVip()) {
                        courseBean.setPrice(new BigDecimal(1.0));
                        courseBean.setDiscountPrice(new BigDecimal(0));
                    }
                }
                /* special vip end **/
            }
        }
        return new ApiResponse(courseBean);
    }

    @ApiOperation(value = "查看用户使用协议")
    @GetMapping(value = "/queryUserUsageProtocol", produces = MediaType.TEXT_HTML_VALUE)
    public String queryUserUsageProtocol() {
        TemplateBean templateBean = templateService.queryById(AppContants.USER_USAGE_PROTOCOL);
        return templateBean == null ? AppContants.HAS_NOT_USER_USAGE_PROTOCOL_TIP : templateBean.getContent();
    }

    @ApiOperation(value = "查看用户隐私协议")
    @GetMapping(value = "/queryUserPrivacyProtocol", produces = MediaType.TEXT_HTML_VALUE)
    public String queryUserPrivacyProtocol() {
        TemplateBean templateBean = templateService.queryById(AppContants.USER_PRIVACY_PROTOCOL);
        return templateBean == null ? AppContants.HAS_NOT_USER_PRIVACY_PROTOCOL_TIP : templateBean.getContent();
    }

    @ApiOperation(value = "查询分享海报信息")
    @PostMapping("/listShareposter")
    public ApiResponse listShareposter(@RequestBody SharePosterBean sharePosterBean, HttpServletRequest request) {
        PageBean page = new PageBean(1, 5);
        sharePosterBean.setIsActive(true);
        PageResult<SharePosterBean> pageResult = sharePosterService.list(sharePosterBean, page);
        ApiResponse response = new ApiResponse(pageResult);
        // 新增分享链接的url
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isNotBlank(loginUserId)) {
            String shareUrl = AppUtils.getStudentShareUrl(loginUserId);
            if (CollectionUtils.isNotEmpty(pageResult.getDataList())) {
                pageResult.getDataList().forEach(item -> item.setShareUrl(shareUrl));
            }
        }
        response.setMessage("成功邀请一位好友，最高可获得1199元奖学金");
        return response;
    }

    @ApiOperation(value = "分享课程")
    @PostMapping("/queryShareCourseInfo/{courseId}")
    public ApiResponse shareCourse(@PathVariable String courseId, HttpServletRequest request) {
        CourseBean courseBean = Optional.ofNullable(courseService.queryById(courseId))
            .orElse(new CourseBean());
        String loginUserId = SsoUtils.getLoginUserId(request);
        Map<String, String> result = new HashMap<>();
        result.put("title", courseBean.getName());
        result.put("description", courseBean.getDescription());
        result.put("url", AppUtils.getStudentShareUrl(loginUserId));
        return new ApiResponse(result);
    }

}
