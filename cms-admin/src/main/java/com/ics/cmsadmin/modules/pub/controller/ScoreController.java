package com.ics.cmsadmin.modules.pub.controller;/*
 *    created by mengyang ${date}
 */

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.pub.bean.Score;
import com.ics.cmsadmin.modules.pub.service.ScoreService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/score")
public class ScoreController extends ResourceController<ScoreService, Score, Integer> {
    @GetMapping
    public ApiResponse get(Integer practiceId, String studentId) {
        Map<String, Object> param = new HashMap<>();
        param.put("practiceId", practiceId);
        param.put("studentId", studentId);
        return super.get(param);
    }
}
