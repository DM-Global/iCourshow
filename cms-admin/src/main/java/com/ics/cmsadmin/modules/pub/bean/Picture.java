package com.ics.cmsadmin.modules.pub.bean;


import lombok.Data;

@Data
public class Picture extends Resource<Integer> {

    private String picUrl;

    private String description;

    private Integer picType;
}
