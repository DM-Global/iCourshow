package com.ics.cmsadmin.modules.agent.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.modules.agent.bean.ApplyWithdrawCashBean;
import com.ics.cmsadmin.modules.agent.service.ApplyWithdrawCashService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_agent controller
 * Created by lvsw on 2018-51-29 14:09:57.
 */
@Log4j2
@Api(description = "代理商提现相关")
@RestController
@RequestMapping("/applyWithdrawCash")
public class ApplyWithdrawCashController {

    @Resource
    private ApplyWithdrawCashService applyWithdrawCashService;
    @Resource
    private RegisterService registerService;

    @Authorize
    @ApiOperation(value = "代理商申请提现")
    @PostMapping("/applyWithdrawCash")
    public ApiResponse applyWithdrawCash(@RequestBody ApplyWithdrawCashBean bean,
                                         HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(applyWithdrawCashService.applyWithdrawCash(bean, loginUserId));
    }

    @Authorize(AuthorizeEnum.ACCEPT_WITHDRAWCASH_APPLY)
    @ApiOperation(value = "接受代理商申请提现")
    @PostMapping("/acceptWithdrawCashApply/{applyId}")
    public ApiResponse acceptWithdrawCashApply(@PathVariable String applyId, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(applyWithdrawCashService.acceptWithdrawCashApply(applyId, loginUserId));
    }

    @Authorize(AuthorizeEnum.REJECT_WITHDRAWCASH_APPLY)
    @ApiOperation(value = "拒绝代理商申请提现")
    @PostMapping("/rejectWithdrawCashApply/{applyId}")
    public ApiResponse rejectWithdrawCashApply(
        @PathVariable String applyId,
        @RequestBody ApplyWithdrawCashBean bean,
        HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(applyWithdrawCashService.rejectWithdrawCashApply(applyId, bean, loginUserId));
    }

    @Authorize(AuthorizeEnum.CANCEL_WITHDRAWCASH_APPLY)
    @ApiOperation(value = "取消代理商申请提现")
    @PostMapping("/cancelWithdrawCashApply/{applyId}")
    public ApiResponse cancelWithdrawCashApply(@PathVariable String applyId,
                                               HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(applyWithdrawCashService.cancelWithdrawCashApply(applyId, loginUserId));
    }

    @Authorize(AuthorizeEnum.APPLY_WITHDRAWCASH_QUERY)
    @ApiOperation(value = "分页查询代理商账户信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody ApplyWithdrawCashBean bean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(applyWithdrawCashService.listByLoginUserId(bean, loginUserId, pageBean));
    }

    @Authorize(AuthorizeEnum.EXPORT_WITHDRAWCASH_QUERY)
    @ApiOperation(value = "导出代理商账户信息")
    @PostMapping(value = "/export")
    public ApiResponse export(ApplyWithdrawCashBean bean,
                              HttpServletRequest request) {
        int exportNum = registerService.queryRegisterValue(RegisterKeyValueEnum.EXPORT_MAX_NUM, Integer::valueOf);
        if (exportNum <= 0) {
            exportNum = Integer.valueOf(RegisterKeyValueEnum.EXPORT_MAX_NUM.getDefaultValue());
        }
        PageBean pageBean = new PageBean(1, exportNum);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(applyWithdrawCashService.listByLoginUserId(bean, loginUserId, pageBean));
    }
}
