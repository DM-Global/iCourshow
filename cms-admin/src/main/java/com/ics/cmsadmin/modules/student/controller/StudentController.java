package com.ics.cmsadmin.modules.student.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.modules.auth.service.AccessService;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.student.bean.StudentShareDetailBean;
import com.ics.cmsadmin.modules.student.service.StudentAccountDetailService;
import com.ics.cmsadmin.modules.student.service.StudentAccountService;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.basic.service.VideoHistoryService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.student.service.StudentShareDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * t_student controller
 * Created by lvsw on 2018-52-22 19:09:32.
 */
@Api(description = "学生管理")
@RestController("newStudentController")
@RequestMapping("/student")
public class StudentController {

    @Resource(name = "newStudentService")
    private StudentService studentService;
    @Resource
    private AccessService accessService;
    @Resource
    private VideoHistoryService videoHistoryService;
    @Resource
    private OrderService orderService;
    @Resource
    private StudentAccountDetailService studentAccountDetailService;
    @Resource
    private StudentAccountService studentAccountService;

    @Authorize(AuthorizeEnum.STUDENT_QUERY)
    @ApiOperation(value = "查询信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(studentService.queryById(id));
    }

    @Authorize(AuthorizeEnum.STUDENT_QUERY)
    @ApiOperation(value = "分页查询信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody StudentBean studentBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(studentService.listByLoginUserId(studentBean, loginUserId, pageBean));
    }


    @ApiOperation(value = "分页查询学生关联视频信息")
    @Authorize(AuthorizeEnum.STUDENT_VIDEO_QUERY)
    @PostMapping("/listViewVideo/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody VideoHistoryBean bean,
                            @ApiParam("页码") @PathVariable Integer pageNo,
                            @ApiParam("每页条数") @PathVariable Integer pageSize) {
        return ApiResponse.getDefaultResponse(videoHistoryService.list(bean, new PageBean(pageNo, pageSize)));
    }

    @Authorize(AuthorizeEnum.STUDENT_QUERY)
    @ApiOperation(value = "获取账户明细列表")
    @PostMapping("/listAccountDetail/{studentId}/{pageNo}/{pageSize}")
    public ApiResponse listMyAccountDetail(@RequestBody StudentAccountDetailBean studentAccountDetailBean,
                                           @ApiParam("学生id") @PathVariable String studentId,
                                           @ApiParam("页码") @PathVariable Integer pageNo,
                                           @ApiParam("每页条数") @PathVariable Integer pageSize) {
        studentAccountDetailBean.setStudentId(studentId);
        return new ApiResponse(studentAccountDetailService.list(studentAccountDetailBean, new PageBean(pageNo, pageSize)));
    }

    @Authorize(AuthorizeEnum.STUDENT_QUERY)
    @ApiOperation(value = "获取账户信息")
    @PostMapping("/queryAccountInfo/{studentId}")
    public ApiResponse queryAccountInfo(@ApiParam("学生id") @PathVariable String studentId) {
        return new ApiResponse(Optional.ofNullable(studentAccountService.queryByStudentId(studentId)).orElse(new StudentAccountBean()));
    }

    @ApiOperation(value = "更新学生信息")
    @Authorize(AuthorizeEnum.STUDENT_UPDATE)
    @PostMapping("/update/{id}")
    public ApiResponse update(@RequestBody StudentBean student,
                              @PathVariable String id,
                              HttpServletRequest request) {
        if (!accessService.canAccessForStudent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权限操作该学生");
        }
        return ApiResponse.getDefaultResponse(studentService.update(id, student));
    }

    @ApiOperation(value = "更新学生信息")
    @Authorize(AuthorizeEnum.STUDENT_UPDATE)
    @PostMapping("/vip/update/{id}")
    public ApiResponse updateVIP(@RequestBody StudentBean student,
                                 @PathVariable String id,
                                 HttpServletRequest request) {
        if (!accessService.canAccessForStudent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权限操作该学生");
        }
        return ApiResponse.getDefaultResponse(studentService.updateVip(id, student));
    }

    @ApiOperation(value = "为学生创建免费订单")
    @Authorize(AuthorizeEnum.STUDENT_CREATE_FREE_ORDER)
    @PostMapping("/createFreeOrder/{studentId}/{days}")
    public ApiResponse createFreeOrder(@PathVariable String studentId,
                                 @PathVariable int days,
                                 HttpServletRequest request) {
        if (!accessService.canAccessForStudent(SsoUtils.getLoginUserId(request), studentId)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权限操作该学生");
        }
        return ApiResponse.getDefaultResponse(orderService.createFreeOrder(studentId, days));
    }
}
