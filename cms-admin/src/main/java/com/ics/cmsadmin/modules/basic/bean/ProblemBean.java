package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * q_problem
 * 问题
 * Created by lvsw on 2018-09-06.
 */
@Data
@Builder
public class ProblemBean {

    // 问题id
//	@NotEmpty(message = "{Problem.problemId.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String problemId;
    // 所属题集id
//	@NotEmpty(message = "{Problem.problemSetId.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String problemSetId;
    // 类型
//	@NotEmpty(message = "{Problem.type.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
//	@Length(message = "{Problem.type.Length}", min = 2, max = 9, groups = {InsertGroup.class, UpdateGroup.class})
    private String type;
    @NotEmpty(message = "标题不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "标题长度必须在{min}~{max}之间", min = 2, max = 9, groups = {InsertGroup.class, UpdateGroup.class})
    private String title;
    // 内容
//	@NotEmpty(message = "{Problem.content.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
//	@Length(message = "{Problem.content.Length}", min = 2, max = 65535, groups = {InsertGroup.class, UpdateGroup.class})
    private String content;
    // 状态
//	@NotEmpty(message = "{Problem.isActive.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String isActive;
    // 排序
//	@NotEmpty(message = "{Problem.orderNo.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String orderNo;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 更新时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public ProblemBean() {
    }
}
