package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

import java.util.List;

@Data
public class Course extends CourseResource {
    private List<Teacher> teachers;

    private Integer progress;

    private Integer unitNumber;

    private Integer topicNumber;

    private Integer boughtNumber;

    private Integer clickNumber;

    private Boolean collected;

    /**
     * 课程是否已经购买
     */
    private Boolean hasBought;

    /**
     * 是否点赞了
     */
    private Boolean liked;
}
