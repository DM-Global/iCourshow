package com.ics.cmsadmin.frame.api.response;

import lombok.Data;

import java.util.List;

@Data
public class PolyvVideoInfo {
    private List<String> images_b;      // 视频截图大图地址
    private List<String> images;        // 视频截图
    private List<String> imageUrls;
    private String tag;                 // 视频标签
    private String mp4;                 // MP4源文件
    private String title;               // 标题
    private int df;                     // 视频码率数
    private String times;               // 播放次数
    private String vid;                 // 视频id
    private String mp4_1;               // 流畅码率mp4格式视频地址
    private String mp4_2;               // 高清码率mp4格式视频地址
    private String mp4_3;               // 超清码率mp4格式视频地址
    private String cataid;              // 分类id， 如1为根目录
    private String swf_link;            // 返回flash连接
    private String status;              // 视频状态 60/61 已发布 10 等待编码 20正在编码 50等待审核 51审核不通过 -1已删除
    private int seed;                   // 加密视频为1，非加密为0
    private String flv1;                // 流畅码率flv格式视频地址
    private String flv2;                // 高清码率flv格式视频地址
    private String flv3;                // 超清码率flv格式视频地址
    private String sourcefile;
    private String playerwidth;         // 视频宽度
    private String default_video;       // 用户默认播放视频
    private String duration;            // 时长
    private String first_image;             // 视频首图
    private String original_definition;     // 最佳分辨率
    private String context;                 // 视频描述
    private String playerheight;            // 视频高度
    private String ptime;                   // 视频上传日期
    private long source_filesize;       // 编码后各个清晰度视频的文件大小（单位：字节），类型为array
    private List<Long> filesize;
    private String md5checksum;
    private List<String> hls;
    private String previewVid;              // 预览视频id

}
