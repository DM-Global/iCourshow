package com.ics.cmsadmin.frame.core.bean;

/**
 * 两位的元组
 * Created by 666666 on 2017/9/26.
 */
public class SixTupleBean<ONE, TWO, THREE, FOUR, FIVE, SIX> extends FiveTupleBean<ONE, TWO, THREE, FOUR, FIVE> {
    public SIX six;

    public SixTupleBean() {
        super();
    }

    public SixTupleBean(ONE one, TWO two, THREE three, FOUR four, FIVE five, SIX six) {
        super(one, two, three, four, five);
        this.six = six;
    }

}
