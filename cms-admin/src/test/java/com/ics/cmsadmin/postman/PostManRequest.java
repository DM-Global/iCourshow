package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class PostManRequest {
    private String method;
    private List<PostManHeader> header;
    private PostManRequestBody body;
    private PostManRequestUrl url;
}
