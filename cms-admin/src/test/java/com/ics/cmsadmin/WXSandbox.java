package com.ics.cmsadmin;

import com.github.wxpay.sdk.WXPay;
import com.github.wxpay.sdk.WXPayConstants;

/**
 * Created by Sandwich on 2018-07-15
 * 若需要使用sandbox环境
 */
public class WXSandbox {

    public static void main(String[] args) throws Exception {
        MyConfig config = new MyConfig();
        WXPay wxpay = new WXPay(config, WXPayConstants.SignType.MD5, true);
        // ......
    }

}
