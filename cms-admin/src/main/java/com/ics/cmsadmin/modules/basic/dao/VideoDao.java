package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.VideoBean;
import com.ics.cmsadmin.modules.basic.bean.VideoSumBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface VideoDao extends BaseDao<VideoBean>, BaseDataDao<VideoBean> {
    VideoBean findByPointId(@Param("pointId") String pointId);

    VideoSumBean statisticByCourse(@Param("courseId") String courseId);
}
