package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.basic.bean.CourseAttributeBean;


/**
 * t_course_attribute 服务类
 * Created by sandwich on 2018-56-10 07:12:28.
 */
public interface CourseAttributeService extends BaseService<CourseAttributeBean>, BaseDataService<CourseAttributeBean> {
}
