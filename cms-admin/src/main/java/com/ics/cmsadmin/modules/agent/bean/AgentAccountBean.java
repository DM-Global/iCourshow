package com.ics.cmsadmin.modules.agent.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * a_agent_account
 * 代理商账户
 * <p>
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Data
@Builder
public class AgentAccountBean extends LoginSearchBean {

    private String accountId;               // 账户id
    private String agentNo;                 // 代理商编号
    private BigDecimal totalIncome;         // 总收益 A = B + C
    private BigDecimal withdrawCash;        // 提现金额 B
    private BigDecimal balance;              // 余额 C = D + E
    private BigDecimal availableBalance;    // 可用余额 D
    private BigDecimal frozenBalance;       // 冻结余额 E
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间

    @Tolerate
    public AgentAccountBean() {
    }
}
