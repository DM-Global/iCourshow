package com.ics.cmsadmin.student;

import org.apache.commons.lang3.RandomUtils;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-03-20 16:36
 */
public enum  EventEnums {
    NEW_STUDENT,            // 新的学生入驻
    PROMOTION_STUDENT,      // 推荐出新的学生
    NEW_ORDER;              // 新的订单

    public static EventEnums radomEvent(){
        EventEnums[] values = EventEnums.values();
        return values[RandomUtils.nextInt(0, values.length)];
    }
}
