package com.ics.cmsadmin.modules.pub.service;

import com.ics.cmsadmin.modules.pub.dao.PictureMapper;
import com.ics.cmsadmin.modules.pub.bean.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("PictureService")
public class PictureService extends ResourceService<PictureMapper, Picture, Integer> {

    @Autowired
    private PictureMapper pictureMapper;

    /**
     * 获取返回总条目数
     *
     * @param picType
     * @return
     */
    public Integer totalCount(Integer picType) {
        return pictureMapper.totalCount(picType);
    }

    public List<Picture> listPage(Integer picType, Integer pageSize, Integer start) {
        return pictureMapper.listPage(picType, pageSize, start);
    }

    /**
     * 如果需操作对象带有id,则执行更新操作
     * 否则执行插入操作
     *
     * @param picture
     * @return
     */
    public Integer save(Picture picture) {
        if (picture.getId() != null) {
            return pictureMapper.update(picture);
        } else {
            return pictureMapper.insert(picture);
        }
    }
}
