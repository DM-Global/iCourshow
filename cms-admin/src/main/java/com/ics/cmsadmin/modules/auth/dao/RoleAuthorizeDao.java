package com.ics.cmsadmin.modules.auth.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2017/10/15.
 */
@Repository
@Mapper
public interface RoleAuthorizeDao {
    /**
     * 批量将权限加到角色中
     *
     * @param authCodes 权限码
     * @param roleId    角色id
     * @param creator   创建人
     * @return
     */
    void addAuthorizes2Role(@Param("authCodes") List<String> authCodes,
                            @Param("roleId") String roleId,
                            @Param("creator") String creator);

    /**
     * 批量将权限码从角色中删除
     *
     * @param authCodes 权限码
     * @param roleId    角色id
     * @return
     */
    void removeAuthorizes2Role(@Param("authCodes") List<String> authCodes,
                               @Param("roleId") String roleId);


    /**
     * 判断角色是否包含该菜单
     *
     * @param roleId 角色id
     * @return
     */
    List<String> listRoleMenu(@Param("roleId") String roleId, @Param("menuId") String menuId);

    /**
     * 根据菜单id查询该角色拥有的权限码
     *
     * @param roleId 角色id
     * @param menuId 菜单id
     * @return
     */
    List<String> listRoleAuthorizesByMenuId(@Param("roleId") String roleId, @Param("menuId") String menuId);

    /**
     * 对角色移除菜单
     *
     * @param menuList 菜单列表
     * @param roleId   角色id
     */
    void removeMenu2Role(@Param("list") List<String> menuList, @Param("roleId") String roleId);

    /**
     * 根据菜单查询菜单总数
     *
     * @param menuList 菜单列表
     * @return
     */
    int countMenuByMenuId(@Param("list") List<String> menuList);

    /**
     * 对角色添加菜单
     *
     * @param menuList    菜单列表
     * @param roleId      角色id
     * @param loginUserId 更新人
     */
    void addMenu2Role(@Param("list") List<String> menuList, @Param("roleId") String roleId, @Param("loginUserId") String loginUserId);
}
