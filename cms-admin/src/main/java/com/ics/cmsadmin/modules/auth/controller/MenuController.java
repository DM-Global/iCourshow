package com.ics.cmsadmin.modules.auth.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.service.TreeDataService;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.auth.bean.SysMenu;
import com.ics.cmsadmin.modules.auth.service.MenuService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Menu controller
 * Created by lvsw on 2018-04-03.
 */
@Api(description = "菜单信息管理接口")
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Resource
    private MenuService menuService;

    @Authorize(AuthorizeEnum.MENU_QUERY)
    @ApiOperation(value = "查询菜单信息信息")
    @GetMapping(value = "/query/{menuId}")
    public ApiResponse queryById(@ApiParam(value = "菜单信息id") @PathVariable String menuId) {
        return new ApiResponse(menuService.queryById(menuId));
    }

    @Authorize(AuthorizeEnum.MENU_INSERT)
    @ApiOperation(value = "新增菜单信息信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody SysMenu menuBean, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(menuService.insert(menuBean));
    }

    @Authorize(AuthorizeEnum.MENU_DELETE)
    @ApiOperation(value = "删除菜单信息信息")
    @PostMapping(value = "/delete/{menuId}")
    public ApiResponse delete(@ApiParam("需要删除的菜单信息id") @PathVariable String menuId) {
        return new ApiResponse(menuService.delete(menuId));
    }

    @Authorize(AuthorizeEnum.MENU_UPDATE)
    @ApiOperation(value = "更新菜单信息信息")
    @PostMapping(value = "/update/{menuId}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody SysMenu menuBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的菜单信息id") @PathVariable String menuId) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(menuService.update(menuId, menuBean));
    }

    @Authorize(AuthorizeEnum.MENU_QUERY)
    @ApiOperation(value = "分页查询菜单信息信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody SysMenu menuBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(menuService.list(menuBean, pageBean));
    }

    @Authorize
    @ApiOperation(httpMethod = "POST", value = "根据登陆用户获取相应的菜单")
    @PostMapping("/listMenuTree")
    public ApiResponse listMenuTree(HttpServletRequest request) {
        return new ApiResponse(menuService.listMenuTreeByUserId(SsoUtils.getLoginUserId(request)));
    }


    @Authorize(AuthorizeEnum.MENU_QUERY)
    @ApiOperation(value = "按树结构列出所有菜单信息")
    @PostMapping("/listAllChildren")
    public ApiResponse listAllChildren() {
        return new ApiResponse(menuService.listAllChildren());
    }

    @Authorize(AuthorizeEnum.MENU_QUERY)
    @ApiOperation(httpMethod = "POST", value = "查询直接下级菜单")
    @PostMapping({"/listDirectChildren/{parentId}", "/listDirectChildren"})
    public ApiResponse listDirectChildren(@ApiParam("父菜单id") @PathVariable(required = false) String parentId) {
        parentId = StringUtils.isBlank(parentId) ? TreeDataService.DEFAULT_ROOT_ID : parentId;
        return new ApiResponse(menuService.listDirectChildren(parentId));
    }

}
