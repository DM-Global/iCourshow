package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.modules.system.bean.CityBean;
import com.ics.cmsadmin.modules.system.service.CityService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ics.cmsadmin.modules.system.steward.SystemRepositories.cityDao;

/**
 * Created by lvsw on 2017-12-02.
 */
@Service
public class CityServiceImpl implements CityService {

    @CacheDbMember(returnClass = CityBean.class, group = CacheGroupEnum.CACHE_CITY)
    @Override
    public List<CityBean> listDirectChildren(String parentCode) {
        if (StringUtils.isBlank(parentCode)) {
            parentCode = "__ROOT__";
        }
        return cityDao.listDirectChildren(parentCode);
    }

    @CacheDbMember(returnClass = CityBean.class, group = CacheGroupEnum.CACHE_CITY)
    @Override
    public List<CityBean> listProvinceCity() {
        List<CityBean> resultList = cityDao.listDirectChildren("__ROOT__");
        if (CollectionUtils.isEmpty(resultList)) {
            return null;
        }
        resultList.stream().forEach(item -> item.setChildren(cityDao.listDirectChildren(item.getCode())));
        return resultList;
    }
}
