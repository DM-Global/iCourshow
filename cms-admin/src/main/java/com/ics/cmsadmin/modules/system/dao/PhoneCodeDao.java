package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.modules.system.bean.CityBean;
import com.ics.cmsadmin.modules.system.bean.PhoneCodeBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2017-12-02.
 */
@Repository
@Mapper
public interface PhoneCodeDao {
    /**
     * 查询手机区域吗
     */
    List<PhoneCodeBean> list(@Param("keyword") String keyword);
}
