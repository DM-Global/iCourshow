package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.modules.system.bean.PhoneCodeBean;
import com.ics.cmsadmin.modules.system.service.PhoneCodeService;
import com.ics.cmsadmin.modules.system.steward.SystemRepositories;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneCodeServiceImpl implements PhoneCodeService {
    @Override
    public List<PhoneCodeBean> list(String keyword) {
        return SystemRepositories.phoneCodeDao.list(keyword);
    }
}
