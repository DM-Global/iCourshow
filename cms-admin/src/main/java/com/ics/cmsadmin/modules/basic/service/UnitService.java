package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.UnitBean;

import java.util.List;

/**
 * t_unit 服务类
 * Created by lvsw on 2018-09-01.
 */
public interface UnitService extends BaseService<UnitBean>, BaseDataService<UnitBean> {
    /**
     * 根据课程Id查询所有单元信息
     *
     * @param courseId 课程Id
     */
    List<UnitBean> listAllUnitByCourseId(String courseId);

    UnitBean unitDetail(String id);

    List<UnitBean> listAllUnitDetailByCourseId(String userId, String courseId);

    UnitBean unitDetailAdditional(String userId, UnitBean unit);

    void updateProblemNum(String unitId, int testNum);
}
