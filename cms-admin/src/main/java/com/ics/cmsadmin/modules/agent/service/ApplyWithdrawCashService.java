package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.agent.bean.AgentAccountBean;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.agent.bean.ApplyWithdrawCashBean;


/**
 * a_agent_info 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface ApplyWithdrawCashService extends LoginBaseDataService<ApplyWithdrawCashBean> {

    /**
     * 申请提现记录
     *
     * @return
     */
    boolean applyWithdrawCash(ApplyWithdrawCashBean bean, String loginUserId);

    /**
     * 接受代理商提现申请
     *
     * @param applyId
     * @param loginUserId
     * @return
     */
    boolean acceptWithdrawCashApply(String applyId, String loginUserId);

    /**
     * 拒绝代理商提现申请
     *
     * @param applyId
     * @param bean
     * @param loginUserId
     * @return
     */
    boolean rejectWithdrawCashApply(String applyId, ApplyWithdrawCashBean bean, String loginUserId);

    /**
     * 取消代理商提现申请
     *
     * @param applyId
     * @param loginUserId
     * @return
     */
    boolean cancelWithdrawCashApply(String applyId, String loginUserId);
}
