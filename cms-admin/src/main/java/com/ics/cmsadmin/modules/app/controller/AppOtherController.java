package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.constant.SwaggerNoteConstants;
import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.basic.bean.*;
import com.ics.cmsadmin.modules.basic.service.*;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.student.bean.StudentCertificateBean;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.security.PublicKey;
import java.util.*;
import java.util.function.Function;

@AppLoginValid
@Api(description = "app其他接口", tags = "其他接口")
@RestController
@RequestMapping("/app/other")
public class AppOtherController {

    @Resource
    private CollectionService collectionService;
    @Resource
    private LikeService likeService;
    @Resource
    private ProblemSetService problemSetService;
    @Resource
    private VideoHistoryService videoHistoryService;
    @Resource
    private CourseService courseService;
    @Resource
    private TopicService topicService;
    @Resource
    private PointService pointService;
    @Resource
    private RegisterService registerService;

    @ApiOperation(value = "收藏", notes = "levelName 取值:subject, course, unit, topic, point")
    @PostMapping("/collection")
    public ApiResponse collection(@RequestBody Collect collect,
                                  HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        collect.setUserId(loginUserId);
        return new ApiResponse(collectionService.toggleCollect(collect));
    }

    @ApiOperation(value = "点赞", notes = "levelName 取值:subject, course, unit, topic, point")
    @PostMapping("/like")
    public ApiResponse like(@RequestBody Like like,
                            HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        like.setUserId(loginUserId);
        return new ApiResponse(likeService.toggleLike(like));
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "获取问题列表", notes = SwaggerNoteConstants.PROBLEM_SET_DETAILS)
    @PostMapping(value = "/findProblemSetDetails")
    public ApiResponse findProblemSetDetails(@RequestBody ProblemSetBean problemSetBean, HttpServletRequest request) {
        String level = problemSetBean.getLevel();
        String levelId = problemSetBean.getLevelId();
        if (StringUtils.isBlank(level) || StringUtils.isBlank(levelId)) {
            new ApiResponse(ApiResultEnum.PARAM_ERROR, "查无此题集");
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isNotBlank(loginUserId)) {
            return new ApiResponse(problemSetService.findProblemSetDetails(problemSetBean));
        }else {
            if (isPublic(level, levelId)) {
                return new ApiResponse(problemSetService.findProblemSetDetails(problemSetBean));
            }else {
                return new ApiResponse(ApiResultEnum.UN_LOGIN, "用户未登陆");
            }
        }
    }

    /**
     * 判断知识点,主题是否为公开内容
     */
    private boolean isPublic(String level, String levelId) {
        switch (level) {
            case "topic":
                TopicBean topicBean = topicService.queryById(levelId);
                return topicBean != null && topicBean.getIsPublic();
            case "point":
                PointBean pointBean = pointService.queryById(levelId);
                return pointBean != null && pointBean.getIsPublic();
        }
        return false;
    }

    @ApiOperation(value = "提交答题分数")
    @PostMapping("/submitProblemScore")
    public ApiResponse submitProblemScore(@Validated @RequestBody ScoreBean scoreBean,
                                          BindingResult bindingResult,
                                          HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        scoreBean.setStudentId(SsoUtils.getLoginUserId(request));
        return new ApiResponse(problemSetService.submitScore(scoreBean));
    }

    @ApiOperation(value = "查询观看过的视频记录")
    @GetMapping("/listVideoHistory")
    public ApiResponse listVideoHistory(HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isEmpty(userId)) {
            return ApiResponse.getDefaultResponse(new ArrayList<>());
        }
        return ApiResponse.getDefaultResponse(videoHistoryService.listByUserId(userId));
    }

    @ApiOperation("查询所有收藏")
    @GetMapping("/listCollection")
    public ApiResponse listCollection(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return ApiResponse.getDefaultResponse(new ArrayList<>());
        }
        return ApiResponse.getDefaultResponse(collectionService.getCollectionList(loginUserId));
    }

    @ApiOperation(value = "查询收藏", notes = "type取值范围 subject, course, unit, topic, point;")
    @GetMapping("/listCollectionByType/{type}")
    public ApiResponse listCollectionByType(@PathVariable String type,
                                            HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return ApiResponse.getDefaultResponse(new ArrayList<>());
        }
        return ApiResponse.getDefaultResponse(collectionService.listCollectionByType(type, loginUserId));
    }

    @ApiOperation(value = "分页查询已购课程信息")
    @GetMapping("/listPurchased/{pageNo}/{pageSize}")
    public ApiResponse listPurchased(@PathVariable int pageNo,
                                     @PathVariable int pageSize,
                                     HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        PageResult pageResult = new PageResult();
        // 未登陆
        if (StringUtils.isBlank(userId)) {
            pageResult.setDataList(new ArrayList<>());
            return ApiResponse.getDefaultResponse(pageResult);
        }
        PageBean page = new PageBean(pageNo, pageSize);
        pageResult = courseService.purchased(userId, page);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @ApiOperation(value = "学习计划页面")
    @GetMapping("/listLearningPlan")
    public ApiResponse listLearningPlan(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        Map<LevelEnum, List> collectionList = collectionService.getCollectionList(loginUserId);
        List<VideoHistoryBean> videoHistoryBeans = videoHistoryService.listByUserId(loginUserId);
        Map<String, Map<String, Object>> result = new TreeMap<>();
        Map<String, Object> videoHistoryMap = new HashMap<>();
        videoHistoryMap.put("name", "最近学习视频");
        videoHistoryMap.put("list", limit(videoHistoryBeans, 7));

        Map<String, Object> collectionVideoMap = new HashMap<>();
        collectionVideoMap.put("name", "收藏视频");
        collectionVideoMap.put("list", limit(collectionList.get(LevelEnum.point), 7));

        Map<String, Object> collectionCourseMap = new HashMap<>();
        collectionCourseMap.put("name", "收藏课程");
        collectionCourseMap.put("list", limit(collectionList.get(LevelEnum.course), 7));

        result.put("learnHistory", videoHistoryMap);
        result.put("collectionVideo", collectionVideoMap);
        result.put("collectionCourse", collectionCourseMap);
        return new ApiResponse(result);

    }

    @ApiOperation(value = "保存视频观看进度", notes = SwaggerNoteConstants.SAVE_VIDEO_HISTORY)
    @PostMapping("/saveVideoHistory")
    public ApiResponse save(@RequestBody VideoHistoryBean videoHistoryBean,
                            HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        videoHistoryBean.setUserId(userId);
        return new ApiResponse(videoHistoryService.save4app(videoHistoryBean));
    }

    @ApiOperation(value = "根据视频id查询视频历史")
    @GetMapping("/findVideoHistory/{videoId}")
    public ApiResponse queryOne(@PathVariable String videoId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        VideoHistoryBean videoHistoryBean = videoHistoryService.findOne(userId, videoId);
        return ApiResponse.getDefaultResponse(videoHistoryBean);
    }

    @ApiOperation(value = "根据课程id获取最后一个播放历史")
    @GetMapping("/queryLastVideoHistoryByCourseId/{courseId}")
    public ApiResponse queryLastVideoHistoryByCourseId(@PathVariable String courseId, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return ApiResponse.getDefaultResponse(videoHistoryService.findByCourseId(userId, courseId));
    }

    private List limit(List videoHistoryBeans, int count) {
        List list = Optional.ofNullable(videoHistoryBeans)
            .orElse(new ArrayList());
        return list.subList(0, Math.min(list.size(), count));
    }


}
