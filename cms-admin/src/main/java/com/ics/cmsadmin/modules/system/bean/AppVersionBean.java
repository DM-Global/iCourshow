package com.ics.cmsadmin.modules.system.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;


/**
 * t_app_version
 * app版本信息
 * <p>
 * Created by lvsw on 2018-17-05 20:12:49.
 */
@Data
@Builder
public class AppVersionBean {

    private String id;              // id
    @NotBlank(message = "请选择app版本类型", groups = {InsertGroup.class, UpdateGroup.class})
    @Pattern(message = "app版本类型只能是ios或android", regexp = "^ios|android$", groups = {InsertGroup.class, UpdateGroup.class})
    private String type;              // app类型 ios,android
    @NotBlank(message = "app版本信息不能为空", groups = {InsertGroup.class, UpdateGroup.class})
//    @Pattern(message = "app版本格式不正确(例:1.0.0.0)", regexp = "^(\\d+\\.){3}\\d+$", groups = {InsertGroup.class, UpdateGroup.class})
    private String version;              // app版本 格式 1.0.0.0
    @NotBlank(message = "请选择app版本更新类型", groups = {InsertGroup.class, UpdateGroup.class})
    @Pattern(message = "请选择app版本更新类型", regexp = "^0|1$", groups = {InsertGroup.class, UpdateGroup.class})
    private String forceUpdate;              // 是否强制更新 0 不强制更新 1 强制更新
    @NotBlank(message = "app下载url不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @URL(message = "app下载url格式不正确", groups = {InsertGroup.class, UpdateGroup.class})
    private String url;              // app下载url
    private String remark;              // 更新备注
    @NotBlank(message = "请选择app版本状态", groups = {InsertGroup.class, UpdateGroup.class})
    @Pattern(message = "请选择app版本状态", regexp = "^0|1$", groups = {InsertGroup.class, UpdateGroup.class})
    private String status;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间


    @Tolerate
    public AppVersionBean() {
    }
}
