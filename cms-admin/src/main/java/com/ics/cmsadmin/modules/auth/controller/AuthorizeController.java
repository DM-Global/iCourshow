package com.ics.cmsadmin.modules.auth.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.auth.bean.SysAuthorize;
import com.ics.cmsadmin.modules.auth.service.AuthorizeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Authorize controller
 * Created by lvsw on 2018-04-03.
 */
@Api(description = "权限管理接口")
@RestController
@RequestMapping("/authorize")
public class AuthorizeController {

    @Resource
    private AuthorizeService authorizeService;

    @Authorize(AuthorizeEnum.AUTHORIZE_QUERY)
    @ApiOperation(value = "分页查询权限信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody SysAuthorize authorizeBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(authorizeService.list(authorizeBean, pageBean));
    }

    @Authorize(AuthorizeEnum.ROLE_QUERY)
    @ApiOperation(value = "根据菜单id查询角色信息")
    @PostMapping("/listByMenuIds")
    public ApiResponse listByMenuIds(@RequestBody List<String> menuIds) {
        return new ApiResponse(authorizeService.listByMenuIds(menuIds));
    }

    @Authorize(AuthorizeEnum.ROLE_QUERY)
    @ApiOperation(value = "根据菜单id查询角色信息")
    @PostMapping("/listByMenuId/{menuId}")
    public ApiResponse listByMenuId(@PathVariable String menuId) {
        return new ApiResponse(authorizeService.listAuthorizeByMenuId(menuId));
    }
}
