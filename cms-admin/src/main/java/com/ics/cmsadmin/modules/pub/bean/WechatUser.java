package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class WechatUser {
    public String openid;
    public String nickname;
    public Integer sex;
    public String language;
    public String country;
    public String province;
    public String city;
    public String headimgurl;
}
