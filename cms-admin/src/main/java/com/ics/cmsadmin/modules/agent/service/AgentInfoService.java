package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.bean.TwoTupleBean;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.agent.bean.*;

import java.text.ParseException;
import java.util.List;
import java.util.Map;


/**
 * a_agent_info 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface AgentInfoService extends LoginBaseDataService<AgentInfoBean> {
    /**
     * 根据主键id查询bean
     *
     * @param id 主键id
     * @return config
     */
    AgentInfoBean queryById(String id);

    /**
     * 通过用户查询代理商相关信息
     *
     * @param userId 用户id
     * @return 代理商信息
     */
    AgentDto queryMoreInfoById(String userId);

    /**
     * 通过用户查询代理商相关信息
     *
     * @param userId 用户id
     * @return 代理商信息
     */
    AgentInfoBean queryByUserId(String userId);

    /**
     * 通过用户查询代理商相关信息
     *
     * @param userId 用户id
     * @return 代理商信息
     */
    AgentDto queryMoreInfoByUserId(String userId);
//    /**
//     * 根据用户查询代理商节点
//     * @param userId 用户id
//     */
//    String queryAgentNoByUserId(String userId);

    /**
     * 新增代理商信息
     *
     * @param agentDto
     * @return
     */
    boolean insertAgent(AgentDto agentDto);

    /**
     * 更新代理商信息
     */
    boolean updateAgent(String id, AgentDto agentDto);

    /**
     * 查询所有一级代理商信息
     */
    List<AgentInfoBean> queryLevelAgent();

    /**
     * 重置代理商的登陆密码
     *
     * @param agentNo
     * @param loginUserId
     * @return
     */
    boolean resetPassword(String agentNo, String loginUserId);

    /**
     * 禁用代理商
     */
    boolean disable(String id);

    /**
     * 启用代理商
     */
    boolean enable(String id);

    /**
     * 分配分销商给业务员
     */
    boolean changeSalesman(String agentNo, String salesmane);
}
