package com.ics.cmsadmin.modules.agent.bean;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * a_agent_day_summary
 * 代理日结汇总数据
 * <p>
 * Created by sandwich on 2018-07-14 17:11:49.
 */
@Data
@Builder
public class AgentDaySummaryBean {

    private String id;              // id
    private String agentNo;              // 分销商编号
    private String agentName;              // 分销商编号
    private String parentId;              // 分销商父级编号
    private String summaryDay;              // 汇总日期
    private int funsCusm;              // 学生关注累积数
    private int funsNewly;              // 当日学生关注新增数
    private int buyerCusm;              // 购买课程学生累积数
    private int buyerNewly;              // 当日购买课程学生新增数
    private BigDecimal orderMoneyCusm;              // 订单金额累积数
    private BigDecimal orderMoneyNewly;              // 当日订单金额新增数
    private int childAgentCusm;              // 下级代理商累积数
    private int childAgentNewly;              // 当日下级代理商新增数
    private int childFunsCusm;              // 下级分销商学生关注累积数
    private int childFunsNewly;              // 当日下级分销商学生关注新增数
    private int childBuyerCusm;              // 下级分销商购买课程学生累积数
    private int childBuyerNewly;              // 当日下级分销商购买课程学生新增数
    private int childOrderMoneyCusm;              // 订单金额累积数
    private BigDecimal childOrderMoneyNewly;              // 当日订单金额新增数
    private BigDecimal selfShareMoneyCusm;              // 自己推广学生分润累积数
    private BigDecimal selfShareMoneyNewly;              // 当日自己推广学生分润新增数
    private BigDecimal childShareMoneyCusm;              // 下级推广学生分润金额累积数
    private BigDecimal childShareMoneyNewly;              // 当日下级推广学生分润金额新增数
    private BigDecimal parentShareMoneyCusm;              // 为上级贡献分润金额累积数
    private BigDecimal parentShareMoneyNewly;              // 当日为上级贡献分润金额新增数
    private BigDecimal allShareMoneyCusm;              // 获取分润累积数
    private BigDecimal allShareMoneyNewly;              // 当日获取分润新增数
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    @Tolerate
    public AgentDaySummaryBean() {
    }
}
