package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * t_share_poster
 */
@Repository
@Mapper
public interface SharePosterDao extends BaseDao<SharePosterBean>, BaseDataDao<SharePosterBean> {
}
