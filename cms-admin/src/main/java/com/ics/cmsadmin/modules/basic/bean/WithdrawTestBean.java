package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by Sandwich on 2019/6/3
 */
@Data
@Builder
public class WithdrawTestBean {

    private BigDecimal amount;

    private String studentId;

}
