package com.ics.cmsadmin.modules.system.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.system.bean.UserOperLogBean;
import com.ics.cmsadmin.modules.system.service.UserOperLogService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * com_user_oper_log controller
 * Created by lvsw on 2018-41-11 15:10:21.
 */
@Api(description = "用户操作日志")
@RestController
@RequestMapping("/userOperLog")
public class UserOperLogController {

    @Resource
    private UserOperLogService userOperLogService;

    @Authorize(AuthorizeEnum.USER_OPER_LOG_QUERY)
    @ApiOperation(value = "分页查询用户操作日志信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody UserOperLogBean userOperLogBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(userOperLogService.list(userOperLogBean, pageBean));
    }

}
