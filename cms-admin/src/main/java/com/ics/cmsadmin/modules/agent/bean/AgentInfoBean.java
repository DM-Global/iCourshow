package com.ics.cmsadmin.modules.agent.bean;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.BankAccount;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.constraints.NotBlank;
import java.util.Date;


/**
 * a_agent_info
 * 代理商
 * <p>
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Data
@Builder
public class AgentInfoBean extends LoginSearchBean {

    private String agentNo;              // 代理商编号
    private String name;              // 名字
    private String parentId;              // 父级代理商编号
    private String province;            // 省份编码
    private String city;                // 城市编码
    private String county;              // 县编码
    @NotBlank(message = "请选择地址", groups = {InsertGroup.class, UpdateGroup.class})
    private String cityName;              // 省市区地址
    //    @NotBlank(message = "请填写详细地址", groups = {InsertGroup.class, UpdateGroup.class})
    private String address;             // 具体地址
    //    @Pattern(message = "身份证格式不正确", groups = {InsertGroup.class, UpdateGroup.class},
//        regexp = "(^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}[0-9Xx]$)")
    @NotBlank(message = "证件号不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    private String idCard;              // 身份证id
    private String realName;            // 身份证名字
    //    @NotBlank(message = "银行卡不能为空", groups = {InsertGroup.class, UpdateGroup.class})
//    @BankAccount(message = "银行卡不正确", groups = {InsertGroup.class, UpdateGroup.class})
    private String accountNo;           // 银行账号
    private String accountName;           // 银行开户名
    private String cardType;            // 卡类型 DC:借记卡，CC:信用卡
    private String bank;                // 银行简称
    private String userId;
    private String wechatName;
    private String wechatCodeUrl;
    private String zfbName;
    private String zfbCodeUrl;
    private String agentType;       // 类别: 1. 公司/机构 2. 个人
    private String status;       // 状态: 0 无效 1 有效
    private String workPhone;       // 办公电话
    private String description;     // 描述
    private String shareMoney;     // 描述
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间
    private SysUser userInfo;
    private AgentShareRuleBean agentShareRule;
    private AgentAccountBean agentAccountBean;
    private String salesman;
    private String salesmanName;


    private String startCreateTime;
    private String endCreateTime;
    private String studentNumber;
    private String buyerNumber;

    @Tolerate
    public AgentInfoBean() {
    }
}
