package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StudentMapper extends ResourceMapper<Student, String> {
    int addOpenId(@Param("openId") String openId, @Param("unionId") String unionId);

    String getUnionId(@Param("openId") String openId);

    int addOpenId(@Param("openId") String openId, @Param("unionId") String unionId, @Param("client") String client);

    Student findOneByOpenId(@Param("openId") String openId);

    Student findOneByUnionId(@Param("unionId") String unionId);

    Student findOne(@Param("openId") String openId, @Param("unionId") String unionId);
}
