package com.ics.cmsadmin.modules.pub.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 下单所用的表单对象
 */
@Data
@Builder
public class OrderForm {

   /**
     * 订单类型: 1.课程
     */
    @NotNull(message = "订单类型必填")
    private Integer orderType;

    /**
     * 商品id
     */
    @NotBlank(message = "商品id必填")
    private String courseId;

    @Tolerate
    public OrderForm() {
    }
}
