package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.bean.RegisterBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Function;

/**
 * c_register 服务类
 * Created by lvsw on 2018-03-29.
 */
public interface RegisterService extends BaseService<RegisterBean>, BaseDataService<RegisterBean> {

    /**
     * 获取字典表,并转化为对应类型的值
     *
     * @param keyValueEnum
     * @param function
     * @param <T>
     * @return
     */
    <T> T queryRegisterValue(RegisterKeyValueEnum keyValueEnum, Function<String, T> function);

    default <T> T translate(Function<String, T> function, RegisterBean registerBean, T defaultValue) {
        try {
            if (registerBean == null || StringUtils.isBlank(registerBean.getValue())) {
                return defaultValue;
            }
            return function.apply(registerBean.getValue());
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
