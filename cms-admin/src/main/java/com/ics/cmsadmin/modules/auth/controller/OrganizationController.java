package com.ics.cmsadmin.modules.auth.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.auth.bean.OrganizationBean;
import com.ics.cmsadmin.modules.auth.service.OrganizationService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * sys_organization controller
 * Created by lvsw on 2018-29-21 10:10:05.
 */
@Api(description = "组织")
@RestController
@RequestMapping("/organization")
public class OrganizationController {

    @Resource
    private OrganizationService organizationService;

    @Authorize(AuthorizeEnum.ORGANIZATION_QUERY)
    @ApiOperation(value = "查询组织信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(organizationService.queryById(id));
    }

    @Authorize(AuthorizeEnum.ORGANIZATION_INSERT)
    @ApiOperation(value = "新增组织信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody OrganizationBean organizationBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(organizationService.insert(organizationBean));
    }

    @Authorize(AuthorizeEnum.ORGANIZATION_DELETE)
    @ApiOperation(value = "删除组织信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
        return new ApiResponse(organizationService.delete(id));
    }

    @Authorize(AuthorizeEnum.ORGANIZATION_UPDATE)
    @ApiOperation(value = "更新组织信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody OrganizationBean organizationBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(organizationService.update(id, organizationBean));
    }

    @Authorize(AuthorizeEnum.ORGANIZATION_QUERY)
    @ApiOperation(value = "分页查询组织信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody OrganizationBean organizationBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(organizationService.list(organizationBean, pageBean));
    }

    @Authorize(AuthorizeEnum.ORGANIZATION_QUERY_ALL_USER)
    @ApiOperation(value = "按组织分类查询所有的用户")
    @PostMapping("/listAllUserGroupByOrgain")
    public ApiResponse listAllUserGroupByOrgain() {
        return new ApiResponse(organizationService.listAllUserGroupByOrgain());
    }

    @Authorize
    @ApiOperation(value = "根据登录用户分页查询组织信息")
    @PostMapping("/listByLoginUserId")
    public ApiResponse listByLoginUserId(@RequestBody OrganizationBean organizationBean,
                                         HttpServletRequest request) {
        PageBean pageBean = new PageBean(0, Integer.MAX_VALUE);
        return new ApiResponse(organizationService.listByLoginUserId(organizationBean,
            SsoUtils.getLoginUserId(request), pageBean));
    }

    @Authorize(AuthorizeEnum.ORGANIZATION_QUERY)
    @ApiOperation(value = "查询组织信息")
    @PostMapping("listAllChildren")
    public ApiResponse listAllChildren() {
        return new ApiResponse(organizationService.listAllChildren());
    }
}
