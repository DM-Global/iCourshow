package com.ics.cmsadmin.student;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Main {
 
    public static void main(String[] args) {
 
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHssmm");
        String beforeS = TimeMachine.now().format(dateTimeFormatter);
        System.out.println("beforeS " + beforeS);
 
        LocalDateTime fixTime = LocalDateTime.parse("20081212085959", dateTimeFormatter);
        TimeMachine.useFixedClockAt(fixTime);
 
        String afterS = TimeMachine.now().format(dateTimeFormatter);
        System.out.println("afterS "+afterS);
 
    }
 
    public static class TimeMachine {
 
        private static Clock clock = Clock.systemDefaultZone();
        private static ZoneId zoneId = ZoneId.systemDefault();
 
        public static LocalDateTime now() {
            return LocalDateTime.now(getClock());
        }
 
        public static void useFixedClockAt(LocalDateTime date) {
            clock = Clock.fixed(date.atZone(zoneId).toInstant(), zoneId);
        }
 
        public static void useSystemDefaultZoneClock() {
            clock = Clock.systemDefaultZone();
        }
 
        private static Clock getClock() {
            return clock;
        }
    }
}
