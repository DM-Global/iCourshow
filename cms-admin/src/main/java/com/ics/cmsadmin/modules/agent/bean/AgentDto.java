package com.ics.cmsadmin.modules.agent.bean;


import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


/**
 * a_agent_info
 * 代理商
 * <p>
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Data
@Builder
public class AgentDto {

    @NotNull(message = "代理商基本信息不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Valid
    private AgentInfoBean agentInfo;
    @NotNull(message = "用户登陆基本信息不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Valid
    private SysUser userInfo;
    private AgentShareRuleBean shareRule;
    private AgentAccountBean account;
    private AgentDaySummaryBean agentDaySummary;

    @Tolerate
    public AgentDto() {
    }
}
