package com.ics.cmsadmin.modules.app.utils;

import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.function.Function;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-06-26 11:20
 */
@Component
public final class AppUtils {
    private static RegisterService registerService;

    @Resource
    public void setRegisterService(RegisterService registerService) {
        AppUtils.registerService = registerService;
    }

    public static String getStudentShareUrl(String studentId) {
        String wechatBaseUrl = registerService.queryRegisterValue(RegisterKeyValueEnum.WECHAT_BASE_URL, Function.identity());
//        String wechatBaseUrl = "http://weixin-test.icourshow.com";
        return String.format("%s/#/guidePage?recommendStudentId=%s", wechatBaseUrl, studentId);
    }
}
