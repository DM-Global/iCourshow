package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Sandwich on 2018-09-17
 */
@Data
@Component
@ConfigurationProperties(prefix = "polyv")
public class PolyvConfig {

    private String userId;
    private String secretKey;
    private String writeToken;
    private String readToken;
}
