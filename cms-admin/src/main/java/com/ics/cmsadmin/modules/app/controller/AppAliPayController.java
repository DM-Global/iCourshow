package com.ics.cmsadmin.modules.app.controller;

import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.ics.cmsadmin.modules.basic.bean.AliPayBean;
import com.jpay.alipay.AliPayApi;
import com.jpay.alipay.AliPayApiConfig;
import com.jpay.util.StringUtils;
import com.jpay.vo.AjaxResult;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by Sandwich on 2018/12/31
 */
@Api(description = "app支付宝支付相关接口", tags = "支付宝支付接口")
@RestController
@RequestMapping("app/alipay")
@Log4j2
public class AppAliPayController extends AliPayApiController {
    @Autowired
    private AliPayBean aliPayBean;

    private AjaxResult result = new AjaxResult();

    /**
     * app支付
     */
    @RequestMapping(value = "/appPay")
    @ResponseBody
    public AjaxResult appPay() {
        try {
            AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
            model.setBody("我是测试数据-By Javen");
            model.setSubject("App支付测试-By Javen");
            model.setOutTradeNo(StringUtils.getOutTradeNo());
            model.setTimeoutExpress("30m");
            model.setTotalAmount("0.01");
            model.setPassbackParams("callback params");
            model.setProductCode("QUICK_MSECURITY_PAY");
            String orderInfo = AliPayApi.startAppPay(model, aliPayBean.getDomain() + "/alipay/notify_url");
            result.success(orderInfo);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            result.addError("system error:" + e.getMessage());
        }
        return result;
    }

    @RequestMapping(value = "/return_url")
    @ResponseBody
    public String return_url(HttpServletRequest request) {
        try {
            // 获取支付宝GET过来反馈信息
            Map<String, String> map = AliPayApi.toMap(request);
            for (Map.Entry<String, String> entry : map.entrySet()) {
                System.out.println(entry.getKey() + " = " + entry.getValue());
            }

            boolean verify_result = AlipaySignature.rsaCheckV1(map, aliPayBean.getPublicKey(), "UTF-8",
                "RSA2");

            if (verify_result) {// 验证成功
                // TODO 请在这里加上商户的业务逻辑程序代码
                System.out.println("return_url 验证成功");

                return "success";
            } else {
                System.out.println("return_url 验证失败");
                // TODO
                return "failure";
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "failure";
        }
    }

    @RequestMapping(value = "/notify_url")
    @ResponseBody
    public String notify_url(HttpServletRequest request) {
        try {
            // 获取支付宝POST过来反馈信息
            Map<String, String> params = AliPayApi.toMap(request);

            for (Map.Entry<String, String> entry : params.entrySet()) {
                System.out.println(entry.getKey() + " = " + entry.getValue());
            }

            boolean verify_result = AlipaySignature.rsaCheckV1(params, aliPayBean.getPublicKey(), "UTF-8",
                "RSA2");

            if (verify_result) {// 验证成功
                // TODO 请在这里加上商户的业务逻辑程序代码 异步通知可能出现订单重复通知 需要做去重处理
                System.out.println("notify_url 验证成功succcess");
                return "success";
            } else {
                System.out.println("notify_url 验证失败");
                // TODO
                return "failure";
            }
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return "failure";
        }
    }


    @Override
    public AliPayApiConfig getApiConfig() {
        return AliPayApiConfig.New()
            .setAppId(aliPayBean.getAppId())
            .setAlipayPublicKey(aliPayBean.getPublicKey())
            .setCharset("UTF-8")
            .setPrivateKey(aliPayBean.getPrivateKey())
            .setServiceUrl(aliPayBean.getServerUrl())
            .setSignType("RSA2")
            .build();
    }
}
