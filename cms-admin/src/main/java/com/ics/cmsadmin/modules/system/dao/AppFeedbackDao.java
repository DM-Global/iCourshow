package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.system.bean.AppFeedbackBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AppFeedbackDao {

    int insert(@Param("bean") AppFeedbackBean appFeedbackBean);

    AppFeedbackBean queryById(@Param("id") String id);

    List<AppFeedbackBean> list(@Param("bean") AppFeedbackBean appFeedbackBean,
                               @Param("page") PageBean pageBean);

    long count(@Param("bean") AppFeedbackBean appFeedbackBean);
}
