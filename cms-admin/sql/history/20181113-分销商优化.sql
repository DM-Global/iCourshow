UPDATE sys_menu SET menu_id = 'salesman' WHERE menu_id = 'agent';
UPDATE sys_menu SET parent_id = 'salesman' WHERE parent_id = 'agent';

DELETE FROM sys_role_authorize WHERE auth_code IN (
	SELECT auth_code FROM sys_authorize WHERE menu_id IN (
		'accountDetail',
		'agentAccount',
		'agentList',
		'applyWithdrawCash',
		'myAgentInfo',
		'shareDetail'
	)
);
DELETE FROM  sys_role_menu WHERE menu_id IN
(
    'agent',
    'accountDetail',
    'agentAccount',
    'agentList',
    'applyWithdrawCash',
    'myAgentInfo',
    'shareDetail'
);
UPDATE sys_menu SET url=REPLACE (url,'/agent/','/salesman/') WHERE menu_id IN (
	'accountDetail',
	'agentAccount',
	'agentList',
	'applyWithdrawCash',
	'shareDetail'
);


INSERT INTO sys_menu(menu_id, parent_id, label, url, icon, order_no, create_time)
VALUES('agent', '__ROOT__', '我的面板', '/', 'ios-book', 9, NOW());
UPDATE sys_menu SET parent_id='agent' WHERE menu_id = 'myAgentInfo';

INSERT INTO sys_menu(menu_id, parent_id, label, url, icon, order_no, create_time)VALUES
('myRevenue', 'agent', '我的收益', '/agent/myRevenue', 'ios-book', 1, NOW()),
('myStudent', 'agent', '我的学生', '/agent/myStudent', 'ios-book', 2, NOW()),
('myDistributor', 'agent', '我的下级', '/agent/myDistributor', 'ios-book', 3, NOW()),
('myAccountHistory', 'agent', '账户流水', '/agent/myAccountHistory', 'ios-book', 4, NOW());

CREATE TABLE a_agent_day_summary(
    id INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    agent_no VARCHAR(20) NOT NULL COMMENT '分销商编号',
    parent_id VARCHAR(20) NOT NULL COMMENT '分销商父级编号',
    summary_day DATE  NOT NULL COMMENT '汇总日期',
    funs_cusm INT(11) NOT NULL DEFAULT 0 COMMENT '学生关注累积数',
    funs_newly INT(11) NOT NULL DEFAULT 0 COMMENT '当日学生关注新增数',
    buyer_cusm INT(11) NOT NULL DEFAULT 0 COMMENT '购买课程学生累积数',
    buyer_newly INT(11) NOT NULL DEFAULT 0 COMMENT '当日购买课程学生新增数',
    order_money_cusm DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额累积数',
    order_money_newly DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日订单金额新增数',
    child_agent_cusm INT(11) NOT NULL DEFAULT 0 COMMENT '下级代理商累积数',
    child_agent_newly INT(11) NOT NULL DEFAULT 0 COMMENT '当日下级代理商新增数',
    child_funs_cusm INT(11) NOT NULL DEFAULT 0 COMMENT '下级分销商学生关注累积数',
    child_funs_newly INT(11) NOT NULL DEFAULT 0 COMMENT '当日下级分销商学生关注新增数',
    child_buyer_cusm INT(11) NOT NULL DEFAULT 0 COMMENT '下级分销商购买课程学生累积数',
    child_buyer_newly INT(11) NOT NULL DEFAULT 0 COMMENT '当日下级分销商购买课程学生新增数',
    child_order_money_cusm DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额累积数',
    child_order_money_newly DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日订单金额新增数',
    self_share_money_cusm DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '自己推广学生分润累积数',
    self_share_money_newly DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日自己推广学生分润新增数',
    child_share_money_cusm DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '下级推广学生分润金额累积数',
    child_share_money_newly DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日下级推广学生分润金额新增数',
    parent_share_money_cusm DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '为上级贡献分润金额累积数',
    parent_share_money_newly DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日为上级贡献分润金额新增数',
    all_share_money_cusm DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '获取分润累积数',
    all_share_money_newly DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '当日获取分润新增数',
    create_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (id),
    UNIQUE KEY(agent_no, summary_day)
)ENGINE=INNODB  DEFAULT CHARSET=utf8 COMMENT='代理日结汇总数据';
