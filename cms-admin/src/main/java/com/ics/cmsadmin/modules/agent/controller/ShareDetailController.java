package com.ics.cmsadmin.modules.agent.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.modules.agent.bean.ShareDetailBean;
import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_share_detail controller
 * Created by lvsw on 2018-51-29 14:09:57.
 */
@Api(description = "分润明细")
@RestController
@RequestMapping("/shareDetail")
public class ShareDetailController {

    @Resource
    private ShareDetailService shareDetailService;

    @Authorize(AuthorizeEnum.SHARE_DETAIL_QUERY)
    @ApiOperation(value = "分页查询分润明细信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody ShareDetailBean shareDetailBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(shareDetailService.listByLoginUserId(shareDetailBean, loginUserId, pageBean));
    }

    @ApiOperation(value = "订单分润入账")
    @Authorize(AuthorizeEnum.ORDER_SHARE_INTO_ACCOUNT)
    @GetMapping("/orderShareIntoAccount/{orderId}")
    public ApiResponse orderShareIntoAccount(@PathVariable String orderId) {
        return new ApiResponse(shareDetailService.generateShareByOrderId(orderId));
    }
}
