package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Video;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VideoMapper extends ResourceMapper<Video, Integer> {
}
