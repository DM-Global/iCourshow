//package com.ics.cmsadmin.modules.agent.bean;
//
//
//import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
//import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
//import com.ics.cmsadmin.modules.auth.bean.SysUser;
//import lombok.Builder;
//import lombok.Data;
//import lombok.experimental.Tolerate;
//
//import javax.validation.Valid;
//import javax.validation.constraints.NotNull;
//
//
///**
//* a_agent_info
//* 代理商
//*
//* Created by lvsw on 2018-37-29 16:09:14.
//*/
//@Data
//@Builder
//public class AgentSummaryDataBean {
//    private String allShareMoney;  // 今日总收益
//    private String myShareMoney;   // 今日自己推广收益
//    private String childrenShareMoney;   // 今日下级推广收益
//
//    private String buyerNum;   // 今日购买人数
//    private String allBuyerNum;   // 购买总人数
//    private String fansNum;   // 今日关注人数
//    private String allFansNum;   // 关注总人数
//
//    private String childrenBuyerNum;   // 下级今日购买人数
//    private String childrenAllBuyerNum;   // 下级购买总人数
//    private String childrenFansNum;   // 下级今日关注人数
//    private String childrenAllFansNum;   // 下级关注总人数
//
//    @Tolerate
//    public AgentSummaryDataBean() {}
//}
