package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.*;

import java.util.List;

/**
 * q_problem 服务类
 * Created by lvsw on 2018-09-06.
 */
public interface ProblemService {

    /**
     * 根据问题id获取问题详情
     *
     * @param problemId 问题id
     */
    ProblemDto queryDetailsByProblemId(String problemId);

    /**
     * 根据题集id获取问题详情
     *
     * @param problemSetId 题集id
     */
    List<ProblemDto> queryDetailsByProblemSetId(String problemSetId);

    /**
     * 更新问题
     */
    boolean update(String id, ProblemBean bean);

    /**
     * 根据问题id查询问题信息
     */
    public ProblemBean queryById(String id);

    /**
     * 根据查询条件查询列表并汇总
     *
     * @param t 查询条件
     * @return 查询结果
     */
    PageResult list(ProblemBean t, PageBean page);

    /**
     * 添加问题
     *
     * @param problemBean  问题信息
     * @param answersBeans 答案内容
     * @param tipsBeans    提示内容
     */
    boolean insertProblem(ProblemBean problemBean, List<AnswersBean> answersBeans, List<TipsBean> tipsBeans);

    /**
     * 更新问题
     *
     * @param problemBean  问题信息
     * @param answersBeans 答案内容
     * @param tipsBeans    提示内容
     * @param problemId    要更新的问题id
     */
    boolean updateProblem(ProblemBean problemBean, List<AnswersBean> answersBeans, List<TipsBean> tipsBeans, String problemId);

    /**
     * 删除问题
     *
     * @param problemId
     * @return
     */
    boolean delete(String problemId);

    /**
     * 为知识点,主题,单元更新题集数量
     */
    void updateProblemNum4Level(ProblemSetBean problemSetBean);

}
