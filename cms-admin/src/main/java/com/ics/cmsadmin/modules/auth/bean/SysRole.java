package com.ics.cmsadmin.modules.auth.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * Created by lvsw on 2018/8/26.
 */
@Data
@Builder
public class SysRole {

    private String roleId;
    // 角色名
    @NotEmpty(message = "{Role.name.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "{Role.name.Length}", min = 2, max = 50, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    // 描述
    @Length(message = "{Role.description.Length}", min = 2, max = 50, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public SysRole() {
    }
}
