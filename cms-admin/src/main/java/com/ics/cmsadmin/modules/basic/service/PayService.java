package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.BankPayBean;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by Sandwich on 2018-08-18
 */
public interface PayService {

    PayResponse create(OrderBean order, String openId);

    PayResponse orderNotify(String notifyData);

    RefundResponse refund(OrderBean order);

    String handleOrderAfterWxAppPaid(String xmlMsg);

    ApiResponse getWxAppPayResult(HttpServletRequest request);

    Boolean handleOrderAfterIosPaid(String orderId, Boolean isSandboxPay);

    Map<String, String> wxWithdraw(BigDecimal amount, String appOpenId);

    Map getWxWithdrawInfo(String partnerTradeNo);

    Map wxPayBank(BankPayBean bankPayBean);

    String getPkcs1FormatPublicKey() throws Exception;

    Map queryBankInfo(String partnerTradeNo);

}
