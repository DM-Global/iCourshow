package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.TreeDataService;
import com.ics.cmsadmin.modules.system.bean.MetaBean;

import java.util.List;
import java.util.Map;

/**
 * c_meta 服务类
 * Created by lvsw on 2018-03-29.
 */
public interface MetaService extends BaseService<MetaBean>,
    BaseDataService<MetaBean>, TreeDataService<MetaBean> {

    /**
     * 查询直接下级,并转为key-value map格式
     * @param parentNode
     * @return
     */
    Map<String, String> listDirectChildren2Map(String parentNode);
}
