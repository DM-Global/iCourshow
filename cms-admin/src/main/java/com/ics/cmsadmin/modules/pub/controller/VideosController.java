package com.ics.cmsadmin.modules.pub.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.pub.bean.Video;
import com.ics.cmsadmin.modules.pub.service.impl.VideoServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/videos")
public class VideosController extends ResourceController<VideoServiceImpl, Video, Integer> {
    private String userId = "30fe77e88f";
    private String secretKey = "A5070CK7yS";

    @GetMapping
    public ApiResponse get(String name, Integer subjectId, Integer courseId, Integer unitId, Integer topicId, Integer pointId,
                           @RequestParam(value = "page", defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("subjectId", subjectId);
        params.put("courseId", courseId);
        params.put("unitId", unitId);
        params.put("topicId", topicId);
        params.put("pointId", pointId);
        params.put("page", page);
        params.put("pageSize", pageSize);
        return super.get(params);
    }

    @GetMapping("/auth")
    public Map<String, String> getAuth() {
        String readToken = "e09d63ef-3836-4895-8dcc-a7e50f1f0715";
        String writeToken = "8c9f9d4a-adb5-434d-8771-f6651cd44c83";
        String ts = System.currentTimeMillis() + "";
        //String ts = "1531957542312";
        String hash = getHashValue(ts + writeToken, "MD5");
        String sign = getHashValue(secretKey + ts, "MD5");
        Map<String, String> response = new HashMap<>();
        response.put("ts", ts);
        response.put("hash", hash);
        response.put("sign", sign);
        response.put("userId", userId);
        response.put("readToken", readToken);
        return response;
    }

    @GetMapping("/polyVDetail/{vid}")
    public ResponseEntity getDetail(@PathVariable String vid) {
        String format = "json";
        //vid = vid == null ? "30fe77e88f8524c63aa1e876b7d61c59_3" : vid;

        String pTime = System.currentTimeMillis() + "";
        String str = "format=" + format + "&ptime=" + pTime + "&vid=" + vid + secretKey;
        String sign = getHashValue(str, "SHA1");

        String url = "http://api.polyv.net/v2/video/{userId}/get-video-msg?format={format}&ptime={pTime}&vid={vid}&sign={sign}";

        Map<String, String> json = new HashMap<>();
        json.put("userId", userId);
        json.put("format", format);
        json.put("pTime", pTime);
        json.put("vid", vid);
        json.put("sign", sign.toUpperCase());

        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForEntity(url, Map.class, json);
    }

    private String getHashValue(String msg, String algorithm) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert md != null;
        md.update(msg.getBytes());
        return new BigInteger(1, md.digest()).toString(16);
    }
}
