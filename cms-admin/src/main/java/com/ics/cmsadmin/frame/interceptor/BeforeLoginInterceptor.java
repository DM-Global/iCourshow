package com.ics.cmsadmin.frame.interceptor;

import com.ics.cmsadmin.frame.utils.GsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Map;

/**
 * 登陆拦截器
 * Created by Administrator on 2017/7/28.
 */
@Slf4j
@Component
public class BeforeLoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "authorization, Origin, X-Requested-With, Content-Type, Accept, LOGIN_TOKEN");
        response.setCharacterEncoding("UTF-8");
        log.info("访问路径 {}", request.getRequestURI());
        Map<String, String[]> parameterMap = request.getParameterMap();
        log.info("请求参数: {}", GsonUtils.toJson(parameterMap));
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            log.info("参数头 {} = {}", name, request.getHeader(name));
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

}
