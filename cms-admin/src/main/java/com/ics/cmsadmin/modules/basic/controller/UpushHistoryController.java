package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;
import com.ics.cmsadmin.modules.basic.service.UpushHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_upush_history controller
 * Created by Sandwich on 2019-15-13 22:06:38.
 */
@Api(description = "")
@RestController
@RequestMapping("/upushHistory")
public class UpushHistoryController {

    @Resource
    private UpushHistoryService upushHistoryService;

    @Authorize(AuthorizeEnum.UPUSH_HISTORY_QUERY)
    @ApiOperation(value = "查询信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id){
        return new ApiResponse(upushHistoryService.queryById(id));
    }

    @Authorize(AuthorizeEnum.UPUSH_HISTORY_INSERT)
    @ApiOperation(value = "新增信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody UpushHistoryBean upushHistoryBean,
                              BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(upushHistoryService.insert(upushHistoryBean));
    }

    @Authorize(AuthorizeEnum.UPUSH_HISTORY_DELETE)
    @ApiOperation(value = "删除信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id){
        return new ApiResponse(upushHistoryService.delete(id));
    }

    @Authorize(AuthorizeEnum.UPUSH_HISTORY_UPDATE)
    @ApiOperation(value = "更新信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody UpushHistoryBean upushHistoryBean,
                              BindingResult bindingResult,
                               @ApiParam("需要更新的Id") @PathVariable String id){
        if (bindingResult.hasErrors()){
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(upushHistoryService.update(id, upushHistoryBean));
    }

    @Authorize(AuthorizeEnum.UPUSH_HISTORY_QUERY)
    @ApiOperation(value = "分页查询信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody UpushHistoryBean upushHistoryBean,
                             @ApiParam("页码") @PathVariable int pageNo,
                             @ApiParam("每页条数") @PathVariable int pageSize){
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(upushHistoryService.list(upushHistoryBean, pageBean));
    }


}
