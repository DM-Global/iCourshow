package com.ics.cmsadmin.frame.constant;

/**
 * Created by Sandwich on 2018-08-09
 */
public interface Constants {

    String USER_TYPE_ADMIN = "0";
    String USER_TYPE_AGENT = "1";

    String USER_POST_TYPE_EMPLOYEE = "1";
    String USER_POST_TYPE_ASSISTANT = "2";
    String USER_POST_TYPE_MANAGER = "3";
    String USER_POST_TYPE_BOSS = "4";


    String NOT_KNOWN = "not known";

    String UNLOGIN_AGENT_NO = "UNLOGIN_AGENT_NO";   // 未登陆的代理商编号
    String ADMIN_AGENT_NO = null;                     // 超级管理员等级代理商编号

    String DISABLE_STATUS = "0";            // 禁用状态
    String ENABLE_STATUS = "1";             // 启用状态


    String REGISTER_NUMERIC_TIPS = "该配置必须为数字";
    String REGISTER_INTEGER_TIPS = "该配置必须为整数";
    String NOT_CORRECT_DISCOUNT_TIPS = "折扣的范围应该是0到1";
    String NOT_CORRECT_PRICE_FORMATE_TIPS = "vip订单价格应该是数字";
    String MAX_HOME_LIST_COUNT_TIPS = "首页滑动列表的最大显示数应该是整型数字";
    String TRIAL_ORDER_DATE_COUNT_TIPS = "免费试用天数应该是整型数字";


    String AGENT_TYPE_COMPANY = "1";    // 公司/机构
    String AGENT_TYPE_PERSONAL = "2";   // 2. 个人'

    String REGISTER_SOURCE_WECHAT = "1";
    String REGISTER_SOURCE_APP_WECHAT = "2";
    String REGISTER_SOURCE_TOURIST = "3";

    String LETTER_AND_NUMBER = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    // 去掉容易混淆的字母和数字 i I l L o O 0 1
    String LETTER_AND_NUMBER_CLEAR = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789";
    String NUMBER = "0123456789";
}
