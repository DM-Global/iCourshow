package com.ics.cmsadmin.student;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.*;
import java.util.Date;

public class SynSystemDateUtils {

    public static void main(String[] args) throws Exception {
        updateSysDateTime("2018-01-01","12:25:36");
    }
 
    /**
     * 修改系统时间
     * yyyy-MM-dd HH:mm:ss
     */
    public static void updateSysDateTime(String dataStr_,String timeStr_){
        try {
            String osName = System.getProperty("os.name");
            // Window 系统
            if (osName.matches("^(?i)Windows.*$")) {
                String cmd;
                // 格式：yyyy-MM-dd
                cmd = " cmd /c date " + dataStr_;
                Process exec = Runtime.getRuntime().exec(cmd);
                System.out.println(IOUtils.readLines(exec.getInputStream()));
                // 格式 HH:mm:ss
                cmd = " cmd /c time " + timeStr_;
                Runtime.getRuntime().exec(cmd);
                Thread.sleep(500);
                System.out.println("windows 时间修改");
                System.out.println(DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
            } else if (osName.matches("^(?i)Linux.*$")) {
                // Linux 系统 格式：yyyy-MM-dd HH:mm:ss   date -s "2017-11-11 11:11:11"
                FileWriter excutefw = new FileWriter("/usr/updateSysTime.sh");
                BufferedWriter excutebw=new BufferedWriter(excutefw);
                excutebw.write("date -s \"" + dataStr_ +" "+ timeStr_ +"\"\r\n");
                excutebw.close();
                excutefw.close();
                String cmd_date ="sh /usr/updateSysTime.sh";
                Runtime.getRuntime().exec(cmd_date);
                System.out.println("cmd :" + cmd_date + " date :" + dataStr_ +" time :" + timeStr_);
                System.out.println("linux 时间修改");
            } else {
                System.out.println("操作系统无法识别");
            }
        } catch (IOException e) {
            e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 
}
