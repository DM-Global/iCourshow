package com.ics.cmsadmin.student;

import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2018-02-22 16:12
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class TestOrder {
    @Resource
    private OrderService orderService;
    @Test
    public void orderCreate() {
        OrderBean orderBean = orderService.create(
            OrderBean.builder().packageId("4")
                .orderType(2)
                .userId("001797c05f4440dd8d75085aa295b687")
                .build(),
            "001797c05f4440dd8d75085aa295b687");
        log.info(orderBean.toString());
    }

    @Test
    public void orderCreate2() {
        OrderBean orderBean = orderService.createOrderByAccount(
            OrderBean.builder().packageId("6")
                .orderType(2)
                .build(),
            "9e173d7c75b74d639ab2ffe717954360");
        log.info(orderBean.toString());
    }
}
