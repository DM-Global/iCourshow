# 权限设计

1. 定义权限码枚举类
```java
public enum AuthorizeEnum {
    USER_INSERT(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "新增用户"),
    USER_UPDATE(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "更新用户"),
    USER_DELETE(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "删除用户"),
    USER_QUERY(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.QUERY, "查询用户"),
    USER_RESET_PASSWORD(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "重置用户密码");
        
    private AuthorizeGroupEnum group;       // 权限码分组信息
    private AuthorizeTypeEnum type;         // 权限码操作类型,目前只有OPERATE 操作, QUERY 修改两种类型
    private String description;             // 权限码描述
}
```
2. 定义权限分组枚举类
```java
public enum AuthorizeGroupEnum {
    /**
    * 用户组
    */
    USER("userManage");

    private String menuId;      // 权限分组对应的菜单id
}
```

3. 定义权限注解
```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Authorize {
    /**
     * 必须要全部包含的权限,才能执行
     * @return
     */
    AuthorizeEnum[] value() default {};

    /**
     * 只需要包含其中一个权限码就能执行
     * @return
     */
    AuthorizeEnum[] any() default {};
}
```

4. 在需要使用的权限的方法上加上注解`Authorize`
```java
public class UserController {

    @Resource
    private UserService userService;

    @Authorize(AuthorizeEnum.USER_QUERY)
    public ApiResponse queryById(@PathVariable String userId){
        return new ApiResponse(userService.queryById(userId));
    }
}
```

5. 新增权限拦截器
```java
@Component
public class AuthorizeInterceptor extends HandlerInterceptorAdapter {
    @Resource
    private AuthorizeService authorizeService;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LoginInfo loginUser = SsoUtils.loadLoginInfoFromRedis(request);
        if (loginUser == null){
            HttpUtils.setJsonDataRespone(response, new ApiResponse(ApiResult.LOGIN_PAST_DUE), 401);
            return false;
        }
        // 获取登陆甩逇权限码集合
        List<String> userAuthorizes = authorizeService.listAuthorizesByUserId(loginUser.getLoginId());
        // 判断用户是否有权限操作
        return checkUserAuthorize(handler, userAuthorizes, response);
    }
}
```

6. 新增初始化权限码的操作(启动程序自动往数据配置权限码)
 > 程序启动后去扫描所有的加了注解`@Controller`的类  
 > 扫描改类的所有的方法,吧加了`Authorize`的权限码全部存到数据库中  

7. 总结 
    `controller method`  
    -> 在开发过程中就配置对应的权限码(权限码与菜单相关联)   
    -> 程序启动后把用到的权限码全部保存到数据库中  
    -> 在页面配置角色与菜单与权限的关系  
    -> 访问api请求,进入拦截器,判断用户是否有权限访问  
    -> 若有,这继续访问,否则返回无权限消息  

# js根据权限展示或隐藏按钮
- 以`src/views/auth/UserManage.vue`为例子
```$html
<Button type="primary" v-permission="'USER_INSERT'" icon="plus-round" @click="showAdd()">新增用户</Button>
```
> `v-permission="'USER_INSERT'"` 

```js
h('Button', {
    props: {
        type: 'primary',
        size: 'small'
    },
    style: {
        marginRight: '5px'
    },
    on:{
        click: () => this.showUpdate(params.index)
    },
    directives:[{
        name: 'permission',
        value: 'USER_UPDATE'
    }]
}, '修改')
```
```js
    directives:[{
        name: 'permission',
        value: 'USER_UPDATE'
    }]
```
