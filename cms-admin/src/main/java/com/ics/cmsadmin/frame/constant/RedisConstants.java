package com.ics.cmsadmin.frame.constant;

import com.ics.cmsadmin.frame.utils.RedisUtils;
import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import org.apache.commons.lang3.StringUtils;

import static com.ics.cmsadmin.modules.system.steward.SystemServices.registerService;

public class RedisConstants {

    private static final String IMAGE_VERIFY_CODE = "imageVerifyCode:%s";       // 图形验证码key
    private static final long IMAGE_VERIFY_CODE_TTL = 10 * 60;               // 图形验证码ttl
    private static final String SMS_VERIFY_CODE = "smsVerifyCode:%s:%s";           // 短信验证码 key
    private static final long SMS_VERIFY_CODE_TTL = 10 * 60;                 // 短信验证码ttl
    private static final String USER_LOGIN_IS_LOCK = "studentLoginIsLock:%s";                 // 短信验证码ttl
    private static final String LOGIN_ERROR_COUNT = "loginErrorCount:%s";       // 登陆错误次数

    /**
     * 保存短信验证码到reids
     *
     * @param smsTypeEnum 短信验证码类型
     * @param phone       手机号
     * @param smsCode     短信验证码
     */
    public static void saveSmsCode(SmsTypeEnum smsTypeEnum, String phone, String smsCode) {
        if (smsTypeEnum == null || StringUtils.isBlank(phone) || StringUtils.isBlank(smsCode)) {
            return;
        }
        saveSmsCode(smsTypeEnum.name(), phone, smsCode);
    }

    public static void saveSmsCode(String smsType, String phone, String smsCode) {
        if (StringUtils.isBlank(smsType)) {
            return;
        }
        RedisUtils.set(String.format(SMS_VERIFY_CODE, smsType, phone), smsCode, SMS_VERIFY_CODE_TTL);
    }

    /**
     * 从redis获取短信验证码
     *
     * @param smsTypeEnum 短信验证码类型
     * @param phone       手机号
     * @return
     */
    public static String getSmsCode(SmsTypeEnum smsTypeEnum, String phone) {
        if (smsTypeEnum == null || StringUtils.isBlank(phone)) {
            return null;
        }
        return getSmsCode(smsTypeEnum.name(), phone);
    }

    public static String getSmsCode(String smsType, String phone) {
        if (StringUtils.isBlank(smsType)) {
            return null;
        }
        return RedisUtils.get(String.format(SMS_VERIFY_CODE, smsType, phone));
    }

    /**
     * 延长短信验证码的有效期
     *
     * @param smsTypeEnum 短信验证码类型
     * @param phone       手机号
     */
    public static void expireSmsCode(SmsTypeEnum smsTypeEnum, String phone) {
        RedisUtils.expire(String.format(SMS_VERIFY_CODE, smsTypeEnum.name(), phone), SMS_VERIFY_CODE_TTL);
    }

    /**
     * 删除短信验证码
     *
     * @param smsTypeEnum 短信验证码类型
     * @param phone       手机号
     */
    public static void deleteSmsCode(SmsTypeEnum smsTypeEnum, String phone) {
        if (smsTypeEnum == null || StringUtils.isBlank(phone)) {
            return;
        }
        RedisUtils.delete(String.format(SMS_VERIFY_CODE, smsTypeEnum.name(), phone));
    }

    public static void deleteSmsCode(String smsType, String phone) {
        if (StringUtils.isBlank(smsType)) {
            return;
        }
        RedisUtils.delete(String.format(SMS_VERIFY_CODE, smsType, phone));
    }

    /**
     * 保存图形验证码
     *
     * @param imageCodeId 图形验证码id
     * @param imageCode   图形验证码
     */
    public static void saveImageCode(String imageCodeId, String imageCode) {
        if (StringUtils.isBlank(imageCodeId) || StringUtils.isBlank(imageCode)) {
            return;
        }
        RedisUtils.set(String.format(IMAGE_VERIFY_CODE, imageCodeId), imageCode, IMAGE_VERIFY_CODE_TTL);
    }

    /**
     * 获取图形验证码
     *
     * @param imageCodeId 图形验证码id
     * @return
     */
    public static String getImageCode(String imageCodeId) {
        if (StringUtils.isBlank(imageCodeId)) {
            return null;
        }
        return RedisUtils.get(String.format(IMAGE_VERIFY_CODE, imageCodeId));
    }

    /**
     * 删除图形验证码
     *
     * @param imageCodeId 图形验证码id
     */
    public static void deleteImageCode(String imageCodeId) {
        if (StringUtils.isBlank(imageCodeId)) {
            return;
        }
        RedisUtils.delete(String.format(IMAGE_VERIFY_CODE, imageCodeId));
    }

    /**
     * 判断用户是否被登陆锁定
     *
     * @param userName 登陆用户/登陆手机号等
     * @return 是否被锁定
     */
    public static boolean isUserLoginLock(String userName) {
        if (StringUtils.isBlank(userName)) {
            return false;
        }
        String userLockRedisKey = String.format(USER_LOGIN_IS_LOCK, userName);
        return StringUtils.isNotBlank(RedisUtils.get(userLockRedisKey));
    }

    /**
     * 更新登陆错误次数,如果错误次数超过配置的次数,则锁定用户
     *
     * @param userName 登陆用户/登陆手机号等
     */
    public static void updateLoginErrorCount(String userName) {
        if (StringUtils.isBlank(userName)) {
            return;
        }
        Integer maxErrorCount = registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ERROR_MAX_COUNT, Integer::valueOf);
        String errorCountRedisKey = String.format(LOGIN_ERROR_COUNT, userName);
        int currentErrorCount = getLoginErrorCount(userName) + 1;
        if (currentErrorCount < maxErrorCount) {
            RedisUtils.set(errorCountRedisKey, currentErrorCount + "", 600);
        } else {
            RedisUtils.delete(errorCountRedisKey);
            Integer lockTime = registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ERROR_USER_LOCK_TIME, Integer::valueOf);
            String userLockRedisKey = String.format(USER_LOGIN_IS_LOCK, userName);
            RedisUtils.set(userLockRedisKey, "lock", lockTime * 60);
        }
    }

    /**
     * 获取登陆错误次数
     *
     * @param userName 登陆用户/登陆手机号等
     * @return
     */
    private static int getLoginErrorCount(String userName) {
        try {
            if (StringUtils.isBlank(userName)) {
                return 0;
            }
            String redisKey = String.format(LOGIN_ERROR_COUNT, userName);
            return Integer.valueOf(RedisUtils.get(redisKey));
        } catch (Exception e) {
            return 0;
        }
    }
}
