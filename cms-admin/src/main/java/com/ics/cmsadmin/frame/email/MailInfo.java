package com.ics.cmsadmin.frame.email;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.apache.commons.mail.EmailAttachment;

import java.util.List;

@Data
@Builder
public class MailInfo {

    // 收件人
    private List<String> toAddress = null;
    // 抄送人地址
    private List<String> ccAddress = null;
    // 密送人
    private List<String> bccAddress = null;
    // 附件信息
    private List<EmailAttachment> attachments = null;
    // 邮件主题
    private String subject;
    // 邮件的文本内容
    private String content;

    @Tolerate
    public MailInfo() {
    }
}
