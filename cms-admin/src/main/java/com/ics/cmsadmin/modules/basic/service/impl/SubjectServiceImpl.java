package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.basic.bean.*;
import com.ics.cmsadmin.modules.basic.dao.SubjectDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created by lvsw on 2018-09-01.
 */
@Service
public class SubjectServiceImpl implements SubjectService {
    @Resource
    private SubjectDao subjectDao;
    @Resource
    private CourseService courseService;
    @Resource
    private UnitService unitService;
    @Resource
    private TopicService topicService;
    @Resource
    private PointService pointService;

    private List<Function<String, String>> functions;

    @PostConstruct
    public void postConstruct() {
        functions = new ArrayList<>();
        functions.add(id -> {
            SubjectBean bean = this.queryById(id);
            return bean != null ? bean.getName() : "";
        });
        functions.add(id -> {
            CourseBean bean = courseService.queryById(id);
            return bean != null ? bean.getName() : "";
        });
        functions.add(id -> {
            UnitBean bean = unitService.queryById(id);
            return bean != null ? bean.getName() : "";
        });
        functions.add(id -> {
            TopicBean bean = topicService.queryById(id);
            return bean != null ? bean.getName() : "";
        });
        functions.add(id -> {
            PointBean bean = pointService.queryById(id);
            return bean != null ? bean.getName() : "";
        });
    }

    @CacheDbMember(returnClass = SubjectBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public SubjectBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return subjectDao.queryById(id);
    }

    @CacheDbMember(returnClass = SubjectBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public PageResult<SubjectBean> list(SubjectBean bean, PageBean page) {
        long count = subjectDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, subjectDao.list(bean, page));
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean insert(SubjectBean bean) {
        if (bean == null) {
            return false;
        }
        return subjectDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean update(String id, SubjectBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return subjectDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return subjectDao.delete(id) == 1;
    }

    @Override
    public String findNameByLevelNode(String levelNode) {
        if (StringUtils.isBlank(levelNode)) {
            return "";
        }
        String[] split = levelNode.trim().split(",");
        int length = Math.min(split.length, functions.size());
        StringBuffer sb = new StringBuffer();
        Stream.iterate(0, n -> n + 1)
            .limit(length)
            .map(item -> functions.get(item).apply(split[item]))
            .forEach(item -> sb.append(item).append("-"));
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.lastIndexOf("-"));
        }
        return sb.toString();
    }
}
