package com.ics.cmsadmin.modules.system.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.system.bean.AppFeedbackBean;
import com.ics.cmsadmin.modules.system.service.AppFeedbackService;
import com.ics.cmsadmin.modules.system.steward.SystemRepositories;
import org.springframework.stereotype.Service;

@Service
public class AppFeedbackServiceImpl implements AppFeedbackService {

    @Override
    public boolean insert(AppFeedbackBean appFeedbackBean) {
        if (appFeedbackBean == null) {
            return false;
        }
        return SystemRepositories.appFeedbackDao.insert(appFeedbackBean) > 0;
    }

    @Override
    public AppFeedbackBean queryById(String id) {
        return SystemRepositories.appFeedbackDao.queryById(id);
    }

    @Override
    public PageResult<AppFeedbackBean> list(AppFeedbackBean appFeedbackBean, PageBean pageBean) {
        long count = SystemRepositories.appFeedbackDao.count(appFeedbackBean);
        if (count == 0) {
            return new PageResult<>();
        }
        return new PageResult<>(SystemRepositories.appFeedbackDao.list(appFeedbackBean, pageBean), count);
    }
}
