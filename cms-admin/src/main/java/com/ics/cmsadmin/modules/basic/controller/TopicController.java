package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.TopicBean;
import com.ics.cmsadmin.modules.basic.service.TopicService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Topic controller
 * Created by lvsw on 2018-09-01.
 */
@Api(description = "主题管理接口")
@RestController
@RequestMapping("/topic")
public class TopicController {

    @Resource
    private TopicService topicService;

    @Authorize(AuthorizeEnum.TOPIC_QUERY)
    @ApiOperation(value = "查询主题信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(topicService.queryById(id));
    }

    @Authorize(AuthorizeEnum.TOPIC_INSERT)
    @ApiOperation(value = "新增主题信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody TopicBean topicBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(topicService.insert(topicBean));
    }

    @Authorize(AuthorizeEnum.TOPIC_DELETE)
    @ApiOperation(value = "删除主题信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的主题 id") @PathVariable String id) {
        return new ApiResponse(topicService.delete(id));
    }

    @Authorize(AuthorizeEnum.TOPIC_UPDATE)
    @ApiOperation(value = "更新主题信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody TopicBean topicBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的主题Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(topicService.update(id, topicBean));
    }

    @Authorize(AuthorizeEnum.TOPIC_QUERY)
    @ApiOperation(value = "分页查询主题信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody TopicBean topicBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(topicService.list(topicBean, pageBean));
    }

    @Authorize(AuthorizeEnum.UNIT_QUERY)
    @ApiOperation(value = "根据单元Id查询所有主题信息")
    @GetMapping("/listAllTopicByUnitId/{unitId}")
    public ApiResponse listTopicByUnitId(@PathVariable String unitId) {
        return new ApiResponse(topicService.listAllTopicByUnitId(unitId));
    }
}
