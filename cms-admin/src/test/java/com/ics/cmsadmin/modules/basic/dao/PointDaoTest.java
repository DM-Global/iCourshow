package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.api.PolyvApi;
import com.ics.cmsadmin.frame.api.response.PolyvVideoInfo;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.bean.VideoBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;


/**
 * 用于更新所有视频知识点对应的封面
 */
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=pro"})
public class PointDaoTest {

    @Autowired
    PointDao pointDao;

    @Resource
    VideoDao videoDao;

    @Test
    public void updateVideoInfo() {
        List<VideoBean> list = videoDao.list(VideoBean.builder().build(), new PageBean(1, Integer.MAX_VALUE));
        list.forEach(videoDao -> {
            String pointId = videoDao.getPointId();
            PolyvVideoInfo videoMsg = PolyvApi.getVideoMsg(videoDao.getPolyVId());
            if (videoMsg != null) {
                pointDao.updateVideoInfo(pointId, videoMsg.getDuration(), videoMsg.getFirst_image());
            }
        });
    }

    @Test
    public void getOnePoint() {
        List<PointBean> list = pointDao.list(PointBean.builder().pointType("video").build(), new PageBean(1, 1));

    }
}
