package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.property.UmengPushConfig;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.service.UpushHistoryService;
import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeBean;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeEnum;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.umeng.upush.AndroidNotification;
import com.ics.cmsadmin.modules.umeng.upush.PushClient;
import com.ics.cmsadmin.modules.umeng.upush.android.*;
import com.ics.cmsadmin.modules.umeng.upush.ios.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Sandwich on 2019/3/26
 */
@Service
@Slf4j
@Async
public class UPushApiServiceImpl implements UPushApiService {

    private static PushClient client = new PushClient();
    @Resource
    private UmengPushConfig umengPushConfig;
    @Autowired
    private StudentService studentService;
    @Autowired
    private UpushHistoryService upushHistoryService;


    @Override
    public void sendBroadcast(UPushCustomDefined defined) {
        if (defined == null || defined.getUPushType() == null) {
            return;
        }
        UPushTypeBean apply = defined.getUPushType().apply(defined);
        sendAndroidBroadcast(apply.getTitle(), apply.getContent(), GsonUtils.toJson(defined));
        sendIOSBroadcast(apply.getContent(), GsonUtils.toJson(defined));
        UpushHistoryBean upushHistoryBean = UpushHistoryBean.builder()
            .type(defined.getUPushType().name())
            .title(apply.getTitle())
            .content(apply.getContent())
            .details(apply.getDetails())
            .extra(GsonUtils.toJson(defined))
            .isRead(true)
            .build();
        upushHistoryService.insert(upushHistoryBean);
    }

    @Override
    public void sendUnicast(String studentId, UPushCustomDefined defined) {
        if (defined == null || defined.getUPushType() == null) {
            return;
        }
        StudentBean studentBean = studentService.queryById(studentId);
        if (studentBean == null) {
            return;
        }
        UPushTypeBean apply = defined.getUPushType().apply(defined);
        UpushHistoryBean upushHistoryBean = UpushHistoryBean.builder()
            .studentId(studentId)
            .title(apply.getTitle())
            .content(apply.getContent())
            .details(apply.getDetails())
            .extra(GsonUtils.toJson(defined))
            .type(defined.getUPushType().name())
            .isRead(false)
            .build();
        upushHistoryService.insert(upushHistoryBean);

        defined.setMessageId(upushHistoryBean.getId());
        sendAndroidUnicast(studentBean.getAndroidDeviceToken(), apply.getTitle(), apply.getContent(), GsonUtils.toJson(defined));
        sendIOSUnicast(studentBean.getIosDeviceToken(), apply.getContent(), GsonUtils.toJson(defined));

    }

    @Override
    public void sendUnicast(List<String> studentIds, UPushCustomDefined defined) {
        if (defined == null || defined.getUPushType() == null) {
            return;
        }
        List<StudentBean> studentList = studentService.queryByIds(studentIds);
        if (CollectionUtils.isEmpty(studentList)) {
            return;
        }
        UPushTypeBean apply = defined.getUPushType().apply(defined);
        List<String> androidTokenList = studentList.stream()
            .map(StudentBean::getAndroidDeviceToken)
            .filter(StringUtils::isNotBlank)
            .collect(Collectors.toList());
        sendAndroidListUnicast(androidTokenList, apply.getTitle(), apply.getContent(), GsonUtils.toJson(defined));

        List<String> iosTokenList = studentList.stream()
            .map(StudentBean::getIosDeviceToken)
            .filter(StringUtils::isNotBlank)
            .collect(Collectors.toList());
        sendIosListUnicast(iosTokenList, apply.getContent(), GsonUtils.toJson(defined));

        List<String> idList = studentList.stream()
            .map(StudentBean::getId)
            .collect(Collectors.toList());
        UpushHistoryBean upushHistoryBean = UpushHistoryBean.builder()
            .title(apply.getTitle())
            .content(apply.getContent())
            .extra(GsonUtils.toJson(defined))
            .details(apply.getDetails())
            .type(defined.getUPushType().name())
            .isRead(false)
            .build();
        upushHistoryService.insertBatch(idList, upushHistoryBean);
    }

    private void sendIosListUnicast(List<String> tokenList, String content, String extra) {
        try {
            if (CollectionUtils.isEmpty(tokenList)) {
                return;
            }
            IOSUnicast unicast = new IOSUnicast(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"));
            unicast.setAlert(content);
            unicast.setBadge(0);
            unicast.setSound("default");
            if (umengPushConfig.isDevModel()) {
                unicast.setTestMode();
            }else {
                unicast.setProductionMode();
            }
            // Set customized fields
            unicast.setCustomizedField("extra", extra);
            for (int offset = 0, pageSize = 500; offset < tokenList.size(); offset += pageSize) {
                List<String> subList = tokenList.subList(offset, Math.min(tokenList.size(), offset + pageSize));
                String tokens = String.join(",", subList);
                unicast.setDeviceToken(tokens);
                client.send(unicast);
            }
        } catch (Exception e) {
            log.error("ios列播异常 {}", e);
        }
    }

    private void sendAndroidListUnicast(List<String> tokenList, String title, String content, String extra) {
        if (CollectionUtils.isEmpty(tokenList)) {
            return;
        }
        try {
            AndroidUnicast unicast = new AndroidUnicast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
            unicast.setTicker( "Android unicast ticker");
            unicast.setTitle(title);
            unicast.setText(content);
            unicast.goCustomAfterOpen(extra);
            unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            if (umengPushConfig.isDevModel()) {
                unicast.setTestMode();
            }else {
                unicast.setProductionMode();
            }
            unicast.setExtraField("extra", extra);
            for (int offset = 0, pageSize = 500; offset < tokenList.size(); offset += pageSize) {
                List<String> subList = tokenList.subList(offset, Math.min(tokenList.size(), offset + pageSize));
                String tokens = String.join(",", subList);
                unicast.setDeviceToken(tokens);
                client.send(unicast);
            }
        } catch (Exception e) {
            log.info("组播推送异常 {}", e);
        }
    }

    private Boolean sendAndroidBroadcast(String title, String content, String extra) {
        try {
            AndroidBroadcast broadcast = new AndroidBroadcast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
            broadcast.setTicker("Android broadcast ticker");
            broadcast.setTitle(title);
            broadcast.setText(content);
            broadcast.goCustomAfterOpen(extra);
            broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            if (umengPushConfig.isDevModel()) {
                broadcast.setTestMode();
            }else {
                broadcast.setProductionMode();
            }
            broadcast.setExtraField("extra", extra);
            return client.send(broadcast);
        } catch (Exception e) {
            log.info("组播推送异常 {}", e);
        }
        return false;
    }

    public Boolean sendIOSBroadcast(String content, String extra) {
        try {
            IOSBroadcast broadcast = new IOSBroadcast(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"));
            broadcast.setAlert(content);
            broadcast.setBadge(0);
            broadcast.setSound("default");
            if (umengPushConfig.isDevModel()) {
                broadcast.setTestMode();
            }else {
                broadcast.setProductionMode();
            }
            // Set customized fields
            broadcast.setCustomizedField("extra", extra);
            return client.send(broadcast);
        } catch (Exception e) {
            log.error("推送异常 {}", e);
        }
        return false;
    }

    /**
     * 推送给单个用户
     * @param token
     * @param title
     * @param content
     */
    private Boolean sendAndroidUnicast(String token, String title, String content, String extra) {
        try {
            if (StringUtils.isBlank(token)) {
                return false;
            }
            AndroidUnicast unicast = new AndroidUnicast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
            unicast.setDeviceToken(token);
            unicast.setTicker( "Android unicast ticker");
            unicast.setTitle(title);
            unicast.setText(content);
            unicast.goCustomAfterOpen(extra);
            unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
            if (umengPushConfig.isDevModel()) {
                unicast.setTestMode();
            }else {
                unicast.setProductionMode();
            }
            unicast.setExtraField("extra", extra);
            return client.send(unicast);
        } catch (Exception e) {
            log.error("安卓单播异常 {}", e);
            return false;
        }

    }

    private Boolean sendIOSUnicast(String token,String content, String customDefined) {
        try {
            if (StringUtils.isBlank(token)) {
                return false;
            }
            IOSUnicast unicast = new IOSUnicast(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"));
            unicast.setDeviceToken(token);
            unicast.setAlert(content);
            unicast.setBadge(0);
            unicast.setSound("default");
            if (umengPushConfig.isDevModel()) {
                unicast.setTestMode();
            }else {
                unicast.setProductionMode();
            }
            // Set customized fields
            unicast.setCustomizedField("custom", customDefined);
            return client.send(unicast);
        } catch (Exception e) {
            log.error("ios单播异常 {}", e);
            return false;
        }
    }











    public void sendAndroidGroupcast() throws Exception {
        AndroidGroupcast groupcast = new AndroidGroupcast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
        /*  TODO
         *  Construct the filter condition:
         *  "where":
         *	{
         *		"and":
         *		[
         *			{"tag":"test"},
         *			{"tag":"Test"}
         *		]
         *	}
         */
        JSONObject filterJson = new JSONObject();
        JSONObject whereJson = new JSONObject();
        JSONArray tagArray = new JSONArray();
        JSONObject testTag = new JSONObject();
        JSONObject TestTag = new JSONObject();
        testTag.put("tag", "test");
        TestTag.put("tag", "Test");
        tagArray.put(testTag);
        tagArray.put(TestTag);
        whereJson.put("and", tagArray);
        filterJson.put("where", whereJson);
        log.info(filterJson.toString());

        groupcast.setFilter(filterJson);
        groupcast.setTicker("Android groupcast ticker");
        groupcast.setTitle("中文的title");
        groupcast.setText("Android groupcast text");
        groupcast.goAppAfterOpen();
        groupcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.
        // For how to register a test device, please see the developer doc.
        if (umengPushConfig.isDevModel()) {
            groupcast.setTestMode();
        }else {
            groupcast.setProductionMode();
        }
        client.send(groupcast);
    }

    public void sendAndroidCustomizedcast() throws Exception {
//        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(appkey,appMasterSecret);
        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
        // TODO Set your alias here, and use comma to split them if there are multiple alias.
        // And if you have many alias, you can also upload a file containing these alias, then
        // use file_id to send customized notification.
        customizedcast.setAlias("alias", "alias_type");
        customizedcast.setTicker("Android customizedcast ticker");
        customizedcast.setTitle("中文的title");
        customizedcast.setText("Android customizedcast text");
        customizedcast.goAppAfterOpen();
        customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        if (umengPushConfig.isDevModel()) {
            customizedcast.setTestMode();
        }else {
            customizedcast.setProductionMode();
        }
//        customizedcast.setExpireTime("2019-06-11 23:08:16");
        customizedcast.setAfterOpenAction(AndroidNotification.AfterOpenAction.go_app);
        client.send(customizedcast);
    }

    public void sendAndroidCustomizedcastFile() throws Exception {
        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
        // TODO Set your alias here, and use comma to split them if there are multiple alias.
        // And if you have many alias, you can also upload a file containing these alias, then
        // use file_id to send customized notification.
        String fileId = client.uploadContents(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"), "aa" + "\n" + "bb" + "\n" + "alias");
        customizedcast.setFileId(fileId, "alias_type");
        customizedcast.setTicker("Android customizedcast ticker");
        customizedcast.setTitle("中文的title");
        customizedcast.setText("Android customizedcast text");
        customizedcast.goAppAfterOpen();
        customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        if (umengPushConfig.isDevModel()) {
            customizedcast.setTestMode();
        }else {
            customizedcast.setProductionMode();
        }
        customizedcast.setPredefinedKeyValue("timestamp", System.currentTimeMillis());
        client.send(customizedcast);
    }

    public void sendAndroidFilecast() throws Exception {
        AndroidFilecast filecast = new AndroidFilecast(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"));
        // TODO upload your device tokens, and use '\n' to split them if there are multiple tokens
        String fileId = client.uploadContents(umengPushConfig.getAndroidMap().get("appKey"), umengPushConfig.getAndroidMap().get("appMasterSecret"), "aa" + "\n" + "bb");
        filecast.setFileId(fileId);
        filecast.setTicker("Android filecast ticker");
        filecast.setTitle("中文的title");
        filecast.setText("Android filecast text");
        filecast.goAppAfterOpen();
        filecast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        client.send(filecast);
    }



    public void sendIOSGroupcast() throws Exception {
        IOSGroupcast groupcast = new IOSGroupcast(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"));
        /*  TODO
         *  Construct the filter condition:
         *  "where":
         *	{
         *		"and":
         *		[
         *			{"tag":"iostest"}
         *		]
         *	}
         */
        JSONObject filterJson = new JSONObject();
        JSONObject whereJson = new JSONObject();
        JSONArray tagArray = new JSONArray();
        JSONObject testTag = new JSONObject();
        testTag.put("tag", "iostest");
        tagArray.put(testTag);
        whereJson.put("and", tagArray);
        filterJson.put("where", whereJson);
        log.info(filterJson.toString());

        // Set filter condition into rootJson
        groupcast.setFilter(filterJson);
        groupcast.setAlert("IOS 组播测试");
        groupcast.setBadge(0);
        groupcast.setSound("default");
        groupcast.setTestMode();
        client.send(groupcast);
    }

    public void sendIOSCustomizedcast() throws Exception {
        IOSCustomizedcast customizedcast = new IOSCustomizedcast(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"));
        // TODO Set your alias and alias_type here, and use comma to split them if there are multiple alias.
        // And if you have many alias, you can also upload a file containing these alias, then
        // use file_id to send customized notification.
        customizedcast.setAlias("alias", "alias_type");
        customizedcast.setAlert("IOS 个性化测试");
        customizedcast.setBadge(0);
        customizedcast.setSound("default");
        customizedcast.setTestMode();
        client.send(customizedcast);
    }

    public void sendIOSFilecast() throws Exception {
        IOSFilecast filecast = new IOSFilecast(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"));
        // TODO upload your device tokens, and use '\n' to split them if there are multiple tokens
        String fileId = client.uploadContents(umengPushConfig.getIosMap().get("appKey"), umengPushConfig.getIosMap().get("appMasterSecret"), "aa" + "\n" + "bb");
        filecast.setFileId(fileId);
        filecast.setAlert("IOS 文件播测试");
        filecast.setBadge(0);
        filecast.setSound("default");
        filecast.setTestMode();
        client.send(filecast);
    }

}
