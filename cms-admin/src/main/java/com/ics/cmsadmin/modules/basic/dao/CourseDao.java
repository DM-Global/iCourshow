package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.CourseBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2018-09-01.
 */
@Repository
@Mapper
public interface CourseDao extends BaseDataDao<CourseBean>, BaseDao<CourseBean> {
    /**
     * 根据科目Id查询所有课程信息
     *
     * @param subjectId 科目Id
     * @return
     */
    List<CourseBean> listAllCourseBySubjectId(@Param("subjectId") String subjectId);

    List<CourseBean> findByIdsIn(@Param("list") List<String> ids);

    int bindCourseType(@Param("courseId") String courseId, @Param("courseTypeId") String courseTypeId);

    int unbindCourseType(@Param("courseId") String courseId, @Param("courseTypeId") String courseTypeId);

    int bulkUnbindCourseType(@Param("list") List<String> ids, @Param("courseTypeId") String courseTypeId);
}
