package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

import java.util.Date;

@Data
public class SearchHistory {

    private String userId;

    private String keyword;

    private Integer freq;

    private Date createTime;

    private Date updateTime;
}
