package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.auth.bean.SysUser;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.util.List;


/**
 * a_user 服务类
 * Created by lvsw on 2018-03-29.
 */
public interface UserService extends BaseDataService<SysUser>, BaseService<SysUser>, LoginBaseDataService<SysUser> {

    /**
     * 通过用户名和密码查找用户
     *
     * @param userName 用户名
     * @param password 密码
     * @return 用户信息
     */
    SysUser findUserByUserNameAndPassword(String userName, String password);

    /**
     * 通过手机号和密码查找用户
     *
     * @param mobilephone 手机号
     * @param password    密码
     * @return 用户信息
     */
    SysUser findUserByMobilephoneAndPassword(String mobilephone, String password);

    /**
     * 通过邮箱和密码查找用户
     *
     * @param email    邮箱
     * @param password 密码
     * @return 用户信息
     */
    SysUser findUserByEmailAndPassword(String email, String password);

    /**
     * 删除用户之后通过这个方法重新恢复用户状态
     *
     * @param userId 用户id
     */
    boolean recovery(String userId);

    /**
     * 重置密码
     *
     * @param userId 需要重置密码的用户
     */
    boolean resetPassword(String userId);

    /**
     * 重置密码
     *
     * @param userId      需要重置密码的用户
     * @param newPassword 新密码
     */
    boolean resetPassword(String userId, String newPassword);

    /**
     * 修改用户密码
     *
     * @param userId      用户id
     * @param oldPasswrod 旧密码
     * @param newPassword 新密码
     * @return
     */
    boolean modifyPassword(String userId, String oldPasswrod, String newPassword);

    /**
     * 根据条件查询用户
     */
    SysUser findOne(SysUser sysUser);

    /**
     * 做普通员工
     *
     * @param userId
     * @return
     */
    boolean asEmployee(String userId);

    /**
     * 做助理
     *
     * @param userId
     * @return
     */
    boolean asAssistant(String userId);

    /**
     * 做主管
     *
     * @param userId
     * @return
     */
    boolean asManager(String userId);

    /**
     * 做boss
     *
     * @param userId
     * @return
     */
    boolean asBoss(String userId);
}
