package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Topic;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TopicMapper extends ResourceMapper<Topic, Integer> {
    //List<Topic> selectAll(@Param("name") String name, @Param("subjectId") Integer subjectId, @Param("courseId") Integer courseId, @Param("unitId") Integer unitId, @Param("isActive") Boolean isActive);
}
