package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* t_upush_history
* Created by Sandwich on 2019-15-13 22:06:38.
*/
@Repository
@Mapper
public interface UpushHistoryDao extends BaseDao<UpushHistoryBean>, BaseDataDao<UpushHistoryBean> {

    void insertBatch(@Param("studentIds") List<String> studentIds, @Param("bean") UpushHistoryBean upushHistoryBean);

    int updateRead(@Param("messageId") String messageId, @Param("studentId") String studentId);
}
