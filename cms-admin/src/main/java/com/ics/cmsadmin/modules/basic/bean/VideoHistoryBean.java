package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

@Data
@Builder
public class VideoHistoryBean {

    private String id;

    /**
     * 用户id
     */
    private String userId;
    /**
     * 视频id
     */
    private String videoId;

    /**
     * 视频时长
     */
    private Long duration = 0L;

    /**
     * 上次观看视频位置
     */
    private Long position = 0L;

    /**
     * 观看次数
     */
    private Integer viewNum = 0;

    /**
     * 观看总时长
     */
    private Long totalTime = 0L;

    /**
     * 视频名称
     */
    private String videoName;

    /**
     * 知识点名称
     */
    private String pointName;

    /**
     * 知识点封面
     */
    private String pointSrc;

    private String courseName;

    // 对应知识点id
    private String pointId;
    private String topicId;
    private String unitId;
    private String courseId;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间

    @Tolerate
    public VideoHistoryBean() {
    }
}
