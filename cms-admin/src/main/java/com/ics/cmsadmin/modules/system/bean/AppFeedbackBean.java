package com.ics.cmsadmin.modules.system.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

@Builder
@Data
public class AppFeedbackBean {
    private String id;          // id
    private String userId; // 用户id
    private String content; // 反馈内容
    private String phone; // 手机号
    private String wechat; // 微信号
    private String email; // 邮箱
    private String image1; // 反馈图片1
    private String image2; // 反馈图片1
    private String image3; // 反馈图片1
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;


    @Tolerate
    public AppFeedbackBean() {
    }
}
