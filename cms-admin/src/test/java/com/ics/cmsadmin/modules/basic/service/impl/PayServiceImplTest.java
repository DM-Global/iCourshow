package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.property.AppWxPayConfig;
import com.ics.cmsadmin.modules.basic.service.PayService;
import com.jfinal.core.Controller;
import com.jpay.secure.RSAUtils;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.Cipher;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Sandwich on 2019/5/28
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class PayServiceImplTest {

    private static final Controller ServletActionContext = new Controller() {
    };
    @Autowired
    PayService payService;
    @Autowired
    private AppWxPayConfig appWxPayConfig;


    @Test
    public void transfersTest() {
        payService.wxWithdraw(new BigDecimal(0.30),"803601537283531345");
    }

    @Test
    public void testAmount(){
        BigDecimal amount = new BigDecimal(1.321);
        amount = amount.setScale(2,BigDecimal.ROUND_DOWN);
        String amountStr = amount.multiply(new BigDecimal(100)).setScale(0,BigDecimal.ROUND_DOWN).toString();
    }


    @Test
    public void encriptBankCard(){
        String publicKey = "MIIBCgKCAQEAou4BnykCsQE9L5y4RfAlaZeJ4yCdecl9hCndD6SP46lMFo6CzCcnzUK7vAzctRESw2b76DO3UALRxsEzlySWMhlnFTdw7O89O08EvLRn691BizJx90y4vTs1RGs+/OyDSaf7uSZyLA7wBwfGPb8HIFUJ0rZyrZ5it0llcJ7EyZ0l0xViCNtW5O2zQsFuqeW+NsDFB+AvmWPFmyAV8iNh7sxo5Lva16pDiRh5nWbI92JgGpXKO1RU8GbJTLxgi13QtQHDEtCLAffxwJrws0v/lknnnZhAL/vAatcrtQuegN+fi+ng9tgwSL6lSCsuLo25OEdmZqEVjVfNMGR6T9IAsQIDAQAB";
        try {
            byte[] bytes = getEncSymmKey(publicKey.getBytes(),"6222083602011694871".getBytes());
            String encriptBankCard = RSAUtils.encryptByPublicKeyByWx("6222083602011694871", publicKey);
            log.info(encriptBankCard);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private byte[] getEncSymmKey(byte[] pubkeyBuf, byte[] randomKey) throws Exception {
//        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(pubkeyBuf);
//        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
//        Key publicKey = keyFactory.generatePublic(pubKeySpec);
//
//        // 对数据加密
//        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
//        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
//
//        return cipher.doFinal(randomKey);
//    }

    private byte[] getEncSymmKey(byte[] pubkeyBuf, byte[] randomKey) throws Exception {

        byte[] bX509PubKeyHeader = { 48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127,
            -115, 0 };
        byte[] bPubKey = new byte[pubkeyBuf.length + bX509PubKeyHeader.length];
        System.arraycopy(bX509PubKeyHeader, 0, bPubKey, 0, bX509PubKeyHeader.length);
        System.arraycopy(pubkeyBuf, 0, bPubKey, bX509PubKeyHeader.length, pubkeyBuf.length);

        X509EncodedKeySpec rsaKeySpec = new X509EncodedKeySpec(bPubKey);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey rsaPubKey = keyFactory.generatePublic(rsaKeySpec);

        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, rsaPubKey);

        return cipher.doFinal(randomKey);
    }

}
