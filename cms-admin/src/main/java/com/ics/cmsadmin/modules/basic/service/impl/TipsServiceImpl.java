package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.modules.basic.dao.TipsDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.modules.basic.bean.TipsBean;
import com.ics.cmsadmin.modules.basic.service.TipsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lvsw on 2018-09-06.
 */
@Service
public class TipsServiceImpl implements TipsService {
    @Resource
    private TipsDao tipsDao;

    @CacheDbMember(returnClass = TipsBean.class, group = CacheGroupEnum.CACHE_PROBLEM_GROUP)
    @Override
    public List<TipsBean> queryByProblemId(String problemId) {
        return tipsDao.queryByProblemId(problemId);
    }
}
