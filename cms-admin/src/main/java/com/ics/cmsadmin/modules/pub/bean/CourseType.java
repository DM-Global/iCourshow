package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class CourseType extends Resource<Integer> {
    private String name;
    private Boolean isActive;
    private String description;
}
