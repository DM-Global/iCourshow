package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.modules.sso.LoginInfo;

/**
 * 权限控制服务
 */
public interface AccessService {

    /**
     * 登录用户能否控制指定用户
     */
    boolean canAccessForUser(String loginUserId, String userId);

    /**
     * 登录用户能否控制指定代理商
     */
    boolean canAccessForAgent(String loginUserId, String agentNo);

    /**
     * 登录用户能否控制指定学生
     */
    boolean canAccessForStudent(String loginUserId, String studentId);

    /**
     * 根据登陆用户id获取登陆信息
     *
     * @param loginUserId
     * @return
     */
    LoginSearchBean queryLoginInfoByLoginUserId(String loginUserId);
}
