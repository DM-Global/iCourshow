package com.ics.cmsadmin.modules.agent.controller;

import com.github.pagehelper.Page;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.agent.bean.AgentDto;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.agent.bean.StudentOrderBean;
import com.ics.cmsadmin.modules.agent.steward.AgentUtils;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.ics.cmsadmin.modules.agent.steward.AgentServices.agentInfoService;
import static com.ics.cmsadmin.modules.auth.steward.AuthServices.accessService;

/**
 * t_agent controller
 * Created by lvsw on 2018-51-29 14:09:57.
 */
@Api(description = "代理商")
@RestController
@RequestMapping("/agent")
public class AgentInfoController {


    @Authorize(AuthorizeEnum.AGENT_DETAIL_QUERY)
    @ApiOperation(value = "查询代理商信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id,
                                 HttpServletRequest request) {
        if (!accessService.canAccessForAgent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        return new ApiResponse(agentInfoService.queryById(id));
    }

    @Authorize(AuthorizeEnum.AGENT_DETAIL_QUERY)
    @ApiOperation(value = "查询代理商详情信息")
    @GetMapping(value = "/queryMoreInfo/{id}")
    public ApiResponse queryMoreInfo(@ApiParam(value = "id") @PathVariable String id,
                                     HttpServletRequest request) {
        if (!accessService.canAccessForAgent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        return new ApiResponse(agentInfoService.queryMoreInfoById(id));
    }

    @Authorize(AuthorizeEnum.AGENT_DETAIL_QUERY)
    @ApiOperation(value = "查询一级代理商信息")
    @GetMapping(value = "/queryLevelAgent")
    public ApiResponse queryLevelAgent() {
        return new ApiResponse(agentInfoService.queryLevelAgent());
    }

    @Authorize(AuthorizeEnum.AGENT_INSERT)
    @ApiOperation(value = "新增代理商信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody AgentDto agentDto,
                              BindingResult bindingResult,
                              HttpServletRequest request) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        agentDto.getAgentInfo().setSalesman(SsoUtils.getLoginUserId(request));
        return new ApiResponse(agentInfoService.insertAgent(agentDto));
    }


    @Authorize(AuthorizeEnum.AGENT_UPDATE)
    @ApiOperation(value = "更新代理商信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody AgentDto agentDto,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id,
                              HttpServletRequest request) {
        if (!accessService.canAccessForAgent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(agentInfoService.updateAgent(id, agentDto));
    }

    @Authorize(AuthorizeEnum.AGENT_UPDATE_STATUS)
    @ApiOperation(value = "禁用代理商信息")
    @PostMapping(value = "/disable/{id}")
    public ApiResponse disable(@ApiParam("需要更新的Id") @PathVariable String id,
                               HttpServletRequest request) {
        if (!accessService.canAccessForAgent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        return new ApiResponse(agentInfoService.disable(id));
    }

    @Authorize(AuthorizeEnum.AGENT_UPDATE_STATUS)
    @ApiOperation(value = "启用代理商信息")
    @PostMapping(value = "/enable/{id}")
    public ApiResponse enable(@ApiParam("需要更新的Id") @PathVariable String id,
                              HttpServletRequest request) {
        if (!accessService.canAccessForAgent(SsoUtils.getLoginUserId(request), id)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        return new ApiResponse(agentInfoService.enable(id));
    }

    @Authorize(AuthorizeEnum.AGENT_RESET_PASSWORD)
    @ApiOperation(value = "重置代理商登陆密码")
    @GetMapping(value = "/resetPassword/{agentNo}")
    public ApiResponse resetPassword(@ApiParam("需要重置密码的代理商编号") @PathVariable String agentNo,
                                     HttpServletRequest request) {
        if (!accessService.canAccessForAgent(SsoUtils.getLoginUserId(request), agentNo)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentInfoService.resetPassword(agentNo, loginUserId));
    }

    @Authorize(AuthorizeEnum.AGENT_QUERY)
    @ApiOperation(value = "分页查询代理商信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody AgentInfoBean agentBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentInfoService.listByLoginUserId(agentBean, loginUserId, pageBean));
    }

    @Authorize(AuthorizeEnum.AGENT_CHANGE_SALESMAN)
    @ApiOperation(value = "分配分销商给业务员")
    @PostMapping("/changeSalesman/{agentNo}/{salesmane}")
    public ApiResponse changeSalesman(@ApiParam("待分配的分销商") @PathVariable String agentNo,
                                      @ApiParam("被转移的业务员") @PathVariable String salesmane,
                                      HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (!accessService.canAccessForAgent(loginUserId, agentNo)) {
            return new ApiResponse(ApiResultEnum.VALIDATE_ERROR, "您无权操作该代理商");
        }
        if (!(StringUtils.equals(loginUserId, salesmane) || accessService.canAccessForUser(loginUserId, salesmane))) {
            throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "您无权操作该业务员");
        }
        return new ApiResponse(agentInfoService.changeSalesman(agentNo, salesmane));
    }
}
