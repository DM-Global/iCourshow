package com.ics.cmsadmin.frame.config;

import com.ics.cmsadmin.frame.interceptor.*;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.HibernateValidatorConfiguration;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.MessageSourceResourceBundleLocator;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Created by Sandwich on 2018-08-13
 */
@Configuration
public class LoginConfiguration implements WebMvcConfigurer {

    @Resource
    private ApiLoginInterceptor apiLoginInterceptor;      // api登陆校验
    @Resource
    private AppLoginInterceptor appLoginInterceptor;
    @Resource
    private AuthorizeInterceptor authorizeInterceptor;  // 权限校验
    @Resource
    private BeforeLoginInterceptor beforeLoginInterceptor; // 跨域访问

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource =
            new ResourceBundleMessageSource();
        messageSource.setBasename("validator/message");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }

    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class)
            .configure()
            .addProperty(HibernateValidatorConfiguration.FAIL_FAST, "true")
            .messageInterpolator(new ResourceBundleMessageInterpolator(
                new MessageSourceResourceBundleLocator(messageSource())))
            .buildValidatorFactory();
        return validatorFactory.getValidator();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] webExcludePaths = {
            "/", "/api/**", "/error", "logout", "/oauth2/**",
            // 排除资源请求
            "*.css", "*.js", "*.png", "/dist/**", "/pay/**",
            // 排除swagger相关路径
            "/swagger-ui.html", "/configuration/*", "/swagger-resources/**", "/v2/api-docs", "/webjars/**",
            //
            "/app/**"
        };
        // 所有请求登陆之前拦截器: 用于配置跨域请求等
        InterceptorRegistration beforeLoginRegistry = registry.addInterceptor(beforeLoginInterceptor);
        beforeLoginRegistry.addPathPatterns("/**");

        // cms-web 权限拦截请求
        InterceptorRegistration authorizeRegistry = registry.addInterceptor(authorizeInterceptor);
        authorizeRegistry.addPathPatterns("/**");
        authorizeRegistry.excludePathPatterns(webExcludePaths);

        // app 登陆拦截请求
        InterceptorRegistration appLoginRegistration = registry.addInterceptor(appLoginInterceptor);
        appLoginRegistration.addPathPatterns("/app/**");

        //app 微信支付拦截请求
//        registry.addInterceptor(new WxPayInterceptor()).addPathPatterns("/app/wxpay/**");
        //app支付宝支付拦截请求
        registry.addInterceptor(new AliPayInterceptor()).addPathPatterns("/app/alipay/**");

//        appLoginRegistration.excludePathPatterns("/app/common/**");
//        appLoginRegistration.excludePathPatterns("/app/home/**");
    }
}
