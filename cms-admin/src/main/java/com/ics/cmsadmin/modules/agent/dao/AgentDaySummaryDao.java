package com.ics.cmsadmin.modules.agent.dao;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.agent.bean.AgentDaySummaryBean;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.agent.bean.StudentOrderBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface AgentDaySummaryDao {
    /**
     * 插入基础的代理商日结数据
     */
    void insertAgentBaseDaySummery(@Param("formatDate") String formatDate);

    /**
     * 统计关注数量
     */
    void summeryFuns(@Param("formatDate") String formatDate);

    /**
     * 统计购买课程学生数和购买金额
     */
    void summeryBuyerAndOrderMoney(@Param("formatDate") String formatDate);

    /**
     * 统计下级代理商数量
     */
    void summeryChildAgent(@Param("formatDate") String formatDate);

    /**
     * 统计自己推广分润金额
     */
    void summerySelfShareMoney(@Param("formatDate") String formatDate);

    /**
     * 统计为上级贡献的分润金额
     */
    void summeryParentShareMoney(@Param("formatDate") String formatDate);

    /**
     * 统计下级为自己贡献的分润金额
     */
    void summeryChildShareMoney(@Param("formatDate") String formatDate);

    /**
     * 统计自己获取的总分润金额
     */
    void summeryAllShareMoney(@Param("formatDate") String formatDate);

    /**
     * 统计下级分销商关注人数
     */
    void summeryChildAgentFuns(String formatDate);

    /**
     * 统计下级分销商关购买人数和购买订单
     */
    void summeryChildAgentBuyerAndOrderMoney(String formatDate);


    /**
     * 统计分润信息
     */
    AgentDaySummaryBean summaryShareMoney(@Param("agentNo") String agentNo);

    /**
     * 今日购买人数
     */
    int countBuyerNumToday(@Param("agentNo") String agentNo);

    /**
     * 购买总人数
     */
    int countAllBuyerNum(@Param("agentNo") String agentNo);

    /**
     * 今日关注人数
     */
    int countFansNumToday(@Param("agentNo") String agentNo);

    /**
     * 关注总人数
     */
    int countAllFansNum(@Param("agentNo") String agentNo);

    /**
     * 下级今日购买人数
     */
    int countChildrenBuyerNumToday(@Param("bean") LoginSearchBean bean);

    /**
     * 下级购买总人数
     */
    int countChildrenAllBuyerNum(@Param("bean") LoginSearchBean bean);

    /**
     * 下级今日关注人数
     */
    int countChildrenFansNumToday(@Param("bean") LoginSearchBean bean);

    /**
     * 下级关注总人数
     */
    int countChildrenAllFansNum(@Param("bean") LoginSearchBean bean);

    /**
     * 根据条件汇总代理商汇总数据
     */
    Long countAgentSummary(@Param("bean") AgentInfoBean bean);

    /**
     * 根据条件查询代理商汇总数据
     */
    List<AgentDaySummaryBean> listAgentSummary(@Param("bean") AgentInfoBean bean,
                                               @Param("page") PageBean pageBean);

    /**
     * 下级获取的总收益
     */
    BigDecimal countChildrenAllShareMoney(@Param("bean") LoginSearchBean loginSearchBean);

    /**
     * 查询我的下级汇总数据
     */
    List<AgentDaySummaryBean> listDistributorSummary(@Param("bean") AgentInfoBean bean);

    /**
     * 查询我的日结汇总数据-图表
     */
    List<AgentDaySummaryBean> listAgentDaySummary(@Param("bean") AgentInfoBean bean);

    long countAgentDaySummary(@Param("bean") AgentInfoBean agentInfoBean);

    List<AgentDaySummaryBean> listAgentDaySummary(@Param("bean") AgentInfoBean agentInfoBean,
                                                  @Param("page") PageBean pageBean);

    /**
     * 根据条件汇总我的收益相关列表数量
     */
    Long countMyRevenue4Table(@Param("bean") AgentInfoBean t);

    /**
     * 根据条件查询我的收益相关列表
     */
    List<AgentDaySummaryBean> listMyRevenue4Table(@Param("bean") AgentInfoBean t,
                                                  @Param("page") PageBean pageBean);

    Long counyMyStudent4Table(@Param("bean") AgentInfoBean bean);

    List<Map<String, Object>> listMyStudent4Table(@Param("bean") AgentInfoBean bean,
                                                  @Param("page") PageBean pageBean);

    /**
     * 统计分销商今日为上级贡献的分润金额
     */
    BigDecimal countParentShareMoneyToday(@Param("agentNo") String agentNo);

    /**
     * 统计分销商上级贡献的总分润金额
     *
     * @return
     */
    BigDecimal parentAllParentShareMoneyCusm(@Param("agentNo") String agentNo);


    /**
     * 今日购买量
     */
    int countOrderNumTodayByStudent(@Param("userId") String userId);

    /**
     * 购买总量
     */
    int countAllOrderNumByStudent(@Param("userId") String userId);

    /**
     * 购课总金额
     */
    BigDecimal sumAllOrderMoneyByStudent(@Param("userId") String userId);

    /**
     * 学生推广总收益
     */
    BigDecimal sumShareMoneyByStudent(@Param("userId") String userId);

    /**
     * 汇总学生购买课程数量
     *
     * @param studentOrderBean
     * @return
     */
    long countMyStudentBuyOrderInfo(@Param("bean") StudentOrderBean studentOrderBean);

    /**
     * 查询学生购买课程列表
     */
    List<StudentOrderBean> listMyStudentBuyOrderInfo(@Param("bean") StudentOrderBean studentOrderBean,
                                                     @Param("page") PageBean pageBean);

    BigDecimal countShareMoneyToday(@Param("agentNo") String agentNo);
}
