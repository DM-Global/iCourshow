package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.app.utils.SharePosterUtils;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;

@Api(description = "test", tags = "test")
@RestController
@RequestMapping("/app/test")
@Log4j2
public class TestController {
    @ApiOperation(value = "测试发送友盟", notes = "" +
        "- 推送类型 type\n " +
        "   - PARENT_SHARE 二级分润\n" +
        "       - 跳收支列表\n" +
        "   - GRANDFATHER_SHARE 一级分润 \n" +
        "       - 跳收支列表\n" +
        "   - NEW_SUBORDINATE 新用户加入 \n" +
        "       - 跳钱包\n" +
        "   - APPLY_WITHDRAY_FAILED 提现失败 \n " +
        "       - 消息详情页面\n" +
        "   - APPLY_WITHDRAY_SUCCESS 提现成功 \n" +
        "       - 消息详情页面\n")
    @GetMapping(value = "/testSendPush/{type}")
    public ApiResponse testSendPush(
        @PathVariable String type,
        HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        UPushCustomDefined upushHistoryBean = getUPushCustomDefined(type);
        if (upushHistoryBean == null) {
            return new ApiResponse("type类型有误");
        }
        uPushApiService.sendUnicast(loginUserId, upushHistoryBean);
        return new ApiResponse(upushHistoryBean);
    }

    @GetMapping("/genSharePoster/{userName}/{userId}")
    @AppLoginValid(needLogin = false)
    public void genSharePoster(@PathVariable String userName, @PathVariable String userId, HttpServletResponse response) throws Exception {
        response.setContentType("image/png");
        BufferedImage admissionNotice = SharePosterUtils.generateAdmissionNoticeImage(userName, AppUtils.getStudentShareUrl(userId));
        ImageIO.write(admissionNotice, "png", response.getOutputStream());
    }


    @Resource
    private UPushApiService uPushApiService;

    private UPushCustomDefined getUPushCustomDefined(String type) {
        UPushTypeEnum anEnum = EnumUtils.getEnum(UPushTypeEnum.class, type);
        if (anEnum == null) {
            return null;
        }
        UPushCustomDefined uPushCustomDefined = null;
        switch (anEnum){
            case PARENT_SHARE:
                uPushCustomDefined = UPushCustomDefined.builder()
                    .uPushType(anEnum)
                    .studentName("李四")
                    .money(BigDecimal.valueOf(50))
                    .build();
                break;
            case NEW_SUBORDINATE:
                uPushCustomDefined = UPushCustomDefined.builder()
                    .uPushType(anEnum)
                    .studentName("李四")
                    .build();
                break;
            case GRANDFATHER_SHARE:
                uPushCustomDefined = UPushCustomDefined.builder()
                    .uPushType(anEnum)
                    .studentName("李四")
                    .money(BigDecimal.valueOf(25))
                    .build();
                break;
            case APPLY_WITHDRAY_FAILED:
                uPushCustomDefined = UPushCustomDefined.builder()
                    .uPushType(anEnum)
                    .money(BigDecimal.valueOf(999999))
                    .messageId("11")
                    .withdrawId("20")
                    .remark("没钱了")
                    .build();
                break;
            case APPLY_WITHDRAY_SUCCESS:
                uPushCustomDefined = UPushCustomDefined.builder()
                    .uPushType(anEnum)
                    .messageId("11")
                    .money(BigDecimal.valueOf(99))
                    .withdrawId("20")
                    .build();
                break;
            default:break;
        }
        return uPushCustomDefined;
    }
}
