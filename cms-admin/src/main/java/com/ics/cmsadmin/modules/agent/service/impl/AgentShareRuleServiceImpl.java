package com.ics.cmsadmin.modules.agent.service.impl;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.profit.ProfitTypeEnum;
import com.ics.cmsadmin.frame.profit.role.ProfitRole;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.agent.bean.AgentShareRuleBean;
import com.ics.cmsadmin.modules.agent.service.AgentInfoService;
import com.ics.cmsadmin.modules.agent.service.AgentShareRuleService;
import com.ics.cmsadmin.modules.agent.steward.AgentServices;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.function.Function;

import static com.ics.cmsadmin.modules.system.steward.SystemServices.registerService;

/**
 * a_agent_share_rule
 * Created bylvsw on 2018-37-29 16:09:14.
 */
@Service
public class AgentShareRuleServiceImpl implements AgentShareRuleService {

    @Override
    public AgentShareRuleBean queryById(String id) {
        return null;
    }

    @Override
    public boolean insert(AgentShareRuleBean bean) {
        return false;
    }

    @Override
    public boolean update(String id, AgentShareRuleBean bean) {
        return false;
    }

    @Override
    public AgentShareRuleBean queryByAgentNo(String agentNo) {
        AgentInfoBean agentInfoBean = AgentServices.agentInfoService.queryById(agentNo);
        if (agentInfoBean == null) {
            return null;
        }
        String expression;
        if (StringUtils.equalsIgnoreCase(agentInfoBean.getAgentType(), Constants.AGENT_TYPE_COMPANY)) {
            expression = registerService.queryRegisterValue(RegisterKeyValueEnum.COMPANY_AGENT_SHARE_RATE, Function.identity());
        } else {
            expression = registerService.queryRegisterValue(RegisterKeyValueEnum.PERSONAL_AGENT_SHARE_RATE, Function.identity());
        }
        AgentShareRuleBean agentShareRuleBean = AgentShareRuleBean.builder()
            .type(ProfitTypeEnum.GRADIENT_RATE.name())
            .expression(expression).build();
        ProfitRole profitRole = ProfitTypeEnum.GRADIENT_RATE.getProfitRole(expression);
        agentShareRuleBean.setData(profitRole.getData());
        return agentShareRuleBean;
    }

}
