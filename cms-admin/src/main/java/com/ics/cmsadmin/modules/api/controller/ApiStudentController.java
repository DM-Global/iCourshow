package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/student")
public class ApiStudentController {

    private final static String STUDENT_EXIST = "exist";

    @Autowired
    private StudentService studentService;

    @PostMapping("/check")
    public ApiResponse check(@RequestBody StudentBean studentBean, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        PageResult<StudentBean> pageResult = studentService.list(studentBean, null);
        List<StudentBean> studentList = pageResult.getDataList();
        if (CollectionUtils.isEmpty(studentList)) {
            return ApiResponse.getDefaultResponse(false);
        } else {
            for (StudentBean aStudent : studentList) {
                if (aStudent.getId().equals(userId)) {
                    return ApiResponse.getDefaultResponse(false);
                }
            }
            return ApiResponse.getDefaultResponse(true);
        }
    }

    @GetMapping("/{id}")
    public ApiResponse queryById(@PathVariable String id) {
        StudentBean studentBean = studentService.queryById(id);
        return ApiResponse.getDefaultResponse(studentBean);
    }

    @GetMapping("/queryStudentInfo")
    public ApiResponse queryStudentInfo(HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        return ApiResponse.getDefaultResponse(studentService.queryById(userId));
    }

    @PutMapping("/updateStudentInfo")
    public ApiResponse updateStudentInfo(@RequestBody StudentBean student, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        studentService.update(userId, student);
        return ApiResponse.getDefaultResponse();
    }
}
