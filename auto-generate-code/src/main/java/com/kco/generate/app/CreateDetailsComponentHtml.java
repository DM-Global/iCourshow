package com.kco.generate.app;

import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateDetailsComponentHtml {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + "-details.component.html", sb.toString());
    }
}
