package com.ics.cmsadmin.modules.agent.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.agent.bean.AgentAccountBean;
import com.ics.cmsadmin.modules.agent.service.AgentAccountService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_agent_account controller
 * Created by lvsw on 2018-51-29 14:09:57.
 */
@Api(description = "代理商账户")
@RestController
@RequestMapping("/agentAccount")
public class AgentAccountController {

    @Resource
    private AgentAccountService agentAccountService;

    @Authorize(AuthorizeEnum.AGENT_ACCOUNT_QUERY)
    @ApiOperation(value = "分页查询代理商账户信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody AgentAccountBean agentAccountBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(agentAccountService.listByLoginUserId(agentAccountBean, loginUserId, pageBean));
    }

}
