package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.student.bean.StudentShareDetailBean;

import java.math.BigDecimal;

public interface StudentShareDetailService extends BaseDataService<StudentShareDetailBean> {
    /**
     * 获取今日分润
     */
    BigDecimal getTodayProfit(String studentId);
}
