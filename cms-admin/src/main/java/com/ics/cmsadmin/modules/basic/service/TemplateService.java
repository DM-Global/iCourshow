package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.TemplateBean;


/**
 * t_template 服务类
 */
public interface TemplateService extends BaseService<TemplateBean>, BaseDataService<TemplateBean> {
}
