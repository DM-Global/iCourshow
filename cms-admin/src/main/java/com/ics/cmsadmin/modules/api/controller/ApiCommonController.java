package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.constant.RedisConstants;
import com.ics.cmsadmin.frame.constant.SwaggerNoteConstants;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.utils.RandomValidateCodeUtils;
import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.AppVersionService;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.BIND_PHONE_NOTES;
import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.GET_IMAGE_CODE_NOTES;
import static com.ics.cmsadmin.modules.student.steward.StudentServices.studentService;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.phoneCodeService;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.smsService;

@Api(description = "公共接口")
@RestController
@RequestMapping("/api/common")
@Slf4j
public class ApiCommonController {

    @Resource
    private AppVersionService appVersionService;
    @Autowired
    private OrderService orderService;


    @ApiOperation("获取订单活动图片")
    @GetMapping("/getEntranceImage")
    public ApiResponse getEntranceImage(HttpServletRequest request) {
//        String loginUserId = SsoUtils.getLoginUserId(request);
//        String orderPosterImage;
//        boolean hasTrialOrder = false;
//        if (StringUtils.isBlank(loginUserId)) {
//            orderPosterImage = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.UNLOGIN_ENTRANCE, Function.identity());
//        }else if (orderService.hasTrialOrder(loginUserId)) {
//            hasTrialOrder = true;
//            orderPosterImage = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ENTRANCE, Function.identity());
//        }else {
//            orderPosterImage = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_OLD_USER_ENTRANCE, Function.identity());
//        }
//        resultMap.put("hasTrialOrder", hasTrialOrder);
//        resultMap.put("orderPosterImage", orderPosterImage);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("hasTrialOrder", true);
        resultMap.put("orderPosterImage", SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.LOGIN_ENTRANCE, Function.identity()));
        return new ApiResponse(resultMap);
    }

    @ApiOperation("获取app下载地址")
    @GetMapping("/getAppDownloadUrl")
    public ApiResponse getAppDownloadUrl() {
        AppVersionBean android = appVersionService.findLastVersionByType("android");
        AppVersionBean ios = appVersionService.findLastVersionByType("ios");
        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("androidUrl", android == null ? "" : android.getUrl());
        resultMap.put("iosUrl", ios == null ? "" : ios.getUrl());
        return new ApiResponse(resultMap);
    }

    @ApiOperation(value = "获取图形验证码", notes = GET_IMAGE_CODE_NOTES)
    @GetMapping(value = "/getImageCode/{imageCodeId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public void getImageCode(@ApiParam("图形验证码id,随机") @PathVariable String imageCodeId,
                             HttpServletResponse response) throws IOException {
        String code = RandomStringUtils.random(4, Constants.LETTER_AND_NUMBER_CLEAR);
        RedisConstants.saveImageCode(imageCodeId, code);
        BufferedImage randcodeImage = RandomValidateCodeUtils.getRandCodeImage(code);
        ImageIO.write(randcodeImage, "JPEG", response.getOutputStream());
    }

    @ApiOperation(value = "获取短信验证码", notes = SwaggerNoteConstants.SMS_CODE_NOTES)
    @GetMapping(value = "/sendVerifyCodeSms/{smsType}/{phoneCode}/{phone}/{imageCodeId}/{imageCode}")
    public ApiResponse sendVerifyCodeSms(@ApiParam("短信类型") @PathVariable String smsType,
                                         @ApiParam("手机区域码") @PathVariable String phoneCode,
                                         @ApiParam("手机号") @PathVariable String phone,
                                         @ApiParam("图形验证码id") @PathVariable String imageCodeId,
                                         @ApiParam("图形验证码") @PathVariable String imageCode) {
        return smsService.sendVerifyCodeSms(smsType, phoneCode, phone, imageCodeId, imageCode);
    }

    @ApiOperation("查询手机区域码")
    @GetMapping(value = {"/listPhoneCode"})
    public ApiResponse listPhoneCode(@ApiParam("区域码或地区名")  @RequestParam(required = false) String keyword) {
        return new ApiResponse(phoneCodeService.list(keyword));
    }

    @ApiOperation(value = "绑定手机", notes = BIND_PHONE_NOTES)
    @GetMapping("/bindPhone/{phoneCode}/{phone}/{smsCode}")
    public ApiResponse bindPhone(
        @ApiParam("要绑定的手机号区域码") @PathVariable String phoneCode,
        @ApiParam("要绑定的手机号码") @PathVariable String phone,
        @ApiParam("短信验证码") @PathVariable String smsCode,
        HttpServletRequest request) {
        LoginInfo loginUser = SsoUtils.getLoginUser(request);
        if (loginUser == null) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN, "用户未登陆");
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        String redisSmsCode = RedisConstants.getSmsCode(SmsTypeEnum.bindPhoneVerify, phone);
        if (StringUtils.isBlank(redisSmsCode) || !StringUtils.equalsIgnoreCase(smsCode, redisSmsCode)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR);
        }
        RedisConstants.deleteSmsCode(SmsTypeEnum.bindPhoneVerify, phone);
        boolean result = studentService.bindPhone(loginUserId, phoneCode, phone);
        if (result) {
            StudentBean studentBean = studentService.queryById(loginUserId);
            studentBean.setLoginToken(loginUser.getLoginToken());
            studentBean.setPassword(null);
            return new ApiResponse(studentBean, "绑定成功");
        } else {
            return new ApiResponse(result, "绑定失败");
        }
    }
}
