package com.ics.cmsadmin.frame.core.enums;

/**
 * 权限枚举
 * Created by 666666 on 2017/9/25.
 */
public enum AuthorizeEnum {

    USER_INSERT(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "新增用户"),
    USER_UPDATE(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "更新用户"),
    USER_DELETE(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "禁用用户"),
    USER_QUERY(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.QUERY, "查询用户"),
    USER_ROLE_QUERY(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.QUERY, "查询用户角色"),
    USER_ROLE_UPDATE(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "为用户分配角色"),
    USER_RESET_PASSWORD(AuthorizeGroupEnum.USER, AuthorizeTypeEnum.OPERATE, "重置用户密码"),
    USER_AS_EMPLOYEE(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "用户做员工"),
    USER_AS_ASSISTANT(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "用户做助理"),
    USER_AS_MANAGER(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "用户做主管"),
    USER_AS_BOSS(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "用户做Boss"),


    AUTHORIZE_QUERY(AuthorizeGroupEnum.AUTHORIZE, AuthorizeTypeEnum.QUERY, "查询权限"),

    ROLE_USER_UPDATE(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.OPERATE, "为角色分配用户"),
    ROLE_QUERY(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.QUERY, "查询角色"),
    AUTHORIZE_ROLE_QUERY(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.QUERY, "查询角色权限"),
    AUTHORIZE_ROLE_UPDATE(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.OPERATE, "为角色分配菜单"),
    ROLE_INSERT(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.OPERATE, "新增角色"),
    ROLE_UPDATE(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.OPERATE, "更新角色"),
    ROLE_DELETE(AuthorizeGroupEnum.ROLE, AuthorizeTypeEnum.OPERATE, "删除角色"),

    ORGANIZATION_QUERY(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.QUERY, "查询组织"),
    ORGANIZATION_QUERY_ALL_USER(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.QUERY, "按组织分类查询所有的用户"),
    ORGANIZATION_INSERT(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "新增组织"),
    ORGANIZATION_UPDATE(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "更新组织"),
    ORGANIZATION_DELETE(AuthorizeGroupEnum.ORGANIZATION, AuthorizeTypeEnum.OPERATE, "删除组织"),

    MENU_QUERY(AuthorizeGroupEnum.MENU, AuthorizeTypeEnum.QUERY, "查询菜单"),
    MENU_INSERT(AuthorizeGroupEnum.MENU, AuthorizeTypeEnum.OPERATE, "新增菜单"),
    MENU_UPDATE(AuthorizeGroupEnum.MENU, AuthorizeTypeEnum.OPERATE, "更新菜单"),
    MENU_DELETE(AuthorizeGroupEnum.MENU, AuthorizeTypeEnum.OPERATE, "删除菜单"),

    REGISTER_QUERY(AuthorizeGroupEnum.REGISTER, AuthorizeTypeEnum.QUERY, "查询配置信息"),
    REGISTER_INSERT(AuthorizeGroupEnum.REGISTER, AuthorizeTypeEnum.OPERATE, "新增配置信息"),
    REGISTER_UPDATE(AuthorizeGroupEnum.REGISTER, AuthorizeTypeEnum.OPERATE, "更新配置信息"),
    REGISTER_DELETE(AuthorizeGroupEnum.REGISTER, AuthorizeTypeEnum.OPERATE, "删除配置信息"),

    META_QUERY(AuthorizeGroupEnum.META, AuthorizeTypeEnum.QUERY, "查询meta信息"),
    META_INSERT(AuthorizeGroupEnum.META, AuthorizeTypeEnum.OPERATE, "新增meta信息"),
    META_UPDATE(AuthorizeGroupEnum.META, AuthorizeTypeEnum.OPERATE, "更新meta信息"),
    META_DELETE(AuthorizeGroupEnum.META, AuthorizeTypeEnum.OPERATE, "删除meta信息"),

    SUBJECT_QUERY(AuthorizeGroupEnum.SUBJECT, AuthorizeTypeEnum.QUERY, "查询科目信息"),
    SUBJECT_INSERT(AuthorizeGroupEnum.SUBJECT, AuthorizeTypeEnum.OPERATE, "新增科目信息"),
    SUBJECT_DELETE(AuthorizeGroupEnum.SUBJECT, AuthorizeTypeEnum.OPERATE, "删除科目信息"),
    SUBJECT_UPDATE(AuthorizeGroupEnum.SUBJECT, AuthorizeTypeEnum.OPERATE, "更新科目信息"),

    COURSE_QUERY(AuthorizeGroupEnum.COURSE, AuthorizeTypeEnum.QUERY, "查询课程信息"),
    COURSE_INSERT(AuthorizeGroupEnum.COURSE, AuthorizeTypeEnum.OPERATE, "新增课程信息"),
    COURSE_DELETE(AuthorizeGroupEnum.COURSE, AuthorizeTypeEnum.OPERATE, "删除课程信息"),
    COURSE_UPDATE(AuthorizeGroupEnum.COURSE, AuthorizeTypeEnum.OPERATE, "更新课程信息"),

    COURSE_ATTRIBUTE_QUERY(AuthorizeGroupEnum.COURSE_ATTRIBUTE, AuthorizeTypeEnum.QUERY, "查询课程属性信息"),
    COURSE_ATTRIBUTE_INSERT(AuthorizeGroupEnum.COURSE_ATTRIBUTE, AuthorizeTypeEnum.OPERATE, "新增课程属性信息"),
    COURSE_ATTRIBUTE_DELETE(AuthorizeGroupEnum.COURSE_ATTRIBUTE, AuthorizeTypeEnum.OPERATE, "删除课程属性信息"),
    COURSE_ATTRIBUTE_UPDATE(AuthorizeGroupEnum.COURSE_ATTRIBUTE, AuthorizeTypeEnum.OPERATE, "更新课程属性信息"),

    COURSE_TYPE_QUERY(AuthorizeGroupEnum.COURSE_TYPE, AuthorizeTypeEnum.QUERY, "查询课程属性信息"),
    COURSE_TYPE_INSERT(AuthorizeGroupEnum.COURSE_TYPE, AuthorizeTypeEnum.OPERATE, "新增课程属性信息"),
    COURSE_TYPE_DELETE(AuthorizeGroupEnum.COURSE_TYPE, AuthorizeTypeEnum.OPERATE, "删除课程属性信息"),
    COURSE_TYPE_UPDATE(AuthorizeGroupEnum.COURSE_TYPE, AuthorizeTypeEnum.OPERATE, "更新课程属性信息"),


    COURSE_ATTR_BIND(AuthorizeGroupEnum.COURSE_ATTR, AuthorizeTypeEnum.OPERATE, "绑定课到程属性信息"),
    COURSE_ATTR_UNBIND(AuthorizeGroupEnum.COURSE_ATTR, AuthorizeTypeEnum.OPERATE, "绑定课到程属性信息"),

    VIDEO_QUERY(AuthorizeGroupEnum.VIDEO, AuthorizeTypeEnum.QUERY, "查询知识点视频信息"),
    VIDEO_INSERT(AuthorizeGroupEnum.VIDEO, AuthorizeTypeEnum.OPERATE, "新增知识点视频信息"),
    VIDEO_DELETE(AuthorizeGroupEnum.VIDEO, AuthorizeTypeEnum.OPERATE, "删除知识点视频信息"),
    VIDEO_UPDATE(AuthorizeGroupEnum.VIDEO, AuthorizeTypeEnum.OPERATE, "更新知识点视频信息"),


    PICTURE_QUERY(AuthorizeGroupEnum.PICTURE, AuthorizeTypeEnum.QUERY, "查询图片信息"),
    PICTURE_INSERT(AuthorizeGroupEnum.PICTURE, AuthorizeTypeEnum.OPERATE, "新增图片信息"),
    PICTURE_DELETE(AuthorizeGroupEnum.PICTURE, AuthorizeTypeEnum.OPERATE, "删除图片信息"),
    PICTURE_UPDATE(AuthorizeGroupEnum.PICTURE, AuthorizeTypeEnum.OPERATE, "更新图片信息"),

    IMAGE_INSERT(AuthorizeGroupEnum.IMAGE, AuthorizeTypeEnum.OPERATE, "新增图片"),
    FILE_INSERT(AuthorizeGroupEnum.FILE, AuthorizeTypeEnum.OPERATE, "新增文件"),

    UNIT_QUERY(AuthorizeGroupEnum.UNIT, AuthorizeTypeEnum.QUERY, "查询单元信息"),
    UNIT_INSERT(AuthorizeGroupEnum.UNIT, AuthorizeTypeEnum.OPERATE, "新增单元信息"),
    UNIT_DELETE(AuthorizeGroupEnum.UNIT, AuthorizeTypeEnum.OPERATE, "删除单元信息"),
    UNIT_UPDATE(AuthorizeGroupEnum.UNIT, AuthorizeTypeEnum.OPERATE, "更新单元信息"),

    TOPIC_QUERY(AuthorizeGroupEnum.TOPIC, AuthorizeTypeEnum.QUERY, "查询主题信息"),
    TOPIC_INSERT(AuthorizeGroupEnum.TOPIC, AuthorizeTypeEnum.OPERATE, "新增主题信息"),
    TOPIC_DELETE(AuthorizeGroupEnum.TOPIC, AuthorizeTypeEnum.OPERATE, "删除主题信息"),
    TOPIC_UPDATE(AuthorizeGroupEnum.TOPIC, AuthorizeTypeEnum.OPERATE, "更新主题信息"),

    POINT_QUERY(AuthorizeGroupEnum.POINT, AuthorizeTypeEnum.QUERY, "查询知识点信息"),
    POINT_INSERT(AuthorizeGroupEnum.POINT, AuthorizeTypeEnum.OPERATE, "新增知识点信息"),
    POINT_DELETE(AuthorizeGroupEnum.POINT, AuthorizeTypeEnum.OPERATE, "删除知识点信息"),
    POINT_UPDATE(AuthorizeGroupEnum.POINT, AuthorizeTypeEnum.OPERATE, "更新知识点信息"),

    PROBLEM_SET_QUERY(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.QUERY, "查询题集信息"),
    PROBLEM_SET_INSERT(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.OPERATE, "新增题集信息"),
    PROBLEM_SET_UPDATE(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.OPERATE, "更新题集信息"),

//    ANSWERS_QUERY(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.QUERY, "查询答案信息"),

//    TIPS_QUERY(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.QUERY, "查询提示信息"),

    PROBLEM_QUERY(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.QUERY, "查询问题信息"),
    PROBLEM_INSERT(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.OPERATE, "新增问题信息"),
    PROBLEM_DELETE(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.OPERATE, "删除问题信息"),
    PROBLEM_UPDATE(AuthorizeGroupEnum.PROBLEM_SET, AuthorizeTypeEnum.OPERATE, "更新问题信息"),

    ORDER_QUERY(AuthorizeGroupEnum.ORDER, AuthorizeTypeEnum.QUERY, "订单查询"),
    ORDER_SHARE_INTO_ACCOUNT(AuthorizeGroupEnum.ORDER, AuthorizeTypeEnum.OPERATE, "订单分润入账"),

    STUDENT_QUERY(AuthorizeGroupEnum.STUDENT, AuthorizeTypeEnum.QUERY, "查询学生"),
    STUDENT_UPDATE(AuthorizeGroupEnum.STUDENT, AuthorizeTypeEnum.OPERATE, "更新学生信息"),
    STUDENT_CREATE_FREE_ORDER(AuthorizeGroupEnum.STUDENT, AuthorizeTypeEnum.OPERATE, "创建免费订单"),
    STUDENT_VIDEO_QUERY(AuthorizeGroupEnum.STUDENT, AuthorizeTypeEnum.QUERY, "学生观看视频查询"),

    ACCEPT_STUDENT_WITHDRAW_AUDITING(AuthorizeGroupEnum.STUDENT_WITHDRAW_AUDITING, AuthorizeTypeEnum.OPERATE, "接受学生提现申请"),
    REJECT_STUDENT_WITHDRAW_AUDITING(AuthorizeGroupEnum.STUDENT_WITHDRAW_AUDITING, AuthorizeTypeEnum.OPERATE, "拒绝学生提现申请"),
    STUDENT_WITHDRAW_AUDITING_QUERY(AuthorizeGroupEnum.STUDENT_WITHDRAW_AUDITING, AuthorizeTypeEnum.QUERY, "查询学生提现申请"),

    TEACHER_QUERY(AuthorizeGroupEnum.TEACHER, AuthorizeTypeEnum.QUERY, "查询教师信息"),
    TEACHER_INSERT(AuthorizeGroupEnum.TEACHER, AuthorizeTypeEnum.OPERATE, "新增教师信息"),
    TEACHER_UPDATE(AuthorizeGroupEnum.TEACHER, AuthorizeTypeEnum.OPERATE, "更新教师信息"),
    TEACHER_DELETE(AuthorizeGroupEnum.TEACHER, AuthorizeTypeEnum.OPERATE, "删除教师信息"),

    PROMOTION_UPDATE(AuthorizeGroupEnum.PROMOTION, AuthorizeTypeEnum.OPERATE, "更新推广信息"),
    PROMOTION_DELETE(AuthorizeGroupEnum.PROMOTION, AuthorizeTypeEnum.OPERATE, "删除推广信息"),
    PROMOTION_INSERT(AuthorizeGroupEnum.PROMOTION, AuthorizeTypeEnum.OPERATE, "新增推广信息"),
    PROMOTION_QUERY(AuthorizeGroupEnum.PROMOTION, AuthorizeTypeEnum.OPERATE, "查询推广信息"),

    PACKAGE_UPDATE(AuthorizeGroupEnum.PACKAGE, AuthorizeTypeEnum.OPERATE, "更新套餐信息"),
    PACKAGE_DELETE(AuthorizeGroupEnum.PACKAGE, AuthorizeTypeEnum.OPERATE, "删除套餐信息"),
    PACKAGE_INSERT(AuthorizeGroupEnum.PACKAGE, AuthorizeTypeEnum.OPERATE, "新增套餐信息"),
    PACKAGE_QUERY(AuthorizeGroupEnum.PACKAGE, AuthorizeTypeEnum.OPERATE, "查询套餐信息"),


    TEMPLATE_QUERY(AuthorizeGroupEnum.TEMPLATE, AuthorizeTypeEnum.QUERY, "查询模板信息"),
    TEMPLATE_INSERT(AuthorizeGroupEnum.TEMPLATE, AuthorizeTypeEnum.OPERATE, "新增模板信息"),
    TEMPLATE_UPDATE(AuthorizeGroupEnum.TEMPLATE, AuthorizeTypeEnum.OPERATE, "更新模板信息"),
    TEMPLATE_DELETE(AuthorizeGroupEnum.TEMPLATE, AuthorizeTypeEnum.OPERATE, "删除模板信息"),
    TEMPLATE_SEND(AuthorizeGroupEnum.TEMPLATE, AuthorizeTypeEnum.OPERATE, "发送模板信息"),

    AGENT_QUERY(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.QUERY, "查询分销商"),
    AGENT_DETAIL_QUERY(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.QUERY, "查询分销商详情"),
    AGENT_INSERT(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.OPERATE, "新增分销商"),
    AGENT_UPDATE(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.OPERATE, "更新分销商"),
    AGENT_UPDATE_STATUS(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.OPERATE, "禁用/启动分销商"),
    AGENT_RESET_PASSWORD(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.OPERATE, "重置分销商登陆密码"),
    AGENT_CHANGE_SALESMAN(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.OPERATE, "分配分销商给业务员"),
//,
//    AGENT_DELETE(AuthorizeGroupEnum.AGENT, AuthorizeTypeEnum.OPERATE, "删除分销商"),

    MY_AGENT_QUERY(AuthorizeGroupEnum.MY_AGENT_INFO, AuthorizeTypeEnum.QUERY, "查询我的信息"),

    ACCEPT_WITHDRAWCASH_APPLY(AuthorizeGroupEnum.APPLY_WITHDRAW_CASH, AuthorizeTypeEnum.OPERATE, "处理提现申请"),
    REJECT_WITHDRAWCASH_APPLY(AuthorizeGroupEnum.APPLY_WITHDRAW_CASH, AuthorizeTypeEnum.OPERATE, "拒绝提现申请"),
    CANCEL_WITHDRAWCASH_APPLY(AuthorizeGroupEnum.APPLY_WITHDRAW_CASH, AuthorizeTypeEnum.OPERATE, "取消提现申请"),
    APPLY_WITHDRAWCASH_QUERY(AuthorizeGroupEnum.APPLY_WITHDRAW_CASH, AuthorizeTypeEnum.QUERY, "查询提现申请"),
    EXPORT_WITHDRAWCASH_QUERY(AuthorizeGroupEnum.APPLY_WITHDRAW_CASH, AuthorizeTypeEnum.QUERY, "导出提现申请"),

    AGENT_ACCOUNT_QUERY(AuthorizeGroupEnum.AGENT_ACCOUNT, AuthorizeTypeEnum.QUERY, "查询分销商账户信息"),
    SHARE_DETAIL_QUERY(AuthorizeGroupEnum.SHARE_DETAIL, AuthorizeTypeEnum.QUERY, "查询分销商分润明细"),
    ACCOUNT_DETAIL_QUERY(AuthorizeGroupEnum.ACCOUNT_DETAIL, AuthorizeTypeEnum.QUERY, "查询分销商账户流水"),
    USER_OPER_LOG_QUERY(AuthorizeGroupEnum.USER_OPER_LOG, AuthorizeTypeEnum.QUERY, "用户操作日志查询"),

    SHARE_POSTER_QUERY(AuthorizeGroupEnum.SHARE_POSTER, AuthorizeTypeEnum.QUERY, "查询分享海报"),
    SHARE_POSTER_INSERT(AuthorizeGroupEnum.SHARE_POSTER, AuthorizeTypeEnum.OPERATE, "新增分享海报"),
    SHARE_POSTER_UPDATE(AuthorizeGroupEnum.SHARE_POSTER, AuthorizeTypeEnum.OPERATE, "更新分享海报"),
    SHARE_POSTER_DELETE(AuthorizeGroupEnum.SHARE_POSTER, AuthorizeTypeEnum.OPERATE, "删除分享海报"),

    UPUSH_HISTORY_QUERY(AuthorizeGroupEnum.UMENG_PUSH, AuthorizeTypeEnum.QUERY, "查询推送历史"),
    UPUSH_HISTORY_INSERT(AuthorizeGroupEnum.UMENG_PUSH, AuthorizeTypeEnum.OPERATE, "新增分享海报"),
    UPUSH_HISTORY_UPDATE(AuthorizeGroupEnum.UMENG_PUSH, AuthorizeTypeEnum.OPERATE, "更新分享海报"),
    UPUSH_HISTORY_DELETE(AuthorizeGroupEnum.UMENG_PUSH, AuthorizeTypeEnum.OPERATE, "删除分享海报"),

    APP_VERSION_QUERY(AuthorizeGroupEnum.APP_VERION, AuthorizeTypeEnum.QUERY, "查询app版本信息"),
    APP_VERSION_INSERT(AuthorizeGroupEnum.APP_VERION, AuthorizeTypeEnum.OPERATE, "新增app版本信息"),
    APP_VERSION_UPDATE(AuthorizeGroupEnum.APP_VERION, AuthorizeTypeEnum.OPERATE, "更新app版本信息"),
    APP_VERSION_DELETE(AuthorizeGroupEnum.APP_VERION, AuthorizeTypeEnum.OPERATE, "删除app版本信息"),
    APP_FEEDBACK_QUERY(AuthorizeGroupEnum.APP_FEEDBACK, AuthorizeTypeEnum.QUERY, "查看app意见反馈");

    private AuthorizeGroupEnum group;
    private AuthorizeTypeEnum type;
    private String description;

    AuthorizeEnum(AuthorizeGroupEnum group, AuthorizeTypeEnum type, String description) {
        this.group = group;
        this.type = type;
        this.description = description;
    }

    public AuthorizeGroupEnum getGroup() {
        return group;
    }

    public AuthorizeTypeEnum getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

}
