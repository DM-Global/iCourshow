package com.ics.cmsadmin.modules.agent.enums;

/**
 * 账户变动明细类型
 */
public enum AccountChangesEnum {
    /**
     * 分润账户收入
     * totalIncome = totalIncome + money
     * withdrawCash = withdrawCash
     * balance = balance + money
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance
     */
    PROFIT_INCOME,
    /**
     * 申请提现
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance - money
     * frozenBalance = frozenBalance + money
     */
    APPLY_WITHDRAW_CASH,
    /**
     * 接受提现
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash + money
     * balance = balance - money
     * availableBalance = availableBalance
     * frozenBalance = frozenBalance - money
     */
    ACCEPT_WITHDRAW_CASH,
    /**
     * 拒绝提现 （管理员拒绝）
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance - money
     */
    REFUSE_WITHDRAW_CASH,
    /**
     * 取消提现申请，(代理商主动取消)
     * totalIncome = totalIncome
     * withdrawCash = withdrawCash
     * balance = balance
     * availableBalance = availableBalance + money
     * frozenBalance = frozenBalance - money
     *
     * @see #REFUSE_WITHDRAW_CASH
     */
    CANCEL_WITHDRAW_CASH,
}
