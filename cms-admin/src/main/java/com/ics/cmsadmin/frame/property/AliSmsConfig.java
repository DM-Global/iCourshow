package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Sandwich on 2018-09-17
 */
@Data
@Component
@ConfigurationProperties(prefix = "alisms")
public class AliSmsConfig {
    public static final String INTERNAL = "internal";
    public static final String FOREIGN = "foreign";
    public String accessKeyID;
    public String accessKeySecret;
    public String signName;
    public Map<String, String> templateCode;
}
