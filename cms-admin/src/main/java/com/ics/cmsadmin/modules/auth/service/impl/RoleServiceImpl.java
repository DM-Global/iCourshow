package com.ics.cmsadmin.modules.auth.service.impl;

import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.auth.bean.SysRole;
import com.ics.cmsadmin.modules.auth.dao.RoleDao;
import com.ics.cmsadmin.modules.auth.service.RoleService;
import com.ics.cmsadmin.modules.system.emums.SequenceNumberEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

import static com.ics.cmsadmin.modules.auth.steward.AuthServices.roleMenuService;
import static com.ics.cmsadmin.modules.auth.steward.AuthServices.userRoleService;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.sequenceNumberService;

/**
 * Created by lvsw on 2018-03-29.
 */
@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleDao roleDao;

    @CacheDbMember(returnClass = SysRole.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public SysRole queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return roleDao.queryById(id);
    }

    @CacheDbMember(returnClass = SysRole.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public PageResult<SysRole> list(SysRole bean, PageBean page) {
        long count = roleDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, roleDao.list(bean, page));
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean insert(SysRole bean) {
        if (bean == null) {
            return false;
        }
        bean.setRoleId(sequenceNumberService.newSequenceNumber(SequenceNumberEnum.ROLE));
        return roleDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean update(String id, SysRole bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL);
        }
        return roleDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL);
        }
        if (userRoleService.countUserByRoleId(id) > 0) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "该角色正在使用中,不能删除");
        }
        boolean result = (roleDao.delete(id) == 1);
        if (result) {
            roleMenuService.deleteRoleMenuByRoleId(id);
        }
        return result;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean forceDelete(String roleId) {
        if (StringUtils.isBlank(roleId) || queryById(roleId) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL);
        }
        roleDao.delete(roleId);
        userRoleService.deleteUserRoleByRoleId(roleId);
        roleMenuService.deleteRoleMenuByRoleId(roleId);
        return true;
    }

    @CacheDbMember(returnClass = SysRole.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<SysRole> listUserRoleByUserId(String userId) {
        return roleDao.listUserRoleByUserId(userId);
    }

    @CacheDbMember(returnClass = SysRole.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<SysRole> listNotBelong2UserRoles(String userId) {
        return roleDao.listNotBelong2UserRoles(userId);
    }

}
