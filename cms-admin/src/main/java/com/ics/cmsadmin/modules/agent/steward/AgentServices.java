package com.ics.cmsadmin.modules.agent.steward;

import com.ics.cmsadmin.modules.agent.dao.AgentDaySummaryDao;
import com.ics.cmsadmin.modules.agent.service.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class AgentServices {
    public static AccountDetailService accountDetailService;
    public static AgentAccountService agentAccountService;
    public static AgentInfoService agentInfoService;
    public static AgentShareRuleService agentShareRuleService;
    public static ApplyWithdrawCashService applyWithdrawCashService;
    public static ShareDetailService shareDetailService;
    public static AgentDaySummaryService agentDaySummaryService;

    @Resource
    public void setAgentDaySummaryService(AgentDaySummaryService agentDaySummaryService) {
        AgentServices.agentDaySummaryService = agentDaySummaryService;
    }

    @Resource
    public void setAccountDetailService(AccountDetailService accountDetailService) {
        AgentServices.accountDetailService = accountDetailService;
    }

    @Resource
    public void setAgentAccountService(AgentAccountService agentAccountService) {
        AgentServices.agentAccountService = agentAccountService;
    }

    @Resource
    public void setAgentInfoService(AgentInfoService agentInfoService) {
        AgentServices.agentInfoService = agentInfoService;
    }

    @Resource
    public void setAgentShareRuleService(AgentShareRuleService agentShareRuleService) {
        AgentServices.agentShareRuleService = agentShareRuleService;
    }

    @Resource
    public void setApplyWithdrawCashService(ApplyWithdrawCashService applyWithdrawCashService) {
        AgentServices.applyWithdrawCashService = applyWithdrawCashService;
    }

    @Resource
    public void setShareDetailService(ShareDetailService shareDetailService) {
        AgentServices.shareDetailService = shareDetailService;
    }
}
