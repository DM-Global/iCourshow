package com.kco.generate.java;

import com.kco.generate.config.TableBean;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kco.generate.config.Utils.saveFile;

/**
 * 创建bean java文件
 * Created by 666666 on 2017/9/26.
 */
public final class CreateServiceJava {

    public static void buildFile(TableBean tableBean) {
        String today = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        StringBuilder sb = new StringBuilder();
        String basePackage = tableBean.getBasePackage();
        String className = tableBean.getClassName();
        String tableName = tableBean.getTableName();

        sb.append("package com.ics.cmsadmin.service." + basePackage + ";\n" +
            "\n" +
            "import com.ics.cmsadmin.model." + basePackage + "." + className + "Bean;\n" +
            "import com.ics.cmsadmin.service.base.BaseDataService;\n" +
            "\n" +
            "/**\n" +
            " * " + tableName + " 服务类\n" +
            " * Created by lvsw on " + today + ".\n" +
            " */\n" +
            "public interface " + className + "Service extends BaseDataService<" + className + "Bean> {\n" +
            "}\n");
        saveFile("service/" + tableBean.getClassName() + "Service.java", sb.toString());
    }
}
