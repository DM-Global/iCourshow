package com.ics.cmsadmin.modules.wechat.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.wechat.model.auth.resp.JsSDKConfig;
import com.ics.cmsadmin.modules.wechat.service.WechatAuthService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/wxauth")
public class WechatAuthController {

    @Autowired
    private WechatAuthService wechatAuthService;

    @ApiOperation(value = "js-sdk接口注入权限验证配置")
    @GetMapping("/jsSDKConfig")
    public ApiResponse jsSDKConfig(String url) {
        JsSDKConfig jsSDKConfig = wechatAuthService.jsSDKConfig(url);
        return ApiResponse.getDefaultResponse(jsSDKConfig);
    }
}
