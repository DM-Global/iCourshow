package com.ics.cmsadmin.modules.auth.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.auth.bean.SysAuthorize;
import com.ics.cmsadmin.modules.auth.bean.SysControllerAuthorize;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2017-10-06.
 */
@Repository
@Mapper
public interface AuthorizeDao extends BaseDataDao<SysAuthorize> {
    /**
     * 根据主键id查询bean
     *
     * @param id 主键id
     * @return config
     */
    SysAuthorize queryById(@Param("id") String id);

    /**
     * 根据权限码汇总权限条数
     *
     * @param authCodes 查询权限码
     * @return
     */
    long countAuthByCodes(@Param("authCodes") List<String> authCodes);

    /**
     * 删除所有的控制层方法权限
     */
    void deleteAllControllerAuthorize();

    /**
     * 批量新增所有的控制层方法权限
     *
     * @param list
     */
    void batchAddControllerAuthorize(@Param("list") List<SysControllerAuthorize> list);

    /**
     * 删除所有权限码
     */
    void deleteAllAuthorizeInfo();

    /**
     * 批量新增所有的权限码
     *
     * @param list
     */
    void batchAddAuthorizeInfo(@Param("list") List<SysAuthorize> list);

    /**
     * 根据用户查询该用户所有的权限码
     *
     * @param userId
     * @return
     */
    List<String> listAuthorizesByUserId(@Param("userId") String userId);

    /**
     * 根据用户id获取用户所有的权限详细信息
     *
     * @param userId 用户id
     * @return 权限详细信息
     */
    List<SysAuthorize> listUserAuthorizesByUserId(@Param("userId") String userId);

    /**
     * 根据角色获取权限信息
     *
     * @param roleId 角色id
     * @return 权限
     */
    List<SysAuthorize> listAuthorizeByRoleId(@Param("roleId") String roleId);

    /**
     * 根据角色获取不属于它的权限信息
     *
     * @param roleId 角色id
     * @return 权限
     */
    List<SysAuthorize> listNotBelong2RoleAuthorizes(@Param("roleId") String roleId);

    /**
     * 根据菜单id获取权限码
     *
     * @param menuId 菜单id
     * @return 返回该菜单id页面所拥有的权限
     */
    List<SysAuthorize> listAuthorizeByMenuId(@Param("menuId") String menuId);

    /**
     * 根据菜单id查询角色信息
     */
    List<SysAuthorize> listByMenuIds(@Param("menuIds") List<String> menuIds);
}
