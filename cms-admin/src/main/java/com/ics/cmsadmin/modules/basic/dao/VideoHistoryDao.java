package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.basic.bean.VideoSumBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface VideoHistoryDao extends BaseDao<VideoHistoryBean>, BaseDataDao<VideoHistoryBean> {

    VideoHistoryBean findOne(@Param("userId") String userId, @Param("videoId") String videoId);

    VideoHistoryBean findByCourseId(@Param("userId") String userId, @Param("courseId") String courseId);

    void merge(@Param("bean") VideoHistoryBean historyBean);

    long sumOfStudyTime(@Param("userId") String userId);

    VideoSumBean statisticByCourse(@Param("userId") String userId, @Param("courseId") String courseId);
}
