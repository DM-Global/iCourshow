package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.system.bean.UserOperLogBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;


/**
 * com_user_oper_log 服务类
 * Created by lvsw on 2018-41-11 15:10:21.
 */
public interface UserOperLogService {
    /**
     * 根据查询条件查询列表并汇总
     *
     * @param t 查询条件
     * @return 查询结果
     */
    PageResult<UserOperLogBean> list(UserOperLogBean t, PageBean page);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    boolean insert(UserOperLogBean t);
}
