package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.OrderStatusEnum;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.*;


@Slf4j
public class OrderServiceTest extends CmsAdminApplicationTests {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CourseService courseService;

    @Test
    public void list() {

        PageBean page = new PageBean(1, Integer.MAX_VALUE);
        PageResult pageResult = orderService.list(OrderBean.builder().userId("2ced1748c830424691f987a4094c4202").status(OrderStatusEnum.SUCCESS.getCode()).build(), page);
        log.info("course list = {}", pageResult.getDataList());
    }

    @Test
    public void count() {
    }

    @Test
    public void update() {
        OrderBean orderBean = orderService.queryById("14ca75e2e66d4db3859eb3493f7849d3");
        orderBean.setStatus(OrderStatusEnum.SUCCESS.getCode());
        orderBean.setPayTime(new Date(System.currentTimeMillis()));
        orderService.update(orderBean);
    }
}
