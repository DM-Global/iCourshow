//package com.ics.cmsadmin.modules.system.service.impl;
//
//import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
//import com.ics.cmsadmin.modules.system.bean.AppSharePosterBean;
//import com.ics.cmsadmin.modules.system.dao.AppSharePosterDao;
//import com.ics.cmsadmin.frame.core.exception.CmsException;
//import com.ics.cmsadmin.frame.core.bean.PageBean;
//import com.ics.cmsadmin.frame.core.bean.PageResult;
//import com.ics.cmsadmin.modules.system.service.AppSharePosterService;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//
///**
// * app_share_poster
// * Created bylvsw on 2019-18-30 16:05:04.
// */
//@Service
//public class AppSharePosterServiceImpl implements AppSharePosterService {
//    @Resource
//    private AppSharePosterDao appSharePosterDao;
//
//    @Override
//    public AppSharePosterBean queryById(String id) {
//        if (StringUtils.isBlank(id)) {
//            return null;
//        }
//        return appSharePosterDao.queryById(id);
//    }
//
//    @Override
//    public PageResult<AppSharePosterBean> list(AppSharePosterBean bean, PageBean page) {
//        long count = appSharePosterDao.count(bean);
//        if (count == 0) {
//            return new PageResult<>();
//        }
//        page = page == null ? new PageBean() : page;
//        return PageResult.getPage(count, appSharePosterDao.list(bean, page));
//    }
//
//    @Override
//    public boolean insert(AppSharePosterBean bean) {
//        if (bean == null) {
//            return false;
//        }
//        return appSharePosterDao.insert(bean) == 1;
//    }
//
//    @Override
//    public boolean update(String id, AppSharePosterBean bean) {
//        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
//            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
//        }
//        return appSharePosterDao.update(id, bean) == 1;
//    }
//
//    @Override
//    public boolean delete(String id) {
//        if (StringUtils.isBlank(id) || queryById(id) == null) {
//            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
//        }
//        return appSharePosterDao.delete(id) == 1;
//    }
//
//}
