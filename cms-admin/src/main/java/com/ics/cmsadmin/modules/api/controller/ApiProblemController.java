package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.ProblemSetBean;
import com.ics.cmsadmin.modules.basic.bean.ScoreBean;
import com.ics.cmsadmin.modules.basic.service.ProblemSetService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Problem controller
 * Created by lvsw on 2018-09-06.
 */
@Api(description = "问题相关接口")
@RestController
@RequestMapping("/api/problem")
public class ApiProblemController {

    @Resource
    private ProblemSetService problemSetService;

    @PostMapping(value = "/findProblemSetDetails")
    public ApiResponse findProblemSetDetails(@RequestBody ProblemSetBean problemSetBean) {
        return new ApiResponse(problemSetService.findProblemSetDetails(problemSetBean));
    }

    @PostMapping("/submitScore")
    public ApiResponse submitScore(@Validated @RequestBody ScoreBean scoreBean, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(problemSetService.submitScore(scoreBean));
    }
}

