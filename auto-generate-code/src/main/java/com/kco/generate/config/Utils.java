package com.kco.generate.config;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by 666666 on 2017/9/26.
 */
public final class Utils {
    public static final String packageName = "com.kco.generate";

    /**
     * 保存文件
     *
     * @param fileName
     * @param data
     */
    public static void saveFile(String fileName, String data) {
        try {
            FileUtils.write(new File("gem/" + fileName), data, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 首字母大写
     *
     * @param input
     * @return
     */
    public static String fristUpper(String input) {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public static String fristLower(String input) {
        return input.substring(0, 1).toLowerCase() + input.substring(1);
    }

}
