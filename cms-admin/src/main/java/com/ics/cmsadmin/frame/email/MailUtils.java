package com.ics.cmsadmin.frame.email;

import com.ics.cmsadmin.frame.core.exception.ExceptionUtils;
import com.ics.cmsadmin.frame.property.MailConfig;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * 发送邮件Util
 */
@Log4j2
@Component
public class MailUtils {

    private static MailUtils mailUtils;
    @Resource
    private MailConfig mailConfig;

    @PostConstruct
    public void postConstruct() {
        if (mailUtils == null) {
            mailUtils = this;
        }
    }

    /**
     * 发送邮件
     */
    public static boolean sendEmail(String toAddress, String subject, String content) {
        return sendEmail(Collections.singletonList(toAddress), subject, content);
    }

    /**
     * 发送邮件
     */
    public static boolean sendEmail(List<String> toAddress, String subject, String content) {
        return sendEmail(MailInfo.builder()
            .toAddress(toAddress)
            .subject(subject)
            .content(content).build());
    }

    /**
     * 发送 邮件方法 (Html格式，支持附件)
     */
    public static boolean sendEmail(MailInfo mailInfo) {

        try {
            HtmlEmail email = new HtmlEmail();
            // 配置信息
            email.setHostName(mailUtils.mailConfig.getServerHost());
            email.setFrom(mailUtils.mailConfig.getSenderAddress(), mailUtils.mailConfig.getSenderNick());
            email.setAuthentication(mailUtils.mailConfig.getSenderUsername(), mailUtils.mailConfig.getSenderPassword());

            email.setCharset("UTF-8");
            email.setSubject(mailInfo.getSubject());
            email.setHtmlMsg(mailInfo.getContent());

            // 添加附件
            List<EmailAttachment> attachments = mailInfo.getAttachments();
            if (null != attachments && attachments.size() > 0) {
                for (EmailAttachment attachment : attachments) {
                    email.attach(attachment);
                }
            }

            // 收件人
            List<String> toAddress = mailInfo.getToAddress();
            if (null != toAddress && toAddress.size() > 0) {
                for (String toAddres : toAddress) {
                    email.addTo(toAddres);
                }
            }
            // 抄送人
            List<String> ccAddress = mailInfo.getCcAddress();
            if (null != ccAddress && ccAddress.size() > 0) {
                for (String ccAddres : ccAddress) {
                    email.addCc(ccAddres);
                }
            }
            //邮件模板 密送人
            List<String> bccAddress = mailInfo.getBccAddress();
            if (null != bccAddress && bccAddress.size() > 0) {
                for (int i = 0; i < bccAddress.size(); i++) {
                    email.addBcc(ccAddress.get(i));
                }
            }
            email.send();
            return true;
        } catch (EmailException e) {
            log.error("发送邮件异常,异常信息{}", ExceptionUtils.collectExceptionStackMsg(e));
        }
        return false;
    }
}
