package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2DateStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.DateString2DataDeserializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import io.swagger.models.auth.In;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * t_order
 * 订单
 * <p>
 * Created by lvsw on 2018-21-23 21:09:27.
 */
@Data
@Builder
public class OrderBean extends LoginSearchBean {

    private String id;                      // id
    private String userId;                  // 用户id
    private String userName;                // 用户名
    private String courseId;                // 课程id
    private String courseName;              // 课程名
    private Integer orderType;               // 订单类型
    private BigDecimal coursePrice;          // 交易是课程原价
    private BigDecimal discountPrice;        // 折扣价
    private Boolean isDeleted;                //是否已经删除
    @JsonSerialize(using = Date2DateStringSerializer.class)
    @JsonDeserialize(using = DateString2DataDeserializer.class)
    private Date startDate;                //生效日期
    @JsonSerialize(using = Date2DateStringSerializer.class)
    @JsonDeserialize(using = DateString2DataDeserializer.class)
    private Date endDate;                //结束日期
    private String packageId;                //套餐id
    private Integer studyStatus;                //学习状态 1.未入学 2.已入学 3.已到期
    private String accStatus;               // 入账状态
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date accTime;               // 入账时间
    private BigDecimal orderAmount;             // 订单总金额
    private Integer paymentMethod;           // 支付方式
    private Integer status;                  // 订单状态
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date payTime;                 // 支付时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 修改时间
    // 分销商编号
    private String agentNo;
    private String salesman;
    private String packageTitle;  //套餐title
    private String packageSecondTitle;  //套餐副title
    private String packageDesc;   //套餐描述
    private Integer packageMonthCount;  //套餐包月数

    @Tolerate
    public OrderBean() {
    }
}
