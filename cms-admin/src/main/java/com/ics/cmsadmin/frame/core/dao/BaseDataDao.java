package com.ics.cmsadmin.frame.core.dao;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface BaseDataDao<T> {
    /**
     * 根据查询条件查询列表
     *
     * @param t 查询条件
     * @return 查询结果
     */
    List<T> list(@Param("bean") T t, @Param("page") PageBean page);

    /**
     * 根据查询条件查询列表,只获取一条数据
     *
     * @param t 查询条件
     * @return 查询结果
     */
    T findOne(@Param("bean") T t);

    /**
     * 根据查询条件汇总
     *
     * @param t 查询条件
     * @return 查询结果
     */
    long count(@Param("bean") T t);
}
