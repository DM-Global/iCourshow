package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.CourseBean;
import com.ics.cmsadmin.modules.basic.service.CourseService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;


@Api(description = "课程查询接口")
@RestController
@RequestMapping("/api/course")
public class ApiCourseController {

    @Autowired
    private CourseService courseService;

    @ApiOperation(value = "分页查询课程信息")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody CourseBean courseBean,
                            @ApiParam("页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                            @ApiParam("每页条数") @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                            HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        PageBean pageBean = new PageBean(pageNo, pageSize);
        courseBean.setIsActive(true);
        PageResult pageResult = courseService.list(userId, courseBean, pageBean);
        return new ApiResponse(pageResult);
    }

    @ApiOperation(value = "分页查询已购课程信息")
    @RequestMapping("/purchased")
    public ApiResponse purchased(@RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                                 HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        PageResult pageResult = new PageResult();
        // 未登陆
        if (StringUtils.isBlank(userId)) {
            pageResult.setDataList(new ArrayList<>());
            return ApiResponse.getDefaultResponse(pageResult);
        }
        PageBean page = new PageBean(pageNo, pageSize);
        pageResult = courseService.purchased(userId, page);

        return ApiResponse.getDefaultResponse(pageResult);
    }

    @ApiOperation(value = "查询课程详细信息")
    @GetMapping("/query/{id}")
    public ApiResponse query(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        CourseBean courseBean = courseService.courseDetail(id);
        return new ApiResponse(courseService.courseAllDetailAdditional(userId, courseBean));
    }

    @ApiOperation(value = "查询课程完整详细信息")
    @GetMapping("/detail/{id}")
    public ApiResponse detail(@PathVariable(value = "id") String id, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        CourseBean courseBean = courseService.courseAllDetail(id);
        return new ApiResponse(courseService.courseAllDetailAdditional(userId, courseBean));
    }

    /**
     * 记录课程点击量
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "记录课程点击量")
    @GetMapping("/view/{id}")
    public ApiResponse increaseViewNum(@PathVariable String id) {
        courseService.increaseViewNum(id);
        return ApiResponse.getDefaultResponse();
    }

    @GetMapping("/sections")
    public ApiResponse sections() {
        return ApiResponse.getDefaultResponse(courseService.listCourseSections());
    }
}
