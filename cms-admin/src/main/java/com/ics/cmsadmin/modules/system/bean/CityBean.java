package com.ics.cmsadmin.modules.system.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.List;

/**
 * t_city
 * 中国城市编码
 * Created by lvsw on 2017-11-15.
 */
@Data
@Builder
public class CityBean {

    // 城市编码
    private String code;
    // 城市名称
    private String name;
    // 上级城市编码
    private String parentCode;
    // 层级
    private String level;
    // 城市类型 province:省,city:市,county:县,town:镇,village:村
    private String type;
    // 创建时间
    private String createTime;

    private List<CityBean> children;

    @Tolerate
    public CityBean() {
    }

}
