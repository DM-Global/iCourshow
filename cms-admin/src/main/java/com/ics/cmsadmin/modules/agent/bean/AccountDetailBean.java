package com.ics.cmsadmin.modules.agent.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * a_account_detail
 * 代理商账户明细
 * <p>
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Data
@Builder
public class AccountDetailBean extends LoginSearchBean {

    private String id;              // 流水id
    private String agentNo;              // 代理商编号
    private String accountId;              // 代理商账户编号
    private BigDecimal money;               // 变动金额
    private String type;
    private BigDecimal totalIncome;         // 总收益 A = B + C
    private BigDecimal withdrawCash;        // 提现金额 B
    private BigDecimal balance;              // 余额 C = D + E
    private BigDecimal availableBalance;    // 可用余额 D
    private BigDecimal frozenBalance;       // 冻结余额 E
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    private String startCreateTime;
    private String endCreateTime;

    @Tolerate
    public AccountDetailBean() {
    }
}
