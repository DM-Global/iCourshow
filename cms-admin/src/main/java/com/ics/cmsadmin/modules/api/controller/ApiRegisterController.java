package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.system.bean.RegisterBean;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/config")
public class ApiRegisterController {

    @Autowired
    private RegisterService registerService;

    @GetMapping("/discount")
    public ApiResponse detail(HttpServletRequest request) {
        RegisterBean registerBean = registerService.queryById("global_price_discount");
        return ApiResponse.getDefaultResponse(registerBean);
    }

}
