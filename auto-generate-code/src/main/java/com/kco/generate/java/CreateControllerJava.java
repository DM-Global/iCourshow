package com.kco.generate.java;

import com.kco.generate.config.TableBean;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kco.generate.config.Utils.fristUpper;
import static com.kco.generate.config.Utils.saveFile;

/**
 * 创建bean java文件
 * Created by 666666 on 2017/9/26.
 */
public final class CreateControllerJava {

    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildHeader(tableBean))
            .append(buildQuery(tableBean))
            .append(buildInsert(tableBean))
            .append(buildDelete(tableBean))
            .append(buildUpdate(tableBean))
            .append(buildList(tableBean))
            .append(buildTail(tableBean));

        saveFile("controller/" + tableBean.getClassName() + "Controller.java", sb.toString());
    }

    private static String buildTail(TableBean tableBean) {
        return "}\n";
    }

    private static String buildList(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String auth = className.toUpperCase().replace("INFO", "");
        String primary = tableBean.getPrimaryPropertyKey();
        sb.append("    @Authorize(AuthorizeEnum." + auth + "_QUERY)\n" +
            "    @ApiOperation(value = \"分页查询" + tableBean.getTableComment() + "信息\")\n" +
            "    @PostMapping(\"/list/{pageNo}/{pageSize}\")\n" +
            "    public ApiResponse list(@RequestBody " + className + "Bean " + className1 + "Bean,\n" +
            "                             @ApiParam(\"页码\") @PathVariable int pageNo,\n" +
            "                             @ApiParam(\"每页条数\") @PathVariable int pageSize){\n" +
            "        PageBean pageBean = new PageBean(pageNo, pageSize);\n" +
            "        return new ApiResponse(" + className1 + "Service.list(" + className1 + "Bean, pageBean));\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildDelete(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String auth = className.toUpperCase().replace("INFO", "");
        String primary = tableBean.getPrimaryPropertyKey();

        sb.append("    @Authorize(AuthorizeEnum." + auth + "_DELETE)\n" +
            "    @ApiOperation(value = \"删除" + tableBean.getTableComment() + "信息\")\n" +
            "    @PostMapping(value = \"/delete/{" + primary + "}\")\n" +
            "    public ApiResponse delete(@ApiParam(\"需要删除的" + tableBean.getTableComment() + " id\") @PathVariable String " + primary + "){\n" +
            "        return new ApiResponse(" + className1 + "Service.delete(" + primary + "));\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildUpdate(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String auth = className.toUpperCase().replace("INFO", "");
        String primary = tableBean.getPrimaryPropertyKey();

        sb.append("    @Authorize(AuthorizeEnum." + auth + "_UPDATE)\n" +
            "    @ApiOperation(value = \"更新" + tableBean.getTableComment() + "信息\")\n" +
            "    @PostMapping(value = \"/update/{" + primary + "}\")\n" +
            "    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody " + className + "Bean " + className1 + "Bean,\n" +
            "                              BindingResult bindingResult,\n" +
            "                               @ApiParam(\"需要更新的" + tableBean.getTableComment() + "Id\") @PathVariable String " + primary + "){\n" +
            "        if (bindingResult.hasErrors()){\n" +
            "            return HttpUtils.validateError(bindingResult);\n" +
            "        }\n" +
            "        return new ApiResponse(" + className1 + "Service.update(" + primary + ", " + className1 + "Bean));\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildInsert(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String auth = className.toUpperCase().replace("INFO", "");
        sb.append("    @Authorize(AuthorizeEnum." + auth + "_INSERT)\n" +
            "    @ApiOperation(value = \"新增" + tableBean.getTableComment() + "信息\")\n" +
            "    @PostMapping(value = \"/insert\")\n" +
            "    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody " + className + "Bean " + className1 + "Bean, \n" +
            "                              BindingResult bindingResult){\n" +
            "        if (bindingResult.hasErrors()){\n" +
            "            return HttpUtils.validateError(bindingResult);\n" +
            "        }\n" +
            "        return new ApiResponse(" + className1 + "Service.insert(" + className1 + "Bean));\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildQuery(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String auth = className.toUpperCase().replace("INFO", "");
        String primary = tableBean.getPrimaryPropertyKey();
        sb.append("    @Authorize(AuthorizeEnum." + auth + "_QUERY)\n" +
            "    @ApiOperation(value = \"查询" + tableBean.getTableComment() + "信息\")\n" +
            "    @GetMapping(value = \"/query/{" + primary + "}\")\n" +
            "    public ApiResponse queryById(@ApiParam(value = \"" + primary + "\") @PathVariable String " + primary + "){\n" +
            "        return new ApiResponse(" + className1 + "Service.queryById(" + primary + "));\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildHeader(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String today = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String basePackage = tableBean.getBasePackage();
        sb.append("package com.ics.cmsadmin.controller." + basePackage + ";\n" +
            "\n" +
            "import com.ics.cmsadmin.frame.core.ApiResponse;\n" +
            "import com.ics.cmsadmin.frame.core.enums.Authorize;\n" +
            "import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;\n" +
            "import com.ics.cmsadmin.frame.core.enums.InsertGroup;\n" +
            "import com.ics.cmsadmin.frame.core.enums.UpdateGroup;\n" +
            "import com.ics.cmsadmin.model.PageBean;\n" +
            "import com.ics.cmsadmin.model." + basePackage + "." + className + "Bean;\n" +
            "import com.ics.cmsadmin.service." + basePackage + "." + className + "Service;\n" +
            "import com.ics.cmsadmin.utils.HttpUtils;\n" +
            "import io.swagger.annotations.Api;\n" +
            "import io.swagger.annotations.ApiOperation;\n" +
            "import io.swagger.annotations.ApiParam;\n" +
            "import org.springframework.validation.BindingResult;\n" +
            "import org.springframework.validation.annotation.Validated;\n" +
            "import org.springframework.web.bind.annotation.*;\n" +
            "\n" +
            "import javax.annotation.Resource;\n" +
            "import javax.servlet.http.HttpServletRequest;\n\n")
            .append("/**\n" +
                " * " + className + " controller\n" +
                " * Created by lvsw on " + today + ".\n" +
                " */\n" +
                "@Api(description = \"" + tableBean.getTableComment() + "管理接口\")\n" +
                "@RestController\n" +
                "@RequestMapping(\"/" + className1 + "\")\n" +
                "public class " + className + "Controller {\n" +
                "\n" +
                "    @Resource\n" +
                "    private " + className + "Service " + className1 + "Service;\n\n");
        return sb.toString();
    }
}
