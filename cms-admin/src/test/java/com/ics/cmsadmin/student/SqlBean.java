package com.ics.cmsadmin.student;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

public abstract class SqlBean {

    protected abstract String tableName();

    public static String insertSqlPrefix(Class<? extends SqlBean> clazz){
        try {
            SqlBean sqlBean = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            String reduce = Arrays.stream(fields)
                    .map(item -> camel2underline(item.getName()))
                    .filter(item -> !item.toLowerCase().endsWith("key"))
                    .reduce("", (all, item) -> all = all + item + ",");
            reduce = reduce.substring(0, reduce.length() - 1);
            return String.format("insert into %s ( %s ) values \n", sqlBean.tableName(), reduce);
        }catch (Exception e){
            return "";
        }

    }

    public String insertSqlSuffix() {
        Field[] fields = this.getClass().getDeclaredFields();
        String reduce = Arrays.stream(fields)
                .filter(item -> !item.getName().toLowerCase().endsWith("key"))
                .map(item -> {
                    try {
                        item.setAccessible(true);
                        Object o = item.get(this);
                        if (o == null) {
                            return null;
                        } else if (o instanceof Date) {
                            return DateFormatUtils.format((Date) o, "yyyy-MM-dd hh:mm:ss");
                        } else {
                            return Objects.toString(o);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    return null;
                }).reduce("", (all, item) -> all = all + (item == null ? "null," : String.format("'%s',", item)));
        reduce = reduce.substring(0, reduce.length() - 1);
        return String.format("(%s),\n", reduce);
    }

    public static String camel2underline(String camel){
        return camel.replaceAll("(.*?)([A-Z])", "$1_$2").toLowerCase();
    }

}
