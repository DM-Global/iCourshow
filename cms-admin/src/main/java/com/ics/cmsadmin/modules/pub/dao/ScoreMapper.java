package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Score;
import org.apache.ibatis.annotations.Mapper;

/*
 *    created by mengyang ${date}
 */
@Mapper
public interface ScoreMapper extends ResourceMapper<Score, Integer> {
}
