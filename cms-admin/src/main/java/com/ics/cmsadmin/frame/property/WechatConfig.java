package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Sandwich on 2018-08-10
 */
@Data
@Component
@ConfigurationProperties(prefix = "wechat")
public class WechatConfig {

    /**
     * 公众平台id
     */
    public String appId;
    public String msgToken;

    /**
     * 公众平台密钥
     */
    public String appSecret;

    /**
     * 开放平台id
     */
    private String openAppId;

    /**
     * 开放平台密钥
     */
    public String openAppSecret;

    /**
     * 商户号
     */
    private String mchId;

    /**
     * 商户密钥
     */
    public String mchKey;

    /**
     * 商户证书路径
     */
    public String keyPath;

    /**
     * 微信支付异步通知地址
     */
    public String notifyUrl;

    /**
     * 微信支付失败重定向地址
     */
    public String payFailRedirectUrl;

    /**
     * 微信模版id
     */
    public Map<String, String> templateId;
}
