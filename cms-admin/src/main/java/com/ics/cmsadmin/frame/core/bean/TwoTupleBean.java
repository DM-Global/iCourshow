package com.ics.cmsadmin.frame.core.bean;

/**
 * 两位的元组
 * Created by 666666 on 2017/9/26.
 */
public class TwoTupleBean<ONE, TWO> {
    public ONE one;
    public TWO two;

    public TwoTupleBean() {
    }

    public TwoTupleBean(ONE one, TWO two) {
        this.one = one;
        this.two = two;
    }

}
