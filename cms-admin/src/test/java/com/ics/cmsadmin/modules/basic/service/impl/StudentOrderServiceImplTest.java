package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.StudentOrderService;
import com.ics.cmsadmin.modules.pub.bean.OrderForm;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by Sandwich on 2019/1/12
 */
@Log4j2
public class StudentOrderServiceImplTest extends CmsAdminApplicationTests {

    @Resource
    private StudentOrderService studentOrderService;

    @Test
    public void testCreateOrder(){
        OrderBean orderBean = studentOrderService.create( OrderForm.builder().orderType(1).courseId("42").build(),"0855b952ad6e4869998175bad8d29803");
        log.info("==================={}",orderBean.toString());
    }

}
