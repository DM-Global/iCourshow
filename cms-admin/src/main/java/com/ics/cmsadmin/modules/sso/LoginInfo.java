package com.ics.cmsadmin.modules.sso;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * Created by lvsw on 2018/8/26.
 */
@Data
public class LoginInfo extends LoginSearchBean {
    protected String loginId;           // 登陆用户id
    protected String loginUserName;     // 登陆用户名
    protected String loginToken;        // 登陆token
    protected long timeoutTime;         // 登陆超时时间

}
