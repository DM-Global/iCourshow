package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.modules.system.bean.CityBean;

import java.util.List;

/**
 * t_city 服务类
 * Created by lvsw on 2017-12-02.
 */
public interface CityService {

    /**
     * @param parentCode
     * @return
     */
    List<CityBean> listDirectChildren(String parentCode);

    /**
     * 获取省市城市集合
     */
    List<CityBean> listProvinceCity();
}
