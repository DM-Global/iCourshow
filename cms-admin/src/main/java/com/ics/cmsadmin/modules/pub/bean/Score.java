package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

/*
 *    created by mengyang ${date}
 */

@Data
public class Score extends Resource<Integer> {
    private Integer practiceId;
    private String studentId;
    private Integer score;
    private Integer correctNumber;
}
