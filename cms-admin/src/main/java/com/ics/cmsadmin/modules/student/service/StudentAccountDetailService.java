package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;

/**
 * 账户明细服务
 */
public interface StudentAccountDetailService extends BaseDataService<StudentAccountDetailBean> {
}
