package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.constant.RedisConstants;
import com.ics.cmsadmin.frame.constant.SwaggerNoteConstants;
import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;
import com.ics.cmsadmin.modules.basic.service.UpushHistoryService;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.bean.AppFeedbackBean;
import com.ics.cmsadmin.modules.system.service.UploadService;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.*;
import static com.ics.cmsadmin.modules.student.steward.StudentServices.studentService;


@AppLoginValid
@Api(description = "app我的信息相关接口", tags = "我的相关接口")
@RestController
@RequestMapping("/app/myInfo")
public class AppMyInfoController {

    @Resource
    private UploadService uploadService;
    @Resource
    private UpushHistoryService upushHistoryService;

    @ApiOperation(value = "绑定微信", notes = BIND_WECHAT_NOTES)
    @GetMapping("/bindWechat/{code}")
    public ApiResponse bindWechat(@ApiParam("微信code") @PathVariable String code,
                                    HttpServletRequest request) {
        LoginInfo loginUser = SsoUtils.getLoginUser(request);
        if (loginUser == null) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN);
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        boolean result = studentService.bindWechat(loginUserId, code);
        if (result) {
            StudentBean studentBean = studentService.queryById(loginUserId);
            studentBean.setLoginToken(loginUser.getLoginToken());
            studentBean.setPassword(null);
            return new ApiResponse(studentBean, "绑定微信成功");
        } else {
            return new ApiResponse(result, "绑定微信失败");
        }
    }

    @ApiOperation(value = "绑定密码", notes = BIND_PASSWORD_NOTES)
    @GetMapping("/bindPassword/{password}")
    public ApiResponse bindPassword(@ApiParam("md5(密码)") @PathVariable String password,
                                    HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(studentService.bindPassword(loginUserId, password));
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "忘记密码/重置密码", notes = FORGOT_PASSWORD_NOTES)
    @GetMapping("/forgotPassword/{phone}/{smsCode}/{password}")
    public ApiResponse forgotPassword(@ApiParam("手机") @PathVariable String phone,
                                      @ApiParam("短信验证码") @PathVariable String smsCode,
                                      @ApiParam("md5(密码)") @PathVariable String password,
                                      HttpServletRequest request) {
        String redisSmsCode = RedisConstants.getSmsCode(SmsTypeEnum.forgotPasswordVerify.getPrepose(), phone);
        if (StringUtils.isBlank(redisSmsCode) || !StringUtils.equalsIgnoreCase(smsCode, redisSmsCode)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR);
        }
        RedisConstants.deleteSmsCode(SmsTypeEnum.forgotPasswordVerify.getPrepose(), phone);
        return new ApiResponse(studentService.forgotPassword(phone, password));
    }

    @ApiOperation(value = "修改密码", notes = MODIFY_PASSWORD_NOTES)
    @GetMapping("/modifyPassword/{oldPassword}/{newPassword}")
    public ApiResponse modifyPassword(@ApiParam("md5(旧密码)") @PathVariable String oldPassword,
                                      @ApiParam("md5(新密码)") @PathVariable String newPassword,
                                      HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(studentService.modifyPassword(loginUserId, oldPassword, newPassword));
    }

    @ApiOperation(value = "绑定手机", notes = BIND_PHONE_NOTES)
    @GetMapping("/bindPhone/{phoneCode}/{phone}/{smsCode}")
    public ApiResponse bindPhone(
        @ApiParam("要绑定的手机号区域码") @PathVariable String phoneCode,
        @ApiParam("要绑定的手机号码") @PathVariable String phone,
        @ApiParam("短信验证码") @PathVariable String smsCode,
        HttpServletRequest request) {
        LoginInfo loginUser = SsoUtils.getLoginUser(request);
        if (loginUser == null) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN, "用户未登陆");
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        String redisSmsCode = RedisConstants.getSmsCode(SmsTypeEnum.bindPhoneVerify, phone);
        if (StringUtils.isBlank(redisSmsCode) || !StringUtils.equalsIgnoreCase(smsCode, redisSmsCode)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR);
        }
        RedisConstants.deleteSmsCode(SmsTypeEnum.bindPhoneVerify, phone);
        boolean result = studentService.bindPhone(loginUserId, phoneCode, phone);
        if (result) {
            StudentBean studentBean = studentService.queryById(loginUserId);
            studentBean.setLoginToken(loginUser.getLoginToken());
            studentBean.setPassword(null);
            return new ApiResponse(studentBean, "绑定成功");
        } else {
            return new ApiResponse(result, "绑定失败");
        }

    }

    @ApiOperation(value = "根据密码修改手机号")
    @GetMapping("/modifyPhoneByPassword/{password}/{phone}/{smsCode}")
    public ApiResponse modifyPhoneByPassword(
        @ApiParam("md5(密码)") @PathVariable String password,
        @ApiParam("新手机号码") @PathVariable String phone,
        @ApiParam("短信验证码 modifyPhoneBySmsCodeOne下发的验证码") @PathVariable String smsCode,
        HttpServletRequest request) {
        LoginInfo loginUser = SsoUtils.getLoginUser(request);
        if (loginUser == null) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN, "用户未登陆");
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        String redisSmsCode = RedisConstants.getSmsCode(SmsTypeEnum.modifyPhoneVerify, phone);
        if (StringUtils.isBlank(redisSmsCode) || !StringUtils.equalsIgnoreCase(smsCode, redisSmsCode)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR);
        }
        RedisConstants.deleteSmsCode(SmsTypeEnum.modifyPhoneVerify, phone);
        boolean result = studentService.modifyPhoneByPassword(loginUserId, password, phone);
        if (result) {
            StudentBean studentBean = studentService.queryById(loginUserId);
            studentBean.setLoginToken(loginUser.getLoginToken());
            studentBean.setPassword(null);
            return new ApiResponse(studentBean, "修改成功");
        } else {
            return new ApiResponse(result, "修改失败");
        }

    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "短信验证码前置接口,本来需要一个接口就完成的事,因为ui拆分两个接口", notes = CHECK_SMS_VERIFY_CODE_NODES)
    @GetMapping("/preCheckSmsVerifyCode/{codeType}/{phone}/{code}")
    public ApiResponse preCheckSmsVerifyCode(
        @ApiParam("验证码类型") @PathVariable String codeType,
        @ApiParam("手机号") @PathVariable String phone,
        @ApiParam("手机号短信") @PathVariable String code) {
        SmsTypeEnum smsTypeEnum = SmsTypeEnum.valueOfSmsType(codeType);
        if (smsTypeEnum == null) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_TYPE_ERROR, "验证码错误，请重新输入");
        }
        String oldRedisSmsCode = RedisConstants.getSmsCode(smsTypeEnum, phone);
        if (StringUtils.isBlank(oldRedisSmsCode) || !StringUtils.equalsIgnoreCase(oldRedisSmsCode, code)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR, "验证码错误，请重新输入");
        }
        RedisConstants.deleteSmsCode(smsTypeEnum, phone);
        String random = RandomStringUtils.random(4, Constants.NUMBER);
        RedisConstants.saveSmsCode(smsTypeEnum.getPrepose(), phone, random);
        ApiResponse apiResponse = new ApiResponse(true);
        apiResponse.setData(random);
        return apiResponse;
    }

    @ApiOperation(value = "根据短信验证码修改手机号", notes = MODIFY_PHONE_BY_SMS_CODE_NOTES)
    @GetMapping("/modifyPhoneBySmsCode/{oldPhone}/{oldCode}/{newPhoneCode}/{newPhone}/{newCode}")
    public ApiResponse modifyPhoneBySmsCode(
        @ApiParam("旧手机号") @PathVariable String oldPhone,
        @ApiParam("旧手机号短信验证码 modifyPhoneBySmsCodeOne下发的验证码") @PathVariable String oldCode,
        @ApiParam("新手机号码区域码") @PathVariable String newPhoneCode,
        @ApiParam("新手机号码") @PathVariable String newPhone,
        @ApiParam("新手机号短信验证码 类型为 bindPhoneVerify") @PathVariable String newCode,
        HttpServletRequest request) {
        LoginInfo loginUser = SsoUtils.getLoginUser(request);
        if (loginUser == null) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN, "用户未登陆");
        }
        String loginUserId = SsoUtils.getLoginUserId(request);
        String oldRedisSmsCode = RedisConstants.getSmsCode(SmsTypeEnum.modifyPhoneVerify.getPrepose(), oldPhone);
        if (StringUtils.isBlank(oldRedisSmsCode) || !StringUtils.equalsIgnoreCase(oldRedisSmsCode, oldCode)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR, "旧手机验证码错误，请重新输入");
        }

        String newRedisSmsCode = RedisConstants.getSmsCode(SmsTypeEnum.bindPhoneVerify, newPhone);
        if (StringUtils.isBlank(newRedisSmsCode) || !StringUtils.equalsIgnoreCase(newRedisSmsCode, newCode)) {
            return new ApiResponse(ApiResultEnum.SMS_VERIFY_CODE_ERROR, "新手机验证码错误，请重新输入");
        }
        RedisConstants.deleteSmsCode(SmsTypeEnum.bindPhoneVerify, newPhone);
        boolean result = studentService.modifyPhoneBySmsCode(loginUserId, oldPhone, newPhoneCode, newPhone);
        if (result) {
            RedisConstants.deleteSmsCode(SmsTypeEnum.modifyPhoneVerify.getPrepose(), oldPhone);
            StudentBean studentBean = studentService.queryById(loginUserId);
            studentBean.setLoginToken(loginUser.getLoginToken());
            studentBean.setPassword(null);
            return new ApiResponse(studentBean, "修改成功");
        } else {
            return new ApiResponse(result, "修改失败");
        }

    }


    @ApiOperation(value = "更新我的用户信息", notes = SwaggerNoteConstants.UPDATE_MY_USER_NOTES)
    @PostMapping("/updateMyUserInfo")
    public ApiResponse updateMyUserInfo(@RequestBody StudentBean bean, HttpServletRequest request) {
        LoginInfo loginUser = SsoUtils.getLoginUser(request);
        if (loginUser == null) {
            return new ApiResponse(ApiResultEnum.UN_LOGIN);
        }
        String loginUserId = loginUser.getLoginId();
        // 从源头限制用户可以更新的字段,避免前端恶意更新其他字段
        StudentBean filterBean = StudentBean.builder()
            .wechatAccount(bean.getWechatAccount())
            .province(bean.getProvince())
            .city(bean.getCity())
            .county(bean.getCounty())
            .cityName(bean.getCityName())
            .email(bean.getEmail())
            .gender(bean.getGender())
            .birthday(bean.getBirthday())
            .build();
        boolean result = studentService.update(loginUserId, filterBean);
        if (result) {
            StudentBean studentBean = studentService.queryById(loginUserId);
            studentBean.setLoginToken(loginUser.getLoginToken());
            studentBean.setPassword(null);
            return new ApiResponse(studentBean);
        } else {
            return new ApiResponse(false);
        }
    }

    @ApiOperation(value = "更新用户头像")
    @PostMapping(value = "/updateUserAvatar")
    public ApiResponse renamedImage(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        String imageUrl = uploadImage(file);
        boolean update = studentService.update(loginUserId, StudentBean.builder().avatar(imageUrl).build());
        if (update) {
            ApiResponse result = new ApiResponse(true, "修改成功");
            result.setData(imageUrl);
            return result;
        } else {
            return new ApiResponse(false, "修改失败");
        }
    }

    @ApiOperation(value = "上传意见反馈图片")
    @PostMapping(value = "/uploadFeedbackImage")
    public ApiResponse uploadFeedbackImage(@RequestParam("file") MultipartFile file) {
        String imageUrl = uploadImage(file);
        ApiResponse result = new ApiResponse(true, "上传成功");
        result.setData(imageUrl);
        return result;
    }

    @ApiOperation(value = "获取消息列表")
    @PostMapping(value = "/listMessage/{pageNo}/{pageSize}")
    public ApiResponse listMessage(@ApiParam("页码") @PathVariable Integer pageNo,
                                   @ApiParam("每页条数") @PathVariable Integer pageSize,
                                   HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        PageResult<UpushHistoryBean> list = upushHistoryService.list(UpushHistoryBean.builder().studentId(loginUserId).build(), new PageBean(pageNo, pageSize));
        return new ApiResponse(list);
    }

    @ApiOperation(value = "获取未读消息数量")
    @PostMapping(value = "/getUnReadCount")
    public ApiResponse getUnReadCount(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(upushHistoryService.getUnReadCount(loginUserId));
    }

    @ApiOperation(value = "读消息", notes = "返回消息详情")
    @PostMapping(value = "/readMessage/{messageId}")
    public ApiResponse readMessage(@ApiParam("消息id") @PathVariable String messageId,
                                   HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(upushHistoryService.readMessage(messageId, loginUserId));
    }

    @ApiOperation(value = "保存意见反馈")
    @PostMapping(value = "/saveFeedback")
    public ApiResponse saveFeedback(@RequestBody AppFeedbackBean appFeedbackBean, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        appFeedbackBean.setUserId(loginUserId);
        boolean result = SystemServices.appFeedbackService.insert(appFeedbackBean);
        return new ApiResponse(result, result ? "提交成功" : "提交失败");
    }

    private String uploadImage(MultipartFile file) {
        if (file.isEmpty()) {
            throw new CmsException(ApiResultEnum.PARAM_ERROR, "请上传头像");
        }
        if (file.getSize() > 1024 * 1024 * 1) {
            throw new CmsException(ApiResultEnum.FILE_TOO_BIG, "头像最大上传1M");
        }
        String originalFilename = file.getOriginalFilename().toLowerCase();
        if (!Arrays.asList(".png", ".jpg", ".gif", ".jpeg").contains(originalFilename.substring(originalFilename.lastIndexOf(".")))) {
            throw new CmsException(ApiResultEnum.FILE_FORMAT_ERR, "请上传图片格式");
        }
        Map<String, Object> map;
        try {
            map = uploadService.uploadRenameImage(file);
            if (!(Boolean) map.get("result")) {
                throw new CmsException(ApiResultEnum.FAILURE_REDIRECT, "图片上传失败");
            }
        } catch (Exception e) {
            throw new CmsException(ApiResultEnum.FAILURE_REDIRECT, "图片上传失败");
        }
        return Objects.toString(map.get("data"));
    }


}
