package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.CourseTypeBean;
import com.ics.cmsadmin.modules.basic.dao.CourseTypeDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

@Slf4j
public class CourseTypeServiceTest extends CmsAdminApplicationTests {

    @Autowired
    private CourseTypeService courseTypeService;

    @Test
    public void list() {

        PageResult pageResult = courseTypeService.list(CourseTypeBean.builder().isActive(true).build(), new PageBean(1, Integer.MAX_VALUE));
        log.info("course type list = {}", pageResult);
    }

    @Test
    public void insert() {
        courseTypeService.insert(CourseTypeBean.builder().isActive(false).description("test").name("test course type").build());
    }

    @Test
    public void update() {
        courseTypeService.update("6", CourseTypeBean.builder().isActive(false).description("test").name("test course").build());
    }

    @Test
    public void delete() {
        courseTypeService.delete("6");
    }
}
