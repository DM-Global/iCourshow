package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;
import com.ics.cmsadmin.modules.basic.dao.UpushHistoryDao;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.service.UpushHistoryService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * t_upush_history
 * Created Sandwich on 2019-15-13 22:06:38.
 */
@Service
public class UpushHistoryServiceImpl implements UpushHistoryService {
    @Resource
    private UpushHistoryDao upushHistoryDao;

    @Override
    public UpushHistoryBean queryById(String id) {
        if (StringUtils.isBlank(id)){
            return null;
        }
        return upushHistoryDao.queryById(id);
    }

    @Override
    public PageResult list(UpushHistoryBean bean, PageBean page) {
        long count = upushHistoryDao.count(bean);
        if (count == 0){
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, upushHistoryDao.list(bean, page));
    }

    @Override
    public boolean insert(UpushHistoryBean bean) {
        if (bean == null){
            return false;
        }
        return upushHistoryDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, UpushHistoryBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null){
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return upushHistoryDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null){
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return upushHistoryDao.delete(id) == 1;
    }

    @Override
    public void insertBatch(List<String> studentIds, UpushHistoryBean upushHistoryBean) {
        if (CollectionUtils.isEmpty(studentIds) || upushHistoryBean == null) {
            return;
        }
        upushHistoryDao.insertBatch(studentIds, upushHistoryBean);
    }

    @Override
    public UpushHistoryBean readMessage(String messageId, String studentId) {
        UpushHistoryBean upushHistoryBean = upushHistoryDao.findOne(UpushHistoryBean.builder()
            .studentId(studentId)
            .id(messageId)
            .build());
        if (upushHistoryBean == null) {
            return null;
        }
        upushHistoryDao.updateRead(messageId, studentId);
        return upushHistoryBean;
    }

    @Override
    public Long getUnReadCount(String studentId) {
        return upushHistoryDao.count(UpushHistoryBean.builder()
            .studentId(studentId)
            .isRead(false)
            .build());
    }
}
