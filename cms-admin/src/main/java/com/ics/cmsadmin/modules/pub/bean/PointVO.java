package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class PointVO {

    private Integer id;

    private String name;

    private Boolean isActive;

    private Boolean isPublic;

    private String description;

    private Integer unitId;

    private String unitName;

    private Integer courseId;

    private String courseName;

    private Integer subjectId;

    private String subjectName;

    private Integer rank;

    private Integer topicId;

    private String topicName;

    private String pointType;

    private String pointSource;

    private Integer finished;

    private Integer score;

    private Integer coverId;

    private String coverSrc;

    private Boolean collected;

    /**
     * 课程是否已经购买
     */
    private Boolean hasBought;

    /**
     * 是否点赞了
     */
    private Boolean liked;

    /**
     * 点赞数量
     */
    private Integer thumbUpCount;

    /**
     * 收藏数量
     */
    private Integer collectCount;
}
