package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.constraints.*;
import java.util.Date;

@Data
@Builder
public class ScoreBean {

    private String id;                    // 得分id
    @NotEmpty(message = "提示id不能为空")
    private String problemSetId;        // 题集id
    private String studentId;           // 用户id
    @NotEmpty(message = "分数不能为空")
    @DecimalMin(value = "0", message = "分数最小值不能小于0")
    @DecimalMax(value = "100", message = "分数最大值不能大于100")
    private String score;               // 分数
    @NotEmpty(message = "答对题目数不能为空")
    @Min(value = 0, message = "答对题目数不能小于0")
    private String correctNumber;       // 答对题目数
    @NotEmpty(message = "总题目数不能为空")
    @Min(value = 0, message = "总题目数不能小于0")
    private String totalNumber;         // 总题目数
    private Date createTime;            // 创建时间
    private Date updateTime;            // 最后更新时间

    @Tolerate
    public ScoreBean() {
    }

}
