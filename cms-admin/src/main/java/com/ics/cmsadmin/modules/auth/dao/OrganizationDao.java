package com.ics.cmsadmin.modules.auth.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.frame.core.dao.TreeDataDao;
import com.ics.cmsadmin.modules.auth.bean.OrganizationBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * sys_organization
 * Created by lvsw on 2018-29-21 10:10:05.
 */
@Repository
@Mapper
public interface OrganizationDao extends BaseDataDao<OrganizationBean>,
    TreeDataDao<OrganizationBean>, BaseDao<OrganizationBean> {

}
