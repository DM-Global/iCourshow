package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

import java.util.List;

@Data
public class Teacher extends Resource<Integer> {

    private String name;

    private String description;

    private String summary;

    private String photoSrc;

    private Boolean isFamous;

    private Integer photoId;

    private Boolean isActive;

    private List<String> subjects;
}
