package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;
import java.util.List;


/**
 * t_teacher
 * <p>
 * <p>
 * Created by lvsw on 2018-14-25 14:09:44.
 */
@Data
@Builder
public class TeacherBean {

    private String id;                 // id
    private String name;               // 名字
    private String description;        // 描述
    private String summary;            // 短描述
    private Boolean isActive;           // 状态
    private Boolean isFamous;           // 著名
    private String photoId;            // 头像id
    private String photoSrc;            // 头像
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 更新时间

    private String courseId;

    private List<String> subjects;

    @Tolerate
    public TeacherBean() {
    }
}
