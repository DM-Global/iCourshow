package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

/**
 * Created by Sandwich on 2019/1/25
 */
@Builder
@Data
public class IosPayDto {

    String orderId;

    String receipt;

    @Tolerate
    public IosPayDto() {
    }

}
