package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.student.bean.StudentCertificateBean;

import java.util.List;

public interface VideoHistoryService extends BaseDataService<VideoHistoryBean>, BaseService<VideoHistoryBean> {

    VideoHistoryBean findOne(String userId, String videoId);

    VideoHistoryBean findByCourseId(String userId, String courseId);

    boolean save(VideoHistoryBean videoHistoryBean);

    List<VideoHistoryBean> listByUserId(String userId);

    StudentCertificateBean save4app(VideoHistoryBean videoHistoryBean);
}
