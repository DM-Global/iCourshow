package com.ics.cmsadmin.modules.auth.service.impl;

import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.bean.FourTupleBean;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.auth.service.RoleAuthorizeService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.modules.auth.steward.AuthRepositories.*;

/**
 * Created by lvsw on 2017/10/15.
 */
@Service
public class RoleAuthorizeServiceImpl implements RoleAuthorizeService {

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean addAuthorizes2Role(List<String> authCodes, String roleId, String creator) {
        if (CollectionUtils.isEmpty(authCodes) || StringUtils.isBlank(roleId)) {
            return true;
        }
        if (roleDao.queryById(roleId) == null) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL);
        }
        if (authorizeDao.countAuthByCodes(authCodes) != authCodes.size()) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL);
        }
        roleAuthorizeDao.addAuthorizes2Role(authCodes, roleId, creator);
        return true;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean removeAuthorizes2Role(List<String> authCodes, String roleId) {
        if (CollectionUtils.isEmpty(authCodes) || StringUtils.isBlank(roleId)) {
            return true;
        }
        roleAuthorizeDao.removeAuthorizes2Role(authCodes, roleId);
        return true;
    }

    @CacheDbMember(returnClass = String.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<String> listRoleMenuAuthorize(String roleId, String menuId) {
        return roleAuthorizeDao.listRoleAuthorizesByMenuId(roleId, menuId);
    }

    @CacheDbMember(returnClass = String.class, group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public List<String> listRoleMenu(String roleId, String menuId) {
        return roleAuthorizeDao.listRoleMenu(roleId, menuId);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_AUTH_GROUP)
    @Override
    public boolean updateRoleMenuAuthorize(String roleId,
                                           FourTupleBean<List<String>, List<String>, List<String>, List<String>> menuAuthorize, String loginUserId) {
        List<String> wantAddMenu = menuAuthorize.one;
        List<String> wantDeleteMenu = menuAuthorize.two;
        List<String> wantAddMenuAuthorize = menuAuthorize.three;
        List<String> wantDeleteMenuAuthorize = menuAuthorize.four;
        addMenu2Role(wantAddMenu, roleId, loginUserId);
        removeMenu2Role(wantDeleteMenu, roleId);
        addAuthorizes2Role(wantAddMenuAuthorize, roleId, loginUserId);
        removeAuthorizes2Role(wantDeleteMenuAuthorize, roleId);
        return true;
    }

    private boolean removeMenu2Role(List<String> menuList, String roleId) {
        if (CollectionUtils.isEmpty(menuList) || StringUtils.isBlank(roleId)) {
            return true;
        }
        roleAuthorizeDao.removeMenu2Role(menuList, roleId);
        return true;
    }

    private boolean addMenu2Role(List<String> menuList, String roleId, String loginUserId) {
        if (CollectionUtils.isEmpty(menuList) || StringUtils.isBlank(roleId)) {
            return true;
        }
        if (roleDao.queryById(roleId) == null) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL);
        }
        menuList = menuList.stream().filter(item -> !StringUtils.equalsAnyIgnoreCase("other", item)).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(menuList)) {
            return true;
        }
        if (roleAuthorizeDao.countMenuByMenuId(menuList) != menuList.size()) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL);
        }
        roleAuthorizeDao.addMenu2Role(menuList, roleId, loginUserId);
        return true;
    }
}
