package com.ics.cmsadmin.modules.system.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Builder
@Data
public class PhoneCodeBean {
    private String code;
    private String nameTw;
    private String nameEn;
    private String nameZh;
    private String locale;
    private String orderIndex;

    @Tolerate
    public PhoneCodeBean() {
    }
}
