package com.kco.generate.config;

import java.util.List;

/**
 * Created by 666666 on 2017/9/26.
 */

public class TableBean {
    private String tableComment;
    private String tableName;
    private String noPrefixTableName;
    private String className;
    private List<ColumnsBean> columnsBeans;
    private String primaryColumnKey;
    private String primaryPropertyKey;
    private String primaryPropertyKeyColumnComment;
    private String basePackage;
    private String classNameVariable;

    public String getBasePackage() {
        return basePackage;
    }

    public TableBean setBasePackage(String basePackage) {
        this.basePackage = basePackage;
        return this;
    }

    public String getClassNameVariable() {
        return classNameVariable;
    }

    public void setClassNameVariable(String classNameVariable) {
        this.classNameVariable = classNameVariable;
    }

    public String getPrimaryPropertyKeyColumnComment() {
        return primaryPropertyKeyColumnComment;
    }

    public TableBean setPrimaryPropertyKeyColumnComment(String primaryPropertyKeyColumnComment) {
        this.primaryPropertyKeyColumnComment = primaryPropertyKeyColumnComment;
        return this;
    }

    public String getNoPrefixTableName() {
        return noPrefixTableName;
    }

    public void setNoPrefixTableName(String noPrefixTableName) {
        this.noPrefixTableName = noPrefixTableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public String getPrimaryColumnKey() {
        return primaryColumnKey;
    }

    public void setPrimaryColumnKey(String primaryColumnKey) {
        this.primaryColumnKey = primaryColumnKey;
    }

    public String getPrimaryPropertyKey() {
        return primaryPropertyKey;
    }

    public void setPrimaryPropertyKey(String primaryPropertyKey) {
        this.primaryPropertyKey = primaryPropertyKey;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<ColumnsBean> getColumnsBeans() {
        return columnsBeans;
    }

    public void setColumnsBeans(List<ColumnsBean> columnsBeans) {
        this.columnsBeans = columnsBeans;
    }

    @Override
    public String toString() {
        return "TableBean{" +
            "tableName='" + tableName + '\'' +
            ", className='" + className + '\'' +
            ", columnsBeans=" + columnsBeans +
            '}';
    }
}
