package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class CourseTypeBean {

    private String id;
    private String name;
    private Boolean isActive;
    private String description;

    @Tolerate
    public CourseTypeBean() {
    }
}
