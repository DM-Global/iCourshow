package com.ics.cmsadmin.modules.system.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.system.bean.RegisterBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-03-29.
 */
@Repository
@Mapper
public interface RegisterDao extends BaseDao<RegisterBean>, BaseDataDao<RegisterBean> {
}
