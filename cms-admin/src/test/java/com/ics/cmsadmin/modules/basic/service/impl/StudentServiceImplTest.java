package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class StudentServiceImplTest extends CmsAdminApplicationTests {

    @Autowired
    private OrderService orderService;

    @Test
    public void test() {
        BigDecimal a = new BigDecimal(1.2);
        System.out.println("BigDecimal.ONE==>" + a.compareTo(BigDecimal.ONE));
    }

    @Test
    public void testBatchDelete() {
        List<String> ids = new ArrayList<String>();
        ids.add("c5a3f8b0153640b38bf8b009a0b50c43");
        ids.add("cea17eb924cc436a9cc0637902eb3822");
        orderService.batchDelete((ArrayList<String>) ids);
        System.out.println("removed all from the list");
    }

    @Test
    public void testBatchSoftRemove() {
        PageResult pageResult = orderService.list(OrderBean.builder().userId("2ced1748c830424691f987a4094c4202").build(), new PageBean(1, Integer.MAX_VALUE));
        List<OrderBean> list = pageResult.getDataList();
        List<String> ids = new ArrayList<>();
        for (OrderBean order : list) {
            order.setIsDeleted(Boolean.TRUE);
            ids.add(order.getId());
        }
        orderService.batchSoftRemove((ArrayList<String>) ids);
        System.out.println("soft removed all from the list");
    }

}
