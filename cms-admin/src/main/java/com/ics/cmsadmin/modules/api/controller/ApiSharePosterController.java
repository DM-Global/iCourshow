package com.ics.cmsadmin.modules.api.controller;


import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import com.ics.cmsadmin.modules.basic.service.SharePosterService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/shareposter")
public class ApiSharePosterController {

    @Autowired
    private SharePosterService sharePosterService;

    @ApiOperation(value = "分页查询分享海报信息")
    @PostMapping("/list")
    public ApiResponse list(@RequestBody SharePosterBean sharePosterBean,
                            @ApiParam("页码") @RequestParam(value = "pageNo", defaultValue = "1") Integer pageNo,
                            @ApiParam("每页条数") @RequestParam(value = "pageSize", defaultValue = "100") Integer pageSize) {
        PageBean page = new PageBean(pageNo, pageSize);
        sharePosterBean.setIsActive(true);
        PageResult pageResult = sharePosterService.list(sharePosterBean, page);
        return ApiResponse.getDefaultResponse(pageResult);
    }

    @GetMapping("/query/{id}")
    public ApiResponse detail(@PathVariable(value = "id") String id, HttpServletRequest request) {
        SharePosterBean sharePosterBean = sharePosterService.queryById(id);
        return ApiResponse.getDefaultResponse(sharePosterBean);
    }

}
