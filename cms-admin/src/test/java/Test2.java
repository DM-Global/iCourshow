import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ics.cmsadmin.modules.system.bean.PhoneCodeBean;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Test2 {
    public static void main(String[] args) throws Exception {
        String string = FileUtils.readFileToString(new File("C:\\Users\\pc\\Desktop\\美国\\code.json"));
//        System.out.println(string);
        Gson gson = new Gson();
        List<Map<String, Object>> s = gson.fromJson(string, new TypeToken<List<Map<String, Object>>>() {
        }.getType());
        List<PhoneCodeBean> collect = s.stream().map(item -> PhoneCodeBean.builder()
            .code(Objects.toString(item.get("code")).replace(".0", ""))
            .nameEn(Objects.toString(item.get("en")))
            .nameTw(Objects.toString(item.get("tw")))
            .nameZh(Objects.toString(item.get("zh")))
            .locale(Objects.toString(item.get("locale")))
            .build()
        ).collect(Collectors.toList());
        StringBuilder sb = new StringBuilder();
        sb.append("insert into com_phone_code(code, name_tw, name_en, name_zh, locale, order_index, create_time)values\n");
        AtomicInteger count = new AtomicInteger(1);
        collect.stream().forEach(item -> {
            sb.append(String.format("('%s','%s','%s', '%s','%s', '%s', now()),\n", item.getCode(), item.getNameTw(), item.getNameEn(), item.getNameZh(), item.getLocale(), (count.getAndIncrement()) + ""));
        });
        System.out.println(sb.toString());
//            .reduce("insert into com_phone_code(code, name_tw, name_en, name_zh, locale, create_time)values\n",
//                (all, item) -> all += String.format("('%s','%s','%s', '%s', now()),\n", item.getCode()));

    }
}
