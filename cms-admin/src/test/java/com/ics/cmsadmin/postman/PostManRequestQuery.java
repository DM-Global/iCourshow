package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PostManRequestQuery {
    private String key;
    private String value;
}
