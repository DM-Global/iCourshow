package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by 666666 on 2018/8/27.
 */
@Data
@Component
@ConfigurationProperties(prefix = "app")
public class AppConfig {
    private boolean initAuthorize;  // 后台启动是否初始化权限
    //    private String androidAppId;
//    private String androidAppSecret;
//    private String iosAppId;
//    private String iosAppSecret;
    private String wechatAppId;
    private String wechatAppSecret;
}
