package com.ics.cmsadmin.modules.student.steward;

import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.basic.steward.BasicServices;
import com.ics.cmsadmin.modules.student.service.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class StudentServices {
    public static StudentService studentService;
    public static StudentAccountService studentAccountService;
    public static StudentAccountDetailService studentAccountDetailService;
    public static StudentDayService studentDayService;
    public static StudentShareDetailService studentShareDetailService;
    public static StudentScoreService studentScoreService;

    @Resource(name = "newStudentService")
    public void setStudentService(StudentService studentService) {
        StudentServices.studentService = studentService;
}

    @Resource
    public void setStudentAccountService(StudentAccountService studentAccountService) {
        StudentServices.studentAccountService = studentAccountService;
    }

    @Resource
    public void setStudentAccountDetailService(StudentAccountDetailService studentAccountDetailService) {
        StudentServices.studentAccountDetailService = studentAccountDetailService;
    }

    @Resource
    public void setStudentDayService(StudentDayService studentDayService) {
        StudentServices.studentDayService = studentDayService;
    }

    @Resource
    public void setStudentShareDetailService(StudentShareDetailService studentShareDetailService) {
        StudentServices.studentShareDetailService = studentShareDetailService;
    }

    @Resource
    public void setStudentScoreService(StudentScoreService studentScoreService) {
        StudentServices.studentScoreService = studentScoreService;
    }
}
