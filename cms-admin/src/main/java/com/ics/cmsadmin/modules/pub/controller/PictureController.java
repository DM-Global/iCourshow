package com.ics.cmsadmin.modules.pub.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.pub.bean.Picture;
import com.ics.cmsadmin.modules.pub.service.PictureService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/pictures")
public class PictureController extends ResourceController<PictureService, Picture, Integer> {


    /**
     * 分页获取图片数据
     *
     * @param picType  图片类型
     * @param pageSize 每页条目数
     * @param page     当前页
     * @return
     */
    @GetMapping
    public ApiResponse list(Integer picType, @RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
                            @RequestParam(value = "page", defaultValue = "1") int page) {

        Map<String, Object> params = new HashMap<>();
        params.put("picType", picType);
        params.put("page", page);
        params.put("pageSize", pageSize);
        return super.get(params);
    }
}
