package com.ics.cmsadmin.modules.system.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * c_meta
 * meta
 * Created by lvsw on 2018-07-03.
 */
@Data
@Builder
public class MetaBean {
    @NotNull(message = "metaId不能为空", groups = InsertGroup.class)
    @Length(message = "metaId长度必须在{min}~{max}之间", min = 2, max = 50, groups = InsertGroup.class)
    @Pattern(message = "metaId只能包含数字和字母,且以字母开头", regexp = "^[a-zA-Z][-a-zA-Z0-9]+$", groups = InsertGroup.class)
    private String metaId;
    private String parentId;
    @NotNull(message = "meta名字不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "meta名字长度必须在{min}~{max}之间", min = 2, max = 30, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    private String value;
    @NotNull(message = "meta排序不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Pattern(message = "meta排序必须是0或者正整数", regexp = "^0|[1-9]\\d*$", groups = {InsertGroup.class, UpdateGroup.class})
    private String orderIndex;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public MetaBean() {
    }
}
