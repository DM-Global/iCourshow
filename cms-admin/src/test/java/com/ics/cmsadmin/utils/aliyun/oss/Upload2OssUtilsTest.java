package com.ics.cmsadmin.utils.aliyun.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.DeleteObjectsRequest;
import com.aliyun.oss.model.DeleteObjectsResult;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.property.AliOssConfig;
import com.ics.cmsadmin.modules.system.service.UploadService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Upload2OssUtilsTest extends CmsAdminApplicationTests {

    @Autowired
    UploadService uploadService;
    @Resource
    private AliOssConfig aliOssConfig;

    @Test
    public void deleteUploadedImage() {
        String url = "https://ics-storage.oss-us-west-1.aliyuncs.com/imge/20180723_0da97729-b64d-4ac4-b858-cca15b3f9813.jpg";
        String fileName = url.substring(url.lastIndexOf("/") + 1);
        boolean b = uploadService.deleteUploadedImageByFileName(fileName);
        Assert.assertTrue(b);
    }

    @Test
    public void uploadFile() throws Exception {
        OSSClient ossClient = new OSSClient("https://" + aliOssConfig.endpoint, aliOssConfig.accessKeyId, aliOssConfig.accessKeySecret);
        File file = new File("D:\\linux\\小说\\have_trial_order.png");
        ossClient.putObject(new PutObjectRequest("ics-app",  "image/" + file.getName(), new FileInputStream(file)));
    }

    @Test
    public void putObjectTest() throws Exception {
        OSSClient ossClient = new OSSClient("https://" + aliOssConfig.endpoint, aliOssConfig.accessKeyId, aliOssConfig.accessKeySecret);
        File file = new File("C:\\Users\\666666\\Desktop\\test\\openproxy.jpg");
        PutObjectResult putObjectResult = ossClient.putObject(new PutObjectRequest(aliOssConfig.bucketName, "image/" + file.getName(), new FileInputStream(file)));
        System.out.println(putObjectResult);
    }
    @Test
    public void deleteObjectTest() throws Exception {
        OSSClient ossClient = new OSSClient("https://" + aliOssConfig.endpoint, aliOssConfig.accessKeyId, aliOssConfig.accessKeySecret);
        ossClient.deleteObject(aliOssConfig.bucketName, "image/openproxy.jpg");

//        String data = https + aliOssConfig.bucketName + "." + aliOssConfig.endpoint + "/" + filePath + fileName;
        // https://ics-img-storage.oss-cn-beijing.aliyuncs.com/image/openproxy.jpg
        //     endpoint: oss-cn-beijing.aliyuncs.com
        //    accessKeyId: LTAIPPlPVaYLC4B3
        //    accessKeySecret: z5EaCBz3GRoURMJqGW7sm60pd1B9N7
        //    bucketName: ics-img-storage
        //    imagePath: imge/
        //    commonPath: normal/

//        File file = new File("C:\\Users\\666666\\Desktop\\test\\openproxy.jpg");
//        PutObjectResult putObjectResult = ossClient.putObject(new PutObjectRequest(aliOssConfig.bucketName, "image/" + file.getName(), new FileInputStream(file)));
//        System.out.println(putObjectResult);
    }

}
