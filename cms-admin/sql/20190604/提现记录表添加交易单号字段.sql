ALTER TABLE `t_student_withdraw_auditing`
ADD COLUMN `payment_no` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '交易单号：微信支付入账通知可以显示这个单号，用于运维投拆跟踪' AFTER `relation_order_no`;
