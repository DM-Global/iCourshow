package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.agent.bean.ShareDetailBean;


/**
 * a_share_detail 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface ShareDetailService extends LoginBaseDataService<ShareDetailBean> {

    /**
     * 生成分润数据
     *
     * @param orderId 订单id
     */
    boolean generateShareByOrderId(String orderId);
}
