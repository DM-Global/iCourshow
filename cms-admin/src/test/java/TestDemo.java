import com.google.gson.Gson;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;
import com.ics.cmsadmin.modules.student.bean.StudentDaySummaryBean;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentDaySummaryDao;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-06-05 09:13
 */
public class TestDemo {
    @Test
    public void test1() {
//        System.out.println(new StudentAccountBean());
// 72k2o9lchpp3u89o0ecjxxj
// coit7ycos8spb69dm8s715j0k
        String s = "d6334d28941342189670fc4c59a9cc84";
        BigInteger bigInteger = new BigInteger(s, 16);
        System.out.println(bigInteger);
        System.out.println(bigInteger.toString(36));

        BigInteger bigInteger1 = new BigInteger("72k2o9lchpp3u89o0ecjxxj", 36);
        System.out.println(bigInteger1.toString(16));
    }

    @Test
    public void test2() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startCreate = LocalDate.parse("2019-07-28", dateTimeFormatter);
        LocalDate endCreate = LocalDate.parse("2019-07-30", dateTimeFormatter);
        List<StudentDaySummaryBean> result = new ArrayList<>();
        Map<String, StudentDaySummaryBean> collect = new HashMap<>();
        while (!startCreate.isAfter(endCreate)) {
            result.add(Optional.ofNullable(collect.get(startCreate.format(dateTimeFormatter)))
                .orElse(StudentDaySummaryBean.builder()
                    .shareMoneyCusm("0")
                    .summaryDay(startCreate.format(dateTimeFormatter))
                    .shareMoneyNewly("0").build()));
            startCreate = startCreate.plusDays(1);
        }
        System.out.println(GsonUtils.toJson(result));
//        return result;
    }
}
