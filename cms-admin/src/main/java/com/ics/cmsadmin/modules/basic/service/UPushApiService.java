package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;

import java.util.List;

/**
 * Created by Sandwich on 2019/4/1
 */
public interface UPushApiService {

    void sendBroadcast(UPushCustomDefined defined);

    void sendUnicast(String studentId, UPushCustomDefined defined);

    void sendUnicast(List<String> studentId, UPushCustomDefined defined);

//    Boolean sendAndroidBroadcast(String title, String content, String customDefined) throws Exception;
//
//    Boolean sendAndroidUnicast(String studentId, String title, String content, String customDefined) throws Exception;
//
//    void sendAndroidCustomizedcast() throws Exception;
//
//    void sendAndroidGroupcast() throws Exception;
//
//    void sendAndroidCustomizedcastFile() throws Exception;
//
//    void sendAndroidFilecast() throws Exception;
//
//    Boolean sendIOSBroadcast(String content, String customDefined) throws Exception;
//
//    Boolean sendIOSUnicast(String studentId, String content, String customDefined) throws Exception;
//
//    void sendIOSGroupcast() throws Exception;
//
//    void sendIOSCustomizedcast() throws Exception;
//
//    void sendIOSFilecast() throws Exception;
}
