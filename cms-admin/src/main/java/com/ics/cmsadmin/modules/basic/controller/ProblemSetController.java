package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.ProblemSetBean;
import com.ics.cmsadmin.modules.basic.service.ProblemSetService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * ProblemSet controller
 * Created by lvsw on 2018-09-06.
 */
@Api(description = "题集管理接口")
@RestController
@RequestMapping("/problemSet")
public class ProblemSetController {

    @Resource
    private ProblemSetService problemSetService;

    @Authorize(AuthorizeEnum.PROBLEM_SET_QUERY)
    @ApiOperation(value = "查询题集信息")
    @GetMapping(value = "/query/{problemSetId}")
    public ApiResponse queryById(@ApiParam(value = "problemSetId") @PathVariable String problemSetId) {
        return new ApiResponse(problemSetService.queryById(problemSetId));
    }

    @Authorize(AuthorizeEnum.PROBLEM_SET_QUERY)
    @ApiOperation(value = "根据问题id查询题集信息")
    @GetMapping(value = "/queryByProblemId/{problemId}")
    public ApiResponse queryByProblemId(@ApiParam(value = "problemId") @PathVariable String problemId) {
        return new ApiResponse(problemSetService.queryByProblemId(problemId));
    }

    @Authorize(AuthorizeEnum.PROBLEM_SET_INSERT)
    @ApiOperation(value = "新增题集信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody ProblemSetBean problemSetBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(problemSetService.insert(problemSetBean));
    }

//    @Authorize(AuthorizeEnum.PROBLEM_SET_DELETE)
//    @ApiOperation(value = "删除题集信息")
//    @PostMapping(value = "/delete/{problemSetId}")
//    public ApiResponse delete(@ApiParam("需要删除的题集 id") @PathVariable String problemSetId){
//        return new ApiResponse(problemSetService.delete(problemSetId));
//    }

    @Authorize(AuthorizeEnum.PROBLEM_SET_UPDATE)
    @ApiOperation(value = "禁用题集信息")
    @PostMapping(value = "/disable/{problemSetId}")
    public ApiResponse disable(@ApiParam("需要禁用的题集 id") @PathVariable String problemSetId) {
        return new ApiResponse(problemSetService.update(problemSetId, ProblemSetBean.builder().isActive("0").build()));
    }

    @Authorize(AuthorizeEnum.PROBLEM_SET_UPDATE)
    @ApiOperation(value = "启用题集信息")
    @PostMapping(value = "/enable/{problemSetId}")
    public ApiResponse enable(@ApiParam("需要启用的题集 id") @PathVariable String problemSetId) {
        return new ApiResponse(problemSetService.update(problemSetId, ProblemSetBean.builder().isActive("1").build()));
    }

    @Authorize(AuthorizeEnum.PROBLEM_SET_UPDATE)
    @ApiOperation(value = "更新题集信息")
    @PostMapping(value = "/update/{problemSetId}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody ProblemSetBean problemSetBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的题集Id") @PathVariable String problemSetId) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(problemSetService.update(problemSetId, problemSetBean));
    }

    @Authorize(AuthorizeEnum.PROBLEM_SET_QUERY)
    @ApiOperation(value = "分页查询题集信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody ProblemSetBean problemSetBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(problemSetService.list(problemSetBean, pageBean));
    }

}
