package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Sandwich on 2018-09-17
 */
@Data
@Component
@ConfigurationProperties(prefix = "alioss")
public class AliOssConfig {

    public String endpoint;
    public String accessKeyId;
    public String accessKeySecret;
    public String bucketName;
    //bucket里面的文件路径
    public String imagePath;
    public String commonPath;
}
