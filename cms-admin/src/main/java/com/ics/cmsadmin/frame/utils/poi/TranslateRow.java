package com.ics.cmsadmin.frame.utils.poi;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

/**
 * Excel 解析转化接口
 * Created by Administrator on 2017/5/24.
 */
public interface TranslateRow<T> {
    /**
     * 解析转化接口
     *
     * @param row    Excel 行
     * @param index  第几行
     * @param errors 错误信息集合
     * @return 解析后的数据
     */
    T translate(Row row, int index, List<ExcelErrorMsgBean> errors);

    /**
     * 解析表格
     *
     * @param sheet  表格
     * @param errors 错误信息
     * @return 解析后的数据
     */
    default List<T> paserSheet(Sheet sheet, List<ExcelErrorMsgBean> errors) {
        List<T> resultList = new ArrayList<>();
        int len = sheet.getLastRowNum();
        for (int i = 1; i <= len; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                errors.add(new ExcelErrorMsgBean(i + 1, -1, String.format("第%d行为空!", i + 1)));
            } else {
                T result = translate(row, i, errors);
                if (result != null) {
                    resultList.add(result);
                }
            }
        }
        return resultList;
    }
}
