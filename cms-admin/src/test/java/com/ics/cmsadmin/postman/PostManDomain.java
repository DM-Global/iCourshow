package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PostManDomain {
    private String name;

    private List<PostManItemDomain> item;

}
