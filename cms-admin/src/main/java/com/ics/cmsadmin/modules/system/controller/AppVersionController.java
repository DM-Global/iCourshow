package com.ics.cmsadmin.modules.system.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import com.ics.cmsadmin.modules.system.service.AppVersionService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import static com.ics.cmsadmin.modules.system.steward.SystemServices.appVersionService;

/**
 * t_app_version controller
 * Created by lvsw on 2018-17-05 20:12:49.
 */
@Api(description = "app版本信息")
@RestController
@RequestMapping("/appVersion")
public class AppVersionController {

    @Authorize(AuthorizeEnum.APP_VERSION_QUERY)
    @ApiOperation(value = "查询app版本信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(appVersionService.queryById(id));
    }

    @Authorize(AuthorizeEnum.APP_VERSION_INSERT)
    @ApiOperation(value = "新增app版本信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody AppVersionBean appVersionBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(appVersionService.insert(appVersionBean));
    }

    @Authorize(AuthorizeEnum.APP_VERSION_DELETE)
    @ApiOperation(value = "删除app版本信息信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
        return new ApiResponse(appVersionService.delete(id));
    }

    @Authorize(AuthorizeEnum.APP_VERSION_UPDATE)
    @ApiOperation(value = "更新app版本信息信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody AppVersionBean appVersionBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(appVersionService.update(id, appVersionBean));
    }

    @Authorize(AuthorizeEnum.APP_VERSION_QUERY)
    @ApiOperation(value = "分页查询app版本信息信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody AppVersionBean appVersionBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(appVersionService.list(appVersionBean, pageBean));
    }

}
