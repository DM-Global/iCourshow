package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.app.utils.SharePosterUtils;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import com.ics.cmsadmin.modules.basic.dao.SharePosterDao;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Sandwich on 2019/6/22
 */
@Api(description = "app分享海报接口", tags = "app分享海报")
@RestController
@RequestMapping("app/sharePoster")
@Log4j2
public class AppSharePosterController {

    @Autowired
    private StudentService studentService;

    @ApiOperation(value = "支付成功后查询分享海报")
    @GetMapping("/afterPaid")
    public ApiResponse sharePosterAfterPaid(HttpServletRequest request) throws IOException {
        String loginUserId = SsoUtils.getLoginUserId(request);
        StudentBean studentBean = studentService.queryById(loginUserId);
        if (studentBean == null) {
            return new ApiResponse(Boolean.FALSE);
        }
        String userName = studentBean.getNickname();
        if (StringUtils.isBlank(userName)) {
            userName = studentBean.getUsername();
        }
        return ApiResponse.getDefaultResponse(SharePosterUtils.getAdmissionNotice(userName, loginUserId));
    }

}
