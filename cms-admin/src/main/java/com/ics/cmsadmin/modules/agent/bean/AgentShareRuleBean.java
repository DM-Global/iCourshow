package com.ics.cmsadmin.modules.agent.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.validation.constraints.NotNull;
import java.util.Date;


/**
 * a_agent_share_rule
 * 代理商分润规则
 * <p>
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Data
@Builder
public class AgentShareRuleBean {

    private String shareId;              // 分润id
    private String agentNo;              // 代理商编号
    private String type;              // 分润类型
    private String expression;              // 分润表达式
    private String status;              // 状态
    @NotNull(message = "分润规则不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    private Object data;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间

    @Tolerate
    public AgentShareRuleBean() {
    }
}
