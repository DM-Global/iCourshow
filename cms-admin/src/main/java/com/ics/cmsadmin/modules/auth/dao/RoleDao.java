package com.ics.cmsadmin.modules.auth.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.auth.bean.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lvsw on 2018-03-29.
 */
@Repository
@Mapper
public interface RoleDao extends BaseDataDao<SysRole>, BaseDao<SysRole> {
    /**
     * 查询不属于用户的角色信息
     */
    List<SysRole> listNotBelong2UserRoles(String userId);

    /**
     * 查询属于用户的角色信息
     */
    List<SysRole> listUserRoleByUserId(String userId);
}
