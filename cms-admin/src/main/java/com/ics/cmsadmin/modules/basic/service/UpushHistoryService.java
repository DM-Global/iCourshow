package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;

import java.util.List;


/**
* t_upush_history 服务类
* Created by Sandwich on 2019-15-13 22:06:38.
*/
public interface UpushHistoryService extends BaseService<UpushHistoryBean>, BaseDataService<UpushHistoryBean> {

    void insertBatch(List<String> studentIds, UpushHistoryBean upushHistoryBean);

    UpushHistoryBean readMessage(String messageId, String studentId);

    Long getUnReadCount(String studentId);
}
