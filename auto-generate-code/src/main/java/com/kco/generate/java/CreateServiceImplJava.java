package com.kco.generate.java;

import com.kco.generate.config.TableBean;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kco.generate.config.Utils.saveFile;

/**
 * 创建bean java文件
 * Created by 666666 on 2017/9/26.
 */
public final class CreateServiceImplJava {

    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildHeader(tableBean))
            .append(buildQueryById(tableBean))
            .append(buildList(tableBean))
            .append(buildInsert(tableBean))
            .append(buildUpdate(tableBean))
            .append(buildDelete(tableBean))
            .append(buildTail(tableBean));
        saveFile("service/impl/" + tableBean.getClassName() + "ServiceImpl.java", sb.toString());
    }

    private static String buildTail(TableBean tableBean) {

        return "}\n";
    }

    private static String buildDelete(TableBean tableBean) {
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();

        sb.append("    @Override\n" +
            "    public boolean delete(String id) {\n" +
            "        if (StringUtils.isBlank(id) || queryById(id) == null){\n" +
            "            throw new CmsException(ApiResult.DELETE_TABLE_ITEM_FAIL, \"参数有误\");\n" +
            "        }\n" +
            "        return " + className1 + "Dao.delete(id) == 1;\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildUpdate(TableBean tableBean) {
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();

        sb.append("    @Override\n" +
            "    public boolean update(String id, " + className + "Bean bean) {\n" +
            "        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null){\n" +
            "            throw new CmsException(ApiResult.UPDATE_TABLE_ITEM_FAIL, \"参数有误\");\n" +
            "        }\n" +
            "        return " + className1 + "Dao.update(id, bean) == 1;\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildInsert(TableBean tableBean) {
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();

        sb.append("    @Override\n" +
            "    public boolean insert(" + className + "Bean bean) {\n" +
            "        if (bean == null){\n" +
            "            return false;\n" +
            "        }\n" +
            "        return " + className1 + "Dao.insert(bean) == 1;" +
            "   }\n\n");
        return sb.toString();
    }

    private static String buildList(TableBean tableBean) {
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();

        sb.append("    @Override\n" +
            "    public PageResult list(" + className + "Bean bean, PageBean page) {\n" +
            "        long count = " + className1 + "Dao.count(bean);\n" +
            "        if (count == 0){\n" +
            "            return new PageResult();\n" +
            "        }\n" +
            "        page = page == null ? new PageBean() : page;\n" +
            "        return PageResultUtil.getPage(count, " + className1 + "Dao.list(bean, page));\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildQueryById(TableBean tableBean) {
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();

        sb.append("    @Override\n" +
            "    public " + className + "Bean queryById(String id) {\n" +
            "        if (StringUtils.isBlank(id)){\n" +
            "            return null;\n" +
            "        }\n" +
            "        return " + className1 + "Dao.queryById(id);\n" +
            "    }\n\n");
        return sb.toString();
    }

    private static String buildHeader(TableBean tableBean) {
        String today = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String className = tableBean.getClassName();
        String className1 = className.substring(0, 1).toLowerCase() + className.substring(1);
        String basePackage = tableBean.getBasePackage();

        StringBuilder sb = new StringBuilder();
        sb.append("package com.ics.cmsadmin.service." + basePackage + ".impl;\n" +
            "\n" +
            "import com.ics.cmsadmin.frame.core.ApiResult;\n" +
            "import com.ics.cmsadmin.dao." + basePackage + "." + className + "Dao;\n" +
            "import com.ics.cmsadmin.frame.core.exception.CmsException;\n" +
            "import com.ics.cmsadmin.model.PageBean;\n" +
            "import com.ics.cmsadmin.model.PageResult;\n" +
            "import com.ics.cmsadmin.model." + basePackage + "." + className + "Bean;\n" +
            "import com.ics.cmsadmin.service." + basePackage + "." + className + "Service;\n" +
            "import com.ics.cmsadmin.utils.PageResultUtil;\n" +
            "import org.apache.commons.lang3.StringUtils;\n" +
            "import org.springframework.stereotype.Service;\n" +
            "\n" +
            "import javax.annotation.Resource;\n" +
            "\n" +
            "/**\n" +
            " * Created by lvsw on " + today + ".\n" +
            " */\n" +
            "@Service\n" +
            "public class " + className + "ServiceImpl implements " + className + "Service {\n" +
            "    @Resource\n" +
            "    private " + className + "Dao " + className1 + "Dao;\n\n");
        return sb.toString();
    }
}
