TRUNCATE a_account_detail;
TRUNCATE a_agent_account;
TRUNCATE a_agent_info;
TRUNCATE a_agent_share_rule;
TRUNCATE a_apply_withdraw_cash;
TRUNCATE a_share_detail;

ALTER TABLE t_order ADD COLUMN acc_status VARCHAR(20) NOT NULL DEFAULT '0' COMMENT '入账状态: 0 未入账, 1 已入账' AFTER  STATUS;
ALTER TABLE t_order ADD COLUMN acc_time TIMESTAMP  NULL  COMMENT '入账时间' AFTER acc_status;

ALTER TABLE sys_user ADD COLUMN `user_type` VARCHAR(10) NOT NULL DEFAULT '0' COMMENT '用户类型: 0 系统管理员, 1: 代理商' AFTER email;
ALTER TABLE t_student ADD COLUMN agent_node VARCHAR(20) NOT NULL DEFAULT '0-' COMMENT '代理商节点' AFTER  gender;
ALTER TABLE t_student ADD COLUMN agent_no VARCHAR(20) NOT NULL DEFAULT '0' COMMENT '代理商编号' AFTER  gender;
ALTER TABLE t_order ADD COLUMN agent_node VARCHAR(20) NOT NULL DEFAULT '0-' COMMENT '代理商节点' AFTER  STATUS;
ALTER TABLE t_order ADD COLUMN agent_no VARCHAR(20) NOT NULL DEFAULT '0' COMMENT '代理商编号' AFTER  STATUS;
ALTER TABLE sys_user MODIFY COLUMN user_id VARCHAR(20) NOT NULL COMMENT '用户id';
ALTER TABLE sys_role MODIFY COLUMN role_id VARCHAR(20) NOT NULL COMMENT '角色id';
ALTER TABLE sys_role_authorize MODIFY COLUMN role_id VARCHAR(20) NOT NULL COMMENT '角色id';
ALTER TABLE sys_role_menu MODIFY COLUMN role_id VARCHAR(20) NOT NULL COMMENT '角色id';
ALTER TABLE sys_user_role MODIFY COLUMN user_id VARCHAR(20) NOT NULL COMMENT '用户id';
ALTER TABLE sys_user_role MODIFY COLUMN role_id VARCHAR(20) NOT NULL COMMENT '角色id';

drop table IF EXISTS com_user_oper_log;
CREATE TABLE `com_user_oper_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '日志编号',
  `user_id` VARCHAR(50) DEFAULT NULL COMMENT '代理商编号',
   `request_uri` VARCHAR(512) DEFAULT NULL COMMENT '请求uri',
  `request_method` VARCHAR(512) DEFAULT NULL COMMENT '请求方法',
  `request_desc` VARCHAR(512) DEFAULT NULL COMMENT '请求描述',
  `request_params` TEXT COMMENT '请求参数',
  `return_result` TEXT COMMENT '返回结果',
  `oper_ip` VARCHAR(50) DEFAULT NULL COMMENT '请求ip',
  `oper_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='用户操作日志';

drop table IF EXISTS a_agent_info;
CREATE TABLE `a_agent_info` (
  `agent_no` varchar(20) NOT NULL COMMENT '代理商编号',
  `name` varchar(100) NOT NULL COMMENT '名字',
  `parent_id` varchar(20) NOT NULL COMMENT '父级代理商编号',
  `agent_type` varchar(20) NOT null default '2' comment '类别: 1. 公司/机构 2. 个人',
  `status` varchar(20) NOT null default '1' comment '状态: 0 无效, 1 有效',
  `work_phone` varchar(20) null comment '办公电话',
  `description` text null comment '描述',
  `province` varchar(20) NULL COMMENT '省份编码',
    `city` varchar(20) NULL COMMENT '城市编码',
    `county` varchar(20) NULL COMMENT '县编码',
    `city_name` varchar(256) NULL COMMENT '省市区地址',
    `address` varchar(256) NULL COMMENT '具体地址',
    `id_card` varchar(50) null comment '证件号',
    `real_name` varchar(50) null comment '证件名',
    `account_no` varchar(50) null comment '银行账号',
    `account_name` varchar(50) null comment '银行账号开户名',
    `wechat_name` varchar(100) null comment '微信真实姓名',
    `wechat_code_url` varchar(250) null comment '微信收款码',
    `zfb_name` varchar(100) null comment '支付宝真实姓名',
    `zfb_code_url` varchar(250) null comment '微信收款码',
    `card_type` varchar(50) null comment '卡类型 DC:借记卡，CC:信用卡',
    `bank` varchar(50) null comment '银行简称',
  `user_id` VARCHAR(20) NOT NULL COMMENT '用户id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`agent_no`),
    UNIQUE KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理商';


drop table IF EXISTS a_agent_share_rule;
CREATE TABLE `a_agent_share_rule` (
  `share_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分润id',
  `agent_no` varchar(20) NOT NULL COMMENT '代理商编号',
  `type` varchar(20) NOT NULL COMMENT '分润类型: GRADIENT_RATE 阶梯分润',
  `expression` varchar(512) NOT NULL COMMENT '分润表达式',
  `status` varchar(2) NOT NULL DEFAULT '1' COMMENT '状态: 0 禁用,1启用',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`share_id`),
  UNIQUE KEY `agent_no` (`agent_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理商分润规则';


DROP TABLE IF EXISTS a_share_detail;
CREATE TABLE `a_share_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `agent_no` VARCHAR(20) NOT NULL COMMENT '代理商编号',
  `share_type` VARCHAR(20) NOT NULL COMMENT '分润类型: 0 直推分润, 1. 二级贡献分润',
  `share_money` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '分润金额',
  `order_money` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `share_rate` DECIMAL(20,2) NOT NULL DEFAULT '0.00' COMMENT '分润比例',
  `order_id` VARCHAR(40) NOT NULL COMMENT '分润订单id',
  `student_id` VARCHAR(40) NOT NULL COMMENT '分润学生id',
  `student_num` INT(11) NULL DEFAULT null COMMENT '分润时已经购买课程学生数量',
  `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='分润明细表';


drop table IF EXISTS a_agent_account;
CREATE TABLE `a_agent_account` (
  `account_id` varchar(20) NOT NULL COMMENT '账户id',
  `agent_no` varchar(20) NOT NULL COMMENT '代理商编号',
  `total_income` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '总收益: A = B + C',
  `withdraw_cash` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '提现金额: B',
  `balance` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '余额: C = D + E',
  `available_balance` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '可用余额: D',
  `frozen_balance` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '冻结余额: E',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `agent_no` (`agent_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理商账户';

drop table IF EXISTS a_account_detail;
CREATE TABLE `a_account_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流水id',
  `agent_no` varchar(20) NOT NULL COMMENT '代理商编号',
  `account_id` varchar(20) NOT NULL COMMENT '代理商账户编号',
  `money` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '操作金额',
  `type` varchar(50) NOT NULL COMMENT '类型: 查看meta(accoutDetail-type)',
  `total_income` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '总收益: A = B + C',
  `withdraw_cash` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '提现金额: B',
  `balance` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '余额: C = D + E',
  `available_balance` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '可用余额: D',
  `frozen_balance` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '冻结余额: E',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理商账户明细表';



drop table if exists a_apply_withdraw_cash;
create table a_apply_withdraw_cash(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '申请流水id',
  `agent_no` varchar(20) NOT NULL COMMENT '代理商编号',
  `withdraw_cash` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '申请提现金额',
  `status` varchar(10) not null default '0' comment '状态, 0: 申请中, 1: 处理成功, 2: 处理失败， 3：已拒绝 4取消提现',
  `remark` varchar(512) null comment '备注',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
   PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='代理商申请提现记录';

drop table IF EXISTS com_sequence;
CREATE TABLE IF NOT EXISTS`com_sequence` (
  `name` VARCHAR(50) NOT NULL COMMENT '名字',
  `prefix` VARCHAR(50) NOT NULL COMMENT '前缀',
  description VARCHAR(50) NOT NULL COMMENT '主键描述',
  `current_num` BIGINT(22) NOT NULL COMMENT '当前值',
  `increment` INT(11) NOT NULL DEFAULT '1' COMMENT '自增值',
  num_length  INT(11) NOT NULL DEFAULT '8' COMMENT '数字长度',
  update_time TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
  PRIMARY KEY (`name`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='序列表';


















