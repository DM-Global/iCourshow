package com.ics.cmsadmin.modules.pub.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.pub.bean.Promotion;
import com.ics.cmsadmin.modules.pub.service.PromotionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/promotions")
public class PromotionsController extends ResourceController<PromotionService, Promotion, Integer> {
    @GetMapping
    public ApiResponse get(LevelEnum levelName, Boolean isActive,
                           @RequestParam(value = "page", defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Map<String, Object> param = new HashMap<>();
        param.put("level", levelName);
        param.put("isActive", isActive);
        param.put("page", page);
        param.put("pageSize", pageSize);
        return super.get(param);
    }
}
