package com.ics.cmsadmin.modules.agent.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * a_share_detail
 * 分润明细
 * <p>
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Data
@Builder
public class ShareDetailBean extends LoginSearchBean {

    private String id;              // 流水id
    private String agentNo;              // 代理商编号
    private String shareType;              // 分润类型
    private BigDecimal shareMoney;              // 分润金额
    private BigDecimal orderMoney;              // 订单金额
    private BigDecimal shareRate;              // 分润比例
    private String orderId;              // 分润订单id
    private String studentId;              // 分润学生id
    private String studentNum;              // 分润时已经购买课程学生数量
    private String sourceAgentNo;              // 分润学生对应的代理商编号
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    @Tolerate
    public ShareDetailBean() {
    }
}
