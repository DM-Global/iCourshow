package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.Collect;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface CollectionService {

    int delete(Collect collect);

    int insert(Collect collect);

    Boolean select(Collect collect);

    Map<LevelEnum, List> getCollectionList(String userId);

    List listCollectionByType(String levelName, String loginUserId);

    Integer queryTotal(LevelEnum levelName, String levelId);

    boolean toggleCollect(Collect collect);

    /**
     * 根据用户查询所给的科目/课程等id是否被收藏
     *
     * @param userId    用户id
     * @param levelEnum 类型
     * @param ids       需要查询的id
     * @return
     */
    List<String> list(String userId, LevelEnum levelEnum, List<String> ids);


}
