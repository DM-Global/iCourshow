package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.modules.student.bean.StudentCertificateBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2019-07-02 11:39
 */
@Repository
@Mapper
public interface StudentCertificateDao {
    int merge(@Param("bean") StudentCertificateBean bean);

    StudentCertificateBean queryByPictureName(@Param("pictureName") String pictureName);
}
