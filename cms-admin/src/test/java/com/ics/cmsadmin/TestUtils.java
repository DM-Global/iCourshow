package com.ics.cmsadmin;

import com.ics.cmsadmin.modules.wechat.utils.SignatureUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

//public class TestUtils  extends CmsAdminApplicationTests
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class TestUtils {

    @Test
    public void test() {
        SignatureUtil.checkSignature("1", "2", "3");
    }
}
