package com.ics.cmsadmin.modules.auth.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by 666666 on 2018/4/4.
 */
@Repository
@Mapper
public interface RoleMenuDao {
    /**
     * 根据菜单id删除"角色-菜单"
     *
     * @param menuId 菜单id
     */
    int deleteRoleMenuByMenuId(@Param("menuId") String menuId);

    /**
     * 根据角色id删除 "角色-菜单"
     *
     * @param roleId 角色id
     */
    int deleteRoleMenuByRoleId(@Param("roleId") String roleId);
}
