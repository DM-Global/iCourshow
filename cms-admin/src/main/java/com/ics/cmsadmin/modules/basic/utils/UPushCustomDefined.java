package com.ics.cmsadmin.modules.basic.utils;

import lombok.*;

import java.math.BigDecimal;

@Data
@Builder
@Getter
@AllArgsConstructor
public class UPushCustomDefined {

    private UPushTypeEnum uPushType;
    private String messageId;
    private String studentId;
    private String studentName;
    private BigDecimal money;
    private String remark;
    private String withdrawId;

}

