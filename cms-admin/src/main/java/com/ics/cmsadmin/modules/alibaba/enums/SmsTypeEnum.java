package com.ics.cmsadmin.modules.alibaba.enums;

public enum SmsTypeEnum {
    // 忘记密码验证码
    forgotPasswordVerify,
    // 绑定手机号
    bindPhoneVerify,
    // 修改手机号码
    modifyPhoneVerify,
    // 登陆验证码
    loginVerify;


    public String getPrepose() {
        return "prepose" + this.name();
    }

    public static boolean isSmsTypeEnum(String type) {
        try {
            SmsTypeEnum sdds = SmsTypeEnum.valueOf(type);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static SmsTypeEnum valueOfSmsType(String type) {
        try {
            return SmsTypeEnum.valueOf(type);
        } catch (Exception e) {
            return null;
        }
    }
}
