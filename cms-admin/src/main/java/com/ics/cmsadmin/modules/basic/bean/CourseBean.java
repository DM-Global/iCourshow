package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * t_course
 * 课程
 * Created by lvsw on 2018-09-01.
 */
@Data
@Builder
public class CourseBean {

    // 课程id
    private String id;
    // 课程名
    @NotEmpty(message = "课程名不能为空", groups = {InsertGroup.class})
    @Length(message = "课程名长度必须在{min}~{max}之间", min = 1, max = 50, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    // 状态
    private Boolean isActive;
    // 完成状态
    private Boolean isCompleted;
    //课程标语
    private String courseSlogan;
    // 课程描述
    @Length(message = "课程描述长度必须在{min}~{max}之间", max = 65535, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    // 主题id
    private String subjectId;
    //主题name
    private String subjectName;
    // 等级
    private String rank;
    // 封面id
    private String coverId;
    //海报id
    private String longPosterId;
    // 封面地址
    private String coverSrc;
    // 海报地址
    private String longPosterSrc;
    // 原价
//    @NotNull(message = "{Course.price.NotNull}", groups = {InsertGroup.class, UpdateGroup.class})
    private BigDecimal price;
    // 折扣价
    private BigDecimal discountPrice;
    // 点击次数
    private Integer clickNumber;
    // 学员数量
    private Integer learnerCount;
    // 点赞数
    private Integer thumbUpCount;
    // 收藏数
    private Integer collectCount;
    // 分享数
    private Integer shareCount;
    // 主题数
    private Integer topicNumber;
    //购买数
    private Integer boughtNumber;
    //课时
    private Integer period;
    // 视频数
    private Long videoNumber;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 更新时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    private Date startTime;
    private Date endTime;

    // 课程类型id
    private String courseTypeId;
    // 教师id
    private String teacherId;
    // 课程教师
    private List<TeacherBean> teachers;

    /**
     * 用户登陆后，需考虑
     */
    // 课程是否已经购买
    private Boolean hasBought;
    // 课程是否已收藏
    private Boolean collected;
    // 是否点赞了
    private Boolean liked;

    private List<UnitBean> unitList;

    @Tolerate
    public CourseBean() {
    }
}
