package com.ics.cmsadmin.modules.system.service.impl;


import com.ics.cmsadmin.modules.system.bean.SequenceNumberBean;
import com.ics.cmsadmin.modules.system.emums.SequenceNumberEnum;
import com.ics.cmsadmin.modules.system.service.SequenceNumberService;
import org.springframework.stereotype.Service;

import static com.ics.cmsadmin.modules.system.steward.SystemRepositories.sequenceNumberDao;

/**
 * com.kco.service
 * Created by swlv on 2016/10/25.
 */
@Service
public class SequenceNumberServiceImpl implements SequenceNumberService {

    @Override
    public synchronized String newSequenceNumber(SequenceNumberEnum sequenceNumberEnum) {
        if (sequenceNumberEnum == null) {
            return null;
        }

        SequenceNumberBean sequenceNumberBean = sequenceNumberDao.newSequenceNumber(sequenceNumberEnum.getSequenceNumberBean().getName());
        if (sequenceNumberBean == null) {
            sequenceNumberBean = sequenceNumberEnum.getSequenceNumberBean();
        }
        sequenceNumberDao.updateSequenceNumber(sequenceNumberBean);
        return String.format("%s%0" + sequenceNumberBean.getNumLength() + "d",
            sequenceNumberBean.getPrefix(), sequenceNumberBean.getCurrentNum());


    }

}
