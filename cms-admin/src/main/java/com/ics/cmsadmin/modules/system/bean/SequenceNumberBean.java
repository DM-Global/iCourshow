package com.ics.cmsadmin.modules.system.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.UniqueElements;

/**
 * 主键生成策略的bean
 * com.kco.config
 * Created by swlv on 2016/10/25.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SequenceNumberBean {
    private String name;            // 主键名 唯一
    private String prefix;          // 前缀
    private String description;     // 描述
    private long currentNum;         // 当前值
    private long numLength;          // 主键数字长度
    private long increment;          // 每次递增数

}
