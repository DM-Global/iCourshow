package com.ics.cmsadmin.modules.pub.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.pub.bean.Teacher;
import com.ics.cmsadmin.modules.pub.service.TeacherService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/teachers")
public class TeacherController extends ResourceController<TeacherService, Teacher, Integer> {
    @GetMapping
    public ApiResponse teachersGet(String name, Boolean isActive, Boolean isFamous, Integer subjectId, Integer courseId, Integer unitId,
                                   @RequestParam(value = "page", defaultValue = "1") Integer page,
                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        params.put("isActive", isActive);
        params.put("isFamous", isFamous);
        params.put("subjectId", subjectId);
        params.put("courseId", courseId);
        params.put("unitId", unitId);
        params.put("page", page);
        params.put("pageSize", pageSize);
        return super.get(params);
    }
}
