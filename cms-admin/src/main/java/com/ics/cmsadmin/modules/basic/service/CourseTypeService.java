package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.CourseTypeBean;

public interface CourseTypeService {

    PageResult list(CourseTypeBean courseTypeBean, PageBean page);

    boolean insert(CourseTypeBean t);

    boolean update(String id, CourseTypeBean t);

    boolean delete(String id);
}
