package com.ics.cmsadmin.modules.system.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Sandwich on 2018-09-20
 */
public interface UploadService {

    Map uploadImage(MultipartFile file) throws Exception;

    Map uploadFile(MultipartFile file) throws Exception;

    Map uploadRenameImage(MultipartFile file) throws Exception;

    String getNewFileName(MultipartFile file);

    Map uploadRenameFile(MultipartFile file) throws Exception;

    Map toUploadImage(MultipartFile file, String fileName, String filePath) throws IOException;

    boolean deleteUploadedFile(String key);

    boolean deleteUploadedImageByFileName(String fileName);

    boolean deleteUploadedImageByUrl(String url);

}
