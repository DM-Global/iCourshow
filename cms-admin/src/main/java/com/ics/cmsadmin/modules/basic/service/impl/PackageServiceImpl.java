package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;
import com.ics.cmsadmin.modules.basic.dao.PackageDao;
import com.ics.cmsadmin.modules.basic.service.PackageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * t_package
 * Created bysandwich on 2018-18-14 21:12:28.
 */
@Service
public class PackageServiceImpl implements PackageService {
    @Resource
    private PackageDao packageDao;

    @Override
    public PackageBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return packageDao.queryById(id);
    }

    @Override
    public PageResult<PackageBean> list(PackageBean bean, PageBean page) {
        long count = packageDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, packageDao.list(bean, page));
    }

    @Override
    public boolean insert(PackageBean bean) {
        if (bean == null) {
            return false;
        }
        return packageDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, PackageBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return packageDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return packageDao.delete(id) == 1;
    }

}
