package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.UnitBean;
import com.ics.cmsadmin.modules.basic.service.UnitService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Unit controller
 * Created by lvsw on 2018-09-01.
 */
@Api(description = "单元管理接口")
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Resource
    private UnitService unitService;

    @Authorize(AuthorizeEnum.UNIT_QUERY)
    @ApiOperation(value = "查询单元信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(unitService.queryById(id));
    }

    @Authorize(AuthorizeEnum.UNIT_INSERT)
    @ApiOperation(value = "新增单元信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody UnitBean unitBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(unitService.insert(unitBean));
    }

    @Authorize(AuthorizeEnum.UNIT_DELETE)
    @ApiOperation(value = "删除单元信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的单元 id") @PathVariable String id) {
        return new ApiResponse(unitService.delete(id));
    }

    @Authorize(AuthorizeEnum.UNIT_UPDATE)
    @ApiOperation(value = "更新单元信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@RequestBody UnitBean unitBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的单元Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(unitService.update(id, unitBean));
    }

    @Authorize(AuthorizeEnum.UNIT_QUERY)
    @ApiOperation(value = "分页查询单元信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody UnitBean unitBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(unitService.list(unitBean, pageBean));
    }

    @Authorize(AuthorizeEnum.UNIT_QUERY)
    @ApiOperation(value = "根据课程Id查询所有单元信息")
    @GetMapping("/listAllUnitByCourseId/{courseId}")
    public ApiResponse listAllUnitByCourseId(@PathVariable String courseId) {
        return new ApiResponse(unitService.listAllUnitByCourseId(courseId));
    }
}
