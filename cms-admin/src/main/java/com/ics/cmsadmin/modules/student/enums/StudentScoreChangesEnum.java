package com.ics.cmsadmin.modules.student.enums;

/**
 * 积分变动明细类型
 */
public enum StudentScoreChangesEnum {
    // 推广用户
    PROMOTION_STUDENT;
}
