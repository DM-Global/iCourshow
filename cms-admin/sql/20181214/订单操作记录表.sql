SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for zh_order
-- ----------------------------
DROP TABLE IF EXISTS `zh_order`;
CREATE TABLE `zh_order`  (
  `id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `user_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户id',
  `open_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '微信openid',
  `course_id` int(11) NULL DEFAULT NULL COMMENT '课程id',
  `order_type` tinyint(3) NULL DEFAULT NULL COMMENT '订单类型: 1.课程 2.包年包月',
  `course_price` decimal(8, 2) NULL DEFAULT NULL COMMENT '交易时课程原价',
  `order_amount` decimal(8, 2) NOT NULL COMMENT '订单总金额',
  `payment_method` tinyint(3) NULL DEFAULT 1 COMMENT '支付方式: 1.微信公众号 2微信app  3.支付宝 4.免费试用',
  `status` tinyint(3) NULL DEFAULT 1 COMMENT '订单状态: 1. 未支付 2. 已支付 3. 已取消',
  `acc_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '入账状态: 0 未入账, 1 已入账 2不入账',
  `acc_time` timestamp(0) NULL DEFAULT NULL COMMENT '入账时间',
  `agent_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '代理商编号',
  `salesman` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属业务员',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `pay_time` timestamp(0) NULL DEFAULT NULL COMMENT '支付时间',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除：0.未删除，1.已删除',
  `start_date` datetime(0) NULL DEFAULT NULL COMMENT '生效日期',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '结束日期',
  `package_id` int(11) NULL DEFAULT NULL COMMENT '套餐id',
  `study_status` tinyint(1) NULL DEFAULT NULL COMMENT '学习状态：1.未入学 2.已入学 3.已到期',
  `operate_type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作类型:I(insert),U(update),D(delete)',
  `operate_ts` timestamp(0) NULL DEFAULT NULL COMMENT '操作时间'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单备份表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

CREATE TRIGGER `tri_order_insert` AFTER INSERT ON `t_order` FOR EACH ROW insert into zh_order(id,user_id,open_id,course_id,order_type,course_price,order_amount,payment_method,status,acc_status,acc_time,agent_no,salesman,create_time,
pay_time,update_time,is_deleted,start_date,end_date,package_id,study_status,operate_type,operate_ts) values(new.id,new.user_id,new.open_id,new.course_id,new.order_type,new.course_price,new.order_amount,new.payment_method,new.status,new.acc_status,new.acc_time,new.agent_no,new.salesman,new.create_time,
new.pay_time,new.update_time,new.is_deleted,new.start_date,new.end_date,new.package_id,new.study_status,'I',CURRENT_TIMESTAMP);

CREATE TRIGGER `tri_order_update` AFTER UPDATE ON `t_order` FOR EACH ROW insert into zh_order(id,user_id,open_id,course_id,order_type,course_price,order_amount,payment_method,status,acc_status,acc_time,agent_no,salesman,create_time,
pay_time,update_time,is_deleted,start_date,end_date,package_id,study_status,operate_type,operate_ts) values(new.id,new.user_id,new.open_id,new.course_id,new.order_type,new.course_price,new.order_amount,new.payment_method,new.status,new.acc_status,new.acc_time,new.agent_no,new.salesman,new.create_time,
new.pay_time,new.update_time,new.is_deleted,new.start_date,new.end_date,new.package_id,new.study_status,'U',CURRENT_TIMESTAMP);

CREATE TRIGGER `tri_order_delete` AFTER DELETE ON `t_order` FOR EACH ROW insert into zh_order(id,user_id,open_id,course_id,order_type,course_price,order_amount,payment_method,status,acc_status,acc_time,agent_no,salesman,create_time,
pay_time,update_time,is_deleted,start_date,end_date,package_id,study_status,operate_type,operate_ts) values(old.id,old.user_id,old.open_id,old.course_id,old.order_type,old.course_price,old.order_amount,old.payment_method,old.status,old.acc_status,old.acc_time,old.agent_no,old.salesman,old.create_time,
old.pay_time,old.update_time,old.is_deleted,old.start_date,old.end_date,old.package_id,old.study_status,'D',CURRENT_TIMESTAMP);

