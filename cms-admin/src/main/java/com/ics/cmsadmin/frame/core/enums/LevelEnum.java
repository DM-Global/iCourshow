package com.ics.cmsadmin.frame.core.enums;

import lombok.Getter;

@Getter
public enum LevelEnum {
    subject, course, unit, topic, point;

    private static LevelEnum[] vals = values();

    public LevelEnum next() {
        return vals[(ordinal() + 1) % vals.length];
    }

    public LevelEnum prev() {
        return vals[(ordinal() - 1 + vals.length) % vals.length];
    }
}
