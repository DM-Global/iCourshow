package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_order controller
 * Created by lvsw on 2018-16-23 21:09:34.
 */
@Api(description = "订单")
@RestController("newOrderController")
@RequestMapping("/order")
public class OrderController {

    @Resource(name = "newOrderService")
    private OrderService orderService;

    @Authorize(AuthorizeEnum.ORDER_QUERY)
    @ApiOperation(value = "分页查询订单信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody OrderBean orderBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(orderService.listByLoginUserId(orderBean, loginUserId, pageBean));
    }

    @Authorize(AuthorizeEnum.ORDER_QUERY)
    @ApiOperation(value = "查询订单信息总额")
    @PostMapping("/totalMoney")
    public ApiResponse totalMoney(@RequestBody OrderBean orderBean, HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(orderService.totalMoney(orderBean, loginUserId));
    }

}
