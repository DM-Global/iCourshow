package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.ProblemBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * Created by lvsw on 2018-09-06.
 */
@Repository
@Mapper
public interface ProblemDao extends BaseDataDao<ProblemBean>, BaseDao<ProblemBean> {
}
