package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import com.ics.cmsadmin.modules.basic.dao.SharePosterDao;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

import javax.annotation.Resource;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Created by Sandwich on 2019/6/22
 */
public class AppSharePosterControllerTest extends CmsAdminApplicationTests {

    @Resource
    private SharePosterDao sharePosterDao;

    @Test
    public void test(){
        List<SharePosterBean> sharePosterList = sharePosterDao.list(SharePosterBean.builder().isActive(Boolean.TRUE).shareType(CommonEnums.SharePosterType.MY_SHARE.getCode()).build(),
            new PageBean(1, 5));
        if (!CollectionUtils.isEmpty(sharePosterList)) {
            sharePosterList.forEach(sharePosterBean -> sharePosterBean.setShareUrl("123"));
        }
    }

}
