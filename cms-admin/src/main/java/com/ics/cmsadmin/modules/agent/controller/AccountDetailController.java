package com.ics.cmsadmin.modules.agent.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.agent.bean.AccountDetailBean;
import com.ics.cmsadmin.modules.agent.service.AccountDetailService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_account_detail controller
 * Created by lvsw on 2018-55-29 14:09:20.
 */
@Api(description = "代理商账户明细")
@RestController
@RequestMapping("/accountDetail")
public class AccountDetailController {

    @Resource
    private AccountDetailService accountDetailService;

    @Authorize(AuthorizeEnum.ACCOUNT_DETAIL_QUERY)
    @ApiOperation(value = "分页查询代理商账户明细信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody AccountDetailBean accountDetailBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize,
                            HttpServletRequest request) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        String loginUserId = SsoUtils.getLoginUserId(request);
        return new ApiResponse(accountDetailService.listByLoginUserId(accountDetailBean, loginUserId, pageBean));
    }

}
