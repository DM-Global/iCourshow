package com.ics.cmsadmin.modules.basic.service.impl;

import com.github.wxpay.sdk.WXPayConstants.SignType;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.exception.ExceptionUtils;
import com.ics.cmsadmin.frame.property.AppWxPayConfig;
import com.ics.cmsadmin.frame.utils.*;
import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.basic.bean.BankPayBean;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.basic.service.PayService;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.student.service.StudentWithdrawAuditingService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import com.jpay.ext.kit.IpKit;
import com.jpay.ext.kit.PaymentKit;
import com.jpay.ext.kit.StrKit;
import com.jpay.secure.RSAUtils;
import com.jpay.weixin.api.WxPayApi;
import com.jpay.weixin.api.WxPayApiConfig;
import com.jpay.weixin.api.WxPayApiConfigKit;
import com.lly835.bestpay.enums.BestPayTypeEnum;
import com.lly835.bestpay.model.PayRequest;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundRequest;
import com.lly835.bestpay.model.RefundResponse;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import com.lly835.bestpay.utils.JsonUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Sandwich on 2018-08-18
 */
@Service
@Log4j2
public class PayServiceImpl implements PayService {

    private static final String ORDER_NAME = "course order";

    @Autowired
    private BestPayServiceImpl bestPayService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ShareDetailService shareDetailService;
    @Autowired
    private AppWxPayConfig appWxPayConfig;
    @Autowired
    private StudentService studentService;
    @Autowired
    private StudentWithdrawAuditingService studentWithdrawAuditingService;

    @Override
    public PayResponse create(OrderBean order, String openId) {
        PayRequest payRequest = new PayRequest();
        payRequest.setOpenid(openId);
        payRequest.setOrderAmount(order.getOrderAmount().doubleValue());
        payRequest.setOrderId(order.getId());
        payRequest.setOrderName(ORDER_NAME);
        payRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信支付】发起支付, request={}", JsonUtil.toJson(payRequest));

        PayResponse payResponse = bestPayService.pay(payRequest);
        log.info("【微信支付】发起支付, response={}", JsonUtil.toJson(payResponse));
        return payResponse;
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public PayResponse orderNotify(String notifyData) {
        //1. 验证签名
        //2. 支付的状态
        //3. 支付金额
        //4. 支付人(下单人 == 支付人)

        PayResponse payResponse = bestPayService.asyncNotify(notifyData);
        log.info("【微信支付】异步通知, payResponse={}", JsonUtil.toJson(payResponse));

        //查询订单
        OrderBean order = orderService.queryById(payResponse.getOrderId());

        //判断订单是否存在
        if (order == null) {
            log.error("【微信支付】异步通知, 订单不存在, orderId={}", payResponse.getOrderId());
            throw new CmsException(ApiResultEnum.ORDER_NOT_EXIST);
        }

        //判断金额是否一致(0.10   0.1)
        if (!MathUtil.equals(payResponse.getOrderAmount(), order.getOrderAmount().doubleValue())) {
            log.error("【微信支付】异步通知, 订单金额不一致, orderId={}, 微信通知金额={}, 系统金额={}",
                payResponse.getOrderId(),
                payResponse.getOrderAmount(),
                order.getOrderAmount());
            throw new CmsException(ApiResultEnum.WXPAY_NOTIFY_MONEY_VERIFY_ERROR);
        }
        order.setPaymentMethod(CommonEnums.PaymentMethod.WX_PUBLIC.getCode());
        handleOrderAfterPaid(order);
        return payResponse;
    }

    /**
     * 退款
     *
     * @param order
     */
    @Override
    public RefundResponse refund(OrderBean order) {
        RefundRequest refundRequest = new RefundRequest();
        refundRequest.setOrderId(order.getId());
        refundRequest.setOrderAmount(order.getOrderAmount().doubleValue());
        refundRequest.setPayTypeEnum(BestPayTypeEnum.WXPAY_H5);
        log.info("【微信退款】request={}", JsonUtil.toJson(refundRequest));

        RefundResponse refundResponse = bestPayService.refund(refundRequest);
        log.info("【微信退款】response={}", JsonUtil.toJson(refundResponse));

        return refundResponse;
    }

    /**
     * app微信支付回调处理
     *
     * @param xmlMsg
     * @return
     */
    @Override
    public String handleOrderAfterWxAppPaid(String xmlMsg) {
        Map<String, String> params = PaymentKit.xmlToMap(xmlMsg);
        String result_code = params.get("result_code");
        String orderId = params.get("out_trade_no");
        String attach = params.get("attach");

        OrderBean order = orderService.queryById(orderId);
        if (null != order) {
            if (order.getStatus() == CommonEnums.PayStatus.PAID.getCode()) {
                log.info("This order is already paid!");
                return null;
            }
            ;
            if (order.getStatus() == CommonEnums.PayStatus.CANCELED.getCode()) {
                log.info("This order is already canceled!");
                return null;
            }
            if (PaymentKit.verifyNotify(params, appWxPayConfig.getPartnerKey())) {
                if (("SUCCESS").equals(result_code)) {
                    // 更新订单信息
                    log.warn("更新订单信息:" + attach);
                    order.setPaymentMethod(CommonEnums.PaymentMethod.WX_APP.getCode());
                    handleOrderAfterPaid(order);
                    // 发送通知等
                    Map<String, String> xml = new HashMap<String, String>();
                    xml.put("return_code", "SUCCESS");
                    xml.put("return_msg", "OK");
                    return PaymentKit.toXml(xml);
                }
            }
        }
        return null;
    }

    /**
     * app微信支付参数获取
     *
     * @param request
     * @return
     */
    @Override
    public ApiResponse getWxAppPayResult(HttpServletRequest request) {
        String ip = IpKit.getRealIp(request);
        if (StrKit.isBlank(ip)) {
            ip = "127.0.0.1";
        }
        String orderId = request.getParameter("orderId");
        log.info("Weixin app pay started==>");

        OrderBean order = orderService.queryById(orderId);
        if (null == order) return new ApiResponse(ApiResultEnum.ORDER_NOT_EXIST);
        WxPayApiConfig wxPayApiConfig = WxPayApiConfig.New()
            .setAppId(appWxPayConfig.getAppId())
            .setMchId(appWxPayConfig.getMchId())
            .setPaternerKey(appWxPayConfig.getPartnerKey())
            .setPayModel(WxPayApiConfig.PayModel.BUSINESSMODEL);
        WxPayApiConfigKit.putApiConfig(wxPayApiConfig);
        Map<String, String> params = WxPayApiConfigKit
            .getWxPayApiConfig()
            .setAttach("iCourshow")
            .setBody("iCourshow App")
            .setSpbillCreateIp(ip)
            .setTotalFee(String.valueOf(order.getOrderAmount().multiply(BigDecimal.valueOf(100)).longValue()))//以分为单位
            .setTradeType(WxPayApi.TradeType.APP)
            .setNotifyUrl(appWxPayConfig.getPayNotifyUrl())
            .setOutTradeNo(orderId)
            .build();
        String xmlResult = WxPayApi.pushOrder(false, params);

        log.info(xmlResult);
        Map<String, String> resultMap = PaymentKit.xmlToMap(xmlResult);

        String return_code = resultMap.get("return_code");
        String return_msg = resultMap.get("return_msg");
        if (!PaymentKit.codeIsOK(return_code)) {
            log.info(xmlResult);
            return new ApiResponse(ApiResultEnum.WX_APP_PREPAY_FAILED);
        }
        String result_code = resultMap.get("result_code");
        if (!PaymentKit.codeIsOK(result_code)) {
            log.info(xmlResult);
            return new ApiResponse(ApiResultEnum.WX_APP_PREPAY_FAILED);
        }
        // 以下字段在return_code 和result_code都为SUCCESS的时候有返回

        String prepay_id = resultMap.get("prepay_id");
        // 封装调起微信支付的参数 https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=9_12

        Map<String, String> packageParams = new HashMap<String, String>();
        packageParams.put("appid", WxPayApiConfigKit.getWxPayApiConfig().getAppId());
        packageParams.put("partnerid", WxPayApiConfigKit.getWxPayApiConfig().getMchId());
        packageParams.put("prepayid", prepay_id);
        packageParams.put("package", "Sign=WXPay");
        packageParams.put("noncestr", System.currentTimeMillis() + "");
        packageParams.put("timestamp", System.currentTimeMillis() / 1000 + "");
        String packageSign = PaymentKit.createSign(packageParams,
            WxPayApiConfigKit.getWxPayApiConfig().getPaternerKey());
        packageParams.put("sign", packageSign);
        packageParams.put("packageId", order.getPackageId());
        return new ApiResponse(packageParams);
    }

    @Override
    public Boolean handleOrderAfterIosPaid(String orderId, Boolean isSandboxPay) {
        OrderBean order = orderService.queryById(orderId);
        if (null == order) {
            log.info("Order not exist for {}", orderId);
            return Boolean.FALSE;
        }
        if (order.getStatus() == CommonEnums.PayStatus.PAID.getCode()) return Boolean.TRUE;
        order.setPaymentMethod(isSandboxPay ? CommonEnums.PaymentMethod.APPLE_SANDBOX_PAY.getCode() : CommonEnums.PaymentMethod.APPLE_PAY.getCode());
        handleOrderAfterPaid(order);
        return Boolean.TRUE;
    }

    private void handleOrderAfterPaid(OrderBean order) {
        PageResult pageResult = orderService.list(OrderBean.builder().userId(order.getUserId()).isDeleted(Boolean.FALSE).orderType(CommonEnums.OrderType.PACKAGE.getCode())
            .isDeleted(false).status(CommonEnums.PayStatus.PAID.getCode()).build(), new PageBean(1, Integer.MAX_VALUE));
        List<OrderBean> paidList = pageResult.getDataList();
        if (null != paidList && paidList.size() > 0) {
            List<OrderBean> onStudyList = paidList.stream().filter((OrderBean o) -> o.getStudyStatus() == CommonEnums.StudyStatus.STARTED.getCode()).collect(Collectors.toList());
            if (null != onStudyList && onStudyList.size() > 0) {
                //存在正在入学的订单，订单状态更新为未入学
                order.setStudyStatus(CommonEnums.StudyStatus.NOT_STARTED.getCode());
            }
        } else {
            //已入学状态
            order.setStudyStatus(CommonEnums.StudyStatus.STARTED.getCode());
        }
        //订单状态己支付
        order.setStatus(CommonEnums.PayStatus.PAID.getCode());
        order.setPayTime(new Date());
        orderService.update(order);
        try {
            shareDetailService.generateShareByOrderId(order.getId());
        } catch (Exception e) {
            log.error("订单{}分润入账异常{}", order.getId(), ExceptionUtils.collectExceptionStackMsg(e));
        }
    }

    /**
     * 企业付款到零钱
     */
    public Map<String, String> wxWithdraw(BigDecimal amount, String appOpenId) {

        Boolean systemIsDebugger = SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.SYSTEM_IS_DEBUGGER_MODE, Boolean::new);
        // 如果是测试模式,这调用模拟出款
        if (systemIsDebugger) {
            log.info("模拟出款...");
            Map<String, String> result = new HashMap<>();
            result.put("nonce_str", System.currentTimeMillis() + "");
            result.put("mchid", System.currentTimeMillis() + "");
            result.put("partner_trade_no", RandomStringUtils.random(32, "0123456789abcdef"));
            result.put("payment_no", RandomStringUtils.random(28, "0123456789"));
            result.put("payment_time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            result.put("mch_appid", "wxMonixxxxxxxxxxx");
            result.put("result_code", "SUCCESS");
            result.put("return_code", "SUCCESS");
            return result;
        }
        log.info("实际出款...");
        if (StringUtils.isBlank(appOpenId)) {
            //用户没有openId
            log.info("Withdraw failed without openId");
            throw new CmsException(ApiResultEnum.WX_APP_WITHDRAW_WITHOUT_OPENID);
        }
        amount = amount.setScale(2, BigDecimal.ROUND_DOWN);
        Map<String, String> params = new HashMap<String, String>();
        params.put("mch_appid", appWxPayConfig.getAppId());
        params.put("mchid", appWxPayConfig.getMchId());
        String nonceStr = String.valueOf(System.currentTimeMillis());
        params.put("nonce_str", nonceStr);
        String partnerTradeNo = KeyUtil.genUniqueKey();
        params.put("partner_trade_no", partnerTradeNo);
        params.put("openid", appOpenId);
        params.put("check_name", "NO_CHECK");
        params.put("amount", amount.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_DOWN).toString());
        params.put("desc", "iCourshow APP微信提现");
        String ip = null;
        try {
            ip = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        if (StrKit.isBlank(ip)) {
            ip = "127.0.0.1";
        }
        params.put("spbill_create_ip", ip);

        params.put("sign", PaymentKit.createSign(params, appWxPayConfig.getPartnerKey()));
        // 提现
        String transfers = WxPayApi.transfers(params, appWxPayConfig.getCertPath(), appWxPayConfig.getMchId());
        log.info("Withdraw result:" + transfers);
        Map<String, String> map = PaymentKit.xmlToMap(transfers);
        if (MapUtils.isEmpty(map) ||
            !StringUtils.equalsIgnoreCase("SUCCESS", map.get("return_code")) ||
            !StringUtils.equalsIgnoreCase("SUCCESS", map.get("result_code"))) {
            log.info("Withdraw {} failed for {}", amount, appOpenId);
            throw new CmsException(ApiResultEnum.WX_APP_WITHDRAW_FAILED);
        }
        return map;
    }

    /**
     * 查询企业付款到零钱
     */
    public Map getWxWithdrawInfo(String partnerTradeNo) {
        String transferInfo = "";
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("nonce_str", System.currentTimeMillis() + "");
            params.put("partner_trade_no", partnerTradeNo);
            params.put("mch_id", appWxPayConfig.getMchId());
            params.put("appid", appWxPayConfig.getAppId());
            params.put("sign", PaymentKit.createSign(params, appWxPayConfig.getPartnerKey()));

            transferInfo = WxPayApi.getTransferInfo(params, appWxPayConfig.getCertPath(), appWxPayConfig.getMchId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Map<String, String> map = PaymentKit.xmlToMap(transferInfo);
        return map;
    }

    /**
     * 企业付款到银行卡
     * @return
     */
    public Map wxPayBank(BankPayBean bankPayBean) {
        Map<String, String> map = null;
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("mch_id", appWxPayConfig.getMchId());
            params.put("partner_trade_no", KeyUtil.genUniqueKey());
            params.put("nonce_str", System.currentTimeMillis() + "");
            String encriptBankCard = RSAUtils.encryptByPublicKeyByWx(bankPayBean.getBankCardNo(), appWxPayConfig.getPublicKey());
            String name = RSAUtils.encryptByPublicKeyByWx(bankPayBean.getBankCardName(), appWxPayConfig.getPublicKey());
            params.put("enc_bank_no", encriptBankCard);//收款方银行卡号
            params.put("enc_true_name", name);//收款方用户名
            params.put("bank_code", bankPayBean.getBankCode());//收款方开户行
            BigDecimal amount = bankPayBean.getAmount().setScale(2, BigDecimal.ROUND_DOWN);
            params.put("amount", amount.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_DOWN).toString());
            params.put("desc", "iCourshow APP提现到银行卡");
            params.put("sign", PaymentKit.createSign(params, appWxPayConfig.getPartnerKey()));
            String payBank = WxPayApi.payBank(params, appWxPayConfig.getCertPath(), appWxPayConfig.getMchId());
            map = PaymentKit.xmlToMap(payBank);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 获取pkcs1格式的public key，如果要用来加密银行卡和用户名，还要先转换成pksc8格式
     *
     * @return
     * @throws Exception
     */
    public String getPkcs1FormatPublicKey() throws Exception {
        Map<String, String> paramsForPublicKey = new HashMap<String, String>();
        paramsForPublicKey.put("mch_id", appWxPayConfig.getMchId());
        paramsForPublicKey.put("nonce_str", System.currentTimeMillis() + "");
        String sign = WXPayUtil.generateSignature(paramsForPublicKey, appWxPayConfig.partnerKey, SignType.MD5);
        paramsForPublicKey.put("sign", sign);
        String publicKeyXmlResult = WxPayApi.getPublicKey(paramsForPublicKey, appWxPayConfig.getCertPath(), appWxPayConfig.getMchId());
        Map<String, String> publicKeyMap = PaymentKit.xmlToMap(publicKeyXmlResult);
        return publicKeyMap.get("pub_key");
    }

    /**
     * 查询企业付款到银行
     *
     * @param partnerTradeNo
     * @return
     */
    public Map queryBankInfo(String partnerTradeNo) {
        Map<String, String> map = null;
        try {
            Map<String, String> params = new HashMap<String, String>();
            params.put("mch_id", appWxPayConfig.getMchId());
            params.put("partner_trade_no", partnerTradeNo);
            params.put("nonce_str", System.currentTimeMillis() + "");
            params.put("sign", PaymentKit.createSign(params, appWxPayConfig.getPartnerKey()));
            String queryBank = WxPayApi.queryBank(params, appWxPayConfig.getCertPath(), appWxPayConfig.getMchId());
            map = PaymentKit.xmlToMap(queryBank);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

}
