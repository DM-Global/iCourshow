package com.ics.cmsadmin.frame.profit;

import com.ics.cmsadmin.frame.profit.factory.GradientRateRoleFactory;
import com.ics.cmsadmin.frame.profit.factory.ProfitRoleFactory;
import com.ics.cmsadmin.frame.profit.role.ProfitRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
@Getter
public enum ProfitTypeEnum {
    // 梯度分润 例如 5%<100<10%<200<15%<300<20%
    GRADIENT_RATE(new GradientRateRoleFactory());

//    private String regular;
    private ProfitRoleFactory factory;

    /**
     * 获取分润规则
     *
     * @param expression 分润表达式
     */
    public ProfitRole getProfitRole(String expression) {
        return this.factory.createProfitRole(expression);
    }

    public static ProfitTypeEnum valueOfProfitType(String profitType) {
        try {
            return ProfitTypeEnum.valueOf(profitType);
        } catch (Exception e) {
            log.error("ProfitTypeEnum is {}", e);
            return null;
        }
    }
}
