package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.bean.StudyHistory;

import java.util.List;

public interface StudyHistoryService {

    List<PointBean> getRecentStudy(StudyHistory studyHistory, PageBean page);

    int insert(StudyHistory studyHistory);

    void markFinish(StudyHistory studyHistory);

    StudyHistory findOne(StudyHistory studyHistory);
}
