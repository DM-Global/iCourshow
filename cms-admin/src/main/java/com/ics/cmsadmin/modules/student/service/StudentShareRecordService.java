package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.modules.student.bean.StudentShareRecordBean;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;



/**
* t_student_share_record 服务类
* Created by lvsw on 2019-03-03 00:06:16.
*/
public interface StudentShareRecordService extends BaseService<StudentShareRecordBean>, BaseDataService<StudentShareRecordBean> {

}
