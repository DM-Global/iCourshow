package com.ics.cmsadmin.modules.agent.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.agent.bean.AccountDetailBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * a_account_detail
 * Created by lvsw on 2018-37-29 16:09:14.
 */
@Repository
@Mapper
public interface AccountDetailDao extends BaseDataDao<AccountDetailBean> {
    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") AccountDetailBean t);

}
