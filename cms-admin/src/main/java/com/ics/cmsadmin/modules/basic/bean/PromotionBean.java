package com.ics.cmsadmin.modules.basic.bean;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;


/**
 * t_promotion
 * <p>
 * <p>
 * Created by sandwich on 2018-12-06 21:12:37.
 */
@Data
@Builder
public class PromotionBean {

    private String id;              // 
    private String level;              // 
    private String levelId;              // 
    private String levelNode;              //
    private String levelNodeName;
    private String imageSrc;              //
    private Boolean isActive;              //
    private String description;              // 
    private String rank;              // 
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 
    private String imageId;              // 图片id

    @Tolerate
    public PromotionBean() {
    }
}
