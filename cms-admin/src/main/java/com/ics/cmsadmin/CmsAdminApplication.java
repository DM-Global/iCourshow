package com.ics.cmsadmin;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@Log4j2
@EnableAsync
public class CmsAdminApplication {

    public static void main(String[] args) {
        log.info("Springboot 启动开始");
        SpringApplication.run(CmsAdminApplication.class, args);
    }
}
