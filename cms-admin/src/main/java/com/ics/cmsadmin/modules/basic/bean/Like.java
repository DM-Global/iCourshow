package com.ics.cmsadmin.modules.basic.bean;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Like {

    private String userId;

    private LevelEnum levelName;

    private String levelId;

    public Like(String userId, LevelEnum levelName, String levelId) {
        this.userId = userId;
        this.levelName = levelName;
        this.levelId = levelId;
    }

    public Like() {
    }
}
