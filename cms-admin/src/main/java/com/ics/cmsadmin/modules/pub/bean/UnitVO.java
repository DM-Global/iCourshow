package com.ics.cmsadmin.modules.pub.bean;

import com.ics.cmsadmin.modules.pub.bean.Teacher;
import lombok.Data;

@Data
public class UnitVO {

    private Integer id;

    private String name;

    private Boolean isActive;

    private String description;

    private Integer courseId;

    private String courseName;

    private Integer subjectId;

    private String subjectName;

    private Integer teacherId;

    private String teacherName;

    private Integer rank;

    private Integer coverId;

    private String coverSrc;

    private Integer progress;

    private Boolean collected;

    /**
     * 课程是否已经购买
     */
    private Boolean hasBought;

    /**
     * 是否点赞了
     */
    private Boolean liked;

    /**
     * 点赞数量
     */
    private Integer thumbUpCount;

    /**
     * 收藏数量
     */
    private Integer collectCount;

    /**
     * 教师信息
     */
    private Teacher teacher;

    /**
     * 课程的学员数量
     */
    private Integer learnerCount;

    /**
     * 更新的主题数
     */
    private Integer topicCount;

    private Integer iconId;

    private String iconSrc;
}
