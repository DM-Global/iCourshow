package com.ics.cmsadmin.modules.wechat.service;


import com.ics.cmsadmin.modules.wechat.model.auth.resp.AuthAccessToken;
import com.ics.cmsadmin.modules.wechat.model.auth.resp.AuthUserInfo;
import com.ics.cmsadmin.modules.wechat.model.auth.resp.JsSDKConfig;
import com.ics.cmsadmin.modules.wechat.model.param.AbstractParams;
import com.ics.cmsadmin.modules.wechat.result.ResultState;

/**
 * @author phil
 * @date 2017年8月5日
 */
public interface WechatAuthService {

    String getAccessToken();

    /**
     * 获取授权请求url
     *
     * @param basic
     * @param url
     * @return
     * @throws Exception
     */
    String getAuthPath(AbstractParams basic, String url) throws Exception;

    /**
     * 获取网页授权凭证
     *
     * @param basic
     * @param url
     * @return
     */
    AuthAccessToken getAuthAccessToken(AbstractParams basic, String url);

    /**
     * 刷新网页授权验证
     *
     * @param basic 参数
     * @param url   请求路径
     * @return
     */
    AuthAccessToken refreshAuthAccessToken(AbstractParams basic, String url);

    /**
     * 通过网页授权获取用户信息
     *
     * @param accessToken
     * @param openid
     * @return
     */
    AuthUserInfo getAuthUserInfo(String accessToken, String openid);

    /**
     * 检验授权凭证（access_token）是否有效
     *
     * @param accessToken 网页授权接口调用凭证
     * @param openid      用户的唯一标识
     * @return { "errcode":0,"errmsg":"ok"}表示成功     { "errcode":40003,"errmsg":"invalid openid"}失败
     */
    ResultState authToken(String accessToken, String openid);

    /**
     * 获取jsapi_ticket 调用微信JS接口的临时票据
     *
     * @return
     */
    String getTicket();

    /**
     * js-sdk接口注入权限验证配置信息
     *
     * @param url
     * @return
     */
    JsSDKConfig jsSDKConfig(String url);
}
