package com.ics.cmsadmin.modules.sso.handler;

import com.ics.cmsadmin.frame.api.WechatApi;
import com.ics.cmsadmin.frame.api.WechatApiResponse;
import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.property.AppConfig;
import com.ics.cmsadmin.modules.app.bean.AppInfoBean;
import com.ics.cmsadmin.modules.app.utils.AppContants;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.sso.LoginHandler;
import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoContants;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * android app 微信登陆
 * Created by 666666 on 2018/8/25.
 */
@Log4j2
@Component
public class AppWechatLoginHandler implements LoginHandler<StudentBean> {

    @Resource
    private AppConfig appConfig;
    @Resource(name = "newStudentService")
    private StudentService studentService;

    @Override
    public StudentBean dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum) {
        String code = request.getParameter(SsoContants.LOGIN_CODE);
        String agentNo = request.getParameter(SsoContants.LOGIN_AGENT_NO);
        String recommenderStudent = request.getParameter(SsoContants.LOGIN_RECOMMEND_STUDENT_ID);
        WechatApiResponse accessTokenByCode = WechatApi.getAccessTokenByCode(appConfig.getWechatAppId(), appConfig.getWechatAppSecret(), code);
        if (accessTokenByCode == null || accessTokenByCode.getErrcode() != 0) {
            return null;
        }
//        WechatApiResponse newAccessToken = WechatApi.getAccessToken(appConfig.getWechatAppId(), appConfig.getWechatAppSecret());
        WechatApiResponse userinfo = WechatApi.getUserinfo(accessTokenByCode.getAccessToken(), accessTokenByCode.getOpenid());
        if (userinfo == null || userinfo.getErrcode() != 0) {
            return null;
        }
        AppInfoBean appInfoBean = SsoUtils.getAppInfo(request);
        StudentBean student = StudentBean.builder()
            .agentNo(agentNo)
            .parentId(recommenderStudent)
            .registerSource(Constants.REGISTER_SOURCE_APP_WECHAT)
            .appOpenId(userinfo.getOpenid())
            .unionId(userinfo.getUnionid())
            .nickname(userinfo.getNickname())
            .isFollowed(userinfo.getSubscribe() > 0)
            .avatar(userinfo.getHeadimgurl())
            .gender(userinfo.getSex() + "")
            .build();
        if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_IOS)){
            student.setIosDeviceToken(appInfoBean.uMengDeviceToken);
        }else if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_ANDROID)){
            student.setAndroidDeviceToken(appInfoBean.uMengDeviceToken);
        }
        StudentBean existStudent = studentService.refreshStudentInfoByWechat(student, true);
        existStudent.setHasPassword(StringUtils.isNotBlank(existStudent.getPassword()));
        existStudent.setPassword(null);
        existStudent.setShareUrl(AppUtils.getStudentShareUrl(existStudent.getId()));
        SsoUtils.saveLoginInfo2Redis(loginTypeEnum, existStudent, 30 * 24 * 3600L);
        existStudent.setAccessToken(accessTokenByCode.getAccessToken());
        return existStudent;
    }
}
