package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.PromotionBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * t_promotion
 * Created by sandwich on 2018-12-06 21:12:37.
 */
@Repository
@Mapper
public interface PromotionDao extends BaseDao<PromotionBean>, BaseDataDao<PromotionBean> {
}
