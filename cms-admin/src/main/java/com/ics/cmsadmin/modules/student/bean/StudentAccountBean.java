package com.ics.cmsadmin.modules.student.bean;

                                            
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
* t_student_account
* 学生账户
*
* Created by lvsw on 2019-18-11 21:03:47.
*/
@Data
@Builder
public class StudentAccountBean {

    private String accountId;              // 账户id
    private String studentId;              // 学生编号
    private BigDecimal totalIncome = BigDecimal.ZERO;              // 总收益
    private BigDecimal withdrawCash = BigDecimal.ZERO;              // 提现金额
    private BigDecimal balance = BigDecimal.ZERO;              // 余额
    private BigDecimal availableBalance = BigDecimal.ZERO;              // 可用余额
    private BigDecimal frozenBalance = BigDecimal.ZERO;              // 冻结余额

    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;              // 最后更新时间

    @Tolerate
    public StudentAccountBean () {
    }
}
