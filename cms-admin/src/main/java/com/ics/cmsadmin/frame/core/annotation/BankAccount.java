package com.ics.cmsadmin.frame.core.annotation;

import javax.validation.Constraint;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = BankAccountCheck.class)
@Target({ElementType.METHOD, ElementType.FIELD})  // 注解作用于方法上
@Retention(RetentionPolicy.RUNTIME)
public @interface BankAccount {
    String forbidden() default "mmp";

    String message() default "无效银行卡";

    Class<?>[] groups() default {};

    Class<? extends javax.validation.Payload>[] payload() default {};

}
