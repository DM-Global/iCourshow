package com.ics.cmsadmin.modules.basic.bean;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by Sandwich on 2018/12/26
 */
@Data
@Builder
public class OrderDescription {

    private String studyPriceTitle;//入学金额
    private String payPriceTitle;//实付金额
    private String studyRightTitle;//入学权益
    private String studyRight1;
    private String studyRight2;
    private String studyRight3;
    private String purchaseInfoTitle;//购买须知
    private String purchaseInfoContent;//购买须知
    private BigDecimal entranceAmount; //入学金额
    private BigDecimal totalAmount; //总金额

}
