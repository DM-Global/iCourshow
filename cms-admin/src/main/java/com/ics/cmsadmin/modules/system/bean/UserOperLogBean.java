package com.ics.cmsadmin.modules.system.bean;


import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;


/**
 * com_user_oper_log
 * 用户操作日志
 * <p>
 * Created by lvsw on 2018-41-11 15:10:21.
 */
@Data
@Builder
public class UserOperLogBean {

    private String id;              // 日志编号
    private String userId;              // 代理商编号
    private String requestUri;              // 请求uri
    private String requestMethod;              // 请求方法
    private String requestDesc;              // 请求描述
    private String requestParams;              // 请求参数
    private String returnResult;              // 返回结果
    private String operIp;              // 请求ip
    private String operTime;              // 操作时间
    private long costTime;              // 执行时间

    @Tolerate
    public UserOperLogBean() {
    }
}
