package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.modules.pub.bean.Promotion;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PromotionMapper extends ResourceMapper<Promotion, Integer> {
}
