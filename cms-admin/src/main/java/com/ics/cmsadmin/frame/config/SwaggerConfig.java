package com.ics.cmsadmin.frame.config;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by Sandwich on 2018/6/14
 */
@Configuration
@ComponentScan(basePackages = {"com.ics.cmsadmin.modules"})//配置controller路径
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket allApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
            .apis(RequestHandlerSelectors.basePackage("com.ics.cmsadmin.modules"))
            .build()
            .groupName("所有接口")
            .pathMapping("/")
            .apiInfo(apiInfo("所有接口", "所有接口", "1.0"));
    }

    @Bean
    public Docket loginApi() {
        return new Docket(DocumentationType.SWAGGER_2).select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.ant("/oauth2/**"))
            .build()
            .groupName("登陆接口")
            .pathMapping("/")
            .apiInfo(apiInfo("登陆接口", "统一的登陆接口", "1.0"));
    }

    @Bean
    public Docket cmsWebApi() {

        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.withMethodAnnotation(Authorize.class))
            .build()
            .groupName("cms-web 相关接口")
            .pathMapping("/")
            .apiInfo(apiInfo("cms-web 相关接口", "cms-web 相关接口", "1.0"));
    }

    @Bean
    public Docket wechatApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.ics.cmsadmin.modules.api"))
            .build()
            .groupName("微信公众号相关接口")
            .pathMapping("/")
            .apiInfo(apiInfo("微信公众号相关接口", "微信公众号相关接口", "1.0"));
    }

    @Bean
    public Docket pubApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.ics.cmsadmin.modules.pub"))
            .build()
            .groupName("微信公众号相关接口-即将废弃的接口")
            .pathMapping("/")
            .apiInfo(apiInfo("微信公众号相关接口", "微信公众号相关接口-即将废弃的接口", "1.0"));
    }

    @Bean
    public Docket appApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.ics.cmsadmin.modules.app"))
            .build()
            .groupName("app相关接口")
            .pathMapping("/")
            .apiInfo(apiInfo("app相关接口", "app相关接口", "1.0"));
    }

    private ApiInfo apiInfo(String name, String description, String version) {
        ApiInfo apiInfo = new ApiInfoBuilder()
            .title(name)
            .description(description)
            .version(version).build();
        return apiInfo;
    }

    /**
     * 这个地方要重新注入一下资源文件，不然不会注入资源的，也没有注入requestHandlerMappping,相当于xml配置的
     * <!--swagger资源配置-->
     * <mvc:resources location="classpath:/META-INF/resources/" mapping="swagger-ui.html"/>
     * <mvc:resources location="classpath:/META-INF/resources/webjars/" mapping="/webjars/**"/>
     * 不知道为什么，这也是spring boot的一个缺点
     * //     * @param registry
     */
//    @Bean
//    public Docket createRestApi() {
//        return new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(apiInfo())
//                .select()
//                //为当前包路径,选定要展示的接口
//                .apis(RequestHandlerSelectors.basePackage("com.ics.cmsadmin.modules"))
//                .paths(PathSelectors.any())
//                .build();
//    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            //页面标题
            .title("iCourshow RESTful API")
            //创建人
            .contact(new Contact("Sandwich", "", ""))
            //版本号
            .version("1.0")
            //描述
            .description("API 描述")
            .build();
    }


}
