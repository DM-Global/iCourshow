package com.ics.cmsadmin.modules.student.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

@Data
@Builder
public class StudentDaySummaryBean {
    private String id;
    private String studentId;
    private String parentId;
    private String oldShareRate;
    private String newShareRate;
    private String currentMonthChildOrderMoneyCusm;
    private String summaryDay;
    private String childrenCusm;
    private String childrenNewly;
    private String childOrderMoneyCusm;
    private String childOrderMoneyNewly;
    private String shareMoneyCusm;
    private String shareMoneyNewly;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;

    private String startCreateDate;
    private String endCreateDate;

    @Tolerate
    public StudentDaySummaryBean() {
    }

}
