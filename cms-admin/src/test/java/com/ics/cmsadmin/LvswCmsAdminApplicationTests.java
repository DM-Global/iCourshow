package com.ics.cmsadmin;

import com.ics.cmsadmin.frame.api.PolyvApi;
import com.ics.cmsadmin.frame.api.response.PolyvVideoInfo;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.modules.basic.bean.UpushHistoryBean;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.service.UpushHistoryService;
import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeEnum;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.dao.StudentDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class LvswCmsAdminApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Resource
    private StudentDao studentDao;

    @Resource
    private UPushApiService uPushApiService;
    @Resource
    private UpushHistoryService upushHistoryService;

    @Test
    public void test1() {
        Long unReadCount = upushHistoryService.getUnReadCount("9e173d7c75b74d639ab2ffe717954360");
        System.out.println("--->" + unReadCount);
    }
    @Test
    public void test11() {
        StudentBean studentBean = studentDao.queryById("0013b30f343e43a1a8b8d0465b860563");
        System.out.println(studentBean);
    }
    @Test
    public void test() {
        uPushApiService.sendUnicast("ef717f325f32484eb81d89e98c7b3caa",
            UPushCustomDefined.builder()
                .uPushType(UPushTypeEnum.APPLY_WITHDRAY_FAILED)
                .withdrawId("25")
                .money(BigDecimal.valueOf(1000000))
                .remark("test")
                .build());
    }

    @Test
    public void test2() {
        UpushHistoryBean build = UpushHistoryBean.builder()
            .studentId("sdfasdf")
            .content("sss")
            .details("sda")
            .title("ss")
            .type("sadfa")
            .build();
        upushHistoryService.insert(build);
        System.out.println(build.getId());
    }
    @Test
    public void test3() {
        PolyvVideoInfo videoMsg = PolyvApi.getVideoMsg("30fe77e88f6de5ae918dc1835bedbee0_3");
        System.out.println(GsonUtils.toJson(videoMsg));
    }

    @Test
    public void test4() {
        // 001797c05f4440dd8d75085aa295b687

    }
}
