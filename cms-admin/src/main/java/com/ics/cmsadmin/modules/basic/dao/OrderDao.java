package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * t_order
 * Created by lvsw on 2018-16-23 21:09:34.
 */
@Repository
@Mapper
public interface OrderDao extends BaseDataDao<OrderBean> {

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") OrderBean t);

    int update(@Param("bean") OrderBean t);

    /**
     * 根据查询条件汇总金额
     *
     * @param t 查询条件
     * @return 查询结果
     */
    BigDecimal totalMoney(@Param("bean") OrderBean t);

    OrderBean queryById(@Param("id") String id);

    long countDistinctBuyOrderStudentNum(@Param("agentNo") String agentNo, @Param("orderPayTime") String orderPayTime);

    /**
     * 入账
     *
     * @param id
     * @return
     */
    int updateIntoAccount(@Param("id") String id);

    /**
     * 因代理商被禁用,而不入账
     *
     * @param orderId
     */
    int updateNotIntoAccount(String orderId);

    void batchDelete(@Param("ids") List<String> ids);

    void batchSoftRemove(@Param("ids") List<String> ids);

    /**
     * 查询用户有效的包年包月套餐
     *
     * @param userId
     * @return
     */
    OrderBean queryEffectivePackageOrder(@Param("userId") String userId);

    /**
     * 汇总学生交易总金额
     * @param parentId  父级学生id
     * @param startPayTime   开始交易时间
     * @param endPayTime   结束交易时间
     */
    BigDecimal sumTransAmount(@Param("parentId") String parentId,
                              @Param("startPayTime") String startPayTime,
                              @Param("endPayTime") String endPayTime);

    /**
     * 使用人民币购买的订单的数量
     */
    int countBuyWithMoney(@Param("userId") String userId);

    void deleteAllUnpaid(@Param("userId") String userId, @Param("orderType") int orderType);

    OrderBean findLastByEndDate(@Param("bean") OrderBean orderBean);
}
