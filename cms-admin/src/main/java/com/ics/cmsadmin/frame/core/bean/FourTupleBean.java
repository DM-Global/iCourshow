package com.ics.cmsadmin.frame.core.bean;

/**
 * 两位的元组
 * Created by 666666 on 2017/9/26.
 */
public class FourTupleBean<ONE, TWO, THREE, FOUR> extends ThreeTupleBean<ONE, TWO, THREE> {
    public FOUR four;

    public FourTupleBean() {
        super();
    }

    public FourTupleBean(ONE one, TWO two, THREE three, FOUR four) {
        super(one, two, three);
        this.four = four;
    }

}
