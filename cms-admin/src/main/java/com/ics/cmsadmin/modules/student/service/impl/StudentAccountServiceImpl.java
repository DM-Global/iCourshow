package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.student.bean.StudentAccountBean;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentAccountService;

import com.ics.cmsadmin.modules.system.emums.SequenceNumberEnum;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentAccountDao;
import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentAccountDetailDao;
import static com.ics.cmsadmin.modules.student.steward.StudentServices.studentService
    ;
@Service
public class StudentAccountServiceImpl implements StudentAccountService {

    @Override
    public StudentAccountBean queryByStudentId(String studentId) {
        if (StringUtils.isBlank(studentId)) {
            return null;
        }
        return studentAccountDao.findOne(StudentAccountBean.builder().studentId(studentId).build());
    }

    @Override
    public boolean accountChanges(String studentId, BigDecimal money,
                                  StudentAccountChangesEnum studentAccountChangesEnum, StudentAccountDetailBean additional) {
        if (money == null || studentAccountChangesEnum == null || StringUtils.isBlank(studentId)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "账户变动参数有误");
        }

        if (money.compareTo(BigDecimal.ZERO) < 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "账户变动金额不能为负数");
        } else if (money.compareTo(new BigDecimal(0)) == 0) {
            return true;
        }
        synchronized (studentId.intern()) {
            StudentBean studentBean = studentService.queryById(studentId);
            if (studentBean == null) {
                throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "不存在该学生信息");
            }
            StudentAccountBean beforeAccount = queryByStudentId(studentId);
            if (beforeAccount == null) {
                beforeAccount = new StudentAccountBean();
                beforeAccount.setStudentId(studentId);
                insert(beforeAccount);
            }
            switch (studentAccountChangesEnum) {
                case PARENT_PROFIT_INCOME:
                case GRANDFATHER_PROFIT_INCOME:
                    profitIncome(beforeAccount, money);
                    break;
                case APPLY_WITHDRAW_CASH:
                    applyWithdrawCash(beforeAccount, money);
                    break;
                case ACCEPT_WITHDRAW_CASH_SUCCESS:
                    acceptWithdrawCashSuccess(beforeAccount, money);
                    break;
                case ACCEPT_WITHDRAW_CASH_FAILED:
                    acceptWithdrawCashFailed(beforeAccount, money);
                    break;
                case REFUSE_WITHDRAW_CASH:
                    refuseWithdrawCash(beforeAccount, money);
                    break;
                case CANCEL_WITHDRAW_CASH:
                    cancelWithdrawCash(beforeAccount, money);
                    break;
                case BUY_ORDER:
                    buyOrder(beforeAccount, money);
                default:
                    // never go here
                    break;
            }
            StudentAccountBean afterAccount = studentAccountDao.queryById(beforeAccount.getAccountId());
            // todo4lvsw 插入前后流水记录
            studentAccountDetailDao.insert(afterAccount, money, studentAccountChangesEnum, additional);
        }
        return true;
    }

    private void acceptWithdrawCashFailed(StudentAccountBean studentAccountBean, BigDecimal money) {
        if (money.compareTo(studentAccountBean.getFrozenBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("提现金额%s元不得大于冻结余额%s元", money.toString(), studentAccountBean.getFrozenBalance()));
        }
        studentAccountDao.acceptWithdrawCashFailed(studentAccountBean.getAccountId(), money);
    }


    private void buyOrder(StudentAccountBean studentAccountBean, BigDecimal money) {
        if (money.compareTo(studentAccountBean.getAvailableBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("可用余额的金额%s不足以支付订单的金额%s", studentAccountBean.getAvailableBalance(), money.toString()));
        }
        studentAccountDao.buyOrder(studentAccountBean.getAccountId(), money);
    }

    /**
     * 申请提现
     * @param studentAccountBean
     * @param money
     */
    private void applyWithdrawCash(StudentAccountBean studentAccountBean, BigDecimal money) {
        if (money.compareTo(studentAccountBean.getAvailableBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("申请提现金额%s元不得大于可用余额%s元", money.toString(), studentAccountBean.getAvailableBalance()));
        }
        studentAccountDao.applyWithdrawCash(studentAccountBean.getAccountId(), money);
    }

    /**
     * 接收提现申请
     * @param studentAccountBean
     * @param money
     */
    private void acceptWithdrawCashSuccess(StudentAccountBean studentAccountBean, BigDecimal money) {
        // todo4lvsw 接收提现申请需要创建支付宝或微信的提现记录单
        if (money.compareTo(studentAccountBean.getBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("提现金额%s元不得大于余额%s元", money.toString(), studentAccountBean.getBalance()));
        }
        if (money.compareTo(studentAccountBean.getFrozenBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("提现金额%s元不得大于冻结余额%s元", money.toString(), studentAccountBean.getFrozenBalance()));
        }
        studentAccountDao.acceptWithdrawCashSuccess(studentAccountBean.getAccountId(), money);
    }

    /**
     * 拒绝提现申请
     * @param studentAccountBean
     * @param money
     */
    private void refuseWithdrawCash(StudentAccountBean studentAccountBean, BigDecimal money) {
        if (money.compareTo(studentAccountBean.getFrozenBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("提现金额%s元不得大于冻结余额%s元", money.toString(), studentAccountBean.getFrozenBalance()));
        }
        studentAccountDao.refuseWithdrawCash(studentAccountBean.getAccountId(), money);
    }

    /**
     * 取消提现申请
     * @param studentAccountBean
     * @param money
     */
    private void cancelWithdrawCash(StudentAccountBean studentAccountBean, BigDecimal money) {
        if (money.compareTo(studentAccountBean.getFrozenBalance()) > 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL,
                String.format("提现金额%s元不得大于冻结余额%s元", money.toString(), studentAccountBean.getFrozenBalance()));
        }
        studentAccountDao.cancelWithdrawCash(studentAccountBean.getAccountId(), money);
    }

    /**
     * 分润输入
     * @param studentAccountBean
     * @param money
     */
    private void profitIncome(StudentAccountBean studentAccountBean, BigDecimal money) {
        studentAccountDao.profitIncome(studentAccountBean.getAccountId(), money);
    }

    @Override
    public boolean insert(StudentAccountBean bean) {
        if (bean == null) {
            return false;
        }
        if (StringUtils.isBlank(bean.getStudentId())) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "该学生不存在");
        }
        StudentBean studentBean = studentService.queryById(bean.getStudentId());
        if (studentBean == null) {
            throw new CmsException(ApiResultEnum.INSERT_TABLE_ITEM_FAIL, "该学生不存在");
        }
        bean.setAccountId(SystemServices.sequenceNumberService.newSequenceNumber(SequenceNumberEnum.STUDENT_ACCOUNT));
        return studentAccountDao.insert(bean) == 1;
    }
}
