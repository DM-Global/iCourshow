package com.ics.cmsadmin.frame.converter.serializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 666666 on 2018/8/27.
 */
public class LongString2DataDeserializer extends JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(p.getText());
        } catch (Exception e) {
            return null;
        }
    }
}
