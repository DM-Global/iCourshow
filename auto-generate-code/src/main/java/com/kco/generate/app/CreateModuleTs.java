package com.kco.generate.app;

import com.kco.generate.config.TableBean;

import static com.kco.generate.config.Utils.saveFile;

/**
 * Created by 666666 on 2017/11/6.
 */
public class CreateModuleTs {
    public static void buildFile(TableBean tableBean) {
        StringBuilder sb = new StringBuilder();
        sb.append(buildImport(tableBean))
            .append(buildNgModule(tableBean))
            .append(buildModuleClass(tableBean));
        String className = tableBean.getClassName();
        String fileName = className.substring(0, 1).toLowerCase() + className.substring(1);
        saveFile("js/" + fileName + "/" + fileName + ".module.ts", sb.toString());
    }

    private static String buildModuleClass(TableBean tableBean) {
        String className = tableBean.getClassName();
        StringBuilder sb = new StringBuilder();
        sb.append("export class " + className + "Module { }\n");
        return sb.toString();
    }

    private static String buildNgModule(TableBean tableBean) {
        String className = tableBean.getClassName();
        StringBuilder sb = new StringBuilder();
        sb.append("@NgModule({\n" +
            "  imports: [\n" +
            "    CommonModule,\n" +
            "    FormsModule,\n" +
            "    " + className + "RoutingModule,\n" +
            "    DataTableModule,\n" +
            "    SharedModule,\n" +
            "    PanelModule,\n" +
            "    InputTextModule,\n" +
            "    ButtonModule,\n" +
            "    TabViewModule,\n" +
            "    DialogModule,\n" +
            "    MenubarModule\n" +
            "  ],\n" +
            "  declarations: [\n" +
            "    " + className + "Component,\n" +
            "    " + className + "ListComponent,\n" +
            "    " + className + "DetailsComponent,\n" +
            "  ],\n" +
            "  exports: [\n" +
            "  ],\n" +
            "  providers: [\n" +
            "    " + className + "Service\n" +
            "  ]\n" +
            "})\n");
        return sb.toString();
    }

    private static String buildImport(TableBean tableBean) {
        String className = tableBean.getClassName();
        String tempName = className.substring(0, 1).toLowerCase() + className.substring(1);
        StringBuilder sb = new StringBuilder();
        sb.append("import {NgModule} from \"@angular/core\";\n")
            .append("import {CommonModule} from \"@angular/common\";\n")
            .append("import {FormsModule} from \"@angular/forms\";\n\n")
            .append("import {" + className + "RoutingModule} from \"./" + tempName + "-routing.module\";\n")
            .append("import {" + className + "Component} from \"./" + tempName + ".component\";\n")
            .append("import {" + className + "ListComponent} from \"./" + tempName + "-list.component\";\n")
            .append("import {" + className + "DetailsComponent} from \"./" + tempName + "-details.component\";\n")
            .append("import {\n" +
                "  DataTableModule, TabViewModule, PanelModule, SharedModule, InputTextModule, ButtonModule,\n" +
                "  DialogModule, MenubarModule\n" +
                "} from \"primeng/primeng\";\n")
            .append("import {" + className + "Service} from \"./" + tempName + ".service\";\n\n\n");
        return sb.toString();
    }
}
