package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * q_problem_set
 * 题集
 * Created by lvsw on 2018-09-06.
 */
@Data
@Builder
public class ProblemSetBean {

    // 题集id
//	@NotEmpty(message = "{ProblemSet.problemSetId.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String problemSetId;
    // 标题
//	@Length(message = "{ProblemSet.title.Length}", min = 2, max = 45, groups = {InsertGroup.class, UpdateGroup.class})
    private String title;
    //
//	@NotEmpty(message = "{ProblemSet.level.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
//	@Length(message = "{ProblemSet.level.Length}", min = 2, max = 7, groups = {InsertGroup.class, UpdateGroup.class})
    private String level;
    // 等级id
//	@NotEmpty(message = "{ProblemSet.levelId.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String levelId;
    private String levelNode;
    private String levelNodeName;
    // 状态
//	@NotEmpty(message = "{ProblemSet.isActive.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String isActive;
    // 排序
//	@NotEmpty(message = "{ProblemSet.orderNo.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    private String orderNo;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 更新时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;
    private Date startTime;
    private Date endTime;

    @Tolerate
    public ProblemSetBean() {
    }
}
