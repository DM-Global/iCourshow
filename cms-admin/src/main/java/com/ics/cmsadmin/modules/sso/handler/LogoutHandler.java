package com.ics.cmsadmin.modules.sso.handler;

import com.ics.cmsadmin.modules.sso.LoginHandler;
import com.ics.cmsadmin.modules.sso.LoginInfo;
import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 注销登陆
 * Created by 666666 on 2018/8/25.
 */
@Log4j2
@Component
public class LogoutHandler implements LoginHandler<LoginInfo> {


    @Override
    public LoginInfo dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum) {
        SsoUtils.deleteLoginInfoFromRedis(request);
        return null;
    }
}
