package com.ics.cmsadmin.modules.wechat.model.resp;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

/**
 * 图文消息
 *
 * @author phil
 */
public class RespNewsMsg extends RespAbstractMsg {

    private int ArticleCount;       //是 图文消息个数，限制为10条以内

    //    @XStreamImplicit
    private List<Article> Articles; //是 多条图文消息信息，默认第一个item为大图,注意，如果图文数超过10，则将会无响应

    public int getArticleCount() {
        return ArticleCount;
    }

    public void setArticleCount(int articleCount) {
        ArticleCount = articleCount;
    }

    public List<Article> getArticles() {
        if (Articles == null) Articles = new ArrayList<Article>();
        return Articles;
    }

    public void setArticles(List<Article> articles) {
        Articles = articles;
    }

    @Override
    public String setMsgType() {
        return "news";
    }

    //    @XStreamAlias("item")
    public static class Article {
        private String Title;               //否 图文消息标题
        private String Description;         //否 图文消息描述
        private String PicUrl;              //否 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200
        private String Url;                 //否 点击图文消息跳转链接

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getPicUrl() {
            return PicUrl;
        }

        public void setPicUrl(String picUrl) {
            PicUrl = picUrl;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String url) {
            Url = url;
        }
    }
}
