package com.ics.cmsadmin.modules.wechat.constanst;

public class Constant {

    public static final String DEFAULT_CHARACTER_ENCODING = "UTF-8";
    public static final String GET_METHOD = "GET";
    public static final String POST_METHOD = "POST";

    public static final String WECHAT_ACCESS_TOKEN = "WECHAT_ACCESS_TOKEN";


    public static final String WECHAT_JS_SDK_TICKET = "WECHAT_JS_SDK_TICKET";
}
