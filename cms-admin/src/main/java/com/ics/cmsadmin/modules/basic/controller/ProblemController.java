package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.ThreeTupleBean;
import com.ics.cmsadmin.modules.basic.bean.AnswersBean;
import com.ics.cmsadmin.modules.basic.bean.ProblemBean;
import com.ics.cmsadmin.modules.basic.bean.TipsBean;
import com.ics.cmsadmin.modules.basic.service.AnswersService;
import com.ics.cmsadmin.modules.basic.service.ProblemService;
import com.ics.cmsadmin.modules.basic.service.TipsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Problem controller
 * Created by lvsw on 2018-09-06.
 */
@Api(description = "问题管理接口")
@RestController
@RequestMapping("/problem")
public class ProblemController {

    @Resource
    private ProblemService problemService;
    @Resource
    private AnswersService answersService;
    @Resource
    private TipsService tipsService;

    @Authorize(AuthorizeEnum.PROBLEM_QUERY)
    @ApiOperation(value = "查询问题信息")
    @GetMapping(value = "/query/{problemId}")
    public ApiResponse queryById(@ApiParam(value = "problemId") @PathVariable String problemId) {
        ProblemBean problemBean = problemService.queryById(problemId);
        List<AnswersBean> answersBeans = answersService.queryByProblemId(problemId);
        List<TipsBean> tipsBeans = tipsService.queryByProblemId(problemId);
        return new ApiResponse(new ThreeTupleBean<>(problemBean, answersBeans, tipsBeans));
    }

    @Authorize(AuthorizeEnum.PROBLEM_INSERT)
    @ApiOperation(value = "新增问题信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@RequestBody ThreeTupleBean<ProblemBean, List<AnswersBean>, List<TipsBean>> bean) {
        return new ApiResponse(problemService.insertProblem(bean.one, bean.two, bean.three));
    }

    @Authorize(AuthorizeEnum.PROBLEM_UPDATE)
    @ApiOperation(value = "更新问题信息")
    @PostMapping(value = "/update/{problemId}")
    public ApiResponse update(@RequestBody ThreeTupleBean<ProblemBean, List<AnswersBean>, List<TipsBean>> bean,
                              @PathVariable String problemId) {
        return new ApiResponse(problemService.updateProblem(bean.one, bean.two, bean.three, problemId));
    }

    @Authorize(AuthorizeEnum.PROBLEM_UPDATE)
    @ApiOperation(value = "禁用问题信息")
    @PostMapping(value = "/disable/{problemId}")
    public ApiResponse disable(@ApiParam("需要禁用的问题 id") @PathVariable String problemId) {
        return new ApiResponse(problemService.update(problemId, ProblemBean.builder().isActive("0").build()));
    }

    @Authorize(AuthorizeEnum.PROBLEM_UPDATE)
    @ApiOperation(value = "启用问题信息")
    @PostMapping(value = "/enable/{problemId}")
    public ApiResponse enable(@ApiParam("需要启用的问题 id") @PathVariable String problemId) {
        return new ApiResponse(problemService.update(problemId, ProblemBean.builder().isActive("1").build()));
    }

    @Authorize(AuthorizeEnum.PROBLEM_DELETE)
    @ApiOperation(value = "删除问题信息")
    @PostMapping(value = "/delete/{problemId}")
    public ApiResponse delete(@ApiParam("需要删除的问题 id") @PathVariable String problemId) {
        return new ApiResponse(problemService.delete(problemId));
    }

    @Authorize(AuthorizeEnum.PROBLEM_QUERY)
    @ApiOperation(value = "分页查询问题信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody ProblemBean problemBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(problemService.list(problemBean, pageBean));
    }

}
