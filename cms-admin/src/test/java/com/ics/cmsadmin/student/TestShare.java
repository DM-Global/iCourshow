package com.ics.cmsadmin.student;

import com.ics.cmsadmin.modules.agent.service.ShareDetailService;
import com.ics.cmsadmin.modules.student.service.StudentDayService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author kco1989
 * @email kco1989@qq.com
 * @date 2018-02-22 16:12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw"})
public class TestShare {
    @Resource
    private ShareDetailService shareDetailService;
    @Resource
    private StudentDayService studentDayService;
    @Resource
    private JdbcTemplate jdbcTemplate;
    private static List<StudentBean> studentBeanList = new ArrayList<>();
    private static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void test1() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String start = "2019-01-20";
        String end = "2019-02-05";
        Date startDate = dateFormat.parse(start);
        Date endDate = dateFormat.parse(end);
        do {
            studentDayService.dayStatistics(DateFormatUtils.format(startDate, "yyyy-MM-dd"));
            startDate = DateUtils.addDays(startDate, 1);
        } while (endDate.after(startDate));

    }

    @Test
    public void test2() {
        studentDayService.dayStatistics("2019-02-01");
    }
}
