CREATE TABLE `t_bank`  (
  `band_code` int(10) NOT NULL COMMENT '银行ID',
  `band_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '银行名称',
  `is_active` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态:0 禁用,1激活',
  PRIMARY KEY (`band_code`)
);
