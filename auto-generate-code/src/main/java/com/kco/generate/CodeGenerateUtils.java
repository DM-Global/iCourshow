package com.kco.generate;

import com.kco.generate.app.*;
import com.kco.generate.config.SqlUtils;
import com.kco.generate.config.TableBean;
import com.kco.generate.java.*;

/**
 * Created by 666666 on 2017/9/26.
 *
 * @see VmCodeGenerateUtils
 * @deprecated
 */
@Deprecated
public class CodeGenerateUtils {

    public static void main(String[] args) throws Exception {
        String[] tables = {
            "t_template",
        };

        for (String table : tables) {
            TableBean tableBean = SqlUtils.queryByDBAndTableName("icourshow", table);

            tableBean.setBasePackage("basic");
            // java
            CreateBeanJava.buildFile(tableBean);              // bean.java
            CreateServiceJava.buildFile(tableBean);           // service.java
            CreateServiceImplJava.buildFile(tableBean);       // serviceImpl.java
            CreateDaoJava.buildFile(tableBean);               // dao.java
            CreateDaoXml.buildFile(tableBean);                // dao.xml
            CreateControllerJava.buildFile(tableBean);          // controller.java
            CreateOther.buildFile(tableBean);
            // app
            tableBean.setClassName(tableBean.getClassName().replaceAll("Info", ""));
//            CreateModuleTs.buildFile(tableBean);
//            CreateRoutingTs.buildFile(tableBean);
//            CreateComponentTs.buildFile(tableBean);
//            CreateListComponentHtml.buildFile(tableBean);
//            CreateListComponentTs.buildFile(tableBean);
//            CreateServiceTs.buildFile(tableBean);
//            CreateDetailsComponentHtml.buildFile(tableBean);
//            CreateDetailsComponentTs.buildFile(tableBean);
            CreateVue.buildFile(tableBean);
        }

    }

}
