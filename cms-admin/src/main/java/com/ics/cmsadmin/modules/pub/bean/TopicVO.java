package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class TopicVO {

    private Integer id;

    private String name;

    private Boolean isActive;

    private Boolean isPublic;

    private String description;

    private Integer unitId;

    private String unitName;

    private Integer courseId;

    private String courseName;

    private Integer subjectId;

    private String subjectName;

    private Integer rank;

    private Integer progress;

    private Boolean collected;

    /**
     * 课程是否已经购买
     */
    private Boolean hasBought;
}
