package com.ics.cmsadmin.frame.api.response;

import lombok.Data;

@Data
public class PolyvResponse<T> {
    private int code;
    private String status;
    private String message;
    private T data;
}
