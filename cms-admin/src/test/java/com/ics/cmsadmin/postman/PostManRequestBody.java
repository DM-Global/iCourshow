package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PostManRequestBody {
    private String mode = "raw";
    private String raw;
}
