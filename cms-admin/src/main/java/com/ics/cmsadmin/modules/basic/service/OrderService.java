package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.jpay.vo.AjaxResult;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


/**
 * t_order 服务类
 * Created by lvsw on 2018-16-23 21:09:34.
 */
public interface OrderService extends LoginBaseDataService<OrderBean> {

    OrderBean queryById(String id);

    /**
     * 根据查询条件汇总金额
     *
     * @param t 查询条件
     * @return 查询结果
     */
    BigDecimal totalMoney(OrderBean t, String loginUserId);

    /**
     * 根据查询条件查询列表并汇总
     *
     * @param t 查询条件
     * @return 查询结果
     */
    PageResult list(OrderBean t, PageBean page);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    boolean insert(OrderBean t);

    boolean update(OrderBean t);

    OrderBean queryUserOrder(String userId, String courseId);

    boolean getPurchaseStatus(String userId, String courseId);

    long count(OrderBean t);

    void batchDelete(List<String> ids);

    void batchSoftRemove(List<String> removeList);

    OrderBean create(OrderBean orderBean, String loginUserId);

    OrderBean createTrialOrderForNewUser(String userId);

    Boolean hasTrialOrder(String userId);

    OrderBean createTrialOrderForOldUser(String userId);

    /**
     * 创建免费订单
     * @param studentId 学生id
     * @param days      免费订单有效期（天数）
     */
    OrderBean createFreeOrder(String studentId, int days);

    /**
     * 通过账户余额购买订单
     * @param orderBean
     * @param loginUserId
     * @return
     */
    OrderBean createOrderByAccount(OrderBean orderBean, String loginUserId);
}
