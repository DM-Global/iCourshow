package com.ics.cmsadmin.modules.system.service;

import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.BaseDataService;


/**
 * t_app_version 服务类
 * Created by lvsw on 2018-17-05 20:12:49.
 */
public interface AppVersionService extends BaseService<AppVersionBean>, BaseDataService<AppVersionBean> {
    /**
     * 检查app版本信息
     *
     * @param bean
     * @return
     */
    AppVersionBean checkAppVersion(AppVersionBean bean);

    /**
     * 获取最新的版本
     * @param type
     * @returns
     */
    AppVersionBean findLastVersionByType(String type);
}
