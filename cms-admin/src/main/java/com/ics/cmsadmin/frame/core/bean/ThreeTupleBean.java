package com.ics.cmsadmin.frame.core.bean;

/**
 * 两位的元组
 * Created by 666666 on 2017/9/26.
 */
public class ThreeTupleBean<ONE, TWO, THREE> extends TwoTupleBean<ONE, TWO> {
    public THREE three;

    public ThreeTupleBean() {
    }

    public ThreeTupleBean(ONE one, TWO two, THREE three) {
        super(one, two);
        this.three = three;

    }

}
