package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.student.bean.StudentDaySummaryBean;
import com.ics.cmsadmin.modules.student.dao.StudentDaySummaryDao;
import com.ics.cmsadmin.modules.student.service.StudentDayService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.ics.cmsadmin.frame.profit.ExpressionConstant.number;
import static com.ics.cmsadmin.frame.profit.ExpressionConstant.rateNumber;
import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentDaySummaryDao;
import static com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum.PARENT_STUDENT_DEFAULT_SHARE_RATE;
import static com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum.PARENT_STUDENT_SHARE_RATE;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.registerService;

@Log4j2
@Service
public class StudentDayServiceImpl implements StudentDayService {

    @Transactional(timeout = -1)
    @Override
    public void dayStatistics() {
        String yesterday = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        this.dayStatistics(yesterday);
    }

    @Transactional(timeout = -1)
    @Override
    public void dayStatistics(String summaryDate) {
        String yesterday = DateFormatUtils.format(DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (StringUtils.isNotBlank(summaryDate)) {
                simpleDateFormat.parse(summaryDate);
            }
        } catch (Exception e) {
            log.error("日期格式不正确(格式：yyyy-MM-dd)");
            throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "日期格式不正确(格式：yyyy-MM-dd)");
        }
        long startTime = System.currentTimeMillis();
        String formatDate = StringUtils.isNotBlank(summaryDate) ? summaryDate : yesterday;
        log.info("开始统计日期为 {}", formatDate);
        studentDaySummaryDao.insertBaseDaySummery(formatDate);
        studentDaySummaryDao.summaryChildrenCusm(formatDate);
        studentDaySummaryDao.summaryChildrenNewly(formatDate);
        studentDaySummaryDao.summaryChildOrderMoneyCusm(formatDate);
        studentDaySummaryDao.summaryChildOrderMoneyNewly(formatDate);
        studentDaySummaryDao.summaryShareMoneyCusm(formatDate);
        studentDaySummaryDao.summaryShareMoneyNewly(formatDate);
        try {
            Date today = DateUtils.addDays(simpleDateFormat.parse(summaryDate), 1);
            String currentMonthFirstDay = DateFormatUtils.format(today, "yyyy-MM-01");
            // 统计当月1号到昨天的交易数据,
            // 如果统计那天是刚好是上个月末,则统计出来的交易量则肯定为0
            studentDaySummaryDao.updateCurrentMonthChildOrderMoneyCusm(formatDate, currentMonthFirstDay);
            // 之后根据统计出来的当月交易量来确定当月新的分润比例
            String parentStudentShareRate = registerService.queryRegisterValue(PARENT_STUDENT_SHARE_RATE, Function.identity());
            String orderMoneySequence = parentStudentShareRate.replaceAll(String.format("%s<|<%s", rateNumber, rateNumber), "")
                .replaceAll("<", ",");
            String shareRateSequence = parentStudentShareRate.replaceAll(String.format("<%s<", number), ",")
                .replaceAll("%", "");
            studentDaySummaryDao.updateNewShareRate(formatDate, orderMoneySequence, shareRateSequence);
        } catch (ParseException e) {
            log.warn("dayStatistics {}", e);
        }
        // 如果统计那天是昨天的话,那么就需要更新学生用户的分润比例
//        if (StringUtils.equalsIgnoreCase(formatDate, yesterday)) {
            String parentStudentDefaultShareRate = registerService.queryRegisterValue(PARENT_STUDENT_DEFAULT_SHARE_RATE, Function.identity());
            studentDaySummaryDao.updateStudentShareRate(formatDate, parentStudentDefaultShareRate);
            log.info("结束统计日期为 {}, updateStudentShareRate 耗时 {}", formatDate, (System.currentTimeMillis() - startTime));
//        }
        long endTime = System.currentTimeMillis();
        log.info("结束统计日期为 {}, 耗时 {}", formatDate, (endTime - startTime));
    }

    @Override
    public List<StudentDaySummaryBean> list(String studentId, String startCreateDate, String endCreateDate) {
        if (StringUtils.isBlank(studentId) || StringUtils.isBlank(startCreateDate) || StringUtils.isBlank(endCreateDate)) {
            return new ArrayList<>();
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate startCreate;
        LocalDate endCreate;
        try {
            startCreate = LocalDate.parse(startCreateDate, dateTimeFormatter);
            endCreate = LocalDate.parse(endCreateDate, dateTimeFormatter);
            if (startCreate.isAfter(endCreate)) {
                throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "开始时间不得大于结束时间");
            }
        } catch (DateTimeParseException e) {
            log.error("请求开始时间:{}, 请求结束时间{}, 格式不正确", startCreateDate, endCreateDate);
            throw new CmsException(ApiResultEnum.VALIDATE_ERROR, "参数有误");
        }

        List<StudentDaySummaryBean> result = new ArrayList<>();
        Map<String, StudentDaySummaryBean> collect = Optional.ofNullable(studentDaySummaryDao.list(studentId, startCreateDate, endCreateDate))
            .orElse(new ArrayList<>())
            .stream()
            .collect(Collectors.toMap(StudentDaySummaryBean::getSummaryDay, Function.identity()));

        while (!startCreate.isAfter(endCreate)) {
            result.add(Optional.ofNullable(collect.get(startCreate.format(dateTimeFormatter)))
                            .orElse(StudentDaySummaryBean.builder()
                                .summaryDay(startCreate.format(dateTimeFormatter))
                                .shareMoneyCusm("0")
                                .shareMoneyNewly("0")
                                .build()));
            startCreate = startCreate.plusDays(1);
        }
        return result;
    }

    @Override
    public StudentDaySummaryBean query(String summaryDay, String studentId) {
        StudentDaySummaryBean result = studentDaySummaryDao.query(summaryDay, studentId);
        return Optional.ofNullable(result)
            .orElse(StudentDaySummaryBean.builder()
                .studentId(studentId)
                .childOrderMoneyCusm("0.00")
                .newShareRate("20")
                .build());
    }
}
