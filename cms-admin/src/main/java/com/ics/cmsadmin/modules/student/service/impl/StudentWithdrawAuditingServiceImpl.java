package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.basic.dao.OrderDao;
import com.ics.cmsadmin.modules.basic.service.PayService;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeEnum;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.bean.StudentWithdrawAuditingBean;
import com.ics.cmsadmin.modules.student.dao.StudentWithdrawAuditingDao;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.student.service.StudentWithdrawAuditingService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;

import static com.ics.cmsadmin.modules.student.steward.StudentServices.studentAccountService;
import static com.ics.cmsadmin.modules.student.steward.StudentServices.studentService;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.registerService;

/**
 * t_student_withdraw_auditing
 * Created bylvsw on 2019-44-31 16:05:02.
 */
@Service
public class StudentWithdrawAuditingServiceImpl implements StudentWithdrawAuditingService {
    @Resource
    private StudentWithdrawAuditingDao studentWithdrawAuditingDao;
    @Resource
    private PayService payService;
    @Resource
    private OrderDao orderDao;
    @Resource
    private UPushApiService uPushApiService;
    @Override
    public StudentWithdrawAuditingBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return studentWithdrawAuditingDao.queryById(id);
    }

    @Override
    public PageResult list(StudentWithdrawAuditingBean bean, PageBean page) {
        long count = studentWithdrawAuditingDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, studentWithdrawAuditingDao.list(bean, page));
    }

    @Override
    public boolean insert(StudentWithdrawAuditingBean bean) {
        if (bean == null) {
            return false;
        }
        return studentWithdrawAuditingDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, StudentWithdrawAuditingBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return studentWithdrawAuditingDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return studentWithdrawAuditingDao.delete(id) == 1;
    }

    @Override
    public boolean acceptStudentWithdrawAuditing(String applyId, String loginUserId) {
        if (StringUtils.isBlank(applyId)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "申请提现id不能为空");
        }
        StudentWithdrawAuditingBean withdrawAuditing = studentWithdrawAuditingDao.findOne(
            StudentWithdrawAuditingBean.builder().id(applyId).status(CommonEnums.WithdrawStatus.APPLY.getCode()).build());
        if (withdrawAuditing == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "提现申请记录不存在或者已经处理");
        }
        try {
            // todo4lvsw 如果这里能处理成功，则调用实际出款方法
            Map<String, String> map = payService.wxWithdraw(withdrawAuditing.getWithdrawCash(), withdrawAuditing.getOpenId());
            studentAccountService.accountChanges(withdrawAuditing.getStudentId(),
                withdrawAuditing.getWithdrawCash(),
                StudentAccountChangesEnum.ACCEPT_WITHDRAW_CASH_SUCCESS,
                StudentAccountDetailBean.builder()
                    .withdrawId(applyId)
                    .build()
                );
            uPushApiService.sendUnicast(withdrawAuditing.getStudentId(),
                UPushCustomDefined.builder()
                    .uPushType(UPushTypeEnum.APPLY_WITHDRAY_SUCCESS)
                    .money(withdrawAuditing.getWithdrawCash())
                    .withdrawId(applyId)
                    .build());
            return studentWithdrawAuditingDao.update(applyId,
                StudentWithdrawAuditingBean.builder()
                    .status(CommonEnums.WithdrawStatus.APPLY_SUCCESS.getCode())
                    .relationOrderNo(map.get("partner_trade_no"))
                    .paymentNo(map.get("payment_no"))
                    .remark("处理成功").build()) == 1;
        } catch (CmsException e) {
            studentAccountService.accountChanges(withdrawAuditing.getStudentId(),
                withdrawAuditing.getWithdrawCash(),
                StudentAccountChangesEnum.ACCEPT_WITHDRAW_CASH_FAILED,
                StudentAccountDetailBean.builder()
                    .withdrawId(applyId)
                    .remark(e.getMessage())
                    .build());
            uPushApiService.sendUnicast(withdrawAuditing.getStudentId(),
                UPushCustomDefined.builder()
                    .uPushType(UPushTypeEnum.APPLY_WITHDRAY_FAILED)
                    .money(withdrawAuditing.getWithdrawCash())
                    .withdrawId(applyId)
                    .remark(e.getMessage())
                    .build());
            return studentWithdrawAuditingDao.update(applyId,
                StudentWithdrawAuditingBean.builder()
                    .status(CommonEnums.WithdrawStatus.APPLY_FAILED.getCode())
                    .remark(e.getMessage())
                    .build()) == 1;
        }
    }

    @Override
    public boolean rejectStudentWithdrawAuditing(String applyId, String remark, String loginUserId) {
        if (StringUtils.isBlank(applyId)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "申请提现id不能为空");
        }
        StudentWithdrawAuditingBean withdrawAuditing = studentWithdrawAuditingDao.findOne(
            StudentWithdrawAuditingBean.builder().id(applyId).status(CommonEnums.WithdrawStatus.APPLY.getCode()).build());
        if (withdrawAuditing == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "提现申请记录不存在或者已经处理");
        }
        studentAccountService.accountChanges(withdrawAuditing.getStudentId(),
            withdrawAuditing.getWithdrawCash(),
            StudentAccountChangesEnum.REFUSE_WITHDRAW_CASH,
            StudentAccountDetailBean.builder()
                .withdrawId(applyId)
                .remark(remark)
                .build()
            );
        uPushApiService.sendUnicast(withdrawAuditing.getStudentId(),
            UPushCustomDefined.builder()
                .uPushType(UPushTypeEnum.APPLY_WITHDRAY_FAILED)
                .money(withdrawAuditing.getWithdrawCash())
                .withdrawId(applyId)
                .remark(remark)
                .build());
        return studentWithdrawAuditingDao.update(applyId, StudentWithdrawAuditingBean.builder()
            .status(CommonEnums.WithdrawStatus.REJECT.getCode())
            .remark(remark)
            .build()) == 1;
    }

    @Override
    public boolean applyStudentWithdrawAuditing(String openId, BigDecimal money, String withdrawType, String loginUserId) {
        // 检查该学生是否已经绑定过微信
        StudentBean studentBean = studentService.queryById(loginUserId);
        if (studentBean == null) {
            throw new CmsException(ApiResultEnum.WITHDRAW_LIMIT, "该学生不存在，无法申请提现");
        }
        if (StringUtils.isBlank(openId)) {
            throw new CmsException(ApiResultEnum.WITHDRAW_LIMIT, "请先绑定微信号");
        }
        if (StringUtils.equalsIgnoreCase("1", withdrawType) && StringUtils.isBlank(studentBean.getAppOpenId())) {
            throw new CmsException(ApiResultEnum.WITHDRAW_LIMIT, "该学生未绑定微信，无法提现");
        }
        // 检查该学生是否已经购买过订单
        if (orderDao.countBuyWithMoney(loginUserId) == 0) {
            throw new CmsException(ApiResultEnum.WITHDRAW_LIMIT_MUST_BUY_ORDER, "您至少有一次购买会员消费记录");
        }
        // 检查提现金额是否满足条件
        BigDecimal minWithdrawMoney = registerService.queryRegisterValue(RegisterKeyValueEnum.STUDENT_MIN_WITHDRAW_MONEY, BigDecimal::new);
        BigDecimal maxWithdrawMoney = registerService.queryRegisterValue(RegisterKeyValueEnum.STUDENT_MAX_WITHDRAW_MONEY, BigDecimal::new);
        if (money == null || money.compareTo(maxWithdrawMoney) > 0 ||
            money.compareTo(minWithdrawMoney) < 0) {
            throw new CmsException(ApiResultEnum.WITHDRAW_LIMIT, String.format("单次提现不可低于%s元，不能超过%s元", minWithdrawMoney.toString(), maxWithdrawMoney.toString()));
        }
        // 检查每天提现次数是否超过限制
        Integer everyDayMaxCount = registerService.queryRegisterValue(RegisterKeyValueEnum.STUDENT_EVERY_DAY_MAX_WITHDRAW_COUNT, Integer::valueOf);
        if (studentWithdrawAuditingDao.countToday(loginUserId) >= everyDayMaxCount) {
            throw new CmsException(ApiResultEnum.WITHDRAW_LIMIT, String.format("每天提现不能超过%s次", everyDayMaxCount));
        }
        StudentWithdrawAuditingBean build = StudentWithdrawAuditingBean.builder().withdrawCash(money)
            .status(CommonEnums.WithdrawStatus.APPLY.getCode())
            .openId(openId)
            .withdrawType(withdrawType)
            .studentId(loginUserId).build();
        boolean insertResult = insert(build);
        studentAccountService.accountChanges(loginUserId,money,StudentAccountChangesEnum.APPLY_WITHDRAW_CASH,
            StudentAccountDetailBean.builder().withdrawId(build.getId()).build());
        return insertResult;
    }

    @Override
    public boolean cancelStudentWithdrawAuditing(String applyId, String loginUserId) {
        if (StringUtils.isBlank(applyId)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "申请提现id不能为空");
        }
        StudentWithdrawAuditingBean withdrawAuditing = studentWithdrawAuditingDao.findOne(
            StudentWithdrawAuditingBean.builder().id(applyId).studentId(loginUserId).status(CommonEnums.WithdrawStatus.APPLY.getCode()).build());
        if (withdrawAuditing == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "提现申请记录不存在或者已经处理");
        }
        studentAccountService.accountChanges(withdrawAuditing.getStudentId(),
            withdrawAuditing.getWithdrawCash(),
            StudentAccountChangesEnum.CANCEL_WITHDRAW_CASH, StudentAccountDetailBean.builder()
                .withdrawId(applyId)
                .remark("主动取消提现申请")
                .build());
        uPushApiService.sendUnicast(withdrawAuditing.getStudentId(),
            UPushCustomDefined.builder()
                .uPushType(UPushTypeEnum.APPLY_WITHDRAY_FAILED)
                .money(withdrawAuditing.getWithdrawCash())
                .withdrawId(applyId)
                .remark("主动取消提现申请")
                .build());
        return studentWithdrawAuditingDao.update(applyId, StudentWithdrawAuditingBean.builder()
            .status(CommonEnums.WithdrawStatus.CANCEL.getCode())
            .remark("主动取消提现申请")
            .build()) == 1;
    }
}
