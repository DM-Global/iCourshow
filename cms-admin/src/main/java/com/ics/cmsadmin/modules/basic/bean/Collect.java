package com.ics.cmsadmin.modules.basic.bean;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import lombok.Data;

@Data
public class Collect {
    private String userId;

    private LevelEnum levelName;

    private String levelId;

    public Collect(String userId, LevelEnum levelName, String levelId) {
        this.userId = userId;
        this.levelName = levelName;
        this.levelId = levelId;
    }

    public Collect() {
    }
}
