package com.ics.cmsadmin.modules.basic.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UPushTypeBean {
    private String title;
    private String content;
    private String details;
}
