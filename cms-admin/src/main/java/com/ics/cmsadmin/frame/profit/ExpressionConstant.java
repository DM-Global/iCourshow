package com.ics.cmsadmin.frame.profit;

import java.util.regex.Pattern;

public final class ExpressionConstant {
    // 非捕捉匹配正实数
    public static final String number = "(?:[1-9]\\d*|0)(?:\\.\\d+)?";
    // 捕捉匹配正实数
    private static final String realNumber = "(" + number + ")";
    // 捕捉匹配百分比
    public static final String rateNumber = "((?:(?:[1-9]\\d?|0)(?:\\.\\d{1,2})?)|(?:100(?:\\.0{1,2})?))" + "%";

    public static final Pattern NUMBER_PATTERN = Pattern.compile(number);

    public static final String GRADIENT_RATE_EXPRESSION_EXCLUDE_RIGHT = "^" + rateNumber + "(<" + realNumber + "<" + rateNumber + ")*$";
    public static final String GRADIENT_RATE_EXPRESSION_INCLUDE_RIGHT = "^" + rateNumber + "(<=" + realNumber + "<=" + rateNumber + ")*$";

    public static void main(String[] args) {
        System.out.println(GRADIENT_RATE_EXPRESSION_INCLUDE_RIGHT);
    }
}
