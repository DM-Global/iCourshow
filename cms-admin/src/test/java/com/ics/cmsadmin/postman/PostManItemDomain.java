package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Builder
@Data
public class PostManItemDomain {
    private String name;
    private PostManRequest request;
    private List<String> response = new ArrayList<>();
}
