package com.ics.cmsadmin.modules.auth.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.auth.bean.SysAuthorize;
import com.ics.cmsadmin.modules.auth.bean.SysControllerAuthorize;

import java.util.List;
import java.util.Map;

/**
 * t_authorize_info 服务类
 * Created by lvsw on 2017-10-06.
 */
public interface AuthorizeService {
    /**
     * 查询用户所有的权限码
     *
     * @param userId 用户id
     * @return
     */
    List<String> listAuthorizesByUserId(String userId);

    /**
     * 根据用户id获取用户所有的权限详细信息
     *
     * @param userId 用户id
     * @return 权限详细信息
     */
    List<SysAuthorize> listUserAuthorizesByUserId(String userId);

    /**
     * 批量增加控制层方法相关权限
     *
     * @param list
     */
    void batchAddControllerAuthorize(List<SysControllerAuthorize> list);

    /**
     * 批量增加所有的权限
     *
     * @param list
     */
    void batchAddAuthorizeInfo(List<SysAuthorize> list);

    /**
     * 根据角色获取权限信息
     *
     * @param roleId 角色id
     * @return 权限
     */
    List<SysAuthorize> listAuthorizeByRoleId(String roleId);

    /**
     * 根据角色获取不属于它的权限信息
     *
     * @param roleId 角色id
     * @return 权限
     */
    List<SysAuthorize> listNotBelong2RoleAuthorizes(String roleId);

    /**
     * 根据菜单id获取权限码
     *
     * @param menuId 菜单id
     * @return 返回该菜单id页面所拥有的权限
     */
    List<SysAuthorize> listAuthorizeByMenuId(String menuId);

    /**
     * 根据查询条件查询列表并汇总
     */
    PageResult list(SysAuthorize t, PageBean page);

    /**
     * 根据菜单id查询角色信息
     */
    Map<String, List<SysAuthorize>> listByMenuIds(List<String> menuIds);
}
