package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.api.WechatApi;
import com.ics.cmsadmin.frame.api.WechatApiResponse;
import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.OrderStatusEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.property.AppConfig;
import com.ics.cmsadmin.frame.utils.KeyUtil;
import com.ics.cmsadmin.modules.agent.bean.AgentInfoBean;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.service.UPushApiService;
import com.ics.cmsadmin.modules.basic.utils.UPushCustomDefined;
import com.ics.cmsadmin.modules.basic.utils.UPushTypeEnum;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.service.StudentService;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.ics.cmsadmin.modules.agent.steward.AgentServices.agentInfoService;
import static com.ics.cmsadmin.modules.basic.steward.BasicServices.orderService;
import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentDao;

/**
 * t_student
 * Created bylvsw on 2018-54-22 19:09:15.
 */
@Service("newStudentService")
public class StudentServiceImpl implements StudentService {
    @Resource
    private AppConfig appConfig;
    @Resource
    private UPushApiService uPushApiService;
    @Resource
    private RegisterService registerService;
    @Override
    public StudentBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return studentDao.queryById(id);
    }

    @Override
    public PageResult list(StudentBean bean, PageBean page) {
        long count = studentDao.count(bean);
        if (count == 0) {
            return new PageResult();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, studentDao.list(bean, page));
    }

    @Override
    public PageResult listByLoginUserId(StudentBean bean, String loginUserId, PageBean page) {
        return listByLoginUserId(bean, loginUserId, page, studentDao);
    }

    @Override
    public boolean insert(StudentBean bean) {
        if (bean == null) {
            return false;
        }
        bean.setId(KeyUtil.genUniqueKey());
        AgentInfoBean agentInfoBean = agentInfoService.queryById(bean.getAgentNo());
        if (agentInfoBean == null) {
            bean.setAgentNo("0");
        } else {
            bean.setSalesman(agentInfoBean.getSalesman());
        }
        // 如果父级id为null或者父级id在数据库中不存在,则重置父级id为null
        if (StringUtils.isBlank(bean.getParentId()) || queryById(bean.getParentId()) == null) {
            bean.setParentId(null);
        }
        BigDecimal shareRate = registerService.queryRegisterValue(RegisterKeyValueEnum.PARENT_STUDENT_DEFAULT_SHARE_RATE, BigDecimal::new);
        bean.setShareRate(shareRate);
        // 如果父级id不为空,则为父级新增积分
        // todo4lvsw 学生积分展示不需要了
//        if (StringUtils.isNotBlank(bean.getParentId())) {
//            studentScoreService.scoreChanges(bean.getParentId(),
//                registerService.queryRegisterValue(RegisterKeyValueEnum.PROMOTION_STUDENT_SCORE, Long::valueOf),
//                StudentScoreChangesEnum.PROMOTION_STUDENT, bean.getId());
//        }
        return studentDao.insert(bean) == 1;
    }

    @Override
    public boolean update(String id, StudentBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return studentDao.update(id, bean) == 1;
    }

    @Override
    public Object updateVip(String id, StudentBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        PageResult pageOrder = orderService.list(OrderBean.builder().userId(bean.getId()).build(), new PageBean(OrderStatusEnum.WAIT.getCode(), Integer.MAX_VALUE));
        List<OrderBean> orderList = pageOrder.getDataList();
        if (orderList != null && !orderList.isEmpty()) {
            if (!bean.getSpecialVip()) {
                ArrayList<String> removeOrderIds = new ArrayList<>();
                for (OrderBean order : orderList) {
                    if (order.getOrderAmount().compareTo(BigDecimal.ONE) == 0) {
                        removeOrderIds.add(order.getId());
                    }
                }
                if (removeOrderIds.size() > 0) {
                    orderService.batchSoftRemove(removeOrderIds);
                }
            } else {
                ArrayList<String> removeOrderIds = new ArrayList<>();
                for (OrderBean order : orderList) {
                    if (order.getStatus() != 2) {
                        removeOrderIds.add(order.getId());
                    }
                }
                if (removeOrderIds.size() > 0) {
                    orderService.batchSoftRemove(removeOrderIds);
                }
            }
        }
        return studentDao.update(id, bean) == 1;
    }

    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return studentDao.delete(id) == 1;
    }

    @Override
    public StudentBean refreshStudentInfoByWechat(StudentBean student, boolean isAppLogin) {
        // 如果openid 和appOpenid 同时为空的就返回null
        if (student == null) {
            return null;
        }
        // 微信登陆必须的unionId,openId,appOpenId都为空的话,直接返回null
        if (StringUtils.isBlank(student.getUnionId()) &&
            StringUtils.isBlank(student.getOpenId()) &&
            StringUtils.isBlank(student.getAppOpenId())
        ){
            return null;
        }
        StudentBean existStudent = null;
        // 1. 通过unionId获取用户
        if (StringUtils.isNotBlank(student.getUnionId())) {
            existStudent = studentDao.findOne(StudentBean.builder().unionId(student.getUnionId()).build());
        }
        // 2. 通过openid获取用户(公众号登陆)
        if (existStudent == null && StringUtils.isNotBlank(student.getOpenId())) {
            existStudent = studentDao.findOne(StudentBean.builder().openId(student.getOpenId()).build());
        }
        // 3. 通过appOpenId获取用户(app登陆)
        if (existStudent == null && StringUtils.isNotBlank(student.getAppOpenId())) {
            existStudent = studentDao.findOne(StudentBean.builder().appOpenId(student.getAppOpenId()).build());
        }
        if (existStudent == null) {
            if (StringUtils.isNotBlank(student.getParentId())) {
                uPushApiService.sendUnicast(student.getParentId(),
                    UPushCustomDefined.builder()
                        .uPushType(UPushTypeEnum.NEW_SUBORDINATE)
                        .studentName(student.getNickname())
                        .build());
            }
            insert(student);
            return student;
        } else {
            if (isAppLogin) {
                student.setAvatar(null);
                student.setGender(null);
                student.setNickname(null);
            }
            update(existStudent.getId(), student);
            return queryById(existStudent.getId());
        }
    }

    @Override
    public StudentBean findByMobilePhoneAndPassword(String mobilePhone, String password) {
        if (StringUtils.isBlank(mobilePhone) || StringUtils.isBlank(password)) {
            return null;
        }
        return studentDao.findOne(StudentBean.builder().phone(mobilePhone).password(password).build());
    }

    @Override
    public boolean bindPassword(String userId, String password) {
        if (StringUtils.isBlank(userId) ||
            StringUtils.isBlank(password)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        StudentBean studentBean = studentDao.queryById(userId);
        if (StringUtils.isNotBlank(studentBean.getPassword())) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "原始密码不为空,不能绑定");
        }
        return studentDao.update(userId, StudentBean.builder().password(password).build()) > 0;
    }

    @Override
    public boolean forgotPassword(String phone, String password) {
        if (StringUtils.isBlank(phone) ||
            StringUtils.isBlank(password)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        StudentBean studentBean = studentDao.findOne(StudentBean.builder().phone(phone).build());
        if (studentBean == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该手机号码还没有注册过");
        }
        return studentDao.update(studentBean.getId(), StudentBean.builder().password(password).build()) > 0;
    }

    @Override
    public boolean modifyPassword(String userId, String oldPassword, String newPassword) {
        if (StringUtils.isBlank(userId) ||
            StringUtils.isBlank(oldPassword) ||
            StringUtils.isBlank(newPassword)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        StudentBean student = studentDao.findOne(StudentBean.builder().id(userId).password(oldPassword).build());
        if (student == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "旧密码有误,修改失败");
        }
        return studentDao.update(userId, StudentBean.builder().password(newPassword).build()) > 0;
    }

    @Override
    public boolean bindPhone(String userId, String phoneCode, String phone) {
        if (StringUtils.isBlank(userId) ||
            StringUtils.isBlank(phone)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        StudentBean studentBean = studentDao.queryById(userId);
        if (studentBean == null || StringUtils.isNotBlank(studentBean.getPhone())) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该用户已经绑定手机号码");
        }
        if (studentDao.findOne(StudentBean.builder().phone(phone).build()) != null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该手机号已经存在账号，请使用其他手机号");
        }
        return studentDao.update(userId, StudentBean.builder().phoneCode(phoneCode).phone(phone).build()) > 0;
    }

    @Override
    public boolean modifyPhoneByPassword(String userId, String password, String phone) {
        if (StringUtils.isBlank(userId) ||
            StringUtils.isBlank(password) ||
            StringUtils.isBlank(phone)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        StudentBean student = studentDao.findOne(StudentBean.builder()
            .id(userId)
            .password(password)
            .build());
        if (student == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "旧密码有误,修改失败");
        }
        if (studentDao.findOne(StudentBean.builder().phone(phone).build()) != null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该手机号已经存在账号，请使用其他手机号");
        }
        return studentDao.update(userId, StudentBean.builder().phone(phone).build()) > 0;
    }

    @Override
    public boolean modifyPhoneBySmsCode(String userId, String oldPhone, String newPhoneCode, String newPhone) {
        if (StringUtils.isBlank(userId) ||
            StringUtils.isBlank(oldPhone) ||
            StringUtils.isBlank(newPhone)) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        if (studentDao.findOne(StudentBean.builder().id(userId).phone(oldPhone).build()) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "旧手机号码有误");
        }
        if (studentDao.findOne(StudentBean.builder().phone(newPhone).build()) != null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "新手机号已经存在账号，请使用其他手机号");
        }
        return studentDao.update(userId, StudentBean.builder().phoneCode(newPhoneCode).phone(newPhone).build()) > 0;
    }

    @Override
    public boolean bindWechat(String loginUserId, String code) {
        if (StringUtils.isNotBlank(loginUserId)){
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        StudentBean studentBean = studentDao.queryById(loginUserId);
        if (studentBean == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        if (StringUtils.isNotBlank(studentBean.getUnionId()) || StringUtils.isNotBlank(studentBean.getAppOpenId()) || StringUtils.isNotBlank(studentBean.getOpenId())) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该账号已经绑定微信了,不能重复绑定了");
        }
        WechatApiResponse accessTokenByCode = WechatApi.getAccessTokenByCode(appConfig.getWechatAppId(), appConfig.getWechatAppSecret(), code);
        if (accessTokenByCode == null || accessTokenByCode.getErrcode() != 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "微信绑定失败");
        }
        WechatApiResponse userinfo = WechatApi.getUserinfo(accessTokenByCode.getAccessToken(), accessTokenByCode.getOpenid());
        if (userinfo == null || userinfo.getErrcode() != 0 || StringUtils.isBlank(userinfo.getUnionid())) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "微信绑定失败");
        }
        StudentBean one = studentDao.findOne(StudentBean.builder().unionId(userinfo.getUnionid()).build());
        if (one != null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "该微信账号已注册,不能被绑定");
        }
        StudentBean student = StudentBean.builder()
            .registerSource(Constants.REGISTER_SOURCE_APP_WECHAT)
            .appOpenId(userinfo.getOpenid())
            .unionId(userinfo.getUnionid())
            .nickname(userinfo.getNickname())
            .isFollowed(userinfo.getSubscribe() > 0)
            .avatar(userinfo.getHeadimgurl())
            .gender(userinfo.getSex() + "")
            .build();
        return this.update(loginUserId, student);
    }

    @Override
    public StudentBean refreshStudentInfoByTourist(StudentBean student) {
        if (student == null || StringUtils.isBlank(student.getDeviceId())) {
            return null;
        }
        StudentBean oldStudent = studentDao.findOne(StudentBean.builder().deviceId(student.getDeviceId()).build());
        if (oldStudent != null) {
            return oldStudent;
        }
        student.setId(KeyUtil.genUniqueKey());
        studentDao.insert(student);
        return student;
    }

    @Override
    public List<StudentBean> queryByIds(List<String> studentIds) {
        if (CollectionUtils.isEmpty(studentIds)) {
            return null;
        }
        return studentDao.queryByIds(studentIds);
    }
}
