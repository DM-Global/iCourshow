package com.ics.cmsadmin.auth;

import com.ics.cmsadmin.frame.api.PolyvApi;
import com.ics.cmsadmin.frame.utils.GsonUtils;
import com.ics.cmsadmin.modules.basic.bean.SharePosterBean;
import com.ics.cmsadmin.modules.basic.bean.VideoHistoryBean;
import com.ics.cmsadmin.modules.basic.service.VideoHistoryService;
import com.ics.cmsadmin.modules.student.bean.StudentCertificateBean;
import com.ics.cmsadmin.modules.system.emums.SequenceNumberEnum;
import com.ics.cmsadmin.modules.agent.service.AgentInfoService;
import com.ics.cmsadmin.modules.basic.dao.AnswersDao;
import com.ics.cmsadmin.modules.basic.bean.AnswersBean;
import com.ics.cmsadmin.modules.basic.service.PayService;
import com.ics.cmsadmin.modules.system.service.SequenceNumberService;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.platform.commons.util.StringUtils;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.profiles.active=lvsw", "app.init-authorize=false"})
public class TestAnswers {

    @Resource
    private AnswersDao answersDao;
    @Resource
    private SequenceNumberService sequenceNumberService;
    @Resource
    private AgentInfoService agentInfoService;
    @Resource
    private PayService payService;

    @Resource
    private VideoHistoryService videoHistoryService;

    @Test
    public void test1111() throws IOException {
        PolyvApi.getVideoMsg("30fe77e88f6de5ae918dc1835bedbee0_3");

    }

    @Test
    public void test211() throws IOException {
        File file = new File("d://test211.txt");
        FileUtils.write(file, "", "utf8", false);
        long duration = 354000L;
        long position = 0L;
        long tatalTime = 180000L;
        for (int i = 0; i <= 60; i++) {
            position = Math.min(position + tatalTime, duration);
            StudentCertificateBean studentCertificateBean = videoHistoryService.save4app(VideoHistoryBean.builder()
                .userId("012f670d7b4540e4a3a87072075797a3")
                .videoId("185")
                .duration(duration)
                .position(position)
                .totalTime(tatalTime)
                .build());
            FileUtils.write(file, String.format("%d : %s\n", i, GsonUtils.toJson(studentCertificateBean)), "utf8", true);
        }
    }

    @Test
    public void test() {
        AnswersBean ss = AnswersBean.builder().problemId("1").content("ss").correct(true).build();
        System.out.println("=====================");
        answersDao.batchInsert(Arrays.asList(ss), "1");
    }

    @Test
    public void test1() {
        String s = sequenceNumberService.newSequenceNumber(SequenceNumberEnum.AGENT);
        System.out.println("==========> " + s);
    }

    @Test
    public void disableAgent() {
        agentInfoService.disable("A1004");
    }

    @Test
    public void enableAgent() {
        agentInfoService.enable("A1004");
    }

    @Test
    public void notify12() {
        payService.orderNotify(null);
    }

}
