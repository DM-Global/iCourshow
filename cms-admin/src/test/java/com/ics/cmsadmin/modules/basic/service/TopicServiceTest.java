package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.CmsAdminApplicationTests;
import com.ics.cmsadmin.modules.basic.bean.TopicBean;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class TopicServiceTest extends CmsAdminApplicationTests {

    @Autowired
    private TopicService topicService;

    @Test
    public void listDetail() {

        List<TopicBean> topicBeans = topicService.listDetail("162621537318598383", "13");
        log.info("topicList = {}", topicBeans);
        Assert.assertNotEquals(topicBeans.size(), 0);
    }
}
