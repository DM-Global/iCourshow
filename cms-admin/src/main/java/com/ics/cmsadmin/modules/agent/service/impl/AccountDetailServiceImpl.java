package com.ics.cmsadmin.modules.agent.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.agent.bean.AccountDetailBean;
import com.ics.cmsadmin.modules.agent.service.AccountDetailService;
import org.springframework.stereotype.Service;

import static com.ics.cmsadmin.modules.agent.steward.AgentRepositories.accountDetailDao;

/**
 * a_account_detail
 * Created bylvsw on 2018-37-29 16:09:14.
 */
@Service
public class AccountDetailServiceImpl implements AccountDetailService {

    @Override
    public PageResult<AccountDetailBean> listByLoginUserId(AccountDetailBean bean, String loginUserId, PageBean page) {
        return listByLoginUserId(bean, loginUserId, page, accountDetailDao);
    }
}
