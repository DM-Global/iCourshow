package com.ics.cmsadmin.modules.auth.service;


import com.ics.cmsadmin.frame.core.bean.FourTupleBean;

import java.util.List;

/**
 * Created by lvsw on 2017/10/15.
 */
public interface RoleAuthorizeService {
    /**
     * 批量将权限加到角色中
     *
     * @param authCodes 权限码
     * @param roleId    角色id
     * @param creator   创建人
     * @return
     */
    boolean addAuthorizes2Role(List<String> authCodes, String roleId, String creator);

    /**
     * 批量将权限码从角色中删除
     *
     * @param authCodes 权限码
     * @param roleId    角色id
     * @return
     */
    boolean removeAuthorizes2Role(List<String> authCodes, String roleId);

    /**
     * 根据菜单id查询属于角色id的权限码
     *
     * @param roleId 角色id
     * @param menuId 菜单id
     * @return 根据菜单id查询属于角色id的权限码
     */
    List<String> listRoleMenuAuthorize(String roleId, String menuId);

    /**
     * 查询属于角色的菜单id
     *
     * @param roleId 角色id
     * @param menuId 菜单id
     * @return
     */
    List<String> listRoleMenu(String roleId, String menuId);

    /**
     * 更新角色的权限
     *
     * @param roleId        角色
     * @param menuAuthorize 权限信息
     *                      <ul>
     *                      <li>
     *                      one: 角色新增的菜单
     *                      </li>
     *                      <li>
     *                      two: 角色删除的菜单
     *                      </li>
     *                      <li>
     *                      three: 角色新增的菜单权限
     *                      </li>
     *                      <li>
     *                      four: 角色删除的菜单权限
     *                      </li>
     *                      </ul>
     * @param loginUserId
     * @return
     */
    boolean updateRoleMenuAuthorize(String roleId, FourTupleBean<List<String>, List<String>, List<String>, List<String>> menuAuthorize, String loginUserId);
}
