package com.ics.cmsadmin.frame.api;

import com.ics.cmsadmin.frame.utils.GsonUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 * 阿里巴巴开放api
 */
@Log4j2
public class AliBabaApi {
    private static final String validateAndCacheCardInfoUrl = "https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?_input_charset=utf-8&cardNo=%s&cardBinCheck=true";

    /**
     * 校验银行卡是否正确
     *
     * @param accountNo
     * @return
     */
    public static final CcdcapiResponse validateAndCacheCardInfo(String accountNo) {
        try {
            if (!luhnVaild(accountNo)) {
                return null;
            }
            String body = Jsoup.connect(String.format(validateAndCacheCardInfoUrl, accountNo))
                .method(Connection.Method.GET)
                .ignoreContentType(true)
                .execute()
                .body();
            log.info("阿里云校验银行卡 {}, 返回数据为 {}", accountNo, body);
            return GsonUtils.fromJson2Bean(body, CcdcapiResponse.class);
        } catch (Exception e) {
            return null;
        }

    }

    /*
    当你输入信用卡号码的时候，有没有担心输错了而造成损失呢？其实可以不必这么担心，因为并不是一个随便的信用卡号码都是合法的，它必须通过Luhn算法来验证通过。
    该校验的过程：
    1、从卡号最后一位数字开始，逆向将奇数位(1、3、5等等)相加。
    2、从卡号最后一位数字开始，逆向将偶数位数字，先乘以2（如果乘积为两位数，则将其减去9），再求和。
    3、将奇数位总和加上偶数位总和，结果应该可以被10整除。
     */
    public static final boolean luhnVaild(String accountNo) {
        if (StringUtils.isBlank(accountNo) || !accountNo.matches("^\\d{15,20}$")) {
            return false;
        }
        char[] chars = StringUtils.reverse(accountNo).toCharArray();
        int sum = 0;
        int oddSum = 0;
        int evenSum = 0;
        for (int i = 0; i < chars.length; i++) {
            if (i % 2 == 0) {
                oddSum += chars[i] - '0';
                int odd = (chars[i] - '0') * 2;
                sum += odd >= 10 ? odd - 9 : odd;
            } else {
                int temp = (chars[i] - '0') * 2;
                evenSum += temp >= 10 ? temp - 9 : temp;
            }
        }
        return (oddSum + evenSum) % 10 == 0;
    }
}
