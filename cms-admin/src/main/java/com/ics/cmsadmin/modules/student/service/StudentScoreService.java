package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.modules.student.bean.StudentScoreBean;
import com.ics.cmsadmin.modules.student.enums.StudentScoreChangesEnum;

/**
 * todo4lvsw 学生积分暂时不需要了
 */
public interface StudentScoreService {
    /**
     * 查询学生积分信息
     */
    StudentScoreBean queryByStudentId(String studentId);

    /**
     * 积分变动
     *
     * @param studentId            变动学生
     * @param score              变动积分
     * @param studentScoreChangesEnum 变动类型
     */
    boolean scoreChanges(String studentId, long score, StudentScoreChangesEnum studentScoreChangesEnum);

    /**
     * 积分变动
     *
     * @param studentId            变动学生
     * @param score              变动积分
     * @param studentScoreChangesEnum 变动类型
     * @param remark 备注
     */
    boolean scoreChanges(String studentId, long score, StudentScoreChangesEnum studentScoreChangesEnum, String remark);

    /**
     * @param bean
     * @return
     */
    boolean insert(StudentScoreBean bean);
}
