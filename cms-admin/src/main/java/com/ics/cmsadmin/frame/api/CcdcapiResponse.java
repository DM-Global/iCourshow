package com.ics.cmsadmin.frame.api;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.List;

@Data
public class CcdcapiResponse {

    private String bank;
    private String cardType;
    private String key;
    private String stat;
    private boolean validated;
    private List<Messages> messages;

    @Data
    public static class Messages {
        private String errorCodes;
        private String name;
    }
}
