package com.ics.cmsadmin.modules.student.bean;

                                        
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.math.BigDecimal;
import java.util.Date;


/**
* t_student_share_detail
* 学生分润明细
*
* Created by lvsw on 2019-02-12 11:03:21.
*/
@Data
@Builder
public class StudentShareDetailBean {

    private String id;              // 流水id
    private String studentId;              // 收益学生编号
    private String shareType;              // 分润类型
    private BigDecimal shareMoney;              // 分润金额
    private BigDecimal orderMoney;              // 订单金额
    private BigDecimal shareRate;              // 分润比例
    private String orderId;              // 分润订单id
    private String orderStudentId;              // 交易订单的学生id
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;              // 创建时间

    private String startCreateDate;
    private String endCreateDate;
    @Tolerate
    public StudentShareDetailBean () {}
}
