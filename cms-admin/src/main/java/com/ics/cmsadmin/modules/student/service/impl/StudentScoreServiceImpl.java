package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.bean.StudentScoreBean;
import com.ics.cmsadmin.modules.student.enums.StudentScoreChangesEnum;
import com.ics.cmsadmin.modules.student.service.StudentScoreService;
import com.ics.cmsadmin.modules.system.emums.SequenceNumberEnum;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentScoreDao;
import static com.ics.cmsadmin.modules.student.steward.StudentServices.studentService;

/**
 * todo4lvsw 学生积分暂时不需要了
 */
@Service
public class StudentScoreServiceImpl implements StudentScoreService {
    @Override
    public StudentScoreBean queryByStudentId(String studentId) {
        if (StringUtils.isBlank(studentId)) {
            return null;
        }
        return studentScoreDao.findOne(StudentScoreBean.builder().studentId(studentId).build());
    }

    @Override
    public boolean scoreChanges(String studentId, long score, StudentScoreChangesEnum studentScoreChangesEnum) {
        return this.scoreChanges(studentId, score, studentScoreChangesEnum, "");
    }

    @Override
    public boolean scoreChanges(String studentId, long score, StudentScoreChangesEnum studentScoreChangesEnum, String remark) {
        if (StringUtils.isBlank(studentId) || studentScoreChangesEnum == null) {
            return false;
        }
        if (score < 0) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "积分变动不能为负数");
        }
        synchronized (studentId.intern()) {
            StudentBean studentBean = studentService.queryById(studentId);
            if (studentBean == null) {
                throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "不存在该学生信息");
            }
            StudentScoreBean beforeScore = queryByStudentId(studentId);
            if (beforeScore == null) {
                beforeScore = StudentScoreBean.builder().studentId(studentId).build();
                insert(beforeScore);
            }
            switch (studentScoreChangesEnum) {
                case PROMOTION_STUDENT:
                    promotionStudent(beforeScore, score);
                    break;
                default:
                    // never go here
                    break;
            }
            StudentScoreBean afterScore = studentScoreDao.queryById(beforeScore.getScoreId());
            // 插入前后流水记录
            studentScoreDao.insertScoreDetail(afterScore, score, studentScoreChangesEnum, remark);
        }
        return true;
    }

    private void promotionStudent(StudentScoreBean beforeScore, long score) {
        studentScoreDao.promotionStudent(beforeScore.getScoreId(), score);
    }

    @Override
    public boolean insert(StudentScoreBean bean) {
        if (bean == null) {
            return false;
        }
        bean.setScoreId(SystemServices.sequenceNumberService.newSequenceNumber(SequenceNumberEnum.STUDENT_SCORE));
        return studentScoreDao.insert(bean) == 1;
    }

}
