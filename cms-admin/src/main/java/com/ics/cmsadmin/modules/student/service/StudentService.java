package com.ics.cmsadmin.modules.student.service;

import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.student.bean.StudentBean;

import java.util.List;


/**
 * t_student 服务类
 * Created by lvsw on 2018-51-22 19:09:31.
 */
public interface StudentService extends BaseDataService<StudentBean>,
    LoginBaseDataService<StudentBean>, BaseService<StudentBean> {
    /**
     * 更新用户信息
     *
     * @param student 从微信获取到的用户
     */
    StudentBean refreshStudentInfoByWechat(StudentBean student, boolean isAppLogin);

    Object updateVip(String id, StudentBean student);

    StudentBean findByMobilePhoneAndPassword(String mobilePhone, String password);

    /**
     * 绑定密码
     *
     * @param password
     * @return
     */
    boolean bindPassword(String userId, String password);

    /**
     * 绑定手机号
     *
     * @param userId    用户id
     * @param phoneCode 手机号区域码
     * @param phone     手机号
     * @return
     */
    boolean bindPhone(String userId, String phoneCode, String phone);

    /**
     * 忘记密码/重置密码
     *
     * @param phone    用户手机号
     * @param password
     * @return
     */
    boolean forgotPassword(String phone, String password);

    /**
     * 修改密码
     *
     * @param userId
     * @param oldPassword
     * @param newPassword
     * @return
     */
    boolean modifyPassword(String userId, String oldPassword, String newPassword);

    /**
     * 通过密码修改手机号
     *
     * @param userId   用户id
     * @param password 用户密码
     * @param phone    手机号
     * @return
     */
    boolean modifyPhoneByPassword(String userId, String password, String phone);

    /**
     * 通过短信验证修改手机号码
     *
     * @param userId       用户id
     * @param oldPhone     旧手机号码
     * @param newPhoneCode 旧手机号码区域码
     * @param newPhone     新手机号码
     * @return
     */
    boolean modifyPhoneBySmsCode(String userId, String oldPhone, String newPhoneCode, String newPhone);

    /**
     * 绑定微信
     * @param loginUserId
     * @param code
     * @return
     */
    boolean bindWechat(String loginUserId, String code);

    /**
     * 游客登陆
     * @param student
     * @return
     */
    StudentBean refreshStudentInfoByTourist(StudentBean student);

    List<StudentBean> queryByIds(List<String> studentIds);
}
