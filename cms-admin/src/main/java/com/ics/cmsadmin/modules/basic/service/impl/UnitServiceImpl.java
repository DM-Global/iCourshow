package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.CourseBean;
import com.ics.cmsadmin.modules.basic.dao.UnitDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.bean.*;
import com.ics.cmsadmin.modules.basic.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lvsw on 2018-09-01.
 */
@Service
public class UnitServiceImpl implements UnitService {
    @Resource
    private UnitDao unitDao;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TopicService topicService;

    @CacheDbMember(returnClass = UnitBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public UnitBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return unitDao.queryById(id);
    }

    @CacheDbMember(returnClass = UnitBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public PageResult<UnitBean> list(UnitBean bean, PageBean page) {
        long count = unitDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, unitDao.list(bean, page));
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean insert(UnitBean bean) {
        if (bean == null) {
            return false;
        }
        return unitDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean update(String id, UnitBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return unitDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return unitDao.delete(id) == 1;
    }

    @CacheDbMember(returnClass = UnitBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public List<UnitBean> listAllUnitByCourseId(String courseId) {
        return unitDao.list(UnitBean.builder().courseId(courseId).isActive(true).build(),
            new PageBean(1, Integer.MAX_VALUE));
    }

    @CacheDbMember(returnClass = UnitBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public List<UnitBean> listAllUnitDetailByCourseId(String userId, String courseId) {
        List<UnitBean> unitList = unitDao.list(UnitBean.builder().courseId(courseId).isActive(true).build(),
            new PageBean(1, Integer.MAX_VALUE));
        unitList.forEach(unit -> unit.setCollected(collectionService.select(new Collect(userId, LevelEnum.unit, unit.getId()))));
        return unitList;
    }

    @Override
    public UnitBean unitDetailAdditional(String userId, UnitBean unit) {
        if (unit == null || StringUtils.isBlank(unit.getId()) || StringUtils.isBlank(unit.getCourseId())) {
            return unit;
        }
        CourseBean courseBean = courseService.courseDetailAdditional(userId, CourseBean.builder().id(unit.getCourseId()).build());
        unit.setBoughtNumber(courseBean.getBoughtNumber());
        unit.setHasBought(courseBean.getHasBought());
        unit.setCollected(collectionService.select(new Collect(userId, LevelEnum.unit, unit.getId())));
        unit.setLiked(likeService.select(new Like(userId, LevelEnum.unit, unit.getId())));
        return null;
    }

    @CacheDbMember(returnClass = UnitBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public UnitBean unitDetail(String id) {
        UnitBean unitBean = unitDao.queryById(id);
        unitBean.setCollectCount(collectionService.queryTotal(LevelEnum.unit, id));
        unitBean.setThumbUpCount(likeService.queryTotal(LevelEnum.unit, id));
        unitBean.setTeacher(teacherService.queryById(unitBean.getTeacherId()));
        unitBean.setTopicCount(topicService.count(TopicBean.builder().unitId(id).build()));
        // TODO
        return unitBean;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public void updateProblemNum(String unitId, int testNum) {
        unitDao.updateProblemNum(unitId, testNum);
    }
}
