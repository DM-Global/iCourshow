package com.ics.cmsadmin.modules;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@ControllerAdvice
public class AppErrorController implements ErrorController {

    private static final String ERROR_PATH = "/error";

    @Authorize
    @RequestMapping(value = ERROR_PATH)
    public ApiResponse handleError(HttpServletRequest request, HttpServletResponse response) {
        return new ApiResponse(ApiResultEnum.NOT_FOUND);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

    @RequestMapping("/MP_verify_yxxMn7X0oZ9tybwn.txt")
    @ResponseBody
    public String test() {
        return "yxxMn7X0oZ9tybwn";
    }
}
