package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.modules.basic.bean.*;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.basic.dao.PointDao;
import com.ics.cmsadmin.frame.core.annotation.CacheDbMember;
import com.ics.cmsadmin.frame.core.enums.CacheGroupEnum;
import com.ics.cmsadmin.frame.core.annotation.ClearDbMember;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.basic.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by lvsw on 2018-09-01.
 */
@Service
public class PointServiceImpl implements PointService {
    @Resource
    private PointDao pointDao;

    @Autowired
    private TopicService topicService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private LikeService likeService;

    @Autowired
    private StudyHistoryService studyHistoryService;

    @CacheDbMember(returnClass = PointBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public PointBean queryById(String id) {
        if (StringUtils.isBlank(id)) {
            return null;
        }
        return pointDao.queryById(id);
    }

    @CacheDbMember(returnClass = PointBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public PageResult<PointBean> list(PointBean bean, PageBean page) {
        long count = pointDao.count(bean);
        if (count == 0) {
            return new PageResult<>();
        }
        page = page == null ? new PageBean() : page;
        return PageResult.getPage(count, pointDao.list(bean, page));
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean insert(PointBean bean) {
        if (bean == null) {
            return false;
        }
        return pointDao.insert(bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean update(String id, PointBean bean) {
        if (StringUtils.isBlank(id) || bean == null || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.UPDATE_TABLE_ITEM_FAIL, "参数有误");
        }
        return pointDao.update(id, bean) == 1;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public boolean delete(String id) {
        if (StringUtils.isBlank(id) || queryById(id) == null) {
            throw new CmsException(ApiResultEnum.DELETE_TABLE_ITEM_FAIL, "参数有误");
        }
        return pointDao.delete(id) == 1;
    }

    @CacheDbMember(returnClass = PointBean.class, group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public List<PointBean> listAllPointByTopicId(String topicId) {
        return pointDao.list(PointBean.builder().topicId(topicId).isActive(true).build(),
            new PageBean(1, Integer.MAX_VALUE));
    }

    @Override
    public List<PointBean> listAllPointDetailByTopicId(String userId, String topicId) {
        if (StringUtils.isBlank(topicId)) {
            return null;
        }
        List<PointBean> pointList = pointDao.list(PointBean.builder()
            .topicId(topicId)
            .isActive(true).build(), new PageBean(1, Integer.MAX_VALUE));
        Optional.ofNullable(pointList)
            .orElse(new ArrayList<>())
            .forEach(pointBean -> appendAttrs(userId, pointBean, pointBean.getId()));
        return pointList;
    }

    private void appendAttrs(String userId, PointBean pointBean, String id) {
        TopicBean topicBean = topicService.topicDetail(userId, pointBean.getTopicId());
        pointBean.setHasBought(topicBean != null && topicBean.getHasBought());
        pointBean.setCollectCount(collectionService.queryTotal(LevelEnum.point, id));
        pointBean.setThumbUpCount(likeService.queryTotal(LevelEnum.point, id));
        if (StringUtils.isBlank(userId)) {
            pointBean.setCollected(false);
            pointBean.setLiked(false);
            pointBean.setFinished(false);
        } else {
            pointBean.setCollected(collectionService.select(new Collect(userId, LevelEnum.point, id)));
            pointBean.setLiked(likeService.select(new Like(userId, LevelEnum.point, id)));
            pointBean.setFinished(studyHistoryService.findOne(StudyHistory.builder()
                .userId(userId)
                .levelName(LevelEnum.point)
                .levelId(id).progress(1).build()) != null);
        }
    }

    @Override
    public PointBean pointDetail(String userId, String id) {
        PointBean pointBean = queryById(id);
        if (pointBean == null) {
            return null;
        }
        appendAttrs(userId, pointBean, id);
        return pointBean;
    }

    @Override
    public long count(PointBean bean) {
        return pointDao.count(bean);
    }

    @Override
    public int finish(String userId, String id) {
        if (StringUtils.isBlank(userId)) {
            throw new CmsException(ApiResultEnum.UN_LOGIN);
        }
        PointBean pointBean = pointDao.queryById(id);
        if (pointBean == null) {
            throw new CmsException(ApiResultEnum.POINT_NOT_EXIST);
        }
        StudyHistory studyHistory = StudyHistory.builder().userId(userId).levelName(LevelEnum.point).levelId(id).progress(1).build();
        StudyHistory history = studyHistoryService.findOne(studyHistory);
        if (history == null) {
            return studyHistoryService.insert(studyHistory);
        }
        return 0;
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public void updateProblemNum(String pointId, int testNum) {
        pointDao.updateProblemNum(pointId, testNum);
    }

    @ClearDbMember(group = CacheGroupEnum.CACHE_COURSE)
    @Override
    public void updateVideoInfo(String pointId, String videoDuration, String coverSrc) {
        pointDao.updateVideoInfo(pointId, videoDuration, coverSrc);
    }
}
