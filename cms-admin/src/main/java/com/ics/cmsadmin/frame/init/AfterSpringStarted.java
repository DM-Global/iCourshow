package com.ics.cmsadmin.frame.init;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by pc on 2017/10/7.
 */
@Component
public class AfterSpringStarted {
    public static final String TASK_DONE = "DONE";
    @Resource
    private InitAuthorizeCallback initAuthorizeCallback;

    public void InitializationTask() {
        Map<String, Future<String>> mapFutre = new HashMap<>();
        ExecutorService pool = Executors.newFixedThreadPool(5);
        mapFutre.put("初始化权限码", pool.submit(initAuthorizeCallback));
        pool.submit(new InitTaskOverCallback(mapFutre, pool));
    }
}
