-- 2018年11月19日
ALTER TABLE `t_student`
ADD COLUMN `is_followed`  tinyint(1) NULL DEFAULT 0 COMMENT '公众号关注状态 1:关注,0:未关注' AFTER `special_vip`;

-- 2018年11月23日
ALTER TABLE `icourshow`.`t_order`
ADD COLUMN `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除：0.未删除，1.已删除' AFTER `update_time`;

COMMIT;
