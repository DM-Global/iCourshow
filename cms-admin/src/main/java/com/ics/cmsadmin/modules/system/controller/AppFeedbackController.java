package com.ics.cmsadmin.modules.system.controller;

import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.system.bean.AppFeedbackBean;
import com.ics.cmsadmin.modules.system.bean.AppVersionBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.ics.cmsadmin.modules.system.steward.SystemServices.appFeedbackService;
import static com.ics.cmsadmin.modules.system.steward.SystemServices.appVersionService;

/**
 * t_app_version controller
 * Created by lvsw on 2018-17-05 20:12:49.
 */
@Api(description = "app意见反馈")
@RestController
@RequestMapping("/appFeedBack")
public class AppFeedbackController {

    @Authorize(AuthorizeEnum.APP_FEEDBACK_QUERY)
    @ApiOperation(value = "根据id查询app意见反馈信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(appFeedbackService.queryById(id));
    }

    @Authorize(AuthorizeEnum.APP_FEEDBACK_QUERY)
    @ApiOperation(value = "分页查询app意见反馈信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody AppFeedbackBean appFeedbackBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(appFeedbackService.list(appFeedbackBean, pageBean));
    }

}
