package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * q_answers
 * 答案
 * Created by lvsw on 2018-09-06.
 */
@Data
@Builder
public class AnswersBean {

    // id
    private String answersId;
    // 内容
    private String content;
    // 正确性
    private boolean correct;
    private String isCorrect;
    // 问题id
    private String problemId;
    // 排序
    private String orderNo;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 更新时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public AnswersBean() {
    }
}
