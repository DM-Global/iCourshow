package com.ics.cmsadmin.postman;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class PostManRequestUrl {
    private String raw;
    private List<String> host;
    private String[] path;
    private List<PostManRequestQuery> query;
}
