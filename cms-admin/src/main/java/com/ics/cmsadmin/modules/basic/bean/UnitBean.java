package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

/**
 * t_unit
 * 单元
 * Created by lvsw on 2018-09-01.
 */
@Data
@Builder
public class UnitBean {

    // 单元id
    private String id;
    // 名字
    @NotEmpty(message = "单元名字不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "单元名字长度不能超过{max}个字符", max = 10, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    // 课程id
    @NotEmpty(message = "请选择课程id", groups = {InsertGroup.class, UpdateGroup.class})
    private String courseId;
    //课程名
    private String courseName;
    // 状态
    private Boolean isActive;
    // 描述
    @NotEmpty(message = "单元描述不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "单元描述长度不能超过{max}个字符", max = 200, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    // 排序
    private String rank;
    // 老师id
    private String teacherId;
    //教师名
    private String teacherName;
    //主题名
    private String subjectName;
    //主题 id
    private String subjectId;
    // 封面id
    private String coverId;
    // 封面址
    private String coverSrc;
    // 图标id
    private String iconId;
    // 图标
    private String iconSrc;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 更新时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    // 购买数
    private Integer boughtNumber;
    // 点赞数
    private Integer thumbUpCount;
    // 收藏数
    private Integer collectCount;
    // 教师信息
    private TeacherBean teacher;
    // 更新的主题数
    private long topicCount;

    /**
     * 用户登陆后，需考虑
     */
    // 课程是否已经购买
    private Boolean hasBought;
    // 课程是否已收藏
    private Boolean collected;
    // 是否点赞了
    private Boolean liked;

    private Date startTime;
    private Date endTime;

    private List<TopicBean> topicList;
    private boolean hasTestSet;
    private int testNum;

    @Tolerate
    public UnitBean() {
    }
}
