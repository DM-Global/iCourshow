package com.kco.generate.java;

import com.kco.generate.config.TableBean;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kco.generate.config.Utils.saveFile;

/**
 * 创建bean java文件
 * Created by 666666 on 2017/9/26.
 */
public final class CreateDaoJava {

    public static void buildFile(TableBean tableBean) {
        String today = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String basePackage = tableBean.getBasePackage();
        String className = tableBean.getClassName();
        StringBuilder sb = new StringBuilder();
        sb.append("package com.ics.cmsadmin.dao." + basePackage + ";\n" +
            "\n" +
            "import com.ics.cmsadmin.dao.base.BaseDataDao;\n" +
            "import com.ics.cmsadmin.model." + basePackage + "." + className + "Bean;\n" +
            "import org.apache.ibatis.annotations.Mapper;\n" +
            "import org.springframework.stereotype.Repository;\n" +
            "\n" +
            "/**\n" +
            " *\n" +
            " * Created by lvsw on " + today + ".\n" +
            " */\n" +
            "@Repository\n" +
            "@Mapper\n" +
            "public interface " + className + "Dao extends BaseDataDao<" + className + "Bean> {\n" +
            "}\n");
        saveFile("dao/" + tableBean.getClassName() + "Dao.java", sb.toString());
    }


}
