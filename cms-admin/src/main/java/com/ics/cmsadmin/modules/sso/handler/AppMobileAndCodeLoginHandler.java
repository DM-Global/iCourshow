package com.ics.cmsadmin.modules.sso.handler;

import com.ics.cmsadmin.frame.constant.Constants;
import com.ics.cmsadmin.frame.constant.RedisConstants;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.frame.core.exception.CmsException;
import com.ics.cmsadmin.frame.utils.KeyUtil;
import com.ics.cmsadmin.modules.alibaba.enums.SmsTypeEnum;
import com.ics.cmsadmin.modules.app.bean.AppInfoBean;
import com.ics.cmsadmin.modules.app.utils.AppContants;
import com.ics.cmsadmin.modules.app.utils.AppUtils;
import com.ics.cmsadmin.modules.student.bean.StudentBean;
import com.ics.cmsadmin.modules.student.dao.StudentDao;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.sso.LoginHandler;
import com.ics.cmsadmin.modules.sso.utils.LoginTypeEnum;
import com.ics.cmsadmin.modules.sso.utils.SsoContants;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 手机和短信验证码登陆
 * Created by 666666 on 2018/8/25.
 */
@Component
public class AppMobileAndCodeLoginHandler implements LoginHandler<StudentBean> {

    @Resource
    private StudentDao studentDao;
    @Resource(name = "newOrderService")
    private OrderService orderService;

    @Override
    public StudentBean dealWithLogin(HttpServletRequest request, HttpServletResponse response, LoginTypeEnum loginTypeEnum) {
        String code = request.getParameter(SsoContants.LOGIN_CODE);
        String phone = request.getParameter(SsoContants.LOGIN_MOBILE_PHONE);
        String phoneCode = request.getParameter(SsoContants.LOGIN_PHONE_CODE);
        // 判断手机号或者验证码是否为空
        if (StringUtils.isBlank(phone) || StringUtils.isBlank(code)) {
            throw new CmsException(ApiResultEnum.LOGIN_PHONE_OR_CODE_ERROR);
        }
        // 判断用户是否被锁定
        if (RedisConstants.isUserLoginLock(phone)) {
            throw new CmsException(ApiResultEnum.LOGIN_USER_IS_LOCK);
        }
        // 判断redis的短信验证码是否存在
        String redisCode = RedisConstants.getSmsCode(SmsTypeEnum.loginVerify, phone);
        if (StringUtils.isBlank(redisCode)) {
            throw new CmsException(ApiResultEnum.LOGIN_CODE_NON_EXISTENT);
        }
        // 判断验证码是否与redis的验证码一致, 不一致的话,判断登陆次数,如果超过指定次数,则锁定用户,不让用户登陆
        if (!StringUtils.equalsIgnoreCase(redisCode, code)) {
            RedisConstants.updateLoginErrorCount(phone);
            throw new CmsException(ApiResultEnum.LOGIN_CODE_ERROR);
        }
        RedisConstants.deleteSmsCode(SmsTypeEnum.loginVerify, phone);
        // 如果前面的校验全部通过,则通过手机号获取学生信息
        StudentBean studentBean = studentDao.findOne(StudentBean.builder().phone(phone).build());
        AppInfoBean appInfoBean = SsoUtils.getAppInfo(request);
        if (studentBean == null) {
            String userId = KeyUtil.genUniqueKey();
            studentBean = StudentBean.builder()
                .id(userId)
                .registerSource(Constants.REGISTER_SOURCE_APP_WECHAT)
                .username("ICS_" + phone)
                .nickname("ICS_" + phone)
                .phoneCode(phoneCode)
                .phone(phone)
                .build();
            if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_IOS)){
                studentBean.setIosDeviceToken(appInfoBean.uMengDeviceToken);
            }else if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_ANDROID)){
                studentBean.setAndroidDeviceToken(appInfoBean.uMengDeviceToken);
            }
            studentDao.insert(studentBean);
        }else {
            if (null != appInfoBean && null != appInfoBean.getUMengDeviceToken()){
                if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_IOS)){
                    studentBean.setIosDeviceToken(appInfoBean.uMengDeviceToken);
                }else if (StringUtils.equalsIgnoreCase(appInfoBean.getPlatform(), AppContants.PLATFORM_ANDROID)){
                    studentBean.setAndroidDeviceToken(appInfoBean.uMengDeviceToken);
                }
                studentDao.update(studentBean.getId(),studentBean);
            }
        }
        StudentBean temp = studentDao.queryById(studentBean.getId());
        studentBean.setHasPassword(StringUtils.isNotBlank(temp.getPassword()));
        studentBean.setLoginId(studentBean.getId());
        studentBean.setShareUrl(AppUtils.getStudentShareUrl(studentBean.getId()));
        SsoUtils.saveLoginInfo2Redis(loginTypeEnum, studentBean, 30 * 24 * 3600);
        return studentBean;
    }
}
