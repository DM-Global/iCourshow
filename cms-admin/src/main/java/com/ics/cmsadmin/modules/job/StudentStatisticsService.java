package com.ics.cmsadmin.modules.job;

import com.ics.cmsadmin.modules.student.service.StudentDayService;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Log4j2
@Component
public class StudentStatisticsService {

    @Resource
    private StudentDayService studentDayService;

    @Scheduled(cron = "0 5 0 * * *")
    public synchronized void runDaySummary() {
        long startTime = System.currentTimeMillis();
        log.info("=====>>>>>开始执行每天学生相关汇总任务");
        studentDayService.dayStatistics();
        log.info("=====>>>>>结束执行每天学生相关汇总任务,耗时: {}", System.currentTimeMillis() - startTime);
    }
}
