package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by Sandwich on 2019/3/29
 */
@Data
@Component
@ConfigurationProperties(prefix = "upush")
public class UmengPushConfig {

    private boolean devModel;
    private Map<String,String> androidMap;
    private Map<String,String> iosMap;

}
