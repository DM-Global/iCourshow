package com.ics.cmsadmin.modules.api.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.basic.bean.StudyHistory;
import com.ics.cmsadmin.modules.basic.service.StudyHistoryService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
@RequestMapping("/api/studyHistory")
public class ApiStudyHistoryController {

    @Autowired
    private StudyHistoryService studyHistoryService;


    @PostMapping("/list")
    public ApiResponse list(@RequestBody StudyHistory studyHistory, HttpServletRequest request) {

        String userId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isNotBlank(userId)) {
            studyHistory.setUserId(userId);
        }

        return ApiResponse.getDefaultResponse();
    }

    @PostMapping("/finish")
    public ApiResponse finish(@RequestBody StudyHistory studyHistory, HttpServletRequest request) {
        String userId = SsoUtils.getLoginUserId(request);
        studyHistory.setUserId(userId);
        studyHistory.setProgress(1);
        studyHistoryService.insert(studyHistory);
        return ApiResponse.getDefaultResponse();
    }
}
