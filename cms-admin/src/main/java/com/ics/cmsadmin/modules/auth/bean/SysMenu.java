package com.ics.cmsadmin.modules.auth.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * Created by lvsw on 2018/8/26.
 */
@Data
@Builder
public class SysMenu {
    // 菜单id
    @NotEmpty(message = "{Menu.menuId.NotEmpty}", groups = {InsertGroup.class})
    @Length(message = "{Menu.menuId.Length}", min = 2, max = 25, groups = {InsertGroup.class, UpdateGroup.class})
    @Pattern(message = "{Menu.menuId.Pattern}", regexp = "^[a-zA-Z][a-zA-Z0-9]*$", groups = {InsertGroup.class})
    private String menuId;
    // 父菜单id
    @Length(message = "{Menu.parentId.Length}", max = 25, groups = {InsertGroup.class, UpdateGroup.class})
    private String parentId;
    // 菜单名称
    @NotEmpty(message = "{Menu.label.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "{Menu.label.Length}", min = 2, max = 20, groups = {InsertGroup.class, UpdateGroup.class})
    private String label;
    // 关键词
    private String keyword;
    // 菜单跳转url
    @Length(message = "{Menu.url.Length}", min = 1, max = 300, groups = {InsertGroup.class, UpdateGroup.class})
    private String url;
    // 菜单icon
    @Length(message = "{Menu.icon.Length}", min = 2, max = 30, groups = {InsertGroup.class, UpdateGroup.class})
    private String icon;
    // 排序
    @Pattern(message = "{Menu.orderNo.Pattern}", regexp = "0|[1-9]\\d*", groups = {InsertGroup.class, UpdateGroup.class})
    private String orderNo;
    private List<SysMenu> children;
    private String active;

    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;

    @Tolerate
    public SysMenu() {
    }

}
