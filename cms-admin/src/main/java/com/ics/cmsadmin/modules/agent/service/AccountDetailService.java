package com.ics.cmsadmin.modules.agent.service;

import com.ics.cmsadmin.frame.core.service.LoginBaseDataService;
import com.ics.cmsadmin.modules.agent.bean.AccountDetailBean;


/**
 * a_account_detail 服务类
 * Created by lvsw on 2018-37-29 16:09:14.
 */
public interface AccountDetailService extends LoginBaseDataService<AccountDetailBean> {
}
