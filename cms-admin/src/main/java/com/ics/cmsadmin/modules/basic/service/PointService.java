package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.frame.core.service.BaseDataService;

import java.util.List;

/**
 * t_point 服务类
 * Created by lvsw on 2018-09-01.
 */
public interface PointService extends BaseDataService<PointBean>, BaseService<PointBean> {
    /**
     * 根据主题Id查询所有知识点信息
     *
     * @param topicId 主题Id
     */
    List<PointBean> listAllPointByTopicId(String topicId);

    List<PointBean> listAllPointDetailByTopicId(String userId, String topicId);

    PointBean pointDetail(String userId, String id);

    long count(PointBean bean);

    int finish(String userId, String id);

    void updateProblemNum(String pointId, int testNum);

    void updateVideoInfo(String pointId, String videoDuration, String coverSrc);
}
