package com.ics.cmsadmin.modules.app.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.annotation.UserOperLog;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.basic.bean.IosPayDto;
import com.ics.cmsadmin.modules.basic.service.PayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by Sandwich on 2019/1/23
 */
@Api(description = "app ios内购接口", tags = "内购校验")
@RestController
@RequestMapping("app/iap")
@Log4j2
public class AppIapController {

    @Resource
    private PayService payService;

    //购买凭证验证地址
    private static final String certificateUrl = "https://buy.itunes.apple.com/verifyReceipt";

    /**
     * 接收iOS端发过来的购买凭证
     *
     * @param iosPayDto
     */
    @UserOperLog
    @ApiOperation(value = "ios支付后处理", notes = "接收iOS端发过来的购买凭证")
    @PostMapping("/iapProcess")
    public ApiResponse iapProcess(@RequestBody IosPayDto iosPayDto, HttpServletRequest request) throws NoSuchAlgorithmException, KeyManagementException, IOException {
        if (null == iosPayDto || StringUtils.isBlank(iosPayDto.getOrderId()) || StringUtils.isBlank(iosPayDto.getReceipt())) {
            return new ApiResponse(ApiResultEnum.DATA_NOT_EXIST);
        }
        return ApiResponse.getDefaultResponse(sendHttpsCoon(iosPayDto));

    }
    //测试的购买凭证验证地址
    private static final String certificateUrlTest = "https://sandbox.itunes.apple.com/verifyReceipt";

    /**
     * 重写X509TrustManager
     */
    private static TrustManager myX509TrustManager = new X509TrustManager() {

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

        }
    };

    /**
     * 发送请求
     *
     * @param iosPayDto
     * @return
     */
    private Boolean sendHttpsCoon(IosPayDto iosPayDto) throws NoSuchAlgorithmException, KeyManagementException, IOException {
        Boolean isSandboxPay = false;
        Integer status = getIosPayStatus(iosPayDto, certificateUrl);
        log.info("Ios pay status is {}",status);
        if (status == 21007) {
            isSandboxPay = true;
            status = getIosPayStatus(iosPayDto, certificateUrlTest);
            log.info("Because ios pay status is 21007 with real pay certificate url， it should be sandbox pay, and sandbox paid status is {}",status);
        }
        if (status != 0) {
            return Boolean.FALSE;
        }
        Boolean handleOrderResult = payService.handleOrderAfterIosPaid(iosPayDto.getOrderId(),isSandboxPay);
        return handleOrderResult;

    }

    private static Integer getIosPayStatus(IosPayDto iosPayDto, String url) throws NoSuchAlgorithmException, KeyManagementException, IOException {
        //设置SSLContext
        SSLContext ssl = SSLContext.getInstance("SSL");
        ssl.init(null, new TrustManager[]{myX509TrustManager}, null);

        //打开连接
        HttpsURLConnection conn = (HttpsURLConnection) new URL(url).openConnection();
        //设置套接工厂
        conn.setSSLSocketFactory(ssl.getSocketFactory());
        //加入数据
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-type", "application/json");

        JSONObject obj = new JSONObject();
        obj.put("receipt-data", iosPayDto.getReceipt());
        obj.put("password", "18eaff48f1594e108a2c859d079e1c48");

        BufferedOutputStream buffOutStr = new BufferedOutputStream(conn.getOutputStream());
        buffOutStr.write(obj.toString().getBytes());
        buffOutStr.flush();
        buffOutStr.close();

        //获取输入流
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        String line = null;
        StringBuffer sb = new StringBuffer();
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        JSONObject jsonObject = JSON.parseObject(sb.toString());
        log.info("IOS paid result: {} for order:{}", jsonObject.toJSONString(),iosPayDto.getOrderId());
        return jsonObject.getInteger("status");
    }
}
