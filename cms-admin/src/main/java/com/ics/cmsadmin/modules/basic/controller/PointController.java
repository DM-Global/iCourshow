package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.PointBean;
import com.ics.cmsadmin.modules.basic.service.PointService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Point controller
 * Created by lvsw on 2018-09-01.
 */
@Api(description = "知识点管理接口")
@RestController
@RequestMapping("/point")
public class PointController {

    @Resource
    private PointService pointService;

    @Authorize(AuthorizeEnum.POINT_QUERY)
    @ApiOperation(value = "查询知识点信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(pointService.queryById(id));
    }

    @Authorize(AuthorizeEnum.POINT_INSERT)
    @ApiOperation(value = "新增知识点信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody PointBean pointBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(pointService.insert(pointBean));
    }

    @Authorize(AuthorizeEnum.POINT_DELETE)
    @ApiOperation(value = "删除知识点信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的知识点 id") @PathVariable String id) {
        return new ApiResponse(pointService.delete(id));
    }

    @Authorize(AuthorizeEnum.POINT_UPDATE)
    @ApiOperation(value = "更新知识点信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody PointBean pointBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的知识点Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(pointService.update(id, pointBean));
    }

    @Authorize(AuthorizeEnum.POINT_QUERY)
    @ApiOperation(value = "分页查询知识点信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody PointBean pointBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(pointService.list(pointBean, pageBean));
    }

    @Authorize(AuthorizeEnum.POINT_QUERY)
    @ApiOperation(value = "根据主题Id查询所有知识点信息")
    @GetMapping("/listAllPointByTopicId/{topicId}")
    public ApiResponse listAllPointByTopicId(@PathVariable String topicId) {
        return new ApiResponse(pointService.listAllPointByTopicId(topicId));
    }

}
