package com.ics.cmsadmin.modules.pub.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.modules.pub.bean.Subject;
import com.ics.cmsadmin.modules.pub.service.SubjectService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/subjects")
public class SubjectsController extends ResourceController<SubjectService, Subject, Integer> {
    @GetMapping
    public ApiResponse subjectsGet(Integer parentId, Boolean isActive,
                                   @RequestParam(value = "page", required = false) Integer page,
                                   @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        Map<String, Object> param = new HashMap<>();
        param.put("parentId", parentId);
        param.put("isActive", isActive);
        param.put("page", page);
        param.put("pageSize", pageSize);
        return super.get(param);
    }
}
