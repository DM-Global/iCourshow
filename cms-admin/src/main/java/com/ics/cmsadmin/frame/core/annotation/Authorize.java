package com.ics.cmsadmin.frame.core.annotation;

import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by 666666 on 2017/9/25.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Authorize {
    /**
     * 必须要全部包含的权限,才能执行
     *
     * @return
     */
    AuthorizeEnum[] value() default {};

    /**
     * 只需要包含其中一个权限码就能执行
     *
     * @return
     */
    AuthorizeEnum[] any() default {};
}
