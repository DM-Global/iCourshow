package com.ics.cmsadmin.modules.system.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.system.bean.RegisterBean;
import com.ics.cmsadmin.modules.system.service.RegisterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Register controller
 * Created by lvsw on 2018-04-03.
 */
@Api(description = "字典管理接口")
@RestController
@RequestMapping("/register")
public class RegisterController {

    @Resource
    private RegisterService registerService;

    @Authorize
    @ApiOperation(value = "查询字典信息")
    @GetMapping(value = "/query/{registerId}")
    public ApiResponse queryById(@ApiParam(value = "字典id") @PathVariable String registerId) {
        return new ApiResponse(registerService.queryById(registerId));
    }

    @Authorize(AuthorizeEnum.REGISTER_INSERT)
    @ApiOperation(value = "新增字典信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody RegisterBean registerBean, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(registerService.insert(registerBean));
    }

    @Authorize(AuthorizeEnum.REGISTER_DELETE)
    @ApiOperation(value = "删除字典信息")
    @PostMapping(value = "/delete/{registerId}")
    public ApiResponse delete(@ApiParam("需要删除的字典id") @PathVariable String registerId) {
        return new ApiResponse(registerService.delete(registerId));
    }

    @Authorize(AuthorizeEnum.REGISTER_UPDATE)
    @ApiOperation(value = "更新字典信息")
    @PostMapping(value = "/update/{registerId}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody RegisterBean registerBean,
                              @ApiParam("需要更新的字典id") @PathVariable String registerId, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(registerService.update(registerId, registerBean));
    }

    @Authorize(AuthorizeEnum.REGISTER_QUERY)
    @ApiOperation(value = "分页查询字典信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody RegisterBean registerBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(registerService.list(registerBean, pageBean));
    }

}
