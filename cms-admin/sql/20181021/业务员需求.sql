insert  into `sys_menu`(`menu_id`,`parent_id`,`label`,`keyword`,`url`,`icon`,`order_no`,`create_time`,`update_time`) values
('organManage','systemManage','组织管理','','/auth/organManage','ios-person',4,NULL,'2018-09-04 11:37:41');
INSERT INTO sys_organization (org_id, parent_id, parent_node, org_node, `name`, description, create_time)
VALUES('__ROOT__', '0','0', '__ROOT__', 'boss', '超管组织', NOW());
INSERT INTO sys_organization (org_id, parent_id, parent_node, org_node, `name`, description, create_time)
VALUES('OTHER', '__ROOT__','__ROOT__', '__ROOT__,OTHER', '其他', '未知部门', NOW());

insert  into `com_register`(`register_id`,`value`,`description`,`create_time`,`update_time`) values
('user_default_role_id','R10000001','新增用户时默认的用户角色','2018-10-11 08:44:27','2018-10-11 08:44:42');


ALTER TABLE a_agent_info DROP COLUMN salesman;
ALTER TABLE t_student DROP COLUMN salesman;
ALTER TABLE t_order DROP COLUMN salesman;
ALTER TABLE sys_user DROP COLUMN org_id;
alter table sys_user drop column post_type;
ALTER TABLE a_agent_info ADD COLUMN salesman VARCHAR(20) NULL COMMENT '所属业务员' AFTER user_id;
ALTER TABLE t_student ADD COLUMN salesman VARCHAR(20) NULL COMMENT '所属业务员' AFTER agent_no;
ALTER TABLE t_order ADD COLUMN salesman VARCHAR(20) NULL COMMENT '所属业务员' AFTER agent_no;
ALTER TABLE sys_user ADD COLUMN org_id VARCHAR(20) NULL COMMENT '所属组织' AFTER email;
ALTER TABLE sys_user ADD COLUMN post_type VARCHAR(20) NOT NULL default '1' COMMENT '职位: 1.普通员工;2. 助理;3.主管;4.boss' AFTER email;
ALTER TABLE a_share_detail ADD COLUMN source_agent_no VARCHAR(20) COMMENT '分润代理商来源' AFTER student_id;
update sys_user set org_id = '__ROOT__' where user_id = 'U10000000';


DROP TABLE IF EXISTS sys_organization;
CREATE TABLE IF NOT EXISTS sys_organization(
	org_id VARCHAR(20) NOT NULL COMMENT '组织id',
	parent_id VARCHAR(20) NOT NULL COMMENT '父级id',
	parent_node VARCHAR(250) NOT NULL COMMENT '父级节点',
	org_node VARCHAR(250) NOT NULL COMMENT '组织节点',
	`name` VARCHAR(50) NOT NULL COMMENT '组织名',
	description VARCHAR(200) NULL DEFAULT '' COMMENT '描述',
	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
	`update_time` TIMESTAMP NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后更新时间',
	PRIMARY KEY (`org_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='组织表';

-- DROP TABLE IF EXISTS sys_user_organization;
-- CREATE TABLE IF NOT EXISTS sys_user_organization(
-- 	user_id VARCHAR(20) NOT NULL COMMENT '用户id',
-- 	org_id VARCHAR(20) NOT NULL COMMENT '组织id',
-- 	`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
-- 	PRIMARY KEY (user_id, `org_id`)
-- ) ENGINE=INNODB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户组织表';
--
--
