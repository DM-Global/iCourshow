package com.ics.cmsadmin.modules.student.dao;

import com.ics.cmsadmin.modules.student.bean.StudentDaySummaryBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface StudentDaySummaryDao {

    /**
     * 插入学生基本信息
     */
    void insertBaseDaySummery(@Param("formatDate") String formatDate);

    /**
     * 统计下级学生累积数量
     * @param formatDate
     */
    void summaryChildrenCusm(@Param("formatDate")String formatDate);

    /**
     * 统计下级学生新增数量
     * @param formatDate
     */
    void summaryChildrenNewly(@Param("formatDate")String formatDate);

    /**
     * 统计下级累积交易金额
     * @param formatDate
     */
    void summaryChildOrderMoneyCusm(@Param("formatDate")String formatDate);

    /**
     * 统计下级新增交易金额
     * @param formatDate
     */
    void summaryChildOrderMoneyNewly(@Param("formatDate")String formatDate);

    /**
     * 统计累积分润收益金额
     * @param formatDate
     */
    void summaryShareMoneyCusm(@Param("formatDate")String formatDate);

    /**
     * 统计新增分润收益金额
     * @param formatDate
     */
    void summaryShareMoneyNewly(@Param("formatDate")String formatDate);

    /**
     * 更新学生分润比例
     * @param formatDate
     * @param parentStudentDefaultShareRate
     */
    void updateStudentShareRate(@Param("formatDate")String formatDate,
                                @Param("parentStudentDefaultShareRate")String parentStudentDefaultShareRate);

    /**
     * 更新统计1号到统计当天的下级的交易金额
     * @param formatDate
     * @param currentMonthFirstDay
     */
    void updateCurrentMonthChildOrderMoneyCusm(@Param("formatDate")String formatDate,
                                               @Param("currentMonthFirstDay")String currentMonthFirstDay);

    /**
     * 根据"1号到统计当天的下级的交易金额"统计出来的金额确定学生新的分润比例
     * @param formatDate
     * @param orderMoneySequence
     * @param shareRateSequence
     */
    void updateNewShareRate(@Param("formatDate")String formatDate,
                            @Param("orderMoneySequence")String orderMoneySequence,
                            @Param("shareRateSequence")String shareRateSequence);

    /**
     *
     * @param studentId
     * @param startCreateDate
     * @param endCreateDate
     * @return
     */
    List<StudentDaySummaryBean> list(@Param("studentId") String studentId,
                                     @Param("startCreateDate") String startCreateDate,
                                     @Param("endCreateDate") String endCreateDate);

    StudentDaySummaryBean query(@Param("summaryDay") String summaryDay,
                                @Param("studentId")String studentId);
}
