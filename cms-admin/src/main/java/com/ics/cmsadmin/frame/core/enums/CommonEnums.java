package com.ics.cmsadmin.frame.core.enums;

import lombok.Getter;

/**
 * Created by Sandwich on 2019/1/11
 */

public class CommonEnums {

    @Getter
    public enum SharePosterType {
        First_purchase(1, "第一次购买"),
        FULL_HOUR(2, "满一个小时分享"),
        COMPLETE_COURSE(3, "课程结业"),
        MY_SHARE(4, "我的分享");
        private final int code;
        private final String message;

        private SharePosterType(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum ShareRecordType {
        FULL_HOUR("2", "满一个小时分享"), COMPLETE_COURSE("3", "包年包月");
        private final String code;
        private final String message;

        private ShareRecordType(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }


    @Getter
    public enum OrderType {
        COURSE(1, "课程"), PACKAGE(2, "包年包月");
        private final String message;
        private final int code;

        private OrderType(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public static boolean contains(int code) {
            for (OrderType typeEnum : OrderType.values()) {
                if (typeEnum.code == code) {
                    return true;
                }
            }
            return false;
        }
    }

    @Getter
    public enum PaymentMethod {
        WX_PUBLIC(1, "微信公众号"),
        WX_APP(2, "微信app"),
        ALIPAY(3, "支付宝"),
        FREE_TRIAL(4, "免费试用"),
        APPLE_PAY(5, "苹果支付"),
        APPLE_SANDBOX_PAY(6, "苹果沙箱支付"),
        ACCOUNT_PAY(7, "学生账户余额")
        ;
        private final String message;
        private final int code;

        private PaymentMethod(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum PayStatus {
        UNPAID(1, "未支付"), PAID(2, "已支付"), CANCELED(3, "已取消");
        private final String message;
        private final int code;

        private PayStatus(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum StudyStatus {
        NOT_STARTED(1, "未入学"), STARTED(2, "已入学"), EXPIRED(3, "已到期");
        private final String message;
        private final int code;

        private StudyStatus(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum WithdrawType {
        APP_WX_TO_BROKEN_MONEY("1", "app微信提现到零钱"), WECHANT_TO_BROKEN_MONEY("2", "公众号微信提现到零钱"), APP_WX_TO_BANK("3", "app微信提现到银行卡");
        private final String message;
        private final String code;

        private WithdrawType(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    @Getter
    public enum WithdrawStatus {
        APPLY("0", "提交提现申请"),
        APPLY_SUCCESS("1", "接受提现申请成功"),
        APPLY_FAILED("2", "接受提现申请失败"),
        REJECT("3", "拒绝提现申请"),
        CANCEL("4", "主动取消提现申请");
        private final String message;
        private final String code;

        private WithdrawStatus(String code, String message) {
            this.code = code;
            this.message = message;
        }
    }
}
