package com.ics.cmsadmin.modules.app.controller;

import com.ics.cmsadmin.frame.core.enums.CommonEnums;
import com.ics.cmsadmin.frame.core.annotation.AppLoginValid;
import com.ics.cmsadmin.frame.core.annotation.UserOperLog;
import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.enums.ApiResultEnum;
import com.ics.cmsadmin.modules.basic.bean.OrderBean;
import com.ics.cmsadmin.modules.basic.bean.OrderDescription;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;
import com.ics.cmsadmin.modules.basic.service.OrderService;
import com.ics.cmsadmin.modules.basic.service.PackageService;
import com.ics.cmsadmin.modules.sso.utils.SsoUtils;
import com.ics.cmsadmin.modules.system.emums.RegisterKeyValueEnum;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import com.jpay.vo.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.Function;

import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.APP_CREATE_ORDER;
import static com.ics.cmsadmin.frame.constant.SwaggerNoteConstants.APP_LATEST_ORDER;

/**
 * 订单接口
 */
@Api(description = "app订单相关接口", tags = "订单相关接口")
@RestController
@RequestMapping("app/order")
@Log4j2
public class AppOrderController {
    @Autowired
    private OrderService orderService;
    @Resource
    private PackageService packageService;
    private AjaxResult ajax = new AjaxResult();

    @UserOperLog
    @ApiOperation(value = "给老用户创建试用订单", notes = "给老用户创建试用订单")
    @PostMapping("/createTrialOrder")
    public ApiResponse createTrialOrder( HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.LOGIN_PAST_DUE);
        }
        log.info("User:{} is going to create a trail order",loginUserId);
        OrderBean orderBean = orderService.createTrialOrderForOldUser(loginUserId);
        return new ApiResponse(orderBean);
    }

    @UserOperLog
    @ApiOperation(value = "使用账户余额购买订单")
    @PostMapping("/createOrderByAccount")
    public ApiResponse createOrderByAccount( @RequestBody OrderBean orderBean,
                                             HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.LOGIN_PAST_DUE);
        }
        if (null == orderBean) {
            return new ApiResponse(ApiResultEnum.PARAM_ERROR);
        }
        OrderBean order = orderService.createOrderByAccount(orderBean, loginUserId);
        return ApiResponse.getDefaultResponse(order);
    }

    @UserOperLog
    @ApiOperation(value = "创建订单", notes = APP_CREATE_ORDER)
    @PostMapping("/create")
    public ApiResponse create(@RequestBody OrderBean orderBean,
                              HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.LOGIN_PAST_DUE);
        }
        if (null == orderBean) {
            return new ApiResponse(ApiResultEnum.PARAM_ERROR);
        }
        OrderBean order = orderService.create(orderBean, loginUserId);
        Map<String, Object> orderMap = new HashMap<>();
        orderMap.put("order", order);
        OrderDescription orderDescription = OrderDescription.builder()
            .payPriceTitle("实付金额").payPriceTitle("入学金额").studyRightTitle("入学权益")
            .entranceAmount(order.getOrderAmount()).totalAmount(order.getOrderAmount())
            .studyRight1("学籍有效期至")
            .studyRight2("有效期内免费学习全部爱科塾课程")
            .studyRight3("课程安排解释归爱科塾所有")
            .purchaseInfoTitle("购买须知")
            .purchaseInfoContent("你所购买的产品为虚拟产品，已经购买不可退订，转让，退款，请斟酌确认").build();
        orderMap.put("descriptions", orderDescription);
        return ApiResponse.getDefaultResponse(orderMap);
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "包年包月订单列表，或者入学通知书列表", notes = "如果是游客，请在请求头添加游客用户id")
    @GetMapping("/list")
    public ApiResponse orderList(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        if (StringUtils.isBlank(loginUserId)) {
            return new ApiResponse(ApiResultEnum.LOGIN_PAST_DUE);
        }
        PageResult pageResult = orderService.list(OrderBean.builder()
            .userId(loginUserId).isDeleted(false)
            .orderType(CommonEnums.OrderType.PACKAGE.getCode())
            .status(CommonEnums.PayStatus.PAID.getCode()).build(),
            new PageBean(1, Integer.MAX_VALUE));
        List<OrderBean> list = pageResult.getDataList();
        if (null != list && list.size() > 0) {
            Collections.sort(list, Comparator.comparing(OrderBean::getEndDate).reversed());
        }
        return ApiResponse.getDefaultResponse(list);
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "套餐列表", notes = "套餐接口不需要登录，也不需要传参")
    @GetMapping("/packages")
    public ApiResponse packageList() {
        PageResult pageResult = packageService.list(PackageBean.builder().isActive(true).build(), new PageBean(1, Integer.MAX_VALUE));
        return ApiResponse.getDefaultResponse(pageResult.getDataList());
    }

    @ApiOperation(value = "最新订单接口", notes = APP_LATEST_ORDER)
    @GetMapping("/latestOrder")
    public ApiResponse latestOrder(HttpServletRequest request) {
        String loginUserId = SsoUtils.getLoginUserId(request);
        Map<String, Object> result = new HashMap<>();
        // 未登录情况
        if (StringUtils.isBlank(loginUserId)) {
//            result.put("hasTrialOrder", false);
//            result.put("posterImage", SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.HAVE_NOT_TRIAL_ORDER_POSTER_IMAGE, Function.identity()));
            result.put("hasTrialOrder", true);
            result.put("posterImage", SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.HAVE_TRIAL_ORDER_POSTER_IMAGE, Function.identity()));
            return ApiResponse.getDefaultResponse(result);
        }
        // 登陆之后的情况
        PageResult pageResult = orderService.list(OrderBean.builder().userId(loginUserId).isDeleted(false)
            .orderType(CommonEnums.OrderType.PACKAGE.getCode())
            .status(CommonEnums.PayStatus.PAID.getCode())
            .build(), new PageBean(1, Integer.MAX_VALUE));
        List<OrderBean> list = pageResult.getDataList();
        if (CollectionUtils.isNotEmpty(list)) {
            Collections.sort(list, Comparator.comparing(OrderBean::getEndDate).reversed());
            result.put("studyStatus", list.get(0).getStudyStatus() + "");
            result.put("endDate", DateFormatUtils.format(list.get(0).getEndDate(), "yyyy-MM-dd"));
        }
//        Boolean hasTrialOrder = orderService.hasTrialOrder(loginUserId);
//        RegisterKeyValueEnum keyValueEnum = hasTrialOrder ? RegisterKeyValueEnum.HAVE_TRIAL_ORDER_POSTER_IMAGE :
//            RegisterKeyValueEnum.HAVE_NOT_TRIAL_ORDER_POSTER_IMAGE;
//        result.put("hasTrialOrder", hasTrialOrder);
//        result.put("posterImage", SystemServices.registerService.queryRegisterValue(keyValueEnum, Function.identity()));
        result.put("hasTrialOrder", true);
        result.put("posterImage", SystemServices.registerService.queryRegisterValue(RegisterKeyValueEnum.HAVE_TRIAL_ORDER_POSTER_IMAGE, Function.identity()));
        return ApiResponse.getDefaultResponse(result);
    }

    @AppLoginValid(needLogin = false)
    @ApiOperation(value = "游客学籍免费试用一个月海报")
    @GetMapping("/unLogonTrialPoster")
    public ApiResponse unLogonTrialPoster() {
//        RegisterKeyValueEnum keyValueEnum = RegisterKeyValueEnum.HAVE_NOT_TRIAL_ORDER_POSTER_IMAGE;
        RegisterKeyValueEnum keyValueEnum = RegisterKeyValueEnum.HAVE_TRIAL_ORDER_POSTER_IMAGE;
        Map map = new HashMap();
        map.put("posterImage",SystemServices.registerService.queryRegisterValue(keyValueEnum, Function.identity()));
        map.put("hasTrialOrder", true);
        return ApiResponse.getDefaultResponse(map);
    }

}
