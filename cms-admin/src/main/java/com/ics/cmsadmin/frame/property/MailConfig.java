package com.ics.cmsadmin.frame.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "email")
public class MailConfig {
    private String serverHost;
    private String senderAddress;
    private String senderNick;
    private String senderUsername;
    private String senderPassword;
}
