package com.ics.cmsadmin.modules.basic.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

/**
 * t_point
 * 知识点
 * Created by lvsw on 2018-09-01.
 */
@Data
@Builder
public class PointBean {

    // 知识点id
    private String id;
    // 名字
    @NotEmpty(message = "知识点名称不能为空", groups = {InsertGroup.class, UpdateGroup.class})
    @Length(message = "知识点名称长度必须在{min}~{max}之间", min = 2, max = 100, groups = {InsertGroup.class, UpdateGroup.class})
    private String name;
    // 主题id
    @NotEmpty(message = "请选择主题", groups = {InsertGroup.class, UpdateGroup.class})
    private String topicId;
    // 主题名
    private String topicName;
    //单元名
    private String unitName;
    //单元id
    private String unitId;
    //课程名
    private String courseName;
    //科目名
    private String subjectName;
    //科目id
    private String subjectId;
    // 描述
//	@NotEmpty(message = "{Point.description.NotEmpty}", groups = {InsertGroup.class, UpdateGroup.class})
//	@Length(message = "{Point.description.Length}", min = 2, max = 300, groups = {InsertGroup.class, UpdateGroup.class})
    private String description;
    // 类型
    @NotEmpty(message = "请选择知识点类型", groups = {InsertGroup.class, UpdateGroup.class})
    private String pointType;
    // 封面id
    private String coverId;
    // 封面url
    private String coverSrc;
    // 内容
    @Length(message = "请填写知识点内容长度必须在{min}~{max}之间", min = 2, max = 65535, groups = {InsertGroup.class, UpdateGroup.class})
    private String pointSource;
    // 状态
    private Boolean isActive;
    // 公开
    private Boolean isPublic;
    // 排序
    private String rank;
    // 创建时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date createTime;
    // 更新时间
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date updateTime;
    private Date startTime;
    private Date endTime;

    @Tolerate
    public PointBean() {
    }

    // 点赞数
    private Integer thumbUpCount;
    // 收藏数
    private Integer collectCount;

    // 主题是否已收藏
    private Boolean collected;
    // 是否已经购买
    private Boolean hasBought;
    // 是否点赞了
    private Boolean liked;
    // 是否已完成
    private Boolean finished;

    // 课程id
    private String courseId;
    private int testNum;
    private String videoDuration;
    private Integer videoDurationSecond;
}
