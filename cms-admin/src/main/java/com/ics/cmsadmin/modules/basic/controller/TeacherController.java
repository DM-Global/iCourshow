package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.modules.basic.bean.TeacherBean;
import com.ics.cmsadmin.modules.basic.service.TeacherService;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * t_teacher controller
 * Created by lvsw on 2018-14-25 14:09:44.
 */
@Api(description = "教师管理")
@RestController("newTeacherController")
@RequestMapping("/teacher")
public class TeacherController {

    @Resource(name = "newTeacherService")
    private TeacherService teacherService;

    @Authorize(AuthorizeEnum.TEACHER_QUERY)
    @ApiOperation(value = "查询教师信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(teacherService.queryById(id));
    }

    @Authorize(AuthorizeEnum.TEACHER_INSERT)
    @ApiOperation(value = "新增教师信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody TeacherBean teacherBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(teacherService.insert(teacherBean));
    }

    @Authorize(AuthorizeEnum.TEACHER_DELETE)
    @ApiOperation(value = "禁用教师信息")
    @PostMapping(value = "/disable/{id}")
    public ApiResponse disable(@ApiParam("需要禁用的教师id") @PathVariable String id) {
        return new ApiResponse(teacherService.update(id, TeacherBean.builder().isActive(false).build()));
    }

    @Authorize(AuthorizeEnum.TEACHER_DELETE)
    @ApiOperation(value = "启用教师信息")
    @PostMapping(value = "/enable/{id}")
    public ApiResponse enable(@ApiParam("需要启用的教师id") @PathVariable String id) {
        return new ApiResponse(teacherService.update(id, TeacherBean.builder().isActive(true).build()));
    }

    @Authorize(AuthorizeEnum.TEACHER_UPDATE)
    @ApiOperation(value = "更新教师信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody TeacherBean teacherBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(teacherService.update(id, teacherBean));
    }

    @Authorize(AuthorizeEnum.TEACHER_QUERY)
    @ApiOperation(value = "分页查询教师信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody TeacherBean teacherBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        return new ApiResponse(teacherService.list(teacherBean, pageBean));
    }

}
