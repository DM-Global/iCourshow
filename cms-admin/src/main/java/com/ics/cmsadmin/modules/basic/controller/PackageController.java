package com.ics.cmsadmin.modules.basic.controller;

import com.ics.cmsadmin.frame.core.bean.ApiResponse;
import com.ics.cmsadmin.frame.core.annotation.Authorize;
import com.ics.cmsadmin.frame.core.enums.AuthorizeEnum;
import com.ics.cmsadmin.frame.core.annotation.InsertGroup;
import com.ics.cmsadmin.frame.core.annotation.UpdateGroup;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.utils.HttpUtils;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;
import com.ics.cmsadmin.modules.basic.service.PackageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * t_package controller
 * Created by sandwich on 2018-18-14 21:12:28.
 */
@Api(description = "订单套餐")
@RestController
@RequestMapping("/package")
public class PackageController {

    @Resource
    private PackageService packageService;

    @Authorize(AuthorizeEnum.PACKAGE_QUERY)
    @ApiOperation(value = "查询订单套餐信息")
    @GetMapping(value = "/query/{id}")
    public ApiResponse queryById(@ApiParam(value = "id") @PathVariable String id) {
        return new ApiResponse(packageService.queryById(id));
    }

    @Authorize(AuthorizeEnum.PACKAGE_INSERT)
    @ApiOperation(value = "新增订单套餐信息")
    @PostMapping(value = "/insert")
    public ApiResponse insert(@Validated(InsertGroup.class) @RequestBody PackageBean packageBean,
                              BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(packageService.insert(packageBean));
    }

    @Authorize(AuthorizeEnum.PACKAGE_DELETE)
    @ApiOperation(value = "删除订单套餐信息")
    @PostMapping(value = "/delete/{id}")
    public ApiResponse delete(@ApiParam("需要删除的 id") @PathVariable String id) {
        return new ApiResponse(packageService.delete(id));
    }

    @Authorize(AuthorizeEnum.PACKAGE_UPDATE)
    @ApiOperation(value = "更新订单套餐信息")
    @PostMapping(value = "/update/{id}")
    public ApiResponse update(@Validated(UpdateGroup.class) @RequestBody PackageBean packageBean,
                              BindingResult bindingResult,
                              @ApiParam("需要更新的Id") @PathVariable String id) {
        if (bindingResult.hasErrors()) {
            return HttpUtils.validateError(bindingResult);
        }
        return new ApiResponse(packageService.update(id, packageBean));
    }

    @Authorize(AuthorizeEnum.PACKAGE_QUERY)
    @ApiOperation(value = "分页查询订单套餐信息")
    @PostMapping("/list/{pageNo}/{pageSize}")
    public ApiResponse list(@RequestBody PackageBean packageBean,
                            @ApiParam("页码") @PathVariable int pageNo,
                            @ApiParam("每页条数") @PathVariable int pageSize) {
        PageBean pageBean = new PageBean(pageNo, pageSize);
        packageBean.setIsDeleted(false);
        return new ApiResponse(packageService.list(packageBean, pageBean));
    }

}
