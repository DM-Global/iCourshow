package com.ics.cmsadmin.modules.agent.bean;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ics.cmsadmin.frame.converter.serializer.Date2LongStringSerializer;
import com.ics.cmsadmin.frame.converter.serializer.LongString2DataDeserializer;
import lombok.Data;

import java.util.Date;

@Data
public class StudentOrderBean {

    private String userId;
    private String courseName;
    private String orderMoney;
    @JsonSerialize(using = Date2LongStringSerializer.class)
    @JsonDeserialize(using = LongString2DataDeserializer.class)
    private Date buyTime;


    private String startCreateTime;
    private String endCreateTime;
}
