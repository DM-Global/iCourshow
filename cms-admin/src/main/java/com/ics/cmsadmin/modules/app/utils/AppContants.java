package com.ics.cmsadmin.modules.app.utils;

/**
 * app相关常量类
 */
public final class AppContants {

    public static final String PLATFORM_IOS = "ios";
    public static final String PLATFORM_ANDROID = "android";
    public static final String USER_PRIVACY_PROTOCOL = "user_privacy_protocol";
    public static final String HAS_NOT_USER_PRIVACY_PROTOCOL_TIP = "暂无用户隐私协议";
    public static final String USER_USAGE_PROTOCOL = "user_usage_protocol";
    public static final String HAS_NOT_USER_USAGE_PROTOCOL_TIP = "暂无用户使用协议";

}
