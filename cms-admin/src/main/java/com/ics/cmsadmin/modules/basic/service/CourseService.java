package com.ics.cmsadmin.modules.basic.service;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.service.BaseDataService;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.CourseBean;
import com.ics.cmsadmin.modules.basic.bean.CourseSection;

import java.util.List;

/**
 * t_course 服务类
 * Created by lvsw on 2018-09-01.
 */
public interface CourseService extends BaseDataService<CourseBean>, BaseService<CourseBean> {
    /**
     * 根据科目Id查询所有课程信息
     *
     * @param subjectId 科目Id
     */
    List<CourseBean> listAllCourseBySubjectId(String subjectId);

    /**
     * 查询课程列表，带上用户相关的一些属性
     *
     * @param userId
     * @param t
     * @param page
     * @return
     */
    PageResult list(String userId, CourseBean t, PageBean page);

    PageResult purchased(String userId, PageBean page);

    CourseBean courseDetail(String courseId);

    CourseBean courseAllDetail(String courseId);

    void increaseViewNum(String id);

    boolean bindCourseType(String courseId, String courseTypeId);

    boolean unbindCourseType(String courseId, String courseTypeId);

    boolean bulkUnbindCourseType(List<String> ids, String courseTypeId);

    /**
     * 组合查询课程属性下的课程
     *
     * @return
     */
    List<CourseSection> listCourseSections();

    CourseBean courseDetailAdditional(String userId, CourseBean courseBean);

    CourseBean courseAllDetailAdditional(String userId, CourseBean courseBean);
}
