package com.ics.cmsadmin.frame.core.enums;

import lombok.Getter;

@Getter
public enum OrderStatusEnum {

    WAIT(1, "等待支付"),
    SUCCESS(2, "支付成功"),
    CANCEL(3, "取消订单");

    private Integer code;
    private String message;

    OrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
