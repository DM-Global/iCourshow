package com.ics.cmsadmin.modules.basic.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.PackageBean;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * t_package
 * Created by sandwich on 2018-18-14 21:12:28.
 */
@Repository
@Mapper
public interface PackageDao extends BaseDao<PackageBean>, BaseDataDao<PackageBean> {
}
