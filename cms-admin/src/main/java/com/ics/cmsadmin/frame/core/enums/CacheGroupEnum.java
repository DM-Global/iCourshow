package com.ics.cmsadmin.frame.core.enums;


public enum CacheGroupEnum {
    CACHE_DEFAULT,
    CACHE_AUTH_GROUP,
    CACHE_PROBLEM_GROUP,
    CACHE_PROMOTION,
    CACHE_COURSE,
    CACHE_CITY,
    CACHE_AGENT_DAY_SUMMARY;
}
