package com.ics.cmsadmin.modules.basic.service.impl;

import com.ics.cmsadmin.frame.core.enums.LevelEnum;
import com.ics.cmsadmin.frame.core.service.BaseService;
import com.ics.cmsadmin.modules.basic.bean.*;
import com.ics.cmsadmin.modules.basic.dao.CollectionDao;
import com.ics.cmsadmin.modules.basic.service.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CollectionServiceImpl implements CollectionService {

    @Autowired
    private CollectionDao collectionDao;

    @Autowired
    private CourseService courseService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private TopicService topicService;

    @Autowired
    private PointService pointService;

    private Map<String, BaseService> baseServiceMap = new HashMap<>();

    @PostConstruct
    public void postConstruct() {
        baseServiceMap.put(LevelEnum.course.name(), this.courseService);
        baseServiceMap.put(LevelEnum.unit.name(), this.unitService);
        baseServiceMap.put(LevelEnum.topic.name(), this.topicService);
        baseServiceMap.put(LevelEnum.point.name(), this.pointService);
    }

    @Override
    public List listCollectionByType(String levelName, String userId) {
        BaseService baseService = baseServiceMap.get(levelName);
        if (baseService == null) {
            return null;
        }
        List<Collect> collects = collectionDao.findByLevelNameAndUserId(levelName, userId);
        if (CollectionUtils.isEmpty(collects)) {
            return null;
        }
        return collects.stream()
            .map(item -> baseService.queryById(item.getLevelId()))
            .collect(Collectors.toList());
    }

    @Override
    public Map<LevelEnum, List> getCollectionList(String userId) {
        Map<LevelEnum, List> ret = new HashMap<>();
        for (LevelEnum i = LevelEnum.course; i != LevelEnum.subject; i = i.next()) {
            ret.put(i, new LinkedList());
        }
        List<Collect> collects = collectionDao.findByUserId(userId);
        if (CollectionUtils.isEmpty(collects)) {
            return ret;
        }
        Map<LevelEnum, List<Collect>> collectMap = collects.stream()
            .filter(item -> baseServiceMap.get(item.getLevelName().name()) != null)
            .collect(Collectors.groupingBy(Collect::getLevelName));
        for (Map.Entry<LevelEnum, List<Collect>> entry : collectMap.entrySet()) {
            List<Object> collect = entry.getValue().stream()
                .map(item -> baseServiceMap.get(entry.getKey().name()).queryById(item.getLevelId()))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
            ret.get(entry.getKey()).addAll(collect);
        }
        return ret;
    }

    @Override
    public Integer queryTotal(LevelEnum levelName, String levelId) {
        return collectionDao.queryTotal(levelName, levelId);
    }

    @Override
    public int delete(Collect collect) {
        return collectionDao.delete(collect);
    }

    @Override
    public int insert(Collect collect) {
        return collectionDao.insert(collect);
    }

    @Override
    public Boolean select(Collect collect) {
        return collectionDao.select(collect);
    }

    @Override
    public boolean toggleCollect(Collect collect) {
        if (collect == null ||
            collect.getLevelName() == null ||
            StringUtils.isBlank(collect.getUserId()) ||
            StringUtils.isBlank(collect.getLevelId())
        ) {
            return false;
        }
        Boolean collected = collectionDao.select(collect);
        if (collected) {
            return collectionDao.delete(collect) > 0;
        } else {
            return collectionDao.insert(collect) > 0;
        }
    }

    @Override
    public List<String> list(String userId, LevelEnum levelEnum, List<String> ids) {
        if (StringUtils.isBlank(userId) || levelEnum == null || CollectionUtils.isEmpty(ids)) {
            return null;
        }
        return collectionDao.list(userId, levelEnum, ids);
    }

}
