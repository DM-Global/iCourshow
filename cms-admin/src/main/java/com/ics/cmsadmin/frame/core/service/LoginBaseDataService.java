package com.ics.cmsadmin.frame.core.service;

import com.ics.cmsadmin.frame.core.bean.LoginSearchBean;
import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.auth.steward.AuthServices;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface LoginBaseDataService<T extends LoginSearchBean> {
    /**
     * 根据登录用户查询条件查询列表并汇总
     *
     * @param t           查询条件
     * @param loginUserId 登陆用户id
     * @return 查询结果
     */
    PageResult<T> listByLoginUserId(T t, String loginUserId, PageBean page);

    /**
     * 根据登录用户查询条件查询列表并汇总
     */
    default PageResult<T> listByLoginUserId(T t, String loginUserId, PageBean page, BaseDataDao<T> baseDataDao) {
        return LoginBaseDataService.listByLogin(t, loginUserId, page, baseDataDao::count, baseDataDao::list);
    }


    /**
     * 根据登录用户查询条件查询列表并汇总
     */
    static <T extends LoginSearchBean, U> PageResult<U>
    listByLogin(T t, String loginUserId, PageBean page,
                Function<T, Long> countFunction, BiFunction<T, PageBean, List<U>> listBiFunction) {
        if (StringUtils.isBlank(loginUserId)) {
            return new PageResult<>();
        }
        LoginSearchBean loginSearchBean = AuthServices.accessService.queryLoginInfoByLoginUserId(loginUserId);
        if (loginSearchBean == null) {
            return new PageResult<>();
        }
        t.setLoginUserId(loginUserId);
        t.setLoginAgentNo(loginSearchBean.getLoginAgentNo());
        t.setLoginOrgId(loginSearchBean.getLoginOrgId());
        t.setLoginPostType(loginSearchBean.getLoginPostType());
        long count = countFunction.apply(t);
        if (count == 0) {
            return new PageResult<>();
        }
        return new PageResult<>(listBiFunction.apply(t, page), count);
    }
}
