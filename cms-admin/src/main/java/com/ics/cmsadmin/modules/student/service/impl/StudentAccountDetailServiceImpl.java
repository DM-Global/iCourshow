package com.ics.cmsadmin.modules.student.service.impl;

import com.ics.cmsadmin.frame.core.bean.PageBean;
import com.ics.cmsadmin.frame.core.bean.PageResult;
import com.ics.cmsadmin.modules.student.bean.StudentAccountDetailBean;
import com.ics.cmsadmin.modules.student.enums.StudentAccountChangesEnum;
import com.ics.cmsadmin.modules.student.service.StudentAccountDetailService;
import com.ics.cmsadmin.modules.system.steward.SystemServices;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentAccountDetailDao;
import static com.ics.cmsadmin.modules.student.steward.StudentRepositories.studentScoreDao;

@Service
public class StudentAccountDetailServiceImpl implements StudentAccountDetailService {

    @Override
    public PageResult<StudentAccountDetailBean> list(StudentAccountDetailBean studentAccountDetailBean, PageBean page) {
        long count = studentAccountDetailDao.count(studentAccountDetailBean);
        if (count == 0) {
            return new PageResult<>();
        }
        List<StudentAccountDetailBean> list = studentAccountDetailDao.list(studentAccountDetailBean, page);
//        Map<String, String> student_account = SystemServices.metaService.listDirectChildren2Map("studentAccountDetail-type");
        list.stream().forEach(item -> {
            StudentAccountChangesEnum anEnum = EnumUtils.getEnum(StudentAccountChangesEnum.class, item.getType());
            if (anEnum != null) {
                item.setTypeName(anEnum.getName());
                BigDecimal money = Optional.ofNullable(item.getMoney()).orElse(BigDecimal.ZERO);
                item.setMoney(anEnum.isIncome() ? money : money.negate());
            }
        });
        return new PageResult<>(list, count);
    }

    public static void main(String[] args) {
        StudentAccountChangesEnum anEnum = EnumUtils.getEnum(StudentAccountChangesEnum.class, "ss");
        System.out.println(anEnum);
    }
}
