package com.kco.generate.java;

import com.kco.generate.config.ColumnsBean;
import com.kco.generate.config.TableBean;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kco.generate.config.Utils.*;

/**
 * 创建bean java文件
 * Created by 666666 on 2017/9/26.
 */
public final class CreateBeanJava {

    public static void buildFile(TableBean tableBean) {
        String today = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        StringBuilder sb = new StringBuilder();

        sb.append("package com.ics.cmsadmin.model." + tableBean.getBasePackage() + ";\n")
            .append("import com.fasterxml.jackson.databind.annotation.JsonSerialize;\n")
            .append("import com.fasterxml.jackson.databind.annotation.JsonDeserialize;\n")
            .append("import com.ics.cmsadmin.utils.serializer.Date2LongStringSerializer;\n")
            .append("import com.ics.cmsadmin.utils.serializer.LoginString2DataDeserializer;\n")
            .append("import com.ics.cmsadmin.frame.core.enums.InsertGroup;\n")
            .append("import com.ics.cmsadmin.frame.core.enums.UpdateGroup;\n")
            .append("import lombok.Builder;\n")
            .append("import lombok.Data;\n")
            .append("import javax.validation.constraints.NotEmpty;\n")
            .append("import org.hibernate.validator.constraints.Length;\n")
            .append("import lombok.experimental.Tolerate;\n\n")
            .append("import java.util.Date;\n\n");

        sb.append("/**\n")
            .append(" * ").append(tableBean.getTableName()).append("\n")
            .append(" * " + tableBean.getTableComment() + "\n")
            .append(" * Created by lvsw on " + today + ".\n")
            .append(" */\n")
            .append("@Data\n" + "@Builder\n")
            .append("public class ").append(tableBean.getClassName()).append("Bean {\n\n");
        // 生成getter and setter
        for (ColumnsBean bean : tableBean.getColumnsBeans()) {
            sb.append("\t// ").append(bean.getColumnComment()).append("\n");
            if (bean.getJavaType().equals("Date")) {
                sb.append("    @JsonSerialize(using = Date2LongStringSerializer.class)\n" +
                    "    @JsonDeserialize(using = LoginString2DataDeserializer.class)\n");
            }
            sb.append(addValid(tableBean.getClassName(), bean));
            sb.append("\tprivate ").append(bean.getJavaType()).append(" ").append(bean.getPropertyName()).append(";\n");
        }
        sb.append("    @Tolerate\n" + "    public " + tableBean.getClassName() + "Bean () {}\n");
        sb.append("}");
        saveFile("bean/" + tableBean.getClassName() + "Bean.java", sb.toString());
    }

    private static String addValid(String className, ColumnsBean bean) {
        StringBuilder messageSb = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        if (StringUtils.equalsIgnoreCase(bean.getJavaType(), "date")) {
            return sb.toString();
        }
        if (!bean.isNullAble() && bean.getDefaultValue() == null) {
            String key = className + "." + bean.getPropertyName() + ".NotEmpty";
            sb.append("\t@NotEmpty(message = \"{" + key + "}\", groups = {InsertGroup.class, UpdateGroup.class})\n");
            messageSb.append(key + "=" + bean.getPropertyName() + "不能为空\n");
        }
        if (bean.getMaxSize() >= 2) {
            String key = className + "." + bean.getPropertyName() + ".Length";
            sb.append("\t@Length(message = \"{" + key + "}\", min = 2, max = " + bean.getMaxSize() + ", groups = {InsertGroup.class, UpdateGroup.class})\n");
            messageSb.append(key + "=" + bean.getPropertyName() + "长度必须在{min}~{max}之间\n");
        }
        try {
            FileUtils.write(new File("gem/message.properties"), messageSb.toString(), Charset.defaultCharset(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
