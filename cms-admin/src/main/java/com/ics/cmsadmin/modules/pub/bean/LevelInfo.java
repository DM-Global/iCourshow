package com.ics.cmsadmin.modules.pub.bean;

import lombok.Data;

@Data
public class LevelInfo {

    private Integer courseId;
    private String courseName;

    private Integer subjectId;
    private String subjectName;

    private Integer topicId;
    private String topicName;

    private Integer unitId;
    private String unitName;

    private Integer pointId;
    private String pointName;
}
