package com.ics.cmsadmin.modules.pub.dao;

import com.ics.cmsadmin.frame.core.dao.BaseDao;
import com.ics.cmsadmin.frame.core.dao.BaseDataDao;
import com.ics.cmsadmin.modules.basic.bean.StudyHistory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudyHistoryMapper extends BaseDao<StudyHistory>, BaseDataDao<StudyHistory> {
}
