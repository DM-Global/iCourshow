package com.ics.cmsadmin.frame.core.dao;

import org.apache.ibatis.annotations.Param;

/**
 * Created by 666666 on 2017/9/25.
 */
public interface BaseDao<T> {
    /**
     * 根据主键id查询bean
     *
     * @param id 主键id
     * @return config
     */
    T queryById(@Param("id") String id);

    /**
     * 插入数据
     *
     * @param t 数据
     * @return 影响条数
     */
    int insert(@Param("bean") T t);

    /**
     * 根据id更新数据
     *
     * @param t 更新数据
     * @return 影响条数
     */
    int update(@Param("id") String id, @Param("bean") T t);

    /**
     * 根据id删除数据
     *
     * @param id 主键id
     * @return 影响条数
     */
    int delete(@Param("id") String id);
}
